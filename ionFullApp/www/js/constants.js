/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |V4-HU020                     |1.0.1    |John Velasquez   |12-07-2019 | cliente no domiciliado |
 * |--------------------------------------------------------------------------------------------------------|
 */
angular.module('your_app_name.constants', [])
.constant('TIPO_BIEN', {
        VEHICULO: 2,
        INMUEBLE: 1
})
// INICIO VTADIG2-185- José Martinez 12/02/2018
.constant('TIPO_MONEDA', {
        SOLES: 1,
        DOLARES: 2
})
// FIN VTADIG2-185- José Martinez 12/02/2018
.constant('TIPO_ASOCIADO',{TITULAR: "T",RESPALDO: "R"})
.constant('TIPO_DOCUMENTO',{RUC: '8',DNI: '4',PAS: '3',CE: '2'})
.constant('DATOS_RUC',{
        ESTADO:{
                ACTIVO: "00",
                BAJA_DE_OFICIO: "11",
                BAJA_DEFINITIVA: "10"
        },
        FLAGG_22:{
                HABIDO: "00"
        }
})
.constant('OBLIGACION_PAGO',{
        ESTADO:{
                REGISTRADO: 0,
                PAGADO: 1,
                ELIMINADO: 2,
                /**
                * @feature/Mastercard
                */
                POR_CONFIRMAR: 3
        }
})
.constant('ESTADO_CIVIL', {
        CASADO: "C",
        DIVORCIADO: "D",
        CONVIVIENTE : "N",
        SOLTERO: "S",
        VIUDO: "V"
})
.constant('TIPO_RELACION_ID', {
        CONYUGUE: 1,
        CONVIVIENTE: 16,
        REPRESENTANTE_LEGAL: '17'
})
.constant('PANDERO_DATA', {
        RUC: '20100115663'
})
.constant('PERSONA_TIPO',{
        NATURAL: "N",
        JURIDICA: "J"
})
//INICIO VTADIG2-18 John Velasquez 03-10-2017
.constant('LST_PERSONA_TIPO',[
        {codigo: "N", label: "Persona Natural"},
        {codigo: "J", label: "Persona Jurídica"}
])
.constant('LST_TIPO_DOCUMENTO',[
        {codigo: "8", label: "RUC"},
        {codigo: "4", label: "DNI"},
        {codigo: "3", label: "PS"},
        {codigo: "2", label: "CE"}
])
.constant('LST_TIPO_ASOCIADO',[
        {codigo: "T", label: "TITULAR"},
        {codigo: "R", label: "RESPALDO"}
])
.constant('DOCUMENTOS_CONTRATO',{
        LST_ESTADOS: [
                {codigo: 1, label: "Pendiente"},
                {codigo: 2, label: "Pendiente Revision"},
                {codigo: 3, label: "Completado"}
        ],
        ESTADOS: {
                DEPENDIENTE: 1,
                DEPENDIENTE_REVISION: 2,
                COMPLETADO: 3
        }
})
.constant('DOCUMENTO_CONTRATO',{
        ESTADO: {
                GENERADO: 1,
                NO_ENVIADO: 2,
                ENVIADO: 3,
                APROBADO: 4,
                RECHAZADO: 5                
        },
        LST_ESTADOS: [
                {codigo: 1, label: "Generado"},
                {codigo: 2, label: "No Enviado"},
                {codigo: 3, label: "Enviado"},
                {codigo: 4, label: "Aprobado"},
                {codigo: 5, label: "Rechazado"}                
        ],
        TIPO: {
                NO_ADICIONAL: "N",
                ADICIONAL: "A"
        }
})
//FIN VTADIG2-18 John Velasquez 03-10-2017
.constant('TABAJADOR',{
        TIPO: {
                DEPENDIENTE: 'd',
                INDENEPENDIENTE: 'i',
                NO_REMUNERADO: 'n'
        }
})
.constant('USUARIO',{
        TIPO: {
                VENDEDOR: "V",
                VENDEDOR_INTERNO: "I",
                VENDEDOR_EXTERNO:  "E",
                SUPERVISOR: "S",
                OFICINA: "A"
        }
})
/**
 * @feature V4-HU010
 *  - add TODOS: 5
 */
.constant('TIPO_BUSQUEDA_CONTRATO',{
        DOCUMENTO_ASOCIADO : 2,
        NRO_CONTRATO: 3,
        NOMBRE_ASOCIADO: 1,
        TODOS: 5
})
.constant('VISTAS_ID',{
        NUEVA_OBLIGACION_CIA: 1
})
.constant('ESTADO_HORA_ASAMBLEA',{
        QUEDA_SUFICIENTE_TIEMPO: 1,
        QUEDA_POCO_TIEMPO: 2,
        EN_PROCESO: 3,
        YA_PASO: 4,
        ASAMBLE_NO_CONCUERDA: 5
})
.constant('WEB_VERSION_ID','viuvbiueir898934nngl2lgoe929m93cm348nv294')
.constant('FUENTE_DE_INFORMACION',{
        SAF: 1,
        PROSPECTO: 2,
        PIDE: 3
})
//INICIO VTADIG2-59 JOHN VELASQUEZ
/**
 * @feature V4-HU020
 *  - add PERSONAL_NO_DOMICILIADO, LABORAL_NO_DOMICILIADO
 */
.constant('DIRECCION',{
        TIPO: {
                /** inicio @feature V4-HU020 **/
                PERSONAL_NO_DOMICILIADO: '04',
                LABORAL_NO_DOMICILIADO: '05',
                /** fin @feature V4-HU020 **/
                PERSONAL: '01',
                LABORAL: '02'
        }
})
.constant('DIRECTORIES',{
        APP_DIRECTORY_NAME: 'AppIngvta',
        APP_TEMP_DIRECTORY_NAME: 'temp'
})
//FIN VTADIG2-59 JOHN VELASQUEZ
.constant('PDF417PLUGIN',{
        /**
         * Scan these barcode types
         * Available: "PDF417", "USDL", "QR Code", "Code 128", "Code 39", "EAN 13", "EAN 8", "ITF", "UPCA", "UPCE"
         */
        TYPES : ["Code 39", "PDF417"],
        /**
         * Initiate scan with options
         * NOTE: Some features are unavailable without a license
         * Obtain your key at http://pdf417.mobi
         */
        OPTIONS:{
                    beep : true,  // Beep on
                    noDialog : true, // Skip confirm dialog after scan
                    uncertain : false, //Recommended
                    quietZone : false, //Recommended
                    highRes : false, //Recommended
                    inverseScanning: false,
                    frontFace : false
                },
        // Note that each platform requires its own license key

        // This license key allows setting overlay views for this application ID: com.pandero.ingvtaapp
        LICENCEIOS: "ORFETXOD-YT6VGXEF-CIKST2LG-JHF6LI6N-ZPS2HTOL-4WR43S7F-UPG4XZND-ZXF3JTTQ",
        // This license is only valid for package name "com.pandero.ingvtaapp"
        LICENCEANDROID:"MAFU5GKM-N5M2JMX7-IJAWSYYN-XJXV6M6L-BMT3RXAS-VMUFPGYF-2WEZWBOV-RGNVI7QM",
        PLATFORM_ANDROID: "android",
        PLATOFORM_IOS: "ios"
})
.constant('TIPO_BUSQUEDA_INCIDENCIAS',{        
        ESTADO: 1,
})
.constant('INCIDENCIA',{
        ESTADO:{
                EN_PROGRESO: 901006,
                PENDIENES: 901007,
                RESUELTAS: 901008
        }
})
/**
 * @feature [V4-HU016, V4-HU012, V4-HU011]
 *  - add SUB_GERENTE_DE_MARKETING and FUERZA_VENTAS
 */
.constant('ROL',{
        TIPO: {
                /** inicio @feature [V4-HU016, V4-HU012, V4-HU011] **/
                SUB_GERENTE_DE_MARKETING: 307,
                FUERZA_VENTAS: 255, //EDV-SUP
                /** fin @feature [V4-HU016, V4-HU012, V4-HU011] **/
                JEFE_MARKETING: 163,
                ASISTENTE_MARKETING: 165,
                GERENTE_PRODUCTO:  3,
                JEFE_VENTAS: 349
        }
})
.constant('TIPO_PLANTILLA',{        
        PAGO_CUOTA: 33,
        RECORDATORIO_ASAMBLEA: 35
})
/**
 *@feature V4-HU030 
 */
.constant('FORMULARIO',{
    FORMATO_ID_5_TO_5: 'VNV5TO5',
    FORMATO_ID_PASAPORTE_NUEVO_VENDEDOR: 'VNVPASSPORT',
    RESPUESTA: {
        SI: "SI",
        NO: "NO",
        NULL: null    
    },
    TIPO_INPUT: {
        SI_NO_NULL: "SI_NO_NULL",
        SI_NO: "SI_NO",
        NULL: "NULL"
    }
})
/**
 *@feature V4-HU030 
 */
.constant('VIAJE_NUEVO_VENDEDOR',{
    SEMANA: {
        ESTADO: {
                GENERADO: 1,
                FEEDBACK_ENVIADO: 2,
                FEEDBACK_CONFIRMADO: 3
        }
    }
})
.constant('PUNTOS_EXHIBICION',{
        FRIENDS_AND_FAMILY: 1128,
        REFERIDOS_FFVV: 1371
})
/**
 * @feature/Mastercard
 */
.constant('REGISTRO_TRANSACCION_MASTERCARD',{
        ORIGENID_INGRESO_VENTAS:1,
        ORIGENID_OBLIGACION_POR_PAGAR: 3,
        ORIGENID_PAGO_CIA_Y_ACUENTA:2
        

})
.constant("MIME_TYPE",{
        'a'      : 'application/octet-stream',
        'ai'     : 'application/postscript',
        'aif'    : 'audio/x-aiff',
        'aifc'   : 'audio/x-aiff',
        'aiff'   : 'audio/x-aiff',
        'au'     : 'audio/basic',
        'avi'    : 'video/x-msvideo',
        'bat'    : 'text/plain',
        'bin'    : 'application/octet-stream',
        'bmp'    : 'image/x-ms-bmp',
        'c'      : 'text/plain',
        'cdf'    : 'application/x-cdf',
        'csh'    : 'application/x-csh',
        'css'    : 'text/css',
        'dll'    : 'application/octet-stream',
        'doc'    : 'application/msword',
        'dot'    : 'application/msword',
        'dvi'    : 'application/x-dvi',
        'eml'    : 'message/rfc822',
        'eps'    : 'application/postscript',
        'etx'    : 'text/x-setext',
        'exe'    : 'application/octet-stream',
        'gif'    : 'image/gif',
        'gtar'   : 'application/x-gtar',
        'h'      : 'text/plain',
        'hdf'    : 'application/x-hdf',
        'htm'    : 'text/html',
        'html'   : 'text/html',
        'jpe'    : 'image/jpeg',
        'jpeg'   : 'image/jpeg',
        'jpg'    : 'image/jpeg',
        'js'     : 'application/x-javascript',
        'ksh'    : 'text/plain',
        'latex'  : 'application/x-latex',
        'm1v'    : 'video/mpeg',
        'man'    : 'application/x-troff-man',
        'me'     : 'application/x-troff-me',
        'mht'    : 'message/rfc822',
        'mhtml'  : 'message/rfc822',
        'mif'    : 'application/x-mif',
        'mov'    : 'video/quicktime',
        'movie'  : 'video/x-sgi-movie',
        'mp2'    : 'audio/mpeg',
        'mp3'    : 'audio/mpeg',
        'mp4'    : 'video/mp4',
        'mpa'    : 'video/mpeg',
        'mpe'    : 'video/mpeg',
        'mpeg'   : 'video/mpeg',
        'mpg'    : 'video/mpeg',
        'ms'     : 'application/x-troff-ms',
        'nc'     : 'application/x-netcdf',
        'nws'    : 'message/rfc822',
        'o'      : 'application/octet-stream',
        'obj'    : 'application/octet-stream',
        'oda'    : 'application/oda',
        'pbm'    : 'image/x-portable-bitmap',
        'pdf'    : 'application/pdf',
        'pfx'    : 'application/x-pkcs12',
        'pgm'    : 'image/x-portable-graymap',
        'png'    : 'image/png',
        'pnm'    : 'image/x-portable-anymap',
        'pot'    : 'application/vnd.ms-powerpoint',
        'ppa'    : 'application/vnd.ms-powerpoint',
        'ppm'    : 'image/x-portable-pixmap',
        'pps'    : 'application/vnd.ms-powerpoint',
        'ppt'    : 'application/vnd.ms-powerpoint',
        'pptx'    : 'application/vnd.ms-powerpoint',
        'ps'     : 'application/postscript',
        'pwz'    : 'application/vnd.ms-powerpoint',
        'py'     : 'text/x-python',
        'pyc'    : 'application/x-python-code',
        'pyo'    : 'application/x-python-code',
        'qt'     : 'video/quicktime',
        'ra'     : 'audio/x-pn-realaudio',
        'ram'    : 'application/x-pn-realaudio',
        'ras'    : 'image/x-cmu-raster',
        'rdf'    : 'application/xml',
        'rgb'    : 'image/x-rgb',
        'roff'   : 'application/x-troff',
        'rtx'    : 'text/richtext',
        'sgm'    : 'text/x-sgml',
        'sgml'   : 'text/x-sgml',
        'sh'     : 'application/x-sh',
        'shar'   : 'application/x-shar',
        'snd'    : 'audio/basic',
        'so'     : 'application/octet-stream',
        'src'    : 'application/x-wais-source',
        'swf'    : 'application/x-shockwave-flash',
        't'      : 'application/x-troff',
        'tar'    : 'application/x-tar',
        'tcl'    : 'application/x-tcl',
        'tex'    : 'application/x-tex',
        'texi'   : 'application/x-texinfo',
        'texinfo': 'application/x-texinfo',
        'tif'    : 'image/tiff',
        'tiff'   : 'image/tiff',
        'tr'     : 'application/x-troff',
        'tsv'    : 'text/tab-separated-values',
        'txt'    : 'text/plain',
        'ustar'  : 'application/x-ustar',
        'vcf'    : 'text/x-vcard',
        'wav'    : 'audio/x-wav',
        'wiz'    : 'application/msword',
        'wsdl'   : 'application/xml',
        'xbm'    : 'image/x-xbitmap',
        'xlb'    : 'application/vnd.ms-excel',
        'xls'    : 'application/vnd.ms-excel',
        'xlsx'    : 'application/vnd.ms-excel',
        'xml'    : 'text/xml',
        'xpdl'   : 'application/xml',
        'xpm'    : 'image/x-xpixmap',
        'xsl'    : 'application/xml',
        'xwd'    : 'image/x-xwindowdump',
        'zip'    : 'application/zip'
      })
;
