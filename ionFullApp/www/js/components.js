/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |VTADIG2-504					 |1.0      |John Velasquez   |01-08-2018 | integración débito automático    |
 * |V4-HU006					 |1.0.1    |John Velasquez   |16-07-2019 | cargadorImagenesListaHorizontal -> tomarFoto y seleccinarImagen se ajustan al cambio en camaraService. |
 * |--------------------------------------------------------------------------------------------------------|
 */
/*PROFORMA */
angular.module('your_app_name.components', [])
.component('proformaCard',{
    templateUrl:'views/app/templateComponents/proformaCard.html',    
    bindings: {
        proforma: '=',
        mostrarNombreVendedor: '<?'
    },
    controller: function (){
        var ctrl = this;
    }
})
/*CONTRATO CARD */
.component('contratoCiaCard',{
    templateUrl:'views/app/templateComponents/contratoCiaCard.html',    
    bindings: {
        contrato: '=',
        ionicPopover: '=?',
        onOpenIonicPopover: '&?'
    },
    controller: function (){
        var ctrl = this;
    
        ctrl.openIonicPopover = function ($event) {
            if(ctrl.ionicPopover){
                ctrl.ionicPopover.show($event);
                if(ctrl.onOpenIonicPopover){
                    ctrl.onOpenIonicPopover({$event:$event,$contrato: this.contrato})
                }
            }
        };
        ctrl.closeIonicPopover = function () {
            if(ctrl.ionicPopover){
                ctrl.ionicPopover.hide();
            }
        };
    }
})
/*BUSQUEDA DE CONTRATO*/
.component('contratoSearch',{
    templateUrl:'views/app/templateComponents/contratoSearch.html',    
    bindings: {
        onSearch: '&',
        tipo: '<?',
        valor1: '<?',
        valor2: '<?'
    },
    controller: function ($scope,$ionicPopover,$rootScope,TIPO_BUSQUEDA_CONTRATO,TIPO_DOCUMENTO,validacionService,shareData){
        var ctrl = this;
        ctrl.lstTipoDocumento = $rootScope.listaGeneral.DOCUMENTO_IDENTIDAD;    
        ctrl.TIPO_BUSQUEDA_ID = TIPO_BUSQUEDA_CONTRATO;
        ctrl.tipoBusquedas = [
            {label: "Tipo y Nro de Documento", id: ctrl.TIPO_BUSQUEDA_ID.DOCUMENTO_ASOCIADO},
            {label: "Número de contrato", id: ctrl.TIPO_BUSQUEDA_ID.NRO_CONTRATO},
            {label: "Nombre del asociado", id: ctrl.TIPO_BUSQUEDA_ID.NOMBRE_ASOCIADO}
        ];
        if(!ctrl.tipo){
            ctrl.tipoBusqueda = ctrl.tipoBusquedas[0];
        }else{
            ctrl.tipoBusqueda = ctrl.tipoBusquedas.find(function(tipo){
                return tipo.id == ctrl.tipo;
            });
        }
        if(ctrl.tipoBusqueda.id===ctrl.TIPO_BUSQUEDA_ID.DOCUMENTO_ASOCIADO){        
            if(ctrl.valor1){
                ctrl.tipoDocumentoObj = shareData.mapTipoDocumento[ctrl.valor1];
            }else{
                ctrl.tipoDocumentoObj = shareData.mapTipoDocumento[TIPO_DOCUMENTO.DNI];        
            }        
            if(ctrl.valor2){
                ctrl.valor = ctrl.valor2;
            }
        }else{
            ctrl.valor = ctrl.valor1;
        }
    
        //interaction
        ctrl.valorChange = function(){
            switch(ctrl.tipoBusqueda.id){
                case ctrl.TIPO_BUSQUEDA_ID.DOCUMENTO_ASOCIADO:
                    ctrl.valor = validacionService.formatearDocumento(ctrl.tipoDocumentoObj,ctrl.valor)
                break;
                case ctrl.TIPO_BUSQUEDA_ID.NRO_CONTRATO:
                    ctrl.valor = validacionService.formatearCodigoContrato(ctrl.valor)
                break;
                case ctrl.TIPO_BUSQUEDA_ID.NOMBRE_ASOCIADO:
    
                break;
            }
            if(ctrl.valor){
                ctrl.valor = ctrl.valor.toUpperCase();
            }
        }
        ctrl.tipoDocumentoChange = function(){
            ctrl.valor = undefined;
        }
        ctrl.buscar = function(){
            var event = {};
            event.tipo = ctrl.tipoBusqueda.id;
            if(event.tipo == ctrl.TIPO_BUSQUEDA_ID.DOCUMENTO_ASOCIADO){
                event.valor1 = ctrl.tipoDocumentoObj.codigo;
                event.valor2 = ctrl.valor;
            }else{
                event.valor1 = ctrl.valor;
            }
            console.log("componentEvent: "+event)
            ctrl.onSearch({$event: event});
        }
        /*POPOVERS*/
        $ionicPopover.fromTemplateUrl('buscar-contrato-options-popover.html', {
                scope: $scope
            }).then(function (popover) {
                ctrl.contratoSearchOptionsPopover = popover;
            });
        ctrl.openSearchOptionsPopover = function ($event) {
            if(ctrl.contratoSearchOptionsPopover){
                ctrl.contratoSearchOptionsPopover.show($event);
            }
        };
        ctrl.closeSearchOptionsPopover = function () {
            if(ctrl.contratoSearchOptionsPopover){
                ctrl.contratoSearchOptionsPopover.hide();
            }
        };
        $scope.$on('$destroy', function () {
            if(ctrl.contratoSearchOptionsPopover){
                ctrl.contratoSearchOptionsPopover.remove();
            }
        });
        ctrl.selectOption = function(option){
            ctrl.tipoBusqueda = option;
            ctrl.valor = undefined;
            ctrl.closeSearchOptionsPopover();
        }
    }
})
/*CARGAR IMAGENES*/
/**
 * @feature V4-HU006
 * - tomarFoto y seleccionarImagen se ajustan al cambio en camaraService.hacerCaptura y camaraService.seleccionarImagen
 */
.component('cargadorImagenesListaHorizontal',{
    templateUrl:'views/app/templateComponents/cargadorImagenesListaHorizontal.html',    
    bindings: {
        ngModel: '=',
        maximoImagenes: '<?'
    },
    controller: function(camaraService){
        var ctrl = this;
        if(!ctrl.ngModel){
            ctrl.ngModel = [];
        }
        ctrl.quitarImagen = function($index){            
            ctrl.ngModel.splice($index,1);
        }
        ctrl.hayImagenes = function(){
            var hayImagenes = false;
            if(ctrl.ngModel!=undefined){
                if(ctrl.ngModel.length>0){
                    hayImagenes = true;
                }
            }
            return hayImagenes;
        }
        /**
         * feature V4-HU006
         * - ajuste al cambio de camaraService.hacerCaptura
         */
        ctrl.tomarFoto = function(){
            if(!ctrl.seLlegoAlLimiteDeImagenes()){
                camaraService.hacerCaptura().then(function(imagen){
                    /** inicio @feature V4-HU006 **/
                    ctrl.ngModel.push('data:image/jpeg;base64,' + imagen);
                    /** fin @feature V4-HU006 **/
                });
            }      			
		}
        /**
         * feature V4-HU006
         * - ajuste al cambio de camaraService.seleccionarImagen
         */
		ctrl.seleccionarImagen = function(){    
            if(!ctrl.seLlegoAlLimiteDeImagenes()){        
                camaraService.seleccionarImagen().then(function(imagen){
                    /** inicio @feature V4-HU006 **/
                    ctrl.ngModel.push('data:image/jpeg;base64,' + imagen);
                    /** fin @feature V4-HU006 **/
                });
            }
        }
        ctrl.seLlegoAlLimiteDeImagenes = function(){
            var seLlego = false;
            if(ctrl.maximoImagenes){
                if(ctrl.ngModel.length>=ctrl.maximoImagenes){
                    seLlego = true;
                }
            }
            return seLlego;
        }
    }
})
.component('incidencia',{
    templateUrl:'views/app/templateComponents/incidencia.html',    
    bindings: {
        incidencia: '=',
    },
    controller: function(){
        var ctrl  = this;
    }
})
.component('generarlSearch',{
    templateUrl:'views/app/templateComponents/generarlSearch.html',    
    bindings: {
        tipos: '=',
    },
    controller: function(){
        var ctrl  = this;
    }
})
.component('chat',{
    templateUrl:'views/app/templateComponents/chat.html',    
    bindings: {
        chat: '=',
        messageMaxLength: '<?'
    },
    controller: function(chatService, $rootScope,$ionicScrollDelegate){
        var ctrl  = this;
        ctrl.nuevoMensaje = {
            mensaje: undefined
        }
        ctrl.esMiMensaje = function(mensaje){
            return $rootScope.me.usuarioID === mensaje.personaID;
        }
        ctrl.enviarMensaje = function(){
            if(ctrl.nuevoMensaje.texto){                
                chatService.enviarMensaje(ctrl.chat.chatID, ctrl.nuevoMensaje.texto).then(function(data){
                    ctrl.chat.mensajes.push(data);
                    ctrl.nuevoMensaje.texto = undefined;
                    $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);                    
                });
            }            
        }
        ctrl.obtenerCSS = function(mensaje){
            if(ctrl.esMiMensaje(mensaje)){
                return 'mensaje-autor';
            }else{
                return 'mensaje-amigo';
            }
        }
    }
})


/**/
.component('audioRecorder',{
    templateUrl:'views/app/templateComponents/audioRecorder.html',    
    bindings: {
        fileName: '<',
        onRecordComplete: '&?',
        onRecordStart: '&?',
        onCancelRecord: '&?',
        onResetRecord: '&?'
    },
    controller: function ($cordovaMedia, $interval,$rootScope,$scope, $ionicPopup,recorderService) {
        var ctrl = this;
        var media = undefined;
        
        ctrl.STATUS = {
            PRE_RECORDING: -1,
            RECORDING: 0,
            RECORD_COMPLETE: 1,
            PLAYING: 2,
            PAUSE: 3
        }
        var audioProgressInterval;
        ctrl.limit = 300;//300 segundos -> 5 minutos
        ctrl.$onInit = function(){
            ctrl.reproduccion = {};
            ctrl.reproduccion.status=ctrl.STATUS.PRE_RECORDING;
            ctrl.directory = $rootScope.appTempDirectory                 
            $rootScope.$on("recordCanceled",()=>{
                ctrl.cancelRecord()
            });
            $rootScope.$on("recordAcepted", () => {
                ctrl.stop();        
            });     
        }
        ctrl.$onChanges = function(changes){
            if(changes.fileName){
                ctrl.reproduccion.status = ctrl.STATUS.PRE_RECORDING;
                if(ctrl.record){
                    ctrl.record.stop();
                }
                ctrl.record = recorderService.newRecord(ctrl.directory, ctrl.fileName, 300);
            }
        }
        ctrl.$onDestroy = function () {
            ctrl.destroyProgressInterval();
        }
        ctrl.initProgressInterval = function() {
            audioProgressInterval = $interval(ctrl.getProgress,100); 
        }
        ctrl.destroyProgressInterval = function() {
            if (angular.isDefined(audioProgressInterval)) {
                $interval.cancel(audioProgressInterval);
                audioProgressInterval = undefined;
            }
        }
        ctrl.createNewMedia = function(path) {
            if(path){
                if(!path.endsWith('.mp3')){path = path+".mp3";}                
                media = $cordovaMedia.newMedia(path);
            }
        }
        ctrl.startRecord = function() {
            if(ctrl.record) {
                ctrl.record.start().then(function(path){                    
                    ctrl.recordComplete(path);
                });
                ctrl.reproduccion.status = ctrl.STATUS.RECORDING;
                if(ctrl.onRecordStart){
                    ctrl.onRecordStart();
                }
            }
        }
        ctrl.stopRecord = function(){
            if(ctrl.record){
                ctrl.record.stop().then(function(path){
                    ctrl.recordComplete(path);
                });                
            }
        }
        ctrl.recordComplete = function(path){
            ctrl.reproduccion.status = ctrl.STATUS.RECORD_COMPLETE;
            ctrl.createNewMedia(path);
            if(ctrl.onRecordComplete){
                ctrl.onRecordComplete({$media: media});            
            }
            //ctrl.setVolume(0);
            //ctrl.play();            
            //setTimeout(()=>{
                //ctrl.duration = parseInt(media.media.getDuration());
                getDurationOfMedia(media).then(
                    function(duration){
                        ctrl.duration = duration;
                    }, 
                    function(){
                        console.error("error al obtener la duracion del audio");
                    });
                //ctrl.stop();
                //ctrl.setVolume(1);
                ctrl.reproduccion.status = ctrl.STATUS.RECORD_COMPLETE;                
            //},25);
        }

        ctrl.setVolume = function(level){
            if(media){
                media.setVolume(level);
            }
        }

        ctrl.play = function(){
            if(media){
                ctrl.initProgressInterval();
                media.play();
                ctrl.reproduccion.status = ctrl.STATUS.PLAYING;
            }
        }
        ctrl.stop = function(){
            $scope.$apply(function () {
                if(media){
                    ctrl.seekToStart();                    
                    ctrl.reproduccion.status = ctrl.STATUS.RECORD_COMPLETE;                    
                    ctrl.destroyProgressInterval();
                    media.stop();                
                }
            });
        }
        ctrl.cancelRecord = function(){
            if(media){
                media.stop();         
            }
            if(ctrl.record){
                ctrl.record.stop();
            }
            ctrl.destroyProgressInterval();
            ctrl.reproduccion.status = ctrl.STATUS.PRE_RECORDING;
            if(ctrl.onCancelRecord){ctrl.onCancelRecord({$event: ""});}
                
        }
        ctrl.seek = function($event){
            media.seekTo(parseInt(ctrl.duration*($event.clientX/$event.currentTarget.offsetWidth))*1000);
        }
        ctrl.seekToStart = function() {
            media.seekTo(0);
        }
        ctrl.pause = function(){
            if(media){
                media.pause();
                ctrl.reproduccion.status = ctrl.STATUS.PAUSE;
            }
        }
        ctrl.getProgress = function(){
            if(media&&(ctrl.reproduccion.status==ctrl.STATUS.RECORD_COMPLETE||ctrl.reproduccion.status==ctrl.STATUS.PLAYING||ctrl.reproduccion.status==ctrl.STATUS.PAUSE)){                
                media.media.getCurrentPosition(p=>{                    
                    ctrl.progress = parseInt(p);
                    ctrl.progressPct = Math.abs(p*100/ctrl.duration);                    
                    if(p<0){
                        ctrl.stop();
                        ctrl.reproduccion.status = ctrl.STATUS.RECORD_COMPLETE;
                    }
                },e=>console.log(e));
                return true;
            }else{
                return false;
            }
        }
        ctrl.reset = function(){
            var confirmPopup = $ionicPopup.confirm({
				title: 'Grabación',
				template: 'Se eliminará la grabación',
				cancelText: 'Cancelar',
        			okText: 'Aceptar'
			});

			confirmPopup
			.then(function(res) {
				if(res) {
					if(media){
                        media.release();
                        ctrl.destroyProgressInterval();
                        ctrl.reproduccion.status=ctrl.STATUS.PRE_RECORDING;
                        if(ctrl.onResetRecord){ctrl.onResetRecord();}
                    }
				}
            });            
        }                
    }
})
.component('speechRecorder',{
    templateUrl:'views/app/templateComponents/speechRecorder.html',    
    bindings: {
        contrato: '<',
        titular: '<',
        onRecordComplete: '&?',
        onRecordStart: '&?',
        onCancelRecord: '&?',
        onResetRecord: '&?'
    },
    controller: function (speechService,$rootScope,$cordovaMedia,$cordovaFile,$sce,DIRECTORIES,TIPO_ASOCIADO,$ionicPopup) {
        
        var ctrl = this;                

        ctrl.trustAsHtml = function(string) {
            return $sce.trustAsHtml(string);
        };

        ctrl.$onInit = function(){
            var d = new Date();
            
            ctrl.appDirectory = $rootScope.appDirectory;
            
            ctrl.appTempDirectory = $rootScope.appTempDirectory;                        
            
            ctrl.refresh();

            $rootScope.$on('speechRecordCanceled', () => {
                ctrl.stopSpeak();
                $rootScope.$emit("recordCanceled"); 
            });

            $rootScope.$on('speechRecordAcepted', () => {
                ctrl.stopSpeak();
                $rootScope.$emit("recordAcepted");  
            });

        }

        ctrl.$onChanges = function(changes){
            if(changes.titular.currentValue||changes.contrato.currentValue){
                ctrl.refresh();
            }else{
                ctrl.baseAudio = undefined;
            }
        }

        ctrl.refresh = function(){
            if(ctrl.speak){ctrl.speak.release();}
            if(!ctrl.contrato||!ctrl.titular){console.error("contrato y titular con necesarios");return;}
            var date = new Date();
            ctrl.fileName = ctrl.titular.tipoDocumento.descripcion+"-"+ctrl.titular.personaCodigoDocumento+'-'+new Date().getTime()+'.mp3';
            ctrl.baseAudio = "base-" + ctrl.fileName;
            var titular;
            var representanteLegal;
            var respaldo;
            if(ctrl.titular.asociadoTipoID!=TIPO_ASOCIADO.TITULAR&&ctrl.titular.asociadoTipoID!=TIPO_ASOCIADO.RESPALDO){//es representante legal
                representanteLegal = ctrl.titular;
                titular = ctrl.contrato.titulares[0];
            }else{
                if(ctrl.titular.asociadoTipoID === TIPO_ASOCIADO.TITULAR){
                    titular = ctrl.titular;
                    //ctrl.fileName = [ctrl.fileName.slice(0, 7), "TITULAR-", ctrl.fileName.slice(7)].join(''); 
                }else if(ctrl.titular.asociadoTipoID === TIPO_ASOCIADO.RESPALDO){
                    respaldo = ctrl.titular;
                    //ctrl.fileName = [ctrl.fileName.slice(0, 7), "RESPALDO-", ctrl.fileName.slice(7)].join(''); 
                } 
            }
            ctrl.speech = undefined;
            speechService.getSpeechContrato(ctrl.contrato, titular, representanteLegal,respaldo).then(speech=>{                
                ctrl.speech = speech.texto.replace(/  /g,' ');
                ctrl.speechSSML = speech.textoSSML.replace(/  /g,' ');
                speechService.getSpeakBase64(ctrl.speechSSML).then(speak=>{
                    var blob = new Blob([speak],{type: "octet/stream"})
                    $cordovaFile.createFile(ctrl.appTempDirectory, ctrl.baseAudio, true).then(f=>{                        
                        $cordovaFile.writeFile(ctrl.appTempDirectory, ctrl.baseAudio, _base64ToArrayBuffer(speak), true)
                        .then(fl=>{
                            ctrl.speak = $cordovaMedia.newMedia(ctrl.appTempDirectory+ctrl.baseAudio);
                        },e=>{
                            console.log(e);
                        })
                    },e=>{
                        console.log(e);
                    });
                });
            });
        }

        ctrl.recordStart = function(){
            setTimeout(()=>{
                if(ctrl.speak){ctrl.speak.play();};
                if(ctrl.onRecordStart){ctrl.onRecordStart();}
            }, 500);            
        }

        ctrl.recordComplete = function($media){
            ctrl.stopSpeak();
            var duracionGrabacion = 1;
            var duracionSpeak = 0;
            Promise.all([getDurationOfMedia($media), getDurationOfMedia(ctrl.speak)]).then((values)=>{
                duracionGrabacion = values[0];
                duracionSpeak = values[1];
                if(duracionGrabacion > duracionSpeak){
                    if(ctrl.onRecordComplete){                
                        ctrl.onRecordComplete({$media: $media});
                    }
                }else{
                    $ionicPopup.alert({
                        title: 'Grabación firma electrónica',
                        template: 'Debe escuchar todo el speech del contrato resumido y expresar su conformidad y aceptación',
                        okText: 'Aceptar'
                    });
                }
            })            
        }

        ctrl.cancelRecord = function(){
            ctrl.stopSpeak();
            if(ctrl.onCancelRecord){ctrl.onCancelRecord({$event: ""});}
        }

        ctrl.resetRecord = function(){
            if(ctrl.onResetRecord){ctrl.onResetRecord()}
        }

        ctrl.stopSpeak = function(){
            if(ctrl.speak){
                ctrl.speak.stop();
                ctrl.speak.release();
                ctrl.speak = $cordovaMedia.newMedia(ctrl.appTempDirectory+ctrl.baseAudio);
            }
        }
    }
})
.component('firmaDigitalContrato',{
    templateUrl:'views/app/templateComponents/firmaDigitalContrato.html',    
    bindings: {
        contrato: '<',
        personas: '=',
    },
    controller: function ($ionicModal,$scope,$ionicPopup,$http, $rootScope,$cordovaFile,TIPO_ASOCIADO,PERSONA_TIPO) {
        var ctrl = this;

        ctrl.ESTATUS = {
            PLAY: 1,
            STOP: 2
        }

        ctrl.play = function($media){
            if(ctrl.mediaActual){ctrl.mediaActual.stop();}
            if($media){
                ctrl.mediaActual = $media;
                ctrl.mediaActual.play();                
            }
            ctrl.reproduccion.estatus = ctrl.ESTATUS.PLAY;
        }
        ctrl.stop = function(){
            if(ctrl.mediaActual){ctrl.mediaActual.stop();}
            ctrl.reproduccion.estatus = ctrl.ESTATUS.STOP;
        }

        ctrl.openModal = function(persona) {
            ctrl.stop();
            ctrl.complete = false;
            ctrl.modal.show();
            ctrl.personaActual = persona;
            $scope.persona = ctrl.personaActual;
            $scope.contrato = ctrl.contrato;
        };

        ctrl.closeModal = function() {
            ctrl.stop();
            ctrl.modal.hide();
            ctrl.personaActual = undefined;
            $scope.persona = ctrl.personaActual;
            $scope.contrato = undefined;
        };
        
        ctrl.showLabelRespaldo = function(changes){
                if(ctrl.personas){
                    if(ctrl.personas.find(t=>t.asociadoTipoID === TIPO_ASOCIADO.RESPALDO)){
                        return true;
                    }
                }
        }

        ctrl.$onInit = function(){
            ctrl.reproduccion = {};
            ctrl.reproduccion.estatus = ctrl.ESTATUS.STOP;
            ctrl.TIPO_ASOCIADO = TIPO_ASOCIADO;
            ctrl.TIPO_PERSONA = PERSONA_TIPO;

            $rootScope.$on("signatureCanceled", () => {
                ctrl.closeModal();
                $rootScope.$emit("speechRecordCanceled"); 
            });

            $rootScope.$on("signatureAccepted", () => {
                ctrl.closeModal();
                $rootScope.$emit("speechRecordAcepted"); 
            });

            $rootScope.$on("digitalSignatureAccepted", () => {
                ctrl.stop();
            });

            $rootScope.$on("digitalSignatureCanceled", () => {
                ctrl.personas = ctrl.personas || [];
                ctrl.personas.forEach(persona => {
                    if(persona.media){persona.media.release()}
                    persona.media = undefined;
                });
            });

            $scope.cancel = function(){
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Grabación',
                    template: 'Se cancelará la grabación',
                    cancelText: 'Cancelar',
                        okText: 'Aceptar'
                });
    
                confirmPopup
                .then(function(res) {
                    if(res) {
                        $rootScope.$emit("signatureCanceled");
                    }
                });                
            }
            $scope.aceptar = function(){
                if(!ctrl.complete){
                    $ionicPopup.alert({
                        title: 'Grabación firma electrónica',
                        template: 'No hay grabación',
                        okText: 'Aceptar'
                    });
                    return;
                }
                $scope.persona.media = ctrl.tempMedia;
                var $media = ctrl.personaActual.media;
                var path = $media.media.src.substring(0, $media.media.src.lastIndexOf("/")+1);
                var name = $media.media.src.substring($media.media.src.lastIndexOf("/")+1, $media.media.src.length);
                $cordovaFile.readAsArrayBuffer(path,name).then(f=>{
                    _arrayBufferToBase64(new Blob([f],{type: 'audio/mpeg'})).then(dataBase64=>{
                        $scope.persona.speechs = [{grabacionBase64: dataBase64.substring(dataBase64.indexOf(",")+1,dataBase64.length)}]
                        $rootScope.$emit("signatureAccepted");
                    });             
                });                           
            }
            $ionicModal.fromTemplateUrl('modal-firma-digital-speech.html', {
                scope: $scope,                
                animation: 'slide-in-up',
                backdropClickToClose: false,
                hardwareBackButtonClose: false
            }).then(function(modal) {
                ctrl.modal = modal;
            });          
            // Cleanup the modal when we're done with it!
            ctrl.$onDestroy = function() {
                ctrl.modal.remove();
            };
        }

        ctrl.refresh = function(){}

        ctrl.recordComplete = function($media){
            ctrl.stop();
            if($media){
                ctrl.tempMedia = $media;
            }   
            ctrl.stop();
            $scope.$apply(function () {       
                ctrl.complete = true;  
            });
        }        

        ctrl.cancelRecord = function(){
            
        }

        ctrl.resetRecord = function(){
            ctrl.complete = false;
        }

    }
})
.component('characterCounter',{
    templateUrl:'views/app/templateComponents/characterCounter.html',    
    bindings: {
        text: '<',
        maxLength: '<?'
    },
    controller: function(){

    }
})
/**
 * @issue integración débito automático
 * @author John Velasquez
 * @name debitoAutomatico
 * @desc agrega datos de debito automático a los titulares (tipo titular = 'T')
 * @param titulares
 */
.component('debitoAutomatico',{
    templateUrl:'views/app/templateComponents/debitoAutomatico.html',
    bindings: {
        tmpTitulares: '=titulares',
    },
    controller: function(TIPO_ASOCIADO, shareData, debitoAutomaticoService, validacionService){
        var ctrl = this;
        ctrl.validacionService = validacionService;
        ctrl.titulares = [];
        ctrl.currentTitular = null;
        ctrl.bancos = [];
        ctrl.currentBanco = null;
        ctrl.tiposCuenta = [];
        ctrl.currentTipoCuenta = null;

        ctrl.$onChanges = function(changes){
            if(changes.tmpTitulares){
                ctrl.updateTitulares();
            }
        };

        ctrl.changeBanco = function(banco){
            debitoAutomaticoService.getTipocuenta(banco.codigo).then(
                function (data) {
                    ctrl.tiposCuenta = data;
                    console.log(ctrl.tiposCuenta);
                }
            );
        };

        ctrl.$onInit = function(){
            ctrl.updateTitulares();
            ctrl.bancos = shareData.lstBancos.filter(b=>b.debitoAutomatico);
            if (ctrl.currentTitular.debitoAutomatico.banco){
                ctrl.currentTitular.debitoAutomatico.banco = ctrl.bancos.find(function(tipo){
                    return tipo.id == ctrl.currentTitular.debitoAutomatico.banco.id;
                });
            }
            if (ctrl.currentTitular.debitoAutomatico.tipoCuenta){
                ctrl.updatTipoCuenta();    
            }
        };

        ctrl.updatTipoCuenta = function(){
            debitoAutomaticoService.getTipocuenta(ctrl.currentTitular.debitoAutomatico.banco.codigo).then(
                function (data) {
                    ctrl.tiposCuenta = data;
                    ctrl.currentTitular.debitoAutomatico.tipoCuenta = ctrl.tiposCuenta.find(function(tipo){
                        return tipo.id == ctrl.currentTitular.debitoAutomatico.tipoCuenta.id;
                    });
                    console.log(ctrl.currentTitular.debitoAutomatico.tipoCuenta);
                }
            );            
        };
        
        ctrl.updateTitulares = function(){
            if(ctrl.tmpTitulares!=null) {
                ctrl.titulares = ctrl.tmpTitulares.filter(t=>t.asociadoTipoID === TIPO_ASOCIADO.TITULAR);
            }else{
                ctrl.titulares = [];
            }
            ctrl.evaluateDefeault();
        };

        ctrl.evaluateDefeault = function(){
            if(ctrl.titulares&&ctrl.titulares.length>0){
                if(!ctrl.titulares.reduce((p,c,i,a)=> p||ctrl.tieneDebitoAutomaticoActivo(c),false)){//vemos si ya se tiene algún titular definido para el débito
                    ctrl.setTitular(ctrl.titulares[0])
                }else{
                    ctrl.setTitular(ctrl.titulares.find(t=>ctrl.tieneDebitoAutomaticoActivo(t)));
                }
            }
        };

        ctrl.tieneDebitoAutomaticoActivo = function(titular){
            titular.debitoAutomatico = titular.debitoAutomatico || {};
            return titular.debitoAutomatico.activo;
        };

        ctrl.setTitular = function(titular){
            if(ctrl.titulares){
                ctrl.titulares
                    .forEach(t => {
                        t.debitoAutomatico = t.debitoAutomatico || {};
                        t.debitoAutomatico.activo = false;
                    });
            }
            titular.debitoAutomatico.activo = true;
            ctrl.currentTitular = titular;
        };
    }
});
