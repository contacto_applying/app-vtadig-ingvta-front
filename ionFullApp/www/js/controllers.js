/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |VTAOBS1-57                   |1.0      |John Velasquez   |28.09.2017 |                                  |
 * |VTADIG2-18                   |1.0      |John Velasquez   |28.09.2017 |                                  |
 * |VTADIG2-504					 |1.0      |John Velasquez   |01-08-2018 | integración débito automático    |
 * |JIRAV3-002					 |1.0.1    |John Velasquez   |16.11.2018 | inject $stateParams detalleRestriccionCtrl    |
 * |JIRAV3-001					 |1.0.2    |John Velasquez   |19.11.2018 | mostrat mensajes de evaluaciañon crediticia    |
 * |JIRAV3-004					 |1.0.3    |John Velasquez   |23.11.2018 | login no requiere punto de exhibicion y se agrega controlador 'SeleccionPuntoDeVentaCtrl'  |
 * |JIRAV3-005					 |1.0.4    |John Velasquez   |26.11.2018 | agregar opcion 'Mis Prospectos' a Rol Gerente, mostrar prospectos y detalle de prospectos en mode de solo lectura a a gerentes |
 * |JIRAV3-006                   |1.0.5    |John Velasquez   |28/11/2018 | agregar bloques de registro proforma e ingreso de venta, por prospecto,  por restricciones de grupo de fuerzas de venta |
 * |JIRAV3-007                   |1.0.6    |John Velasquez   |28/11/2018 | agregar información de cuotas devengada en la generación de la proforma y autocompletar esta información al ingresar venta |
 * |JIRAV3-007:02                |1.0.7    |John Velasquez   |06/12/2018 |  |
 * |CASOC7                       |1.0.8    |John Velasquez   |19/02/2019 |  |
 * |V4-HU005                     |1.0.9    |John Velasquez   |15.07.2019 | la validación de los teléfonos se realiza en el back y la validación del scanner no es bloqueante|
 * |V4-HU001                     |1.0.10    |John Velasquez   |12/07/2019 | la validación de los telefonos para el registro de prospecto se realiza en el backend |
 * |V4-HU003                     |1.0.11    |John Velasquez   |12/07/2019 | verificación de la proforma antesd e ingreso de venta |
 * |FIDELIZACION                 |1.0.9    |John Velasquez   |06.08.2019 | agregar productos de tipo inmuebles a la ffvv FIDELIZACION|
 * |V4-HU006					 |1.0.9    |John Velasquez   |16-07-2019 | agregar funcionalidad de adjuntar archivo de documento de identidad en la pantalla de datos del titular |
 * |V4-HU010					 |1.1.0    |John Velasquez   |30.07.2019 | implementar envio de recordatorio de pago de cía |
 * |V4-HU011                     |1.2.0      |John Velasquez |16/08/2019 | agregar funcionalidad de simulacion de premios y comisiones|
 * |V4-HU012                     |1.3.0    |John velasquez   |25/07/2019 | agregar controller 'reporteComisionesCtrl' y opción controle de opción de menú reportes->[EDV, Comisiones]  |
 * |V4-HU016                     |1.4.0    |John velasquez   |25/07/2019 | agregar controller 'estadisticasRematesCtrl' y opción controle de opción de menú reportes->[EDV, ESTADISTICO REMATES]  |
 * |C5-Inmuebles                 |1.4.1    |John Velasquez   |09.09.2019 | agrear producto c5-inmuebles y quitar el resto en inmuebles|
 * |V4-HU020                     |1.4.2   |John Velasquez   |12-07-2019 | cliente no domiciliado |
 * |V4-HU030                     |1.5.0    |John velasquez   |07/09/2019 | viaje nuevo vendedor  |
 * |PL-20						 |1.0		|Nataly Otero	 |03/11/2020	| integración pago link	|
 * |--------------------------------------------------------------------------------------------------------|
 */

angular.module('your_app_name.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig,$sessionStorage,CFG_ENVIRONMENT,CFG_FONDO) {
	$sessionStorage.token = undefined;
	$scope.fondo = CFG_FONDO;
	$scope.environment = CFG_ENVIRONMENT;
})

// APP
/**
 * @issue JIRAV3-005
 *  - remove method $scope.showOptionEDV
 *  - add methods $scope.showNuevaProform and $scope.showMisProspectos
 * @feature V4-HU010
 *  - aggregar opciónes de envio de recordatorio de pagos de cía
 * @feature V4-HU011
 *  - agregar opción simulacion PyC
 * @feature [V4-HU016, V4-HU012]
 *  - manejar opción de menú 'reportes'
 * @feature V4-HU030
 *  - agregae opción de menu 'viaje nuevo vendedor'
 */
	.controller('AppCtrl', function($scope,$state, $rootScope, $ionicConfig,authService,me,shareData,listaGeneral,USUARIO,ROL) {
	
	$scope.me = $rootScope.me;
	$scope.TIPO_USUARIO= USUARIO.TIPO;
	$scope.ROL = ROL.TIPO;

	$scope.showIncidencia = false;
	$scope.showMisIncidencias = true;
	$scope.showNuevaIncidencia = true;
	/** inicio @feature V4-HU010 **/
	$scope.showNotificacionPago = false;
	/** fin @feature V4-HU010 **/
	/** inicio @feature [V4-HU016, V4-HU012] **/
	$scope.showReportes = false;
	/** fin @feature [V4-HU016, V4-HU012] **/
	
	/** inicio @feature V4-HU030 **/
	$scope.showViajeNuevoVendedorEdv = false;
	/** fin @feature V4-HU030 **/
	
	$scope.toggleShowIncidencias = function(){
		$scope.showIncidencia = !$scope.showIncidencia;
	}
	$scope.showNuevaIncidencia = function(){
		if($scope.showOptionIncidencia()){
			if($scope.me.rolID == $scope.ROL.JEFE_MARKETING || 
				$scope.me.rolID == $scope.ROL.ASISTENTE_MARKETING ||
				$scope.me.rolID == $scope.ROL.JEFE_VENTAS ||
				$scope.me.rolID == $scope.ROL.GERENTE_PRODUCTO){
					return false;
			}else{
				if(	$scope.me.tipoUsuarioVentas === $scope.TIPO_USUARIO.VENDEDOR||
					$scope.me.tipoUsuarioVentas === $scope.TIPO_USUARIO.VENDEDOR_INTERNO||
					$scope.me.tipoUsuarioVentas === $scope.TIPO_USUARIO.VENDEDOR_EXTERNO||
					$scope.me.tipoUsuarioVentas === $scope.TIPO_USUARIO.SUPERVISOR ){
					return $scope.showIncidencia;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}	
		
	}
	$scope.showMisIncidencias = function(){
		if($scope.showOptionIncidencia()){
			return $scope.showIncidencia;
		}else{
			return false;
		}
	}

	$scope.showOptionIncidencia = function(){
		if($scope.me.tipoUsuarioVentas && 
			$scope.me.tipoUsuarioVentas ===$scope.TIPO_USUARIO.OFICINA){
				return false;		
		}else{
			return true;
		}
	}
    /**
     * @feature V4-HU030
     */
	$scope.toggleShowViajeNuevoVendedorEdvOptions = () => {
	    $scope.showViajeNuevoVendedorEdv = !$scope.showViajeNuevoVendedorEdv;
    };
	/**
	 * @feature V4-HU010
	 */
	$scope.toggleShowNotificacionPago = () => {
		$scope.showNotificacionPago = !$scope.showNotificacionPago;
	};
	/**
	 * @feature V4-HU010
	 * @returns {boolean}
	 */
	$scope.showNotificacionPagoCia = () => {
		return $scope.showNotificacionPago && $scope.showOption();
	};
    /* inicio @issue JIRAV3-005 */
	/*$scope.showOptionEDV = function(){
		let respuesta = false;
		if($scope.me.tipoUsuarioVentas != undefined && $scope.me.tipoUsuarioVentas!=='S'){
			respuesta = true;
		}
		return respuesta;
	}*/
    $scope.showNuevaProforma = function() {
        let respuesta = false;
        if($scope.me !== undefined && $scope.me.tipoUsuarioVentas !== undefined && $scope.me.tipoUsuarioVentas!=='S'){
            respuesta = true;
        }
        return respuesta;
    };

    $scope.showMisProspectos = function(){
        let respuesta = false;
        if($scope.me !== undefined && ($scope.showNuevaProforma()||$scope.me.rolID === 3)){
            respuesta = true;
        }
        return respuesta;
    };
    /* fin @issue JIRAV3-005 */
	/**
	 * @feature [V4-HU016, V4-HU012]
	 */
	$scope.showOptionMisReportes = function(){
		return $scope.showReporteSeguimientoEDV() || $scope.reporteComisiones();
	};
	/**
	 * @feature [V4-HU016, V4-HU012]
	 */
	$scope.toggleShowReportes = function(){	
		$scope.showReportes = !$scope.showReportes;
	};
	/**
	 * @feature [V4-HU016, V4-HU012]
	 * @returns {boolean}
	 */
	$scope.showReporteSeguimientoEDV = function(){
		return $scope.showReportes && $scope.reporteSeguimientoEDV();
	};
	/**
	 * @feature [V4-HU016, V4-HU012]
	 * @returns {boolean}
	 */
	$scope.reporteSeguimientoEDV = function(){
		return $scope.showOption();
	}
	/**
	 * @feature V4-HU012
	 * @returns {boolean}
	 */
	$scope.showReporteComisiones = function(){
		return $scope.showReportes && $scope.reporteComisiones();
	};
	/**
	 * @feature V4-HU012
	 * @returns {boolean}
	 */
	$scope.reporteComisiones = function(){
		return [$scope.ROL.FUERZA_VENTAS].includes($scope.me.rolID);
	};

	/**
	 * @feature V4-HU011
	 */
	$scope.showOptionSimuladorPyC = function(){
		let respuesta = false;
		if([$scope.ROL.FUERZA_VENTAS].includes($scope.me.rolID)){

			respuesta = true;

		}
		return respuesta;
	}
    /**
     * @feature V4-HU016
     * @returns {boolean}
     */
    $scope.showReporteEstadisticoRemates = function(){
        return $scope.showReportes && $scope.reporteEstadisticoRemates();
    };
    /**
     * @feature V4-HU016
     * @returns {boolean}
     */
    $scope.reporteEstadisticoRemates = function (){
        return [$scope.ROL.FUERZA_VENTAS, $scope.ROL.GERENTE_PRODUCTO, $scope.ROL.SUB_GERENTE_DE_MARKETING].includes($scope.me.rolID);
    };
	/**
	 * @feature V4-HU030
	 * @returns {boolean}
	 */
	$scope.showOptionViajeNuevoVendedorSup = function(){
		let respuesta = false;
		if([$scope.ROL.FUERZA_VENTAS].includes($scope.me.rolID) &&
			[$scope.TIPO_USUARIO.SUPERVISOR].includes($scope.me.tipoUsuarioVentas)){
			respuesta = true;
		}
		return respuesta;
	};
	/**
	 * @feature V4-HU030
	 * @returns {boolean}
	 */
	$scope.showOptionViajeNuevoVendedorEdv = () => {
		let respuesta = false;
		if([$scope.ROL.FUERZA_VENTAS].includes($scope.me.rolID) &&
			[$scope.TIPO_USUARIO.VENDEDOR].includes($scope.me.tipoUsuarioVentas) &&
            $scope.me.indNuevoVendedor === true){
			respuesta = true;
		}
		return respuesta;
	};
	/**
	 * @feature V4-HU030
	 * @returns {boolean}
	 */
	$scope.showOptionVieneNuevoVendedorDetalleVendedor = () => {
        return $scope.showOptionViajeNuevoVendedorEdv() && $scope.showViajeNuevoVendedorEdv;
    };
	/**
	 * @feature V4-HU030
	 * @returns {boolean}
	 */
	$scope.showOptionVieneNuevoVendedorPasaporte = () => {
        return $scope.showOptionViajeNuevoVendedorEdv() && $scope.showViajeNuevoVendedorEdv;
    };
	$scope.showOption = function(){
		let respuesta = false;
		if($scope.me.rolID != $scope.ROL.JEFE_MARKETING && 
			$scope.me.rolID != $scope.ROL.ASISTENTE_MARKETING &&
			$scope.me.rolID != $scope.ROL.GERENTE_PRODUCTO &&
			$scope.me.rolID != $scope.ROL.JEFE_VENTAS){
			
		   respuesta = true;
			
		}
		return respuesta;
	}
		

	$scope.go=function(state,params){
		if(params){
			$state.go(state,params);
		}else{
			$state.go(state);
		}
	}

	shareData.lstTipoDocumento=listaGeneral.DOCUMENTO_IDENTIDAD;
	shareData.lstEstadoCivil=listaGeneral.ESTADO_CIVIL;
	shareData.lstTipoVia=listaGeneral.TIPO_VIA;
	shareData.lstTipoZona=listaGeneral.TIPO_ZONA;
	shareData.lstMedioComunicacionPreferente=listaGeneral.CLIENTES_MEDIO_COMUNICACION;
	shareData.lstBancos=listaGeneral.BANCO;
	shareData.lstPuntoDeExhibicion=listaGeneral.PUNTO_EXHIBICION;
	shareData.lstModalidadDevolucion=listaGeneral.CLIENTES_TIPO_CUENTA;
	shareData.lstSmvBienesServicios=listaGeneral.SMV_BIENES_SERVICIOS;
	shareData.lstTipoRelacionada=listaGeneral.RELACIONADOS_DESC;
	shareData.lstTipoVinculada=listaGeneral.VINCULADOS_DESC;
	shareData.lstTiposCorreo=listaGeneral.SMV_PERSONA_TIPO_CORREO;
	shareData.lstActividadLaboral = listaGeneral.ACTIVIDAD_LABORAL;
	shareData.lstPersonaOcupacion = listaGeneral.PERSONA_OCUPACION;

	/*ORDENAR ALFABETICAMENTE*/
	shareData.lstPersonaOcupacion.sort(function (a, b){
		return a.descripcionLarga.localeCompare(b.descripcionLarga);
	});

	angular.forEach(shareData.lstSmvBienesServicios, function (item) {
	shareData.mapSmvBienesServicios[item.codigo] = item;
	});

	angular.forEach(shareData.lstTipoDocumento, function (item) {
	shareData.mapTipoDocumento[item.codigo] = item;
	});

	
	if($rootScope.me.sexoID===undefined){
		$scope.image = 1;
	}else{
		$scope.image = $rootScope.me.sexoId;
	}
	
	$scope.sesionOut = function(){
		authService.clearCredentials();
		location.href = '';
	};
})

.controller("errorCtrl",function($scope){
	
})
.controller("accessDeniedCtrl",function($scope){
	
})
.controller('ProfileCtrl', function($scope,$rootScope,miEmbudo,proformaService,$ionicPopup) {
	$scope.dates = {};
	$scope.dates.firstDay = {};
	$scope.dates.lastDay = {};
	var alertPopup = {};
	
	var date = new Date(), y = date.getFullYear(), m = date.getMonth();
	var firstDay = new Date(y, m, 1);
	var lastDay = new Date(y, m + 1, 0);	

	$scope.dates.firstDay = firstDay;
	$scope.dates.lastDay  = lastDay;
	
	if($rootScope.me.sexoID===undefined){
		$scope.image = 1;
	}else{
		$scope.image = $rootScope.me.sexoID;
	}
	
	$scope.me = $rootScope.me;
	if(miEmbudo){
		$scope.miEmbudo = miEmbudo;
		$scope.chartData = [miEmbudo.prospectos, miEmbudo.proformas, miEmbudo.separacines, miEmbudo.ventas]
		$scope.chartLabels = ["NRO. PROSPECTOS", "NRO. PROFORMAS", "NRO. SEPARACIONES", "NRO. VENTAS"];
	}

	$scope.showFirstDatePicker = function(){
		document.getElementById('firstDayId').click();
	}


	$scope.$watch('dates.firstDay',(newVal, oldVal)=> {
		if(!newVal){
			$scope.dates.firstDay = oldVal;
			alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La fecha Desde es obligatoria." , okType: 'button-assertive'});
			return;
		}	
	})

	$scope.$watch('dates.lastDay',(newVal, oldVal)=> {
		if(!newVal){
			$scope.dates.lastDay = oldVal;
			alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La fecha Hasta es obligatoria." , okType: 'button-assertive'});
			return;
		}	
	})

	$scope.filterByDate = function(){
		
		if(($scope.dates.lastDay && $scope.dates.firstDay) && $scope.dates.lastDay< $scope.dates.firstDay ){
			alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La fecha Hasta debe ser mayor a la fecha Desde." , okType: 'button-assertive'});
			return;
		} else if ($scope.dates.lastDay && $scope.dates.firstDay ){
			proformaService.getMiEmbudo($scope.dates.firstDay,$scope.dates.lastDay).then(function(data){
				$scope.miEmbudo = data;
			 });
		}
	};
})

//LOGIN
/**
 * JIRAV3-004
 *  - quitar injection $sessionStorage y lstPuntoExhibicion
 *  - quitar restriccion de seleccion de punto de venta por defecto
 */
	.controller('LoginCtrl', function($scope, $state, $templateCache, $q, $rootScope/*, $sessionStorage*/,loginService,ListaGeneralService,shareData,$ionicPopup/*,lstPuntoExhibicion*/,CFG_ENVIRONMENT) {
	$scope.environment = CFG_ENVIRONMENT;
	$scope.credenciales = {};
	/* inicio @issue JIRAV3-004 */
	//$scope.lstPuntoExhibicion = lstPuntoExhibicion.PUNTO_EXHIBICION;
	/*$scope.defaults = {
		puntoExhibicion: undefined
	};
	$sessionStorage.defaults = {
		puntoExhibicion: undefined
	};*/
	/* fin @issue JIRAV3-004 */
	$scope.doLogIn = function(){
        /* inicio @issue JIRAV3-004 */
		/*if(!$scope.defaults.puntoExhibicion){
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccione su punto de venta." , okType: 'button-assertive'});
			return;
		}*/
        /* fin @issue JIRAV3-004 */
		loginService.login($scope.credenciales,function(response){
			if(response.success){
                /* inicio @issue JIRAV3-004 */
				//$sessionStorage.defaults.puntoExhibicion = $scope.defaults.puntoExhibicion;
				//console.log($scope.puntoExhibicion);
				//$state.go('app.profile');
				$state.go('auth.seleccionPuntoDeVenta');
                /* fin @issue JIRAV3-004 */
			}else{
				//alert("usuario o contraseña incorrecta");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: response.doc , okType: 'button-assertive'});
			}
		});		
	};

	$scope.user = {};

	$scope.user.email = "john@doe.com";
	$scope.user.pin = "12345";

	// We need this for the form validation
	$scope.selected_tab = "";

	$scope.$on('my-tabs-changed', function (event, data) {
		$scope.selected_tab = data.title;
	});

})
/**
 * @feature V4-HU030
 */
.controller('viajeNuevoVendedorSupervisorCtrl', function ($scope, $ionicModal, $state, $ionicPopup, vendedores, VIAJE_NUEVO_VENDEDOR) {
	$scope.vendedores = vendedores || [];
	$scope.vendedores = $scope.vendedores.filter(vendedor => vendedor.indViajeNuevoVendedor);
	$scope.search = {};
	$scope.SEMANA_ESTADO = VIAJE_NUEVO_VENDEDOR.SEMANA.ESTADO;
	console.log($scope.vendedores);
	$scope.nuevaObservacion = (vendedor) => {
		if(!vendedor.semanaActual){
			$ionicPopup.alert({ title: 'Alerta', template: 'No puede agregar observaciones, la persona ya terminó su viaje.', okText: 'Aceptar', okType: 'button-assertive'});
		}
		else if (vendedor.semanaActual && vendedor.semanaActual.estado === $scope.SEMANA_ESTADO.GENERADO) {
			$state.go('app.viajeNuevoVendedorObservacion', {personaId: vendedor.personaID});
		} else {
			$ionicPopup.alert({ title: 'Alerta', template: 'No puede agregar observaciones, ya se envió feedback para esta semana.', okText: 'Aceptar', okType: 'button-assertive'});
		}
	}
})
/**
 * @feature V4-HU030
 */
.controller('viajeNuevoVendedorObservacionCtrl', function ($scope, $ionicModal, $ionicPopup, $ionicHistory, $state, formulario, vendedor, ViajeNuevoVendedorService, FORMULARIO) {
	$scope.formulario = formulario || {};
	$scope.TIPO_INPUT = FORMULARIO.TIPO_INPUT;
	$scope.RESPUESTA = FORMULARIO.RESPUESTA;
	$scope.vendedor = vendedor;
	$scope.observacion = {
		detalle: {}
	};
	$scope.currentTextArea = null;
	$scope.$ionicGoBack = function() {
		$ionicPopup.confirm({
			title: 'Alerta',
			template: '¿Está seguro de cancelar el registro de la observación?',
			cancelText: 'Cancelar',
			okText: 'Aceptar'
		}).then(
			(res) => {
				if(res) {
					$ionicHistory.goBack();			
				}
			}
		)
	};
	console.log($scope.formulario);
	$scope.init = () => {
		$scope.observacion.detalle = $scope.formulario.items.map( item => {
			return {
				formularioItemId: item.id,
				label: item.label,
				descripcion: item.descripcion,
				tipo: item.tipo
			}
		});	
	};
	$scope.toggleSi = (item, $index) => {
		if(item && item.respuestaSi === true) {
			item.respuestaNo = false;
			item.respuesta = $scope.RESPUESTA.SI;
		}
		if(item.respuestaSi !== true && item.respuestaNo !== true){
			item.respuesta = $scope.RESPUESTA.NULL;
			item.comentario = null;
		}
		if($scope.currentTextArea) {
			$scope.currentTextArea.blur();
		}
		$scope.currentTextArea = document.getElementById(`text_area_${$index}`);
		if($scope.currentTextArea) {
			$scope.currentTextArea.blur();
		}
		let nofocus = document.getElementById(`nofocus`);
		if(nofocus && nofocus.focus) {
			nofocus.focus();
		}
	};
	$scope.toggleNo = (item, $index) => {
		if(item && item.respuestaNo === true) {
			item.respuestaSi = false;
			item.respuesta = $scope.RESPUESTA.NO;
		}
		if(item.respuestaSi !== true && item.respuestaNo !== true){
			item.respuesta = $scope.RESPUESTA.NULL;
			item.comentario = null;
		}
		if($scope.currentTextArea) {
			$scope.currentTextArea.blur();
		}
		$scope.currentTextArea = document.getElementById(`text_area_${$index}`);
		if($scope.currentTextArea) {
			$scope.currentTextArea.blur();
		}
		let nofocus = document.getElementById(`nofocus`);
		if(nofocus && nofocus.focus) {
			nofocus.focus();
		}
	};
	$scope.setCurrentTextArea = ($index) => {
		$scope.currentTextArea = document.getElementById(`text_area_${$index}`);
		if($scope.currentTextArea) {
			$scope.currentTextArea.focus();
		}
	};
	$scope.guardarObservacion = () => {
		$scope.validarObservacion($scope.observacion).then(
			(observacion1) => {
				$ionicPopup.confirm({
					title: 'Alerta',
					template: '¿Está seguro de enviar el formulario?',
					cancelText: 'Cancelar',
					okText: 'Aceptar'
				}).then(function(res) {
					if(res) {
						$scope.enviarObservacion(observacion1)
					}
				});
			},
			(error) => $ionicPopup.alert({ title: 'Alerta', template: error, okText: 'Aceptar', okType: 'button-assertive'})
		)
	};
	$scope.enviarObservacion = (observacion) =>{
		ViajeNuevoVendedorService.guardarObservacion(vendedor.personaID, observacion).then(
			(data)=>{
				$ionicPopup.alert({ title: 'Alerta', template: data.message, okText: 'Aceptar', okType: 'button-assertive'}).then(
					(alert) => $state.go('app.viajeNuevoVendedorSup')
				)
			}
		)
	};
	$scope.validarObservacion = (observacion) => {
		return new Promise( (resolve, reject) => {
			let error = false;
			let detalle;
			for (i = 0; i <  $scope.observacion.detalle.length && !error; i++) {
				detalle = $scope.observacion.detalle[i];
				if(!detalle.tipo.includes($scope.TIPO_INPUT.NULL) && !detalle.respuesta){
					reject(`${detalle.label} es obligatorio`);
					error = true;
				}
				if(detalle.tipo.includes($scope.TIPO_INPUT.SI_NO) && detalle.respuesta === $scope.RESPUESTA.NO && !detalle.comentario){
					reject(`El comentario en <b>${detalle.label}</b> es obligatorio`);
					error = true;
				}
			}
			if(!error) {
				resolve(observacion);
			}
			
		});
	};
	$scope.init();
	
})
/**
 * @feature V4-HU030
 */
.controller('viajeNuevoVendedorDetalleVendedorCtrl', function ($scope, $ionicModal, $stateParams, $ionicPopup, vendedor) {
	$scope.vendedor = vendedor;
	$scope.paramVendedorId = $stateParams.personaId;
	/*$scope.estado = {
		1: "Generado",
		2: "Feedback enviado",
		3: "Feedback confirmado"
	}*/
})
/**
 * @feature V4-HU030
 */
.controller('viajeNuevoVendedorDetalleSemanaCtrl', function ($scope, $stateParams, $ionicModal, $state, $ionicPopup, $ionicHistory, me, reporte, tipofeedbackLst, VIAJE_NUEVO_VENDEDOR, USUARIO, ViajeNuevoVendedorService) {

	$scope.init = () => {
		$scope.reporte = reporte;
		if($scope.reporte.observaciones){
			$scope.reporte.observaciones.forEach(observacion => {
				if (observacion.detalle) {
					observacion.detalle.forEach( ob => {
						if(ob.respuesta) {
							ob.respuesta.toUpperCase();
						}
					})
				}
			});
		}
		$scope.semana = $stateParams.semana;
		$scope.personaId = $stateParams.personaId;
		$scope.tipofeedback = tipofeedbackLst;
		$scope.feedback = {
			semana: $scope.semana
		};
		$scope.SEMANA_ESTADO = VIAJE_NUEVO_VENDEDOR.SEMANA.ESTADO;
		$scope.TIPO_USUARIO = USUARIO.TIPO;
		$scope.me = me;
	};
	
	$scope.guardarFeedback = () => {
			$scope.validarFeedback($scope.feedback).then(
				(feedback) => $scope.enviarFeedback(feedback),
				(error) => $ionicPopup.alert({ title: 'Alerta', template: error, okText: 'Aceptar', okType: 'button-assertive'})
			)
	};

	$scope.validarFeedback = (feedback) => {
		return new Promise( (resolve, reject) => {
			if (!feedback.feedback) {
				reject("" +
					"El feedback es obligatorio");
			} else if (!feedback.tipo) {
				reject("El tipo de feedback es obligatorio");
			} else {
				resolve(feedback);
			}
		});
	};

	$scope.enviarFeedback = (feedback) => {
		$ionicPopup.confirm({
			title: 'Alerta',
			template: '¿Está seguro de enviar el feedback?',
			cancelText: 'Cancelar',
			okText: 'Aceptar'
		}).then(
			(res) => {
				if(res) {
					ViajeNuevoVendedorService.enviarFeedback($scope.personaId, $scope.semana, feedback).then(
						(data) => {
							$ionicPopup.alert({ title: 'Alerta', template: data.message, okText: 'Aceptar', okType: 'button-assertive'})
								.then((data1) => $ionicHistory.goBack());
						}
					);
				}
			}
		)
	};

	$scope.confirmarFeedBack = () => {
		ViajeNuevoVendedorService.confirmarFeedBack($scope.personaId, $scope.semana).then(
			(data) => {
				$ionicPopup.alert({ title: 'Alerta', template: data.message, okText: 'Aceptar', okType: 'button-assertive'})
					.then((data1) => $ionicHistory.goBack());
			}
		);
	};
	
	$scope.irPasaporte = () => {
		$state.go('app.viajeNuevoVendedorPasaporte',{personaId: $scope.personaId, semana: $scope.semana});
	};

	$scope.$ionicGoBack = function() {
		if ($scope.me.tipoUsuarioVentas === $scope.TIPO_USUARIO.SUPERVISOR) {
			if ($scope.reporte.semana.estado === $scope.SEMANA_ESTADO.GENERADO && $scope.reporte.permiteFeedback) {
				if ($scope.feedback.feedback || $scope.feedback.tipo) {
					$ionicPopup.confirm({
						title: 'Alerta',
						template: 'El feedback no se guardará, ¿Está seguro de salir?',
						cancelText: 'Cancelar',
						okText: 'Aceptar'
					}).then(
						(res) => {
							if(res) {
								$ionicHistory.goBack();
							}
						}
					)
				} else {
					$ionicHistory.goBack();
				}
			} else {
				$ionicHistory.goBack();
			}
		} else {
			$ionicHistory.goBack();
		}
	};
	
	$scope.init();
	console.log($stateParams);
	console.log($scope.reporte);
})
/**
 * @feature V4-HU030
 */
.controller('viajeNuevoVendedorPasaporteCtrl', function ($scope, $stateParams, $ionicModal, $state, $ionicPopup, $ionicHistory, me, pasaporte, vendedor, VIAJE_NUEVO_VENDEDOR, USUARIO, ViajeNuevoVendedorService) {
	
	
	$scope.init = () => {
		$scope.vendedor = vendedor;
		$scope.setPasaporte(pasaporte);
	};
	
	$scope.setPasaporte = (pasaporte) => {
		$scope.pasaporte = pasaporte;
		$scope.pasaporte.semanas.forEach(semana => semana.desplegado = false);
	};
	
	$scope.$ionicGoBack = function() {
			$ionicPopup.confirm({
				title: 'Alerta',
				template: 'Perderás la información no guardada, ¿Está seguro de salir?',
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			}).then(
				(res) => {
					if(res) {
						$ionicHistory.goBack();
					}
				}
			)
	};
	
	$scope.actualizarPasaporte = () => {
		$ionicPopup.confirm({
			title: 'Alerta',
			template: 'Se guardarán los cambios realizados ¿Desea continuar?',
			cancelText: 'Cancelar',
			okText: 'Aceptar'
		}).then(function(res) {
			if(res) {
				$scope.enviarPasaporte($scope.pasaporte)
			}
		});
	};

	$scope.enviarPasaporte = (pasaporte) => {
		ViajeNuevoVendedorService.enviarPasaporte($stateParams.personaId, pasaporte).then(
			(data) => {
				$ionicPopup.alert({ title: 'Alerta', template: data.message, okText: 'Aceptar', okType: 'button-assertive'})
					.then((data1) => {
						$scope.setPasaporte(data.data);
					});
			});
	};
	
	$scope.init();
})
/**
 * @feature V4-HU011
 */
.controller('simulacionPremiosComisionesCtrl', function ($scope, $ionicModal, $ionicPopup, simulacion) {
	$scope.simulacion = simulacion;
	console.log($scope.simulacion);
})
/**
 * @feature V4-HU011
 */
.controller('simuladorPremiosComisionesCtrl', function ($scope, $stateParams, $state, $ionicPopup, $state){
		$scope.formData = {
			cuota: $stateParams.cuota ? parseInt($stateParams.cuota) : null,
			certificadosVendidos: $stateParams.certificadosVendidos ? parseInt($stateParams.certificadosVendidos) : null
		};
		$scope.formData.cuotaPre = $scope.formData.cuota;
		$scope.formData.certificadosVendidosPre = $scope.formData.certificadosVendidos;
		
		$scope.updateUrl = () => {
			/*if(!$scope.formData.cuota){
				$scope.formData.cuota = $scope.formData.cuotaPre;
			}
			if(!$scope.formData.certificadosVendidos){
				$scope.formData.certificadosVendidos = $scope.formData.certificadosVendidosPre;
			}*/
			if($scope.formData.cuota > 999999999999){
				$scope.formData.cuota = 999999999999
			}
			if($scope.formData.certificadosVendidos > 999999999999){
				$scope.formData.certificadosVendidos = 999999999999
			}
			$scope.formData.cuotaPre = $scope.formData.cuota;
			$scope.formData.certificadosVendidosPre = $scope.formData.certificadosVendidos;
			if($scope.formData.cuota && $scope.formData.cuota > 0 && $scope.formData.certificadosVendidos && $scope.formData.certificadosVendidos > 0){
				$state.go('.', {cuota: $scope.formData.cuota, certificadosVendidos: $scope.formData.certificadosVendidos}, {notify: false});
			}
		};
		$scope.simulacion = {};
		$scope.obtenerReporte = () => {
			if(!$scope.formData.cuota||$scope.formData.cuota<=0){
				$ionicPopup.alert({
					title: 'Alerta',
					template: 'El valor del importe cuota debe ser mayor a 0.',
					okText: 'Aceptar',
					okType: 'button-assertive'
				});
				return;
			}
			if(!$scope.formData.certificadosVendidos||$scope.formData.certificadosVendidos<=0){
				$ionicPopup.alert({
					title: 'Alerta',
					template: 'El valor del importe de certificados vendidos debe ser mayor a 0.',
					okText: 'Aceptar',
					okType: 'button-assertive'
				});
				return;
			}
			$state.go('app.simulacionPremiosComisiones',{cuota:$scope.formData.cuota,certificadosVendidos:$scope.formData.certificadosVendidos});
		};
})
	
/**
 * JIRAV3-004
 *  - creación
 */
.controller('SeleccionPuntoDeVentaCtrl', function(lstPuntoExhibicion, $scope, $sessionStorage, $state, authService){
    $scope.lstPuntoExhibicion = lstPuntoExhibicion.PUNTO_EXHIBICION;
    $sessionStorage.defaults = {
        puntoExhibicion: undefined
    };
    $scope.selectPuntoExhibicion = function(puntoExhibicion){
        $sessionStorage.defaults.puntoExhibicion = puntoExhibicion;
        $state.go('app.profile');
	};
    $scope.sesionOut = function(){
        authService.clearCredentials();
        $state.go('auth.login');
    };
})
.controller('SignupCtrl', function($scope, $state) {
	$scope.user = {};

	$scope.user.email = "john@doe.com";

	$scope.doSignUp = function(){
		$state.go('app.registroClienteProspecto');
	};
})

.controller('ForgotPasswordCtrl', function($scope, $state) {
	$scope.recoverPassword = function(){
		$state.go('app.registroClienteProspecto');
	};

	$scope.user = {};
})

.controller('RateApp', function($scope) {
	$scope.rateApp = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1234555553>';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
			AppRate.promptForRating(true);
		}
	};
})


.controller('SendMailCtrl', function($scope, $cordovaEmailComposer, $ionicPlatform) {
  //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
  $scope.sendMail = function(){
	$ionicPlatform.ready(function() {		
	  $cordovaEmailComposer.isAvailable().then(function() {
		// is available
		console.log("Is available");
		$cordovaEmailComposer.open({
		  to: 'hi@startapplabs.com',
		  subject: 'Nice Theme!',
				body:    'How are you? Nice greetings from IonFullApp'
		}).then(null, function () {
		  // user cancelled email
		});
	  }, function () {
		// not available
		console.log("Not available");
	  });
	});
  };
})

.controller('MapsCtrl', function($scope, $ionicLoading) {

	$scope.info_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.center_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.my_location = "";

	$scope.$on('mapInitialized', function(event, map) {
		$scope.map = map;
	});

	$scope.centerOnMe= function(){

		$scope.positions = [];

		$ionicLoading.show({
			template: 'Loading...'
		});

		// with this function you can get the user’s current position
		// we use this plugin: https://github.com/apache/cordova-plugin-geolocation/
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			$scope.current_position = {lat: position.coords.latitude, lng: position.coords.longitude};
			$scope.my_location = position.coords.latitude + ", " + position.coords.longitude;
			$scope.map.setCenter(pos);
			$ionicLoading.hide();
		}, function(err) {
				 // error
				$ionicLoading.hide();
		});
	};
})

.controller('AdsCtrl', function($scope, $ionicActionSheet, AdMob, iAd) {

	$scope.manageAdMob = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
				{ text: 'Show Banner' },
				{ text: 'Show Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				AdMob.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
					if (button.text == 'Show Banner') {
						console.log("show banner");
						AdMob.showBanner();
					}

					if (button.text == 'Show Interstitial') {
					console.log("show interstitial");
					AdMob.showInterstitial();
				}

				return true;
			}
		});
	};

	$scope.manageiAd = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
			{ text: 'Show iAd Banner' },
			{ text: 'Show iAd Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show - Interstitial only works in iPad',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				iAd.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
					if (button.text == 'Show iAd Banner') {
						console.log("show iAd banner");
						iAd.showBanner();
					}
					if (button.text == 'Show iAd Interstitial') {
					console.log("show iAd interstitial");
					iAd.showInterstitial();
				}
				return true;
			}
		});
	};
})

// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope, $http) {
	$scope.feeds_categories = [];

	$http.get('feeds-categories.json').success(function(response) {
		$scope.feeds_categories = response;
	});
})

//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams) {
	$scope.category_sources = [];

	$scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
		$scope.categoryTitle = category.title;
		$scope.category_sources = category.feed_sources;
	});
})

//this method brings posts for a source provider
.controller('FeedEntriesCtrl', function($scope, $stateParams, $http, FeedList, $q, $ionicLoading, BookMarkService) {
	$scope.feed = [];

	var categoryId = $stateParams.categoryId,
			sourceId = $stateParams.sourceId;

	$scope.doRefresh = function() {

		$http.get('feeds-categories.json').success(function(response) {

			$ionicLoading.show({
				template: 'Loading entries...'
			});

			var category = _.find(response, {id: categoryId }),
					source = _.find(category.feed_sources, {id: sourceId });

			$scope.sourceTitle = source.title;

			FeedList.get(source.url)
			.then(function (result) {
				$scope.feed = result.feed;
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			}, function (reason) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			});
		});
	};

	$scope.doRefresh();

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkFeedPost(post);
	};
})

// SETTINGS
.controller('SettingsCtrl', function($scope, $ionicActionSheet, $state) {
	$scope.airplaneMode = true;
	$scope.wifi = false;
	$scope.bluetooth = true;
	$scope.personalHotspot = true;

	$scope.checkOpt1 = true;
	$scope.checkOpt2 = true;
	$scope.checkOpt3 = false;

	$scope.radioChoice = 'B';

	// Triggered on a the logOut button click
	$scope.showLogOutMenu = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			// buttons: [
			// { text: '<b>Share</b> This' },
			// { text: 'Move' }
			// ],
			destructiveText: 'Logout',
			titleText: 'Are you sure you want to logout? This app is awsome so I recommend you to stay.',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//Called when one of the non-destructive buttons is clicked,
				//with the index of the button that was clicked and the button object.
				//Return true to close the action sheet, or false to keep it opened.
				return true;
			},
			destructiveButtonClicked: function(){
				//Called when the destructive button is clicked.
				//Return true to close the action sheet, or false to keep it opened.
				$state.go('auth.walkthrough');
			}
		});

	};
})

// TINDER CARDS
.controller('TinderCardsCtrl', function($scope, $http) {

	$scope.cards = [];


	$scope.addCard = function(img, name) {
		var newCard = {image: img, name: name};
		newCard.id = Math.random();
		$scope.cards.unshift(angular.extend({}, newCard));
	};

	$scope.addCards = function(count) {
		$http.get('http://api.randomuser.me/?results=' + count).then(function(value) {
			angular.forEach(value.data.results, function (v) {
				$scope.addCard(v.picture.large, v.name.first + " " + v.name.last);
			});
		});
	};

	$scope.addFirstCards = function() {
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/left.png","Nope");
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/right.png", "Yes");
	};

	$scope.addFirstCards();
	$scope.addCards(5);

	$scope.cardDestroyed = function(index) {
		$scope.cards.splice(index, 1);
		$scope.addCards(1);
	};

	$scope.transitionOut = function(card) {
		console.log('card transition out');
	};

	$scope.transitionRight = function(card) {
		console.log('card removed to the right');
		console.log(card);
	};

	$scope.transitionLeft = function(card) {
		console.log('card removed to the left');
		console.log(card);
	};
})


// BOOKMARKS
.controller('BookMarksCtrl', function($scope, $rootScope, BookMarkService, $state) {

	$scope.bookmarks = BookMarkService.getBookmarks();

	// When a new post is bookmarked, we should update bookmarks list
	$rootScope.$on("new-bookmark", function(event){
		$scope.bookmarks = BookMarkService.getBookmarks();
	});

	$scope.goToFeedPost = function(link){
		window.open(link, '_blank', 'location=yes');
	};
	$scope.goToWordpressPost = function(postId){
		$state.go('app.post', {postId: postId});
	};
})

// WORDPRESS
.controller('WordpressCtrl', function($scope, $http, $ionicLoading, PostService, BookMarkService) {
	$scope.posts = [];
	$scope.page = 1;
	$scope.totalPages = 1;

	$scope.doRefresh = function() {
		$ionicLoading.show({
			template: 'Loading posts...'
		});

		//Always bring me the latest posts => page=1
		PostService.getRecentPosts(1)
		.then(function(data){
			$scope.totalPages = data.pages;
			$scope.posts = PostService.shortenPosts(data.posts);

			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.loadMoreData = function(){
		$scope.page += 1;

		PostService.getRecentPosts($scope.page)
		.then(function(data){
			//We will update this value in every request because new posts can be created
			$scope.totalPages = data.pages;
			var new_posts = PostService.shortenPosts(data.posts);
			$scope.posts = $scope.posts.concat(new_posts);

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.moreDataCanBeLoaded = function(){
		return $scope.totalPages > $scope.page;
	};

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkWordpressPost(post);
	};

	$scope.doRefresh();
})

// WORDPRESS POST
.controller('WordpressPostCtrl', function($scope, post_data, $ionicLoading) {

	$scope.post = post_data.post;
	$ionicLoading.hide();

	$scope.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};
})


.controller('ImagePickerCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaCamera) {

	$scope.images = [];
	// $scope.image = {};

	// $scope.openImagePicker = function() {
	//
	// 	//We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
  //   //implemented for iOS and Android 4.0 and above.
	//
  //   $ionicPlatform.ready(function() {
  //     $cordovaImagePicker.getPictures()
  //      .then(function (results) {
  //         for (var i = 0; i < results.length; i++) {
  //           console.log('Image URI: ' + results[i]);
  //           $scope.images.push(results[i]);
  //         }
  //       }, function(error) {
  //         // error getting photos
  //       });
  //   });
	// };

	$scope.openImagePicker = function(){
	//We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
	//implemented for iOS and Android 4.0 and above.

	$ionicPlatform.ready(function() {
	  var options = {
		quality: 100,
		destinationType: Camera.DestinationType.DATA_URL,
		sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
		allowEdit: true,
		encodingType: Camera.EncodingType.JPEG,
		targetWidth: 350,
		targetHeight: 350,
		saveToPhotoAlbum: false
	  };
	  $cordovaCamera.getPicture(options)
	   .then(function (imageData) {
		  var image = "data:image/jpeg;base64," + imageData;
		  $scope.images.push(image);
		}, function(error) {
		  console.log(error);
		});
	});
  };

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};

	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};
})


// SETTINGS
.controller('registerClientCtrl', function($scope, $ionicPopup,$state) {
	$scope.modoEdicion = true;
	$scope.editar=function(){
		$scope.modoEdicion = false;
	};
	$scope.guardar=function(){
		var alertPopup = $ionicPopup.alert({
		 title: 'Guardado',
		 template: 'Se ha registrado la información correctamente.',
		 okText: 'Aceptar',
		 okType: 'button-assertive'
	   });

	   alertPopup.then(function(res) {
		 $state.go('app.registroProspecto.proforma',{init: '0'});
	   });
	};


})
/**********************************************************************/
/*INICIO INTEGRACION EC*/
/**
 * @issue JIRAV3-005
 *  - inject ROL
 *  - mostrar en modo de solo lectura para gerentes
 *  @issue JIRAV3-006
 *  - mostrar alerta y bloquear, cuando el prospecto está bloquedo para ingreso de venta y registro de prospforma, por restricciones de grupo de fuerzas de venta
 *  @issue JIRAV3-007
 *  - agregar información de detalle de proforma para contrato a tempContrato
 *  @feature V4-HU005
 *  - la validación del documento no es bloqueante
 *  @feature V4-HU003
 *  - verificación de validación de prospecto por parte de prospecto
 */
    .controller('detalleClienteCtrl',function(validacionService, $filter,$scope,$rootScope,$state,$ionicActionSheet,tempContrato,shareData,tempProspecto,$http,ListaGeneralService,listaGeneral,blockUI,BACK_END_URL,tempProforma,tempProformaDetalle,prospectoResult,ProformaDetailService,PersonaService,DATOS_RUC,OBLIGACION_PAGO,proformaService, loginService, $ionicPopup, USUARIO, prospectoService,contratoService, debitoAutomaticoService,OBLIGACION_PAGO, APP, ROL){
/*FIN INTEGRACION EC*/

	$scope.me = $rootScope.me;
	/* inicio @issue JIRAV3-005 */
    $scope.modoSoloLectura = $scope.me && $scope.me.rolID === ROL.TIPO.GERENTE_PRODUCTO;
    /* fin @issue JIRAV3-005 */
	$scope.limpiarListas = function(){
		$rootScope.tempLstProgramaProducto = undefined;
		$rootScope.tempLstGrupo = undefined;
		$rootScope.tempLstCertificado = undefined;
		$rootScope.tempLstMarca = undefined;
		$rootScope.tempLstModelo = undefined;
		$rootScope.tempLstTipoBien = undefined;
		$rootScope.dataMarcas = false;
		$rootScope.porcentajeCuotaAdmin = undefined;
	};
	$scope.limpiarListas();
	$scope.prospecto = prospectoResult;
	var vm = this;
	vm.inEdition = false;
	vm.mapTipoDocumento = shareData.mapTipoDocumento;
	vm.lstTipoDocumento = shareData.lstTipoDocumento;
	vm.TIPO_DOCUMENTO = {
		RUC: '8',
		DNI: '4',
		PAS: '3',
		CE: '2'
	};			

	$scope.prospecto.proformas = proformaService.ajustarProformas($scope.prospecto.proformas);

	vm.client = $scope.prospecto;
	var OPTIONS = {
		CONSULTA_PAGO: 1,
		EDITAR_PROFORMA: 2,
		PAGO_A_CUENTA: 3,
		INGRESAR_VENTA: 4,
		ENVIAR_PROFORMA: 5,
		REENVIAR_OBLIGACION_PAGO: 6,
		REENVIAR_CONFIRMACION_PAGO: 7,
		//INICIO VTADIG2-18 John Velasquez 03-10-2017
		ADJUNTAR_ARCHIVOS: 8,
		//FIN VTADIG2-18 John Velasquez 03-10-2017
		DEBITO_AUTOMATICO_VISANET: 9
	}
	$scope.proformaOptions = [
		{codigo: OPTIONS.CONSULTA_PAGO, 				text: '<b><i class="icon ion-information-circled"></i>Consultar pagos</b>', 						mostrar: true },
		{codigo: OPTIONS.EDITAR_PROFORMA, 				text: '<b><i class="icon ion-document-text"></i>Editar Proforma</b>', 								mostrar: true },
		{codigo: OPTIONS.PAGO_A_CUENTA, 				text: '<b><i class="icon ion-card"></i>Pago a cuenta</b>', 											mostrar: true },
		{codigo: OPTIONS.DEBITO_AUTOMATICO_VISANET, 	text: '<b><i class="icon ion-android-mail"></i>Débito automático VISANET</b>', 				        mostrar: true },
		{codigo: OPTIONS.INGRESAR_VENTA, 				text: '<b class="assertive"><i class="icon ion-document-text assertive"></i>Ingresar venta</b>', 	mostrar: true },
		{codigo: OPTIONS.ENVIAR_PROFORMA, 				text: '<b><i class="icon ion-android-mail"></i>Enviar proforma</b>', 								mostrar: true },
		//INICIO VTADIG2-18 John Velasquez 03-10-2017
		{codigo: OPTIONS.ADJUNTAR_ARCHIVOS,				text: '<b><i class="icon ion-document"></i>Adjuntar Archivos</b>',									mostrar: true } ,
		//FIN VTADIG2-18 John Velasquez 03-10-2017
		{codigo: OPTIONS.REENVIAR_OBLIGACION_PAGO, 		text: '<b><i class="icon ion-android-mail"></i>Re-enviar obligación generada</b>', 					mostrar: true },   
	 	{codigo: OPTIONS.REENVIAR_CONFIRMACION_PAGO, 	text: '<b><i class="icon ion-android-mail"></i>Re-enviar confirmación de pago</b>', 				mostrar: true }
	];	
	$scope.optionsMap={};
	angular.forEach($scope.proformaOptions, function(option){
		$scope.optionsMap[option.codigo] = option;
	})

	if($scope.prospecto.validado!== undefined && $scope.prospecto.validado){
		vm.client.isValidated = true;
	}

    /*inicio @issue VTADIG2-CHECK*/
    if($scope.prospecto.tipoDocumento!=="4"){
        vm.client.isValidated = true;
    }
    /*fin @issue VTADIG2-CHECK*/

	/*inicio @issue EVALCRED-57*/
	/*INICIO - modificado por John Velasquez - VTAOBS1-55 - 28.09.2017*/
	//if($scope.prospecto.tipoDocumento!=="4"){

	//if($scope.prospecto.tipoDocumento!=="4"&&$scope.prospecto.tipoDocumento!=="8"){
	/*FIN - modificado por John Velasquez - VTAOBS1-55 - 28.09.2017*/
	//	vm.client.isValidated = true;
	//}
	/*fin @issue EVALCRED-57*/

	blockUI.stop();
	vm.validateDocument = function(){
		vm.client.isValidated = true;
	};

	vm.consultarPagos = function(proforma){
		tempProspecto.proforma = proforma;
		tempProspecto.prospectoID = $scope.prospecto.prospectoID;
		$state.go('app.detallePago');
	};
	vm.editarProforma = function(proformaID){
		ProformaDetailService.obtenerDetalleProforma(proformaID)
		.then(function(data){      
			tempProforma.proforma = data;
			tempProforma.proforma.proformaPadreID = data.proformaID; 						
			var puntoExhibicionSeleccionado = $filter('filter')(shareData.lstPuntoDeExhibicion, {codigo : parseInt(tempProforma.proforma.perPuntoExhibicion.puntoExhibicionID+'')}, true)[0];	
			tempProforma.proforma.perPuntoExhibicion =  puntoExhibicionSeleccionado;
			var tipoDocumentoSeleccionado = $filter('filter')(shareData.lstTipoDocumento, {codigo : tempProforma.proforma.vtaProspecto.tipoDocumentoID}, true)[0];	
			tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = tipoDocumentoSeleccionado;
			if(!tempProforma.proforma.vtaProspecto.personaID && 
			tempProforma.proforma.vtaProspecto.fuenteInformacion !==3 ){ //PROSPECTO SE PUEDE MODIFICAR
				tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
			}
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;
			tempProforma.proforma.vtaProspecto.esEdicion = true;
			tempProforma.proformaCopia = angular.copy(tempProforma.proforma);
			$state.go('app.registroProspecto.proforma',{init: '1'});
		});
	}
	vm.crearObligacion = function(proformaID){
		if($scope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA &&$scope.prospecto.personaEsEmpleado){						
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El cliente labora en Pandero, realizar pagos por oficina." , okType: 'button-assertive'});
			return false;
		}	       			
		blockUI.start();
		loginService.verificaUsuarioActivo()
		.then(
			function(valido){
				if(valido){
					/*INICIO INTEGRACION EC*/
					//ProformaDetailService.obtenerDetalleProformaContrato(proformaID)
					ProformaDetailService.obtenerDetalleProformaContrato(proformaID,APP.MODULO.PAGO_A_CUENTA.ID)
					/*FIN INTEGRACION EC*/
					.then(function(data){         
						blockUI.stop();
						/*INICIO INTEGRACION EC*/
						if(data.evaluacionCrediticia&&data.evaluacionCrediticia.aprobado==false){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.evaluacionCrediticia.mensaje , okType: 'button-assertive'});
							return;
						}
						/*FIN INTEGRACION EC*/
						if(data.prospectoBean.errorServicio !== undefined && data.prospectoBean.errorServicio === 1){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'En estos momentos el servicio PIDE no se encuentra activo.' , okType: 'button-assertive'});	
						}
						if($scope.prospecto){
							var conforme = true;	
							if($scope.prospecto.personaOFAC){
								//alert("El prospecto se encuentra en la lista OFAC");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});
								proformaService.enviarCorreoOFAC($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,$scope.prospecto.personaNombreCompletoOFAC);
								conforme = false;
							}else if($scope.prospecto.personaContratosResueltos){
								//alert("El prospecto tiene contratos resueltos");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos resueltos." , okType: 'button-assertive'});
								proformaService.enviarCorreoContratosResueltos($scope.prospecto.tipoDocumento, $scope.prospecto.numeroDocumento, tempProspecto.prospecto.prospectoBean.nombreCompleto);
								conforme = false;
							}else if($scope.prospecto.personaContratosLegal){
								//alert("El prospecto tiene contratos en legal");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos en legal." , okType: 'button-assertive'});
								proformaService.enviarCorreoContratosLegal($scope.prospecto.tipoDocumento, $scope.prospecto.numeroDocumento, tempProspecto.prospecto.prospectoBean.nombreCompleto);
								conforme = false;
							}
							if(conforme){								
								tempProspecto.prospecto={};
								tempProspecto.prospecto=data;						
								$state.go('app.crearObligacion');
							}
						}
					}, function errorCallback(response) {
						blockUI.stop();
					});
				}else{
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El usuario no está activo, contactarse con su supervisor." , okType: 'button-assertive'});	       						
				}
			}
		);
	}

	/**
	 * @feature V4-HU005
	 * - no se valida si el documento está validado
	 */
	vm.validarVentaDigital = function(){
		if($scope.me.documentoDigital){
			/** inicio @feature V4-HU005 **/
			/*
			if($scope.prospecto.personaTipoID == 'N' && $scope.prospecto.tipoDocumento == "4"){	
				if(!$scope.prospecto.personaDocumentoValidado){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El DNI del prospecto no ha sido validado por el scanner. Para realizar una venta digital es necesario escanear el DNI", okType: 'button-assertive'});
					return false;	
				}
			}
			*/
			/** fin @feature V4-HU005 **/
		}
	}
        /**
         * @param proformaID
         * @returns {boolean}
         * @issue JIRAV3-006
         *  - bloquer ingreso de venta cuando el prospecto esta bloqueado por restricciones de ingreso de venta de grupos de ingreso de venta
		 * @feature V4-HU005
		 *  - no se valida si el documento está validado
		 */
	vm.ingresaVenta = function(proformaID){

		/*Validación del escaner*/ 
		if($scope.me.documentoDigital){
			/** inicio @feature V4-HU005 **/
			/*
			if($scope.prospecto.personaTipoID == 'N' && $scope.prospecto.tipoDocumento == "4"){	
				if(!$scope.prospecto.validado){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El DNI del prospecto no ha sido validado por el scanner. Para realizar una venta digital es necesario escanear el DNI", okType: 'button-assertive'});
					return false;	
				}
			}
			 */
			/** fin @feature V4-HU005 **/ 
		}

		if($scope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA&&$scope.prospecto.personaEsEmpleado){						
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El cliente labora en Pandero, debe ingresar la venta por oficina." , okType: 'button-assertive'});
			return false;
		}
        /* inicio @issue JIRAV3-006 */
		if($scope.prospecto.restriccionesPorGrupoFuerzaVentaIngresoVenta && $scope.prospecto.restriccionesPorGrupoFuerzaVentaIngresoVenta.length > 0){
            $ionicPopup.alert({ title: 'Alerta', template: $scope.prospecto.restriccionesPorGrupoFuerzaVentaIngresoVenta[0].mensaje , okType: 'button-assertive'});
            return false;
        }
        /* fin @issue JIRAV3-006 */
        /**
		 *  @issue JIRAV3-007
         *  - agregar información de detalle de proforma para contrato a tempContrato
		 *  @feature V4-HU003
		 *  - veriricar si la proforma requiere de verificación por parte del prospecto
         */
		loginService.verificaUsuarioActivo()
		.then(
			function(valido){
				if(valido){
					/*INICIO INTEGRACION EC*/
					//ProformaDetailService.obtenerDetalleProformaContrato(proformaID)
					ProformaDetailService.obtenerDetalleProformaContrato(proformaID,APP.MODULO.INGRESO_DE_VENTA.ID)
					/*FIN INTEGRACION EC*/
					.then(function successCallback(data) {
						/** inicnio @ feature V4-HU003 **/
						if(vm.proformaRequiereValidacion(data)){
							vm.mostrarOpcionesDeValidacion(data);
							return;
						}
						/** fin @feature V4-HU003 **/
						/*INICIO INTEGRACION EC*/
						if(data.evaluacionCrediticia&&data.evaluacionCrediticia.aprobado==false){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.evaluacionCrediticia.mensaje , okType: 'button-assertive'});
							return;
						}
						/*FIN INTEGRACION EC*/
						if(data.prospectoBean!== null && data.prospectoBean !== undefined){
							if(!data.prospectoBean.personaGrupoEconomico||data.prospectoBean.personaGrupoEconomico===null){data.prospectoBean.personaGrupoEconomico = false;}
							if(!data.prospectoBean.personaSujetoObligado||data.prospectoBean.personaSujetoObligado===null){data.prospectoBean.personaSujetoObligado = false;}
							if(!data.prospectoBean.personaPrevLavadoActivos||data.prospectoBean.personaPrevLavadoActivos===null){data.prospectoBean.personaPrevLavadoActivos = false;}
							if(!data.prospectoBean.personaActividadMinera||data.prospectoBean.personaActividadMinera===null){data.prospectoBean.personaActividadMinera = false;}
							if(data.prospectoBean.errorServicio !== undefined && data.prospectoBean.errorServicio === 1){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'En estos momentos el servicio PIDE no se encuentra activo.' , okType: 'button-assertive'});	
							}
				
						}
						//console.log(data.prospectoBean); 
						tempProspecto.prospecto = data;
						
						var tempProforma = $scope.prospecto.proformas.find(function(prof){return prof.proformaID===proformaID});
						if(tempProforma.nmSeparationPct>=30){
							//console.log("mayor al 30%");
							tempProspecto.prospecto.cuotaTotalMensualPagado = tempProforma.nmSeparation;
							tempContrato.contrato.cuotaTotalMensual = tempProforma.certificadoBean.cuotaMensual;						
						}else{
							//console.log("menor al 30%");
						}
						/** INICIO ticket-44914 Anderson Estela Coronel 29-oct-2020 */
						if(data.code === 46){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.message , okType: 'button-assertive'});
						}
						/** FIN ticket-44914 Anderson Estela Coronel 29-oct-2020 */

						tempContrato.titulares = [];
						/* inicio @issue JIRAV3-007 */
						tempContrato.proforma = data;
						/* fin @issue JIRAV3-007 */
						if($scope.prospecto){
							var conforme = true;
							if($scope.prospecto.personaOFAC){
								//alert("El prospecto se encuentra en la lista OFAC");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});
								proformaService.enviarCorreoOFAC($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,$scope.prospecto.personaNombreCompletoOFAC);
								conforme = false;
							}else if($scope.prospecto.personaContratosResueltos){
								//alert("El prospecto tiene contratos resueltos");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos resueltos." , okType: 'button-assertive'});
								proformaService.enviarCorreoContratosResueltos($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								conforme = false;
							}else if($scope.prospecto.personaContratosLegal){
								//alert("El prospecto tiene contratos en legal");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos en legal." , okType: 'button-assertive'});
								proformaService.enviarCorreoContratosLegal($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								conforme = false;
							}
							/*INICIO INTEGRACION EC*/
							else if($scope.prospecto.empresasConContratosLegales||$scope.prospecto.empresasConContratosResueltos){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No es posible ingresar venta a la persona, debido a que cuenta con contrato(s) en situación legal o resueltos con Pandero." , okType: 'button-assertive'});
								if($scope.prospecto.empresasConContratosResueltos){
									//proformaService.enviarCorreoContratosResueltos($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								}
								if($scope.prospecto.empresasConContratosLegales){
									//proformaService.enviarCorreoContratosLegal($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								}
								conforme = false;
							}
							/*FIN INTEGRACION EC*/
							/***************************************************/
							/**INICIO VTADIG2-35 - JOHN VELASQUEZ - 21/11/2017**/
							/***************************************************/
							else if($scope.prospecto.personaMultasElectorales!=undefined&&
									$scope.prospecto.personaMultasElectorales!=null&&
									$scope.prospecto.personaMultasElectorales==true){
								//alert("El prospecto tiene contratos en legal");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene multas electorales." , okType: 'button-assertive'});								
								conforme = false;
							}
							/***************************************************/
							/***FIN VTADIG2-35 - JOHN VELASQUEZ - 21/11/2017****/
							/***************************************************/
							if(conforme){
								tempProspecto.prospecto.prospectoBean.personaEsEmpleado = $scope.prospecto.personaEsEmpleado;
								$scope.limpiarListas();
								$state.go('app.generarContrato.datosContrato',{previous : 0});
							}else{
								$scope.limpiarListas();
							}
						}else{
							tempProspecto.prospecto.prospectoBean.personaEsEmpleado = false;
							$scope.limpiarListas();
							$state.go('app.generarContrato.datosContrato',{previous : 0});
						}
						//console.log(tempProspecto.prospecto.prospectoBean);    
						
					}, function errorCallback(response) {
						$scope.limpiarListas();
					});
				}else{
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El usuario no está activo, contactarse con su supervisor." , okType: 'button-assertive'});	       						
				}
			}
		);
	};
	/**
	 * @feature V4-HU003
	 * @param proforma
	 */
	vm.mostrarOpcionesDeValidacion = function (proforma) {
		proformaService.obtenerMediosDeValidacion().then(
			(medios)=>{
				const options = medios.map((medio)=>{
					return {id: medio.id, text: `<b><i class="icon ${medio.icon}"></i>${medio.descripcion}</b>` };
				});
				$ionicActionSheet.show({
					buttons: options,
					titleText: 'Opciones de Conformidad',
					cancelText: 'Cancelar',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						let medio = medios.find(m => m.id === options[index].id);
						vm.enviarSolicitudValidacionProforma(medio, proforma);
						return true;
					}
				});
			}
		)
	};
	/**
	 * @feature V4-HU003
	 * @param medio
	 * @param proforma
	 */
	vm.enviarSolicitudValidacionProforma = function(medio, proforma) {
		proformaService.enviarValidacionProforma(medio.id, proforma.proformaID).then(
			(data) => {
				console.log(data);
				let alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Mensaje enviado con éxito." , okType: 'button-assertive'});
				alertPopup.then(()=>{
					if(medio.permiteIngresoCodigo === true) {
						vm.mostrarInputCodigoDeConformidad(proforma, data);
					}
				})
			},
			(error) => console.log(error)
		)
	};
	/**
	 * @feature V4-HU003
	 * @param proforma
	 * @param validacion
	 */
	vm.mostrarInputCodigoDeConformidad = function(proforma, validacion){
		vm.validacion = validacion;
		let inputCodigoPopup = $ionicPopup.show({
			title: 'Confirmación proforma',
			subTitle: `proforma ${proforma.proformaID}`,
			scope: $scope,
			template: '<input type = "text" placeholder="Código de confirmación" ng-model = "vm.validacion.codigo">',
			buttons: [
				{text: 'Cancelar'},
				{
					text: '<b>Aceptar</b>',
					type: 'button-positive',
					onTap: function (e) {
						if (!vm.validacion.codigo) {
							//don't allow the user to close unless he enters model...
							e.preventDefault();
						} else {
							return vm.validacion.codigo;
						}
					}
				}
			]
		});
		inputCodigoPopup
		.then(function(res) {
			if(res) {
				vm.validarProforma(proforma, validacion);
			}
		});
	};
	/**
	 * @feature V4-HU003
	 * @param proforma
	 * @param validacion
	 */
	vm.validarProforma = function (proforma, validacion){
		proformaService.validarProforma(proforma, validacion).then(
			(data) => {
				vm.ingresaVenta(proforma.proformaID);
			},
			(error) => {
				$ionicPopup.alert({ title: 'Alerta', template: error , okType: 'button-assertive'});
			}
		);
	};
	/**
	 * @feature V4-HU003
	 * @param proforma
	 * @returns {boolean}
	 */
	vm.proformaRequiereValidacion = function(proforma) {
		return proforma.requiereValidacion === true && proforma.validada !== true
	};
	vm.enviarProforma = function(proformaID){
		proformaService.enviarCorreoProforma(proformaID)
		.then(function(data){         
			blockUI.stop();
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La proforma fue enviada con éxito." , okType: 'button-assertive'});
		}, function errorCallback(response) {
			blockUI.stop();
		}); 
	}
	vm.enviarCorreoConfirmacionPago = function(proformaID){
		contratoService.enviarCorreoConfirmacionPago(proformaID).then(function(data){
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El correo de confirmación de pago fue enviada con éxito." , okType: 'button-assertive'});
		});
	}
	vm.enviarCorreoObligacionPago = function(proformaID){
		contratoService.enviarCorreoObligacionPago(proformaID).then(function(data){
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El correo de obligación de pago fue enviada con éxito." , okType: 'button-assertive'});
		});
	}
	vm.enviarCorreoDebitoAutomaticoVisanet = function(contratoId) {
		debitoAutomaticoService.enviarFormularioVisaNet(contratoId).then(function(data){
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.message , okType: 'button-assertive'});
		});
	}
	//INICIO VTADIG2-18 John Velasquez 03-10-2017
	vm.irAdjuntarArchivos = function(contratoId){
		$state.go('app.documentoContrato',{contratoId : contratoId});
	}
	//FIN VTADIG2-18 John Velasquez 03-10-2017
	vm.showOptions = function(indice,proforma) {	
		var proformaID = proforma.proformaID;
		var contratoSafId = proforma.contratoSafId;
		var contratoId = proforma.contratoId;
		var puedeAdjuntarArchivos = proforma.puedeAdjuntarArchivos;
		var sizePagos = proforma.lstObligacionPagoBean.length;
		
		if(!proforma.lstObligacionPagoBean){
			proforma.lstObligacionPagoBean = [];
		}
		/*evaluar mostrar opciones*/		
		$scope.proformaOptions.forEach(function(option){
			option.mostrar = true;
		});
		if(contratoId!==0) {
			$scope.optionsMap[OPTIONS.EDITAR_PROFORMA].mostrar = false;
			$scope.optionsMap[OPTIONS.PAGO_A_CUENTA].mostrar = false;
			$scope.optionsMap[OPTIONS.INGRESAR_VENTA].mostrar = false;
			$scope.optionsMap[OPTIONS.DEBITO_AUTOMATICO_VISANET].mostrar = !proforma.tieneDebito && $scope.me.indDebitoAutomaticoVisanet === true && proforma.indDebitoAutomaticoVisanet === true;
		} else {
			$scope.optionsMap[OPTIONS.DEBITO_AUTOMATICO_VISANET].mostrar = false;
		}
		if(sizePagos===0){
			$scope.optionsMap[OPTIONS.CONSULTA_PAGO].mostrar = false;
		}		

		//si tiene obligaciones pendientes
		var lstObligacionesPagadas =  proforma.lstObligacionPagoBean.filter(function(obligacion){
			return obligacion.lngEstadoObligacion == OBLIGACION_PAGO.ESTADO.PAGADO;
		});
		var lstObligacionesPendientes =  proforma.lstObligacionPagoBean.filter(function(obligacion){
			return obligacion.lngEstadoObligacion == OBLIGACION_PAGO.ESTADO.REGISTRADO;
		});
		if(!lstObligacionesPagadas){
			lstObligacionesPagadas = [];
		}
		if(!lstObligacionesPendientes){
			lstObligacionesPendientes = [];
		}
		if(lstObligacionesPagadas.length<=0){
			$scope.optionsMap[OPTIONS.REENVIAR_CONFIRMACION_PAGO].mostrar = false;
		}
		if(lstObligacionesPendientes.length<=0){
			$scope.optionsMap[OPTIONS.REENVIAR_OBLIGACION_PAGO].mostrar = false;
		}
		//INICIO VTADIG2-18 John Velasquez 03-10-2017									
			if(!puedeAdjuntarArchivos){
				$scope.optionsMap[OPTIONS.ADJUNTAR_ARCHIVOS].mostrar = false;
			}
		//FIN VTADIG2-18 John Velasquez 03-10-2017
		$scope.buttons = $scope.proformaOptions.filter(function(option){return option.mostrar;});
	   	var hideSheet = $ionicActionSheet.show({
	    	buttons: $scope.buttons,
	     	titleText: 'Opciones',
	     	cancelText: 'Cancelar',
	     	cancel: function() {
	          // add cancel code..
	        	},
	     	buttonClicked: function(index) {
	     		/*if(buttons.length==2 && index > 0){
	     			index = 4;
	     		}
	     		if(buttons.length==4 ){
	     			index = index + 1;
				 }*/
				switch($scope.buttons[index].codigo){
					case OPTIONS.CONSULTA_PAGO:
						vm.consultarPagos($scope.prospecto.proformas[indice]);
					break;
					case OPTIONS.EDITAR_PROFORMA:
						vm.editarProforma(proformaID);
					break;
					case OPTIONS.PAGO_A_CUENTA:
						vm.crearObligacion(proformaID)
					break;
					case OPTIONS.INGRESAR_VENTA:
						vm.ingresaVenta(proformaID);
					break;
					case OPTIONS.ENVIAR_PROFORMA:
						vm.enviarProforma(proformaID);
					break;
					//INICIO VTADIG2-18 John Velasquez 03-10-2017
					case OPTIONS.ADJUNTAR_ARCHIVOS:
						vm.irAdjuntarArchivos(contratoSafId);
					break;
					//FIN VTADIG2-18 John Velasquez 03-10-2017
					case OPTIONS.REENVIAR_CONFIRMACION_PAGO:
						vm.enviarCorreoConfirmacionPago(proformaID);
					break;
					case OPTIONS.REENVIAR_OBLIGACION_PAGO:
						vm.enviarCorreoObligacionPago(proformaID);
					break;
					case OPTIONS.DEBITO_AUTOMATICO_VISANET:
						vm.enviarCorreoDebitoAutomaticoVisanet(contratoSafId);
						break;
				}
				return true;
			}
	   });
	 };

	 vm.regresar=function(){
		if(vm.inEdition){
			vm.inEdition=false;	
		}else{
			$state.go('app.consultaCliente');
		}
		

	 };
        /**
         *
         * @param tipoDocumento
         * @param nroDocumento
         * @returns {boolean}
         * @issue JIRAV3-006
         *  - bloquer registro de proforma cuando el prospesto esta bloqueado por restricciones de registro proforma de grupos de fuerza de venta
         */
	$scope.buscarProspecto = function(tipoDocumento, nroDocumento){
        /* inicio @issue JIRAV3-006 */
	    if($scope.prospecto.restriccionesPorGrupoFuerzaVentaRegistroProforma && $scope.prospecto.restriccionesPorGrupoFuerzaVentaRegistroProforma.length > 0){
            $ionicPopup.alert({ title: 'Alerta', template: $scope.prospecto.restriccionesPorGrupoFuerzaVentaRegistroProforma[0].mensaje , okType: 'button-assertive'});
            return false;
        }
        /* fin @issue JIRAV3-006 */
		proformaService.getProspectoPorDocumento(tipoDocumento,nroDocumento)
		.then(function(data){	
			if(data.errorServicio === 2){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});						
				prospectoService.envioCorreoOFAC(data.personaNombreCompletoOFAC,shareData.mapTipoDocumento[data.tipoDocumentoID].descripcion, data.personaCodigoDocumento);
				return;
			}
			tempProforma.proforma = {};
			tempProforma.proforma.vtaProspecto = data;	
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;
			tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
			tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled = false;
			tempProforma.proforma.vtaProspecto.esEdicion = true;
			tempProforma.proformaCopia = angular.copy(tempProforma.proforma);
			tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[tipoDocumento];
			$state.go('app.registroProspecto.proforma');
			
		});
	};

})
.controller('registroProspectoCtrl', function($scope, $state) {

})
/**
 * @feature V4-HU012
 */
.controller('reporteComisionesCtrl', function($scope, $sessionStorage, $ionicPopup, $sce, validacionService, reporteComisionService, BACK_END_URL){
	$scope.meses = [
		{id: 1, descripcion: "Enero"},
		{id: 2, descripcion: "Febrero"},
		{id: 3, descripcion: "Marzo"},
		{id: 4, descripcion: "Abril"},
		{id: 5, descripcion: "Mayo"},
		{id: 6, descripcion: "Junio"},
		{id: 7, descripcion: "Julio"},
		{id: 8, descripcion: "Agosto"},
		{id: 9, descripcion: "Setiembre"},
		{id: 10, descripcion: "Octubre"},
		{id: 11, descripcion: "Noviembre"},
		{id: 12, descripcion: "Diciembre"}
	];
	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	};
	$scope.token = $sessionStorage.token;
	$scope.pdfUrl = $scope.trustSrc(`${BACK_END_URL}reporte/comision/pdf?appVersion=viuvbiueir898934nngl2lgoe929m93cm348nv294&access_token=${$scope.token}&ano=2019&mes=7`);
	
	$scope.formData = {
		ano: (new Date).getFullYear(),
		mes: (new Date).getMonth() + 1
	};
	
	$scope.formatAno = () => {
		if($scope.formData.ano == null || $scope.formData.ano == undefined){
			return;
		}
		if($scope.formData.ano > 9999){
			$scope.formData.ano = 9999;
		}
		let tmpAno = Math.floor(Math.abs($scope.formData.ano));
		if (!isNaN(tmpAno)) {
			$scope.formData.ano = tmpAno;	
		} else {
			$scope.formData.ano = (new Date).getFullYear();
		}
	};
	
	$scope.descargar = () => {
		if($scope.formData.ano == null || $scope.formData.ano == undefined){
			$ionicPopup.alert({
				title: 'Alerta',
				template: "Ingrese el año",
				okText: 'Aceptar',
				okType: 'button-assertive'
			});
			return;
		}
		reporteComisionService.descargarPdf($scope.formData.ano, $scope.formData.mes);
	}
})
.controller('seguimientoClienteCtrl', function (validacionService, generadorDeMapas,$scope, $state, $ionicPopover,$ionicModal,prospectoResult, shareData, departamento, BACK_END_URL, $http, proformasResult,seguimientoService, $ionicPopup,blockUI, $ionicNavBarDelegate, plantillas, $sce,TIPO_PLANTILLA,$filter,configArchivosPlantilla,$sessionStorage) {
	console.log();
	var vm = this;
	$scope.isEdition = true;
	$scope.configArchivosPlantilla = configArchivosPlantilla;
	$scope.lstPlantillas = plantillas.filter(function(el){
			el.llaveView = el.llave.substring(0,el.llave.indexOf("_"));
			return el.llave === el.llave;
	});	

	vm.mapPlatillas = generadorDeMapas.generarMapaDesdeArreglo($scope.lstPlantillas,'llave');
	
	vm.newRegistry = {};
	$ionicNavBarDelegate.showBackButton(true);
	vm.saveRegistry = function (tipo,proformaID) {
        /*inicio @issue VTADIG2-418*/
		if(proformaID === '' && vm.newRegistry.correo){
            proformaID = undefined;
            vm.newRegistry.correo.vtaProforma = undefined;
		}
        /*fin @issue VTADIG2-418*/
		var tipoSeguimiento = vm.newRegistry.option.name;
		var seguimiento;

		if(vm.newRegistry.option.name === "meeting"){
			var validaCita =  $scope.validaCita();
			if(validaCita){
				vm.newRegistry.cita.tipoSeguimientoID = vm.newRegistry.option.id;
				vm.newRegistry.cita.vtaProspecto = {};
				vm.newRegistry.cita.vtaProspecto.prospectoID = vm.client.prospectoID;				
				if(!vm.newRegistry.cita.estado){
					vm.newRegistry.cita.estado = 0;
				}
				
				vm.newRegistry.cita.fecha = vm.newRegistry.cita.fechaDate.getDate() + '-' + (vm.newRegistry.cita.fechaDate.getMonth() + 1) + '-' + vm.newRegistry.cita.fechaDate.getFullYear() + ' ' + (vm.newRegistry.cita.hora.getHours() + 5 ) + ':' + vm.newRegistry.cita.hora.getMinutes() + ":00";
				seguimiento = vm.newRegistry.cita;
			}else{
				return false;
			}			
		}

		if(vm.newRegistry.option.name === "mail"){
			
			var validaCorreo =  $scope.validaCorreo();
			if(validaCorreo){
				if(tipo == "vistaPrevia"){
					var validaPlantilla = $scope.validaPlantillaPagoAndAsamblea(proformaID);
		
					if(!validaPlantilla){
						return false;
					}

					var dataArchivo = [];
					vm.newRegistry.correo.archivosAdjuntosBase64 =  vm.newRegistry.correo.archivosAdjuntosBase64 || [];
					vm.newRegistry.correo.archivosAdjuntosBase64.forEach(function(archivo,index){
						dataArchivo[index] = archivo.data;
						vm.newRegistry.correo.archivosAdjuntosBase64[index].data = undefined;
					});
					seguimientoService.obtenerVistaPrevia(vm.newRegistry.correo.plantillaId, vm.client.prospectoID, {
						mensajeCorreo: vm.newRegistry.correo.mensajeCorreo,
						fechaDePago: vm.newRegistry.correo.fechaDePago?vm.newRegistry.correo.fechaDePago.toLocaleDateString():undefined,
						asuntoCorreo: vm.newRegistry.correo.asuntoCorreo,
						proformaId: vm.newRegistry.correo.vtaProforma?vm.newRegistry.correo.vtaProforma.proformaID:undefined,
						archivosAdjuntos: vm.newRegistry.correo.archivosAdjuntosBase64,
						puntoExhibicion: $sessionStorage.defaults.puntoExhibicion.descripcion
					})
			  		.then(function(data){	 
						vm.newRegistry.correo.archivosAdjuntosBase64.forEach(function(archivo,index){
							vm.newRegistry.correo.archivosAdjuntosBase64[index].data = dataArchivo[index];
							dataArchivo[index] = undefined;
						});
						$scope.mailPreviewContent = $sce.trustAsHtml(data.plantilla);			  			
						vm.newRegistry.correo.asuntoCorreo = data.asunto
						vm.openMailPreview();
					},function(error){
						vm.newRegistry.correo.archivosAdjuntosBase64.forEach(function(archivo,index){
							vm.newRegistry.correo.archivosAdjuntosBase64[index].data = dataArchivo[index];
							dataArchivo[index] = undefined;
						});
					})
					.catch(function(ex){
						vm.newRegistry.correo.archivosAdjuntosBase64.forEach(function(archivo,index){
							vm.newRegistry.correo.archivosAdjuntosBase64[index].data = dataArchivo[index];
							dataArchivo[index] = undefined;
						});
					});						
					return false;
				}else{					
					vm.newRegistry.correo.tipoSeguimientoID = vm.newRegistry.option.id;
					vm.newRegistry.correo.vtaProspecto = {};
					vm.newRegistry.correo.vtaProspecto.prospectoID = vm.client.prospectoID;
					vm.newRegistry.correo.estado = 0;
					vm.newRegistry.correo.puntoExhibicion = $sessionStorage.defaults.puntoExhibicion.descripcion;
					seguimiento = vm.newRegistry.correo;
				}
			}else{
				return false;
			}
			
		}

		if(vm.newRegistry.option.name === "call"){
			var validaLlamada =  $scope.validaLlamada();
			if(validaLlamada){
				vm.newRegistry.llamada.tipoSeguimientoID = vm.newRegistry.option.id;
				vm.newRegistry.llamada.vtaProspecto = {};
				vm.newRegistry.llamada.vtaProspecto.prospectoID = vm.client.prospectoID;
				vm.newRegistry.llamada.estado = 0;
				vm.newRegistry.llamada.fecha = vm.newRegistry.llamada.fechaDate.getDate() + '-' + (vm.newRegistry.llamada.fechaDate.getMonth() + 1) + '-' + vm.newRegistry.llamada.fechaDate.getFullYear() + ' ' + (vm.newRegistry.llamada.hora.getHours() + 5) + ':' + vm.newRegistry.llamada.hora.getMinutes() + ":00";
				seguimiento = vm.newRegistry.llamada;
			}else{
				return false;
			}
		}

		if(!seguimiento.seguimientoID || (seguimiento.seguimientoID && vm.newRegistry.option.name === "meeting")){
			seguimientoService.registrarSeguimiento(seguimiento)
	  		.then(function(data){	 
	  			var alertPopup = $ionicPopup.alert({
					     title: 'Mensaje',
					     template: data.message,
					     okText: 'Aceptar',
					     okType: 'button-assertive'
					   });
				alertPopup.then(function(res) {
					blockUI.start();
					var optionName = vm.newRegistry.option.name;
					if(vm.newRegistry.llamada){
						var phoneNumber = vm.newRegistry.llamada.telefono;
					}					
					vm.newRegistry = {};
					vm.closeMailPreview();
					vm.closeNewRegistryModal();	
					if(optionName === "call"){
						window.plugins.CallNumber.callNumber(function(data){
							seguimientoService.obtenerDetalleSeguimiento(vm.client.prospectoID)
							.then(function(data){
								vm.history = data.seguimientos;
								$scope.fillSeguimientoData();
								$scope.isEdition = true;
								blockUI.stop();								          
							});
						}, function(){
							console.log("NO SE LLAMO");
						}, phoneNumber, false);
					}else{
						seguimientoService.obtenerDetalleSeguimiento(vm.client.prospectoID)
						.then(function(data){
							vm.history = data.seguimientos;
							$scope.fillSeguimientoData();
							$scope.isEdition = true;
							blockUI.stop();								          
						});
					}											
				});
	  		});				
		}else{
			vm.newRegistry = {};
			vm.closeNewRegistryModal();
			$scope.isEdition = true;	
		}

		

	};

	$scope.validaLlamada = function(){
		if(vm.newRegistry.llamada){
			vm.newRegistry.llamada.fechaDate = new Date();
			vm.newRegistry.llamada.hora = new Date();
			var limitTo = new Date();
			var limitFrom = new Date();
			if(vm.newRegistry.llamada.fechaDate){
				limitTo.setDate(limitTo.getDate() + 30 );
				limitTo.setHours(23,59,0);

				limitFrom.setDate(limitFrom.getDate() - 31 );
				limitFrom.setHours(23,59,0);
			}

			if(!vm.newRegistry.llamada.telefono){
				//alert("Debe seleccionar un número de teléfono.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar un número de teléfono." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.llamada.fechaDate){
				//alert("Debe ingresar la fecha.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la fecha." , okType: 'button-assertive'});
				return false;
			}/*else if(vm.newRegistry.llamada.fechaDate.getTime() > limitTo.getTime() || 
				vm.newRegistry.llamada.fechaDate.getTime() < limitFrom.getTime()){
				//alert("Debe ingresar una fecha no mayor ni menor a 30 días a partir de hoy.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar una fecha no mayor ni menor a 30 días a partir de hoy." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.llamada.hora){
				//alert("Debe ingresar la hora.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la hora." , okType: 'button-assertive'});
				return false;
			}*/
		}else{
			//alert("Debe seleccionar un número de teléfono.");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar un número de teléfono." , okType: 'button-assertive'});
			return false;
		}
		return true;
	};

	

	$scope.validaPlantillaPagoAndAsamblea = function(proformaID){
		
		if(vm.plantillaActual.alertaID == TIPO_PLANTILLA.PAGO_CUOTA || vm.plantillaActual.alertaID == TIPO_PLANTILLA.RECORDATORIO_ASAMBLEA){
			var proforma;
			var mensaje;
			if(vm.plantillaActual.alertaID == TIPO_PLANTILLA.PAGO_CUOTA){
				proforma = $filter('filter')($scope.lstProformas, {obligacionContrato : true,value : parseInt(proformaID)})[0];
				mensaje = "La proforma seleccionada no tiene un pago a cuenta o la venta ya fue ingresada"; 
			} else if (vm.plantillaActual.alertaID == TIPO_PLANTILLA.RECORDATORIO_ASAMBLEA) {
				proforma = $filter('filter')($scope.lstProformas, {obligacionContrato : false,value : parseInt(proformaID)})[0];
				mensaje = "La proforma seleccionada no cuenta con una venta ingresada";
			}

			if(proforma){
				return true;
			} else {
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: mensaje , okType: 'button-assertive'});
				return false;
			}
		} else {
			return true;
		}	
	}

	$scope.validaCorreo = function(){
		var currentDate = new Date();
		currentDate.setHours(0,0,0,0);
		var correoInvalido = false;
		if(vm.newRegistry.correo.correo){
			var correos = vm.newRegistry.correo.correo.split(",");
			for(var item in correos){
				if(!validacionService.validarCorreElectronico(correos[item].trim())){
					correoInvalido = true;
					break;
				}
			} 
		}

		if(vm.newRegistry.correo){
			if(!vm.newRegistry.correo.correo){
				//alert("Debe seleccionar un correo.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar al menos un correo." , okType: 'button-assertive'});
				return false;
			}else if(correoInvalido){
				//alert("Debe seleccionar un correo.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El/Los correo(s) deben tener el formato user@domain.com y deben estar separados por comas." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.correo.plantillaId){
				//alert("Debe seleccionar un correo.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar una plantilla." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.correo.asuntoCorreo){
				//alert("Debe ingresar un asunto.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar un asunto." , okType: 'button-assertive'});
				return false;
			}else if(vm.mostrarMensajeAdicional&&!vm.newRegistry.correo.mensajeCorreo){
				//alert("Debe ingresar la fecha.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar un mensaje." , okType: 'button-assertive'});
				return false;
			}else if(vm.proformaObligatoria && (vm.newRegistry.correo.vtaProforma===null||vm.newRegistry.correo.vtaProforma===undefined)){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La plantilla requiere que seleccione "+(vm.plantillaActual.platillaLabelInputProforma?vm.plantillaActual.platillaLabelInputProforma:'Proforma')+"." , okType: 'button-assertive'});
				return false;
			}else if(vm.proformaObligatoria && (vm.newRegistry.correo.vtaProforma.proformaID===null||vm.newRegistry.correo.vtaProforma.proformaID===undefined||vm.newRegistry.correo.vtaProforma.proformaID==="")){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La plantilla requiere que seleccione "+(vm.plantillaActual.platillaLabelInputProforma?vm.plantillaActual.platillaLabelInputProforma:'Proforma')+"." , okType: 'button-assertive'});
				return false;
			}else if(vm.fechaDePagoObligatoria && (vm.newRegistry.correo.fechaDePago===null||vm.newRegistry.correo.fechaDePago===undefined||vm.newRegistry.correo.fechaDePago==="")){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La plantilla requiere que ingrese la "+(vm.plantillaActual.plantillaLabelInputFechaDePago?vm.plantillaActual.plantillaLabelInputFechaDePago:'Fecha de pago')+"." , okType: 'button-assertive'});
				return false;
			}else if (vm.fechaDePagoObligatoria &&vm.newRegistry.correo.fechaDePago < currentDate){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La plantilla requiere que ingrese una "+(vm.plantillaActual.plantillaLabelInputFechaDePago?vm.plantillaActual.plantillaLabelInputFechaDePago:'Fecha de pago')+" válida." , okType: 'button-assertive'});
				return false;
			}else if(vm.mostrarArchivosAdjuntos && (vm.newRegistry.correo.archivosAdjuntosBase64===null||vm.newRegistry.correo.archivosAdjuntosBase64===undefined||vm.newRegistry.correo.archivosAdjuntosBase64.length===0)){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La plantilla requiere que ingrese "+(vm.plantillaActual.plantillaLabelInputArchivosAdjuntos?vm.plantillaActual.plantillaLabelInputArchivosAdjuntos:'Archivos adjuntos')+"." , okType: 'button-assertive'});
				return false;
			}
		}else{
			//alert("Debe seleccionar un correo.");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar un correo." , okType: 'button-assertive'});
			return false;
		}
		return true;
	};

	$scope.validaCita = function(){
		var currentDate = new Date();
		currentDate.setHours(0,0,0,0);

		var limitTo = new Date();
		var limitFrom = new Date();
		if(vm.newRegistry.cita !== undefined &&  vm.newRegistry.cita.fechaDate){
			limitTo.setDate(limitTo.getDate() + 30 );
			limitTo.setHours(23,59,0);

			limitFrom.setDate(limitFrom.getDate() - 31 );
			limitFrom.setHours(23,59,0);
		}

		if(vm.newRegistry.cita){
			if(!vm.newRegistry.cita.fechaDate){
				//alert("Debe ingresar la fecha.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la fecha." , okType: 'button-assertive'});
				return false;
			} else if ( vm.newRegistry.cita.fechaDate < currentDate ){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar una fecha mayor o igual al día de hoy." , okType: 'button-assertive'});
				return false;
			} else if(vm.newRegistry.cita.fechaDate.getTime() > limitTo.getTime() || 
				vm.newRegistry.cita.fechaDate.getTime() < limitFrom.getTime()){
				//alert("Debe ingresar una fecha no mayor ni menor a 30 días a partir de hoy.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar una fecha no mayor ni menor a 30 días a partir de hoy." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.cita.hora){
				//alert("Debe ingresar la hora.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la hora." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.cita.lugarCita){
				//alert("Debe ingresar un lugar.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar un lugar." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.cita.departamentoID){
				//alert("Debe seleccionar un departamento.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar un departamento." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.cita.provinciaID){
				//alert("Debe seleccionar una provincia.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar una provincia." , okType: 'button-assertive'});
				return false;
			}else if(!vm.newRegistry.cita.distritoID){
				//alert("Debe seleccionar un distrito.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe seleccionar un distrito." , okType: 'button-assertive'});
				return false;
			}
		}else{
			//alert("Debe ingresar la fecha de cita.");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la fecha de cita." , okType: 'button-assertive'});
			return false;
		}
		return true;
	};

	vm.tituloBoton = "Enviar";

	vm.cancelRegistry = function () {
		vm.newRegistry = {};
		vm.closeNewRegistryModal();
		$scope.isEdition = true;
	};
	$ionicModal.fromTemplateUrl('new-registry-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function (modal) {
		vm.newRegistryModal = modal;
	});
	vm.openNewRegistryModal = function (option, esEdicion) {
		if(!esEdicion){			
			$scope.lstTelefonos = angular.copy($scope.lstTelefonosBK);
			vm.newRegistry.correo = {};
			vm.newRegistry.correo.correo = vm.client.correoElectronico;
		}
		
		vm.newRegistry.channel = option.label;
		vm.newRegistry.date = new Date();
		vm.newRegistry.option = option;
		vm.registryOptionsPopover.hide();
		vm.newRegistryModal.show();
		if(option.name=="mail"){
			vm.tituloBoton = "Vista Previa";
		}if(option.name=="call"){
			vm.tituloBoton = "Llamar";
		}else{
			vm.tituloBoton = "Enviar";
		}
	};
	vm.closeNewRegistryModal = function () {
		vm.newRegistryModal.hide();
	};

	$ionicPopover.fromTemplateUrl('registry-options-popover.html', {
		scope: $scope
	}).then(function (popover) {
		vm.registryOptionsPopover = popover;
	});

	vm.openRegistryOptionsPopover = function ($event) {
		vm.registryOptionsPopover.show($event);
	};
	vm.closeRegistryOptionsPopover = function () {
		vm.registryOptionsPopover.hide();
	};
	$scope.$on('$destroy', function () {
		vm.registryOptionsPopover.remove();
		vm.newRegistryModal.remove();
	});
	vm.client = prospectoResult;
	vm.client.cita = {};
	vm.client.correo = {};
	vm.client.llamada = {};

	$scope.lstTelefonos = [];
	$scope.lstTelefonosBK = [];
	
	if(vm.client.numeroCelular && vm.client.numeroCelular!=""){
		var telefono = {};
		telefono.value = vm.client.numeroCelular;
		telefono.view = "Celular - " + vm.client.numeroCelular;
		$scope.lstTelefonos.push(telefono);
		$scope.lstTelefonosBK.push(telefono);
	}
	if(vm.client.numeroTelefono && vm.client.numeroTelefono!=""){
		var telefono = {};
		telefono.value = vm.client.numeroTelefono;
		telefono.view = "Fijo - " + vm.client.numeroTelefono;
		$scope.lstTelefonos.push(telefono);
		$scope.lstTelefonosBK.push(telefono);
	}

	$scope.lstProformas = [];
	for(var item in proformasResult.proformas){
		//if(proformasResult.proformas[item].contratoId === 0 && !proformasResult.proformas[item].esBloqueado){
		if(!proformasResult.proformas[item].esBloqueado){
			var proforma = {};
			proforma.view = proformasResult.proformas[item].productoBean.productoNombre + " - US$ " +  proformasResult.proformas[item].certificadoBean.certificadoValor;
			proforma.value = proformasResult.proformas[item].proformaID;
			proforma.obligacionContrato = proformasResult.proformas[item].obligacionContrato;
			$scope.lstProformas.push(proforma);			
		}
		
	}

	//$scope.staticLstProformas = angular.copy($scope.lstProformas);
	$scope.lstDepartamento=shareData.lstDepartamento;
	$scope.lstProvincia=shareData.lstProvincia;
	$scope.lstDistrito=shareData.lstDistrito;
	$scope.lstProvincia = [];

	
	
	$scope.cambiarDepartamento=function(departamentoID,limpiarDistrito,seguimientoProvinciaID,seguimientoDistritoID){
    	$scope.url=BACK_END_URL+"provincia/"+departamentoID;
    	
			$http({
		  method: 'GET',
		  url: $scope.url
		}).then(function successCallback(response) {				

			$scope.lstProvincia=response.data.data;
			if(seguimientoProvinciaID){
				vm.newRegistry.cita.provinciaID = seguimientoProvinciaID;
				if(vm.newRegistry.cita.departamentoID&&vm.newRegistry.cita.provinciaID){
					$scope.cambiarProvincia(vm.newRegistry.cita.departamentoID,vm.newRegistry.cita.provinciaID,seguimientoDistritoID);
				}
			}
            if(limpiarDistrito){
                $scope.lstDistrito=[];
            }
		}, function errorCallback(response) {
            
		});			    
	};

	$scope.cambiarProvincia=function(departamentoID,provinciaID,seguimientoDistritoID){
			
		$scope.url=BACK_END_URL+"distrito/"+departamentoID+"/"+provinciaID;
		
			$http({
		  method: 'GET',
		  url: $scope.url
		}).then(function successCallback(response) {
			
			$scope.lstDistrito=response.data.data;		
			vm.newRegistry.cita.distritoID = seguimientoDistritoID;
		}, function errorCallback(response) {
            
		});
	};



	vm.registryOptions = [
		{
			id: 0,
			name: 'call',
			label: "Registro de Llamada"
		},
		{
			id: 1,
			name: 'mail',
			label: "Registro de E-mail"
		},
		{
			id: 2,
			name: 'meeting',
			label: "Cita"
		}
	];

	$scope.fillSeguimientoData = function(){
		for(var item in vm.history){
			vm.history[item].detail = vm.selectDetail(vm.history[item]);
			if(vm.history[item].proforma!==undefined){
				var proformaId = vm.history[item].proforma.proformaID;
				for(var proforma in $scope.lstProformas){
					if($scope.lstProformas[proforma].value === proformaId){
						vm.history[item].proforma.strProgram = $scope.lstProformas[proforma].view;
						break;
					}
				}
			}
			/*inicio @issue VTAPROD-181*/
			var fecha = new Date(vm.history[item].fechaCreacion);
            /*fin @issue VTAPROD-181*/
			/*inicio @issue VTADIG2-418*/
            //vm.history[item].fechaCita = (fecha.getDate()<10?'0' + fecha.getDate() :  fecha.getDate()) + '-' + (fecha.getMonth() + 1<10?'0' + (fecha.getMonth()+1) :  (fecha.getMonth()+1)) + '-' + fecha.getFullYear() + ' ' +  (fecha.getHours()<10?'0' + fecha.getHours() :  fecha.getHours())  + ':' + (fecha.getMinutes()<10?'0' + fecha.getMinutes() :  fecha.getMinutes()) + ":00";
			vm.history[item].fechaCita = (fecha.getDate()<10?'0' + fecha.getDate() :  fecha.getDate()) + '-' + (fecha.getMonth() + 1<10?'0' + (fecha.getMonth()+1) :  (fecha.getMonth()+1)) + '-' + fecha.getFullYear() + ' ' +  (fecha.getHours()<10?'0' + fecha.getHours() :  fecha.getHours())  + ':' + (fecha.getMinutes()<10?'0' + fecha.getMinutes() :  fecha.getMinutes()) + ':' + (fecha.getSeconds()<10?'0' + fecha.getSeconds() :  fecha.getSeconds()) ;
            /*fin @issue VTADIG2-418*/
			vm.history[item].prospecto.correoElectronico = undefined;
			vm.history[item].prospecto.numeroTelefono = undefined;
			vm.history[item].prospecto.numeroCelular = undefined;
			if(vm.history[item].tipoSeguimientoID == 2){
				if(vm.history[item].estado === 1){
					vm.history[item].concretado = "Concretado";
				}else{
					vm.history[item].concretado = "No Concretado";
				}
			}
			if(vm.history[item].proforma){
				vm.history[item].proforma.prospectoBean = undefined;
			}	
		}

	};
	vm.history = prospectoResult.seguimientos;
	vm.selectDetail = function(item){	
		let response = "";	
		switch(item.tipoSeguimientoID){
			case 0: response = item.telefono;			
					break;
			case 1: response = item.correo;
					break;
			case 2: response = item.lugarCita;
					break;
			default: response = "-";break;
		}
		item.telefono = undefined;
		item.correo = undefined;
		//item.lugarCita = undefined;
		console.log(response);
		return response;
	}
	$scope.fillSeguimientoData();
	console.log(vm.history);

	departamento.getDepartamento()
		.then(function successCallback(response) {				
			$scope.lstDepartamento = response;
			shareData.lstDepartamento = $scope.lstDepartamento;
			if(vm.newRegistry.cita.departamentoID){
				$scope.cambiarDepartamento(vm.newRegistry.cita.departamentoID,true,vm.newRegistry.cita.provinciaID,vm.newRegistry.cita.distritoID);
			}
			console.log(response);
		}, function errorCallback(response) {
			alert(response);
			
		});	

	$scope.getSeguimiento = function(seguimientoID){
		var seguimiento = vm.history.filter(function(el){
				return el.seguimientoID === seguimientoID;
		})[0];
		var option = vm.registryOptions.filter(function(el){
			return el.id === seguimiento.tipoSeguimientoID;
		})[0];
		/* if(option.id === 0){
			vm.newRegistry.llamada = {};
			vm.newRegistry.llamada.seguimientoID = seguimiento.seguimientoID;
			
			var existeTelefono = $scope.lstTelefonosBK.filter(function(el){
				return el.value === seguimiento.telefono;
			});
			if(existeTelefono.length>0){
				$scope.lstTelefonos = angular.copy($scope.lstTelefonosBK);	
			}else{
				$scope.lstTelefonos = angular.copy($scope.lstTelefonosBK);
				var telefono = {};
				telefono.value = seguimiento.telefono;
				telefono.view =  seguimiento.telefono;
				$scope.lstTelefonos.push(telefono);
			}
			vm.newRegistry.llamada.telefono = seguimiento.telefono;

			vm.newRegistry.llamada.fechaDate = new Date(seguimiento.fecha);
			vm.newRegistry.llamada.hora = new Date(seguimiento.fecha);
			if(seguimiento.proforma){
				vm.newRegistry.llamada.vtaProforma = {};
				vm.newRegistry.llamada.vtaProforma.proformaID = seguimiento.proforma.proformaID.toString();
			}			
		}else if(option.id === 1){
			vm.newRegistry.correo = {};
			vm.newRegistry.correo.seguimientoID = seguimiento.seguimientoID;
			vm.newRegistry.correo.correo = seguimiento.correo;
			vm.newRegistry.correo.asuntoCorreo = seguimiento.asuntoCorreo;
			if(seguimiento.alerta){
				vm.newRegistry.correo.plantillaId = seguimiento.alerta.llave;
			}			
			vm.newRegistry.correo.mensajeCorreo = seguimiento.mensajeCorreo;
			vm.newRegistry.correo.fechaDate = new Date(seguimiento.fecha);
			vm.newRegistry.correo.hora = new Date(seguimiento.fecha);
			if(seguimiento.proforma){
				vm.newRegistry.correo.vtaProforma = {};
				vm.newRegistry.correo.vtaProforma.proformaID = seguimiento.proforma.proformaID.toString();
			}			 */
		if(option.id === 2){
			$scope.isEdition = false;
			vm.newRegistry.cita = {};
			vm.newRegistry.cita.seguimientoID = seguimiento.seguimientoID;
			vm.newRegistry.cita.fechaDate = new Date(seguimiento.fecha);
			vm.newRegistry.cita.hora = new Date(seguimiento.fecha);
			vm.newRegistry.cita.lugarCita = seguimiento.lugarCita;
			vm.newRegistry.cita.departamentoID = seguimiento.departamentoID.toString();
			vm.newRegistry.cita.provinciaID = seguimiento.provinciaID.toString();
			vm.newRegistry.cita.distritoID = seguimiento.distritoID.toString();
			if(seguimiento.estado){
				vm.newRegistry.cita.estado = seguimiento.estado;
			}
			
			if(seguimiento.proforma){
				vm.newRegistry.cita.vtaProforma = {};
				vm.newRegistry.cita.vtaProforma.proformaID = seguimiento.proforma.proformaID.toString();
			}	
			departamento.getDepartamento()
			.then(function successCallback(response) {				
				$scope.lstDepartamento = response;
				shareData.lstDepartamento = $scope.lstDepartamento;
				if(vm.newRegistry.cita.departamentoID){
					$scope.cambiarDepartamento(vm.newRegistry.cita.departamentoID,true,vm.newRegistry.cita.provinciaID,vm.newRegistry.cita.distritoID);
				}
				console.log(response);
			}, function errorCallback(response) {
				alert(response);
				
			});		
			vm.openNewRegistryModal(option, true);
		} else {
			return false;
		}
		
	}


	vm.mailPreview = {};
	vm.cancelMailPreview = function () {
		vm.mailPreview = {};
		vm.closeMailPreview();
		$scope.isEdition = true;
	};
	$ionicModal.fromTemplateUrl('mail-preview.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function (modal) {
		vm.mailPreviewModal = modal;
	});
	vm.openMailPreview = function (option, esEdicion) {

		vm.mailPreviewModal.show();
	};
	vm.closeMailPreview = function () {
		vm.mailPreviewModal.hide();
	};
	vm.cambioDePlantilla = function(){
		vm.plantillaActual = vm.mapPlatillas[vm.newRegistry.correo.plantillaId];
		vm.evaluarAsuntoEditable();
		vm.evaluarProformaObligatoria();
		vm.evaluarFechaDePagoObligatoria();
		vm.evaluarMostrarMensajeAdicional();
		vm.evaluarMostrarFechaPago();	
		vm.evaluarMostrarArchivosAdjuntos();
		vm.newRegistry.correo.vtaProforma.proformaID = undefined;
		//vm.evaluaPagoCuota();
	}

	vm.evaluaPagoCuota = function(){
		if(vm.plantillaActual.alertaID == TIPO_PLANTILLA.PAGO_CUOTA){
			$scope.lstProformas = angular.copy($scope.staticLstProformas);
			$scope.lstProformas = $filter('filter')($scope.lstProformas, {obligacionContrato : true})
		} else if (vm.plantillaActual.alertaID == TIPO_PLANTILLA.RECORDATORIO_ASAMBLEA) {
			$scope.lstProformas = angular.copy($scope.staticLstProformas);
			$scope.lstProformas = $filter('filter')($scope.lstProformas, {obligacionContrato : false})
		} else {
			$scope.lstProformas = angular.copy($scope.staticLstProformas);
		}
	}

	vm.removerArchivo = function($index){		
		if(!vm.newRegistry.correo.archivosAdjuntosBase64){
			vm.newRegistry.correo.archivosAdjuntosBase64 = [];
		}
		var archivo = vm.newRegistry.correo.archivosAdjuntosBase64[$index];
		if(archivo){
			var confirmPopup = $ionicPopup.confirm({
				title: 'Cancelar',
				template: '¿Está seguro de eliminar este archivo?',
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			});
			confirmPopup
			.then(function(res) {
				if(res) {
					vm.newRegistry.correo.archivosAdjuntosBase64.splice($index,1);
				}
			});							
		}		
	}
	vm.agregarArchivo = function(){
		if(filechooser){
			filechooser.open(
				{}, 
				function(uri){
					//console.log(uri);
					$http.get(uri.url,{responseType: "blob"}).then(function(data){	
						var blob = data.data;
						if(!blob){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se pudo localizar el archivo, o está dañado." , okType: 'button-assertive'});
							return;	
						}
						var url = uri.url;
						var filename = url.substring(url.lastIndexOf("/")+1,url.length);
						filename = decodeURIComponent(filename);
						
						var tamanoKB = blob.size/1024;
						var tamanoMaximo = $scope.configArchivosPlantilla.maxTamanio;
							if(tamanoKB>tamanoMaximo){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El tamaño del archivo debe ser menor a "+tamanoMaximo/1024 +"MB.", okType: 'button-assertive'});
								return;
						}

						blobToBase64(data.data,function(base64){
							if(!vm.newRegistry.correo.archivosAdjuntosBase64 ){
								vm.newRegistry.correo.archivosAdjuntosBase64 = [];
							}
							vm.newRegistry.correo.archivosAdjuntosBase64.push({
								nombre: filename,
								data: base64
							});
							$scope.$apply();
						})						
					},function(error){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se necesitan permisos para leer dato de la memoria SD." , okType: 'button-assertive'});
					})
				},
				function(msg){
					console.log(msg);							
				}
			);	
		}
	}

	vm.evaluarMostrarArchivosAdjuntos = function(){
		vm.mostrarArchivosAdjuntos = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.mostrarArchivosAdjuntos = vm.plantillaActual.plantillaRequiereArchivosAdjuntos;
				}				
			}
		}
		if(vm.mostrarArchivosAdjuntos===null||vm.mostrarArchivosAdjuntos===undefined){
			vm.mostrarArchivosAdjuntos = false;
		}
	}

	vm.evaluarMostrarFechaPago = function(){
		vm.mostrarFechaPago = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.mostrarFechaPago = vm.plantillaActual.plantillaRequiereFechaPago;
				}				
			}
		}
		if(vm.mostrarFechaPago===null||vm.mostrarFechaPago===undefined){
			vm.mostrarFechaPago = false;
		}
	}

	vm.evaluarAsuntoEditable = function(){
		vm.asuntoEditable = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.asuntoEditable = vm.plantillaActual.plantillaAsuntoModificable;
				}				
			}
		}
		if(vm.asuntoEditable===null||vm.asuntoEditable===undefined){
			vm.asuntoEditable = false;
		}
		if(!vm.asuntoEditable){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.newRegistry.correo.asuntoCorreo = vm.plantillaActual.asunto;
				}
			}
		}else{
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.newRegistry.correo.asuntoCorreo = vm.plantillaActual.plantillaAsuntoPorDefecto;
				}
			}
		}
	}
	
	vm.evaluarProformaObligatoria = function(){
		vm.proformaObligatoria = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.proformaObligatoria = vm.plantillaActual.plantillaRequiereProforma;
				}				
			}
		}
		if(vm.proformaObligatoria===null||vm.proformaObligatoria===undefined){
			vm.proformaObligatoria = false;
		}
	}
	vm.evaluarFechaDePagoObligatoria = function(){
		vm.fechaDePagoObligatoria = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.fechaDePagoObligatoria = vm.plantillaActual.plantillaRequiereFechaPago;
				}				
			}
		}
		if(vm.fechaDePagoObligatoria===null||vm.fechaDePagoObligatoria===undefined){
			vm.fechaDePagoObligatoria = false;
		}
	}

	vm.evaluarMostrarMensajeAdicional = function(){
		vm.mostrarMensajeAdicional = false;
		if(vm.newRegistry){
			if(vm.newRegistry.correo){
				if(vm.newRegistry.correo.plantillaId){
					vm.mostrarMensajeAdicional = vm.plantillaActual.plantillaPermiteMensajeAdicional;
				}				
			}
		}
		if(vm.mostrarMensajeAdicional===null||vm.mostrarMensajeAdicional===undefined){
			vm.mostrarMensajeAdicional = false;
		}
		if(!vm.mostrarMensajeAdicional){
			if(vm.newRegistry.correo){
				vm.newRegistry.correo.mensajeCorreo = "";
			}
		}
	}

	$scope.init = function(){
		vm.evaluarAsuntoEditable();
		vm.evaluarProformaObligatoria();
		vm.evaluarMostrarMensajeAdicional();
		vm.evaluarMostrarFechaPago();	
	}
	$scope.init();
})
.controller('consultaObligacionesPorPagarCtrl', function (validacionService,$scope, $state, $ionicPopover,obligacionesPorPagarService,lstObligacionResult,tempProformaDetalle,shareData) {
				
		$scope.lstTipoDocumento=shareData.lstTipoDocumento;	
        var vm = this;
	      vm.search = {};
		
        var formatearData = function(data){
		  			$scope.obligaciones = 	data;
		  			for(var i in data){
		  				var tdID = data[i].tipoDocumentoID;
		  				var tdNombre;
		  				 for(var j in $scope.lstTipoDocumento){
		  				 	if($scope.lstTipoDocumento[j].codigo==tdID){
		  				 		tdNombre = $scope.lstTipoDocumento[j].descripcion;
		  				 		break;
		  				 	}
		  				 }
					     data[i].tipoDocumentoNombre = tdNombre;
					     data[i].procedencia = 'Pago a Cuenta';
					     if(data[i].contratoId!=null){
					     	data[i].procedencia = 'Venta';
						 }
						 if(data[i].ObligacionPagoIndPagoCia===true){
							data[i].procedencia = 'Pago CIA';
						 }
					     data[i].json = JSON.stringify(data[i]);
					}
		  			
		  			vm.search.results = $scope.obligaciones;
        }
        //mostramos por defecto las pendientes por pagar
        formatearData(lstObligacionResult);
	})

/**
 * @feature/Mastercard (service pagoMastercardService)
 */
//inicio @feature/pagoLink
//.controller('detalleObligacionCtrl',function($scope,$rootScope,$state,$ionicPlatform,shareData,obligacionResult,obligacionPagoService,pagoService,blockUI, $ionicPopup,pagoMastercardService){
.controller('detalleObligacionCtrl',function($scope,$rootScope,$state,$ionicPlatform,shareData,obligacionResult,obligacionPagoService,pagoService,blockUI, $ionicPopup,pagoMastercardService,pagoLinkService){
//fin @feature/pagoLink
	var vm = this;
	$scope.deshabilitaPago=false;
	$scope.lstTipoDocumento=shareData.lstTipoDocumento;	
	
	var tdID = obligacionResult.tipoDocumentoID;
	for(var j in $scope.lstTipoDocumento){
		if($scope.lstTipoDocumento[j].codigo==tdID){
			tdNombre = $scope.lstTipoDocumento[j].descripcion;
			break;
		}
	}
	obligacionResult.tipoDocumentoNombre = tdNombre;
	$scope.obligacion = obligacionResult;
	vm.obligacion = $scope.obligacion;

	var obligacionPagoID = vm.obligacion.obligacionPagoID;
	var montoObligacion = vm.obligacion.importePago;
	var personaObli=vm.obligacion.nombre+" "+vm.obligacion.nombreAsociado;
	console.log("obligacionPagoID="+obligacionPagoID+' - montoObligacion='+montoObligacion);
	console.log("personaObli="+personaObli );
	var onSuccess = function (mposResponseBeanObject) { 
		console.log("success: "+mposResponseBeanObject);
		var responseBeanObject = JSON.parse(mposResponseBeanObject);
		if(responseBeanObject.statusType==="AUTHORIZED")
		{			
			//pagoService.pagarDirectoObligacionMPOS(responseBeanObject,obligacionPagoID)
			pagoService.pagarObligacionMPOS(responseBeanObject,obligacionPagoID)			
			.then(function(data){
				console.log("response-pagarObligacion:: "+data);	  
				$state.go('app.obligacionesPorPagar');			
			}, function errorCallback(response) {
				console.log("ERROR response-pagarObligacion");
				$state.go('app.obligacionesPorPagar');
			});	
			
		}else{
			$scope.deshabilitaPago=false;
		}
		blockUI.stop();
	}; 

	var onError = function (mposErrorObject) { 
		console.log("error: "+mposErrorObject);
		var errorObject = JSON.parse(mposErrorObject);
		alert('ERROR. '+errorObject.message);		
		blockUI.stop();
		$scope.deshabilitaPago=false;
	};
 
	$scope.cancelar = function(){
		$state.go('app.obligacionesPorPagar');
	}
	
	$scope.pagarObligacionMPOS = function(){
		$ionicPlatform.ready(function () {
			console.log("en pagarObligacionMPOS");	
			montoObligacion = montoObligacion.replace(",","");
			var amount = montoObligacion;
			var reference = obligacionPagoID;
			var email = "";	

			// Obtener datos de la obligacion de pago
			var estadoObligacion = "";
			pagoService.getObligacionPago(obligacionPagoID)
			.then(function(data){
				estadoObligacion = data['estado'];

				if(estadoObligacion!="0"){
					alert("No existe obligación pendiente de pago");

				}else{
					// Ejecutar el pago con MPOS
					$scope.deshabilitaPago=true;
					
					/*blockUI.start();
					window.visanet.authorize(amount, reference, email, onSuccess, onError);
					/**/
					blockUI.start();
					var jsonMPOSBean={
						"responseCode":"00",
						"transactionId":102,
						"bank":"INTERBANK",
						"transactionLocalDate":"28/05/17",
						"transactionLocalTime":"19:20",
						"currencyLabel":"$",
						"transactionAmount":montoObligacion,
						"voucherType":"CREDITO",
						"currencyCode":"840",
						"traceNumber":"101001",
						"statusType":"AUTHORIZED",
						"voucherEmail":"usuario@gmail.com"
					};
				
					// Llamar al servicio de pagos SAF
					//pagoService.pagarDirectoObligacionMPOS(jsonMPOSBean,obligacionPagoID)
					pagoService.pagarObligacionMPOS(jsonMPOSBean,obligacionPagoID)
					.then(function(data){
						console.log("response-pagarObligacion:: "+data);	
						blockUI.stop();						
						//alert("El pago ha sido realizado correctamente");
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El pago ha sido realizado correctamente." , okType: 'button-assertive'});	
						alertPopup.then(function(res){
							$state.go('app.obligacionesPorPagar');
						});						
					}, function errorCallback(response) {
						alert("Se produjo un error");
						blockUI.stop();
						$scope.deshabilitaPago=false;
					});/**/

				}


			}, function errorCallback(response) {
				alert("No se puede realizar el pago");
			});			
			

		 });
	};
/**
 *inicio  @feature/Mastercard
 */
	$scope.pagarObligacionMastercard=function(){
		pagoMastercardService.validaDiasAsamblea(obligacionPagoID)
		.then(function(data){
			if(data.data){
				let origenId=3 //obligacion por pagar
				$state.go('app.registrarPagoMastercardObligacion',{obligacionID:obligacionPagoID,origenObligacionID:origenId});					
			}
			console.log('validaAsamblea en obligacion'+data);
		})
		.catch(function(err){
			console.log("error::",err);
			$ionicPopup.alert({ title: 'Alerta', template: err.message , okType: 'button-assertive'});
		}) ;
	
	}
/**
 * fin @feature/Mastercard
 */

 /**
  * inicio @feature/pagoLink
  */
	$scope.pagarObligacionPagoLink=function(){
		console.log("en pagarObligacionPagoLink");	
		console.log(obligacionPagoID);
		pagoLinkService.generarPagoLink(obligacionPagoID)
		.then(function(data){
			if(data) {
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Correo enviado con éxito." , okType: 'button-assertive'});	
						alertPopup.then(function(res){
							$state.go('app.obligacionesPorPagar');
						});	
			}	
			console.log("pagolink en obligacion"+data);
		})
		.catch(function(err){
			console.log("error::",err);
			$ionicPopup.alert({ title: 'Alerta', template:"No se pudo enviar el link de pago." , okType: 'button-assertive'});
		});
	}
  /**
  * fin @feature/pagoLink
  */
})
/**
 * @issue JIRAV3-005
 *  - inject ROL, me
 *  - mostrar en modo solo lectura para gerentes
 */
	.controller('consultaClienteCtrl', function (validacionService,listaProspecto, me, ROL,$scope, $state, $ionicPopover,proformaService,tempProformaDetalle,shareData) {
		$scope.lstTipoDocumento=shareData.lstTipoDocumento;	
		var vm = this;
		/* inicio @issue JIRAV3-005 */
		vm.modoSoloLectura = me && me.rolID === ROL.TIPO.GERENTE_PRODUCTO;
		/* fin @issue JIRAV3-005 */
		vm.search = {
			filterBy: undefined,
			filters: [
				{
					attr: 'strFullName',
					label: 'Nombre'
				},
				{
					attr: 'strDocument',
					label: 'Nro. documento'
				},                
				{
					attr: 'strPointOfSale',
					label: 'Punto de venta'
				},
				{
					attr: 'nmPhone',
					label: 'Celular'
				}				
			]
		};
		vm.search.filterBy = vm.search.filters[0];

        listaProspecto.filter(function(el){ 		     
		     el.tipoDocumentoNombre = shareData.mapTipoDocumento[el.tipoDocumentoID].descripcion;
		     return true;
		});
		vm.search.results = listaProspecto;
		
		$ionicPopover.fromTemplateUrl('prospect-filters-popover.html', {
			scope: $scope
		}).then(function (popover) {
			vm.prospectFilterPopover = popover;
		});

		vm.openProspectFilterPopover = function ($event) {
			vm.prospectFilterPopover.show($event);
		};
		vm.closeProspectFilterPopover = function () {
			vm.prospectFilterPopover.hide();
		};
		$scope.$on('$destroy', function () {
			vm.prospectFilterPopover.remove();
		});

		vm.findResults = function () {
			if (vm.search.query.length >= 3) {
							
				proformaService.getProformasPorNombre(vm.search.query)
				.then(function(data){	 
					$scope.proformas = 	data;
					for(var i in data){
						var tdID = data[i].tipoDocumentoID;
						var tdNombre;
						 for(var j in $scope.lstTipoDocumento){
							if($scope.lstTipoDocumento[j].codigo==tdID){
								tdNombre = $scope.lstTipoDocumento[j].descripcionCorta;
								break;
							}
						 }
						 data[i].tipoDocumentoNombre = tdNombre;
						 data[i].json = JSON.stringify(data[i]);
					}
					
					vm.search.results = $scope.proformas;
					
				});
			} else {
				vm.search.results = undefined;
			}
		};

		vm.changeFilter = function (filter) {
			vm.search.filterBy = filter;
			vm.closeProspectFilterPopover();
			vm.search.query = undefined;
			vm.search.results = undefined;
		};


	
})

.directive("inputDisabled", function(){
	return function(scope, element, attrs){
	scope.$watch(attrs.inputDisabled, function(val){
		if(val)
		element.removeAttr("disabled");
		else
		element.attr("disabled", "disabled");
	});
	};
})

/**
 *
 * @issue JIRAV3-007
 * - agregar sección de cuotas devengadas
 * @issue JIRAV3-007:02
 *  -
 * @issue FIDELIZACION
 * - agregar productos de tipo inmuebles a la ffvv FIDELIZACION
 * @issue @issue C5-Inmuebles
 * - dejar solo c5-inmuebles en tipo de bien inmuebles
  */
.controller('cotizacionCtrl', function(validacionService,validacionDeTitular,$rootScope,$filter,$stateParams,$scope, $state , shareData,tempProforma,GrupoService,ProgramaProductoService,MarcaService,lstTipoBien,proformaService,dataMarcas,contratoService,$ionicPopup, DATOS_RUC,$sessionStorage) {
		
		//$scope.comisionValor = undefined;
	    if($stateParams.init!==undefined && $stateParams.init==='0'){
	        tempProforma.proforma = {};  
	        $scope.tempProforma=tempProforma;
	        $scope.lstModelo = [];
	        $scope.lstMarca =  [] ;
	        $scope.lstMarcaModeloProducto =  undefined;
	        $scope.lstMarcaModelo =  undefined;
	        $scope.lstModeloProducto =  undefined;
	        $scope.lstGrupo = [];   
	        $scope.lstCertificado=[];
	        $scope.tempProforma.proforma.cuotaIngreso = undefined;
	        $scope.tempProforma.proforma.numeroVacantes  = undefined;
	        $scope.tempProforma.proforma.cuotaMensual  = undefined;
	        $scope.tempProforma.proforma.cuotaIngreso  = undefined;
	        $scope.searchModel = [];
	    }else{
	    	$scope.tempProforma=tempProforma;	
	    }

	    /* inicio @issue JIRAV3-007 */
    	$scope.noProrratear = false;
        /* fin @issue JIRAV3-007 */
	    if($sessionStorage.defaults.puntoExhibicion!==undefined){
	    	$scope.tempProforma.proforma.perPuntoExhibicion = $sessionStorage.defaults.puntoExhibicion;
	    }		

		$scope.TIPO_BIEN = {
			VEHICULO: 2,
			INMUEBLE: 1
		};
		$scope.lstTipoBien = lstTipoBien;		
		$scope.puntosDeVentas = shareData.lstPuntoDeExhibicion;
		$scope.mapSmvBienesServicios = shareData.mapSmvBienesServicios;
    /**
	 *
	 *
	 *
	 * @issue JIRAV3-007:02
	 *  - llamada a scope.evaluarCuotasDevengadas para actualizar los valores de los certificados
     */
		$scope.cargaCertificados = function(){
			/*contratoService.getComisionValor($scope.tempProforma.proforma.productoID,$scope.tempProforma.proforma.grupoID).then(
				function(data){
					$rootScope.porcentajeCuotaAdmin = data.C_ADMIN/100;
					$rootScope.porcentajeCIA = data.CIA/100;

					$scope.porcentajeCuotaAdmin = $rootScope.porcentajeCuotaAdmin;
					$scope.porcentajeCIA = $rootScope.porcentajeCIA;*/

					var grupoSeleccionado = $filter('filter')($scope.lstGrupo, {grupoID : parseInt($scope.tempProforma.proforma.grupoID)}, true)[0];	
					if(grupoSeleccionado){
						proformaService.getCuotaCertificado($scope.tempProforma.proforma.grupoID,0,0)
						.then(function(data){
							$scope.lstCertificado = data;
							angular.forEach($scope.lstCertificado,function(item){
										item.cuotaMensual = item.certificadoCuotaMensual;
										item.cia = validacionService.aDosDecimales(item.certificadoCIA);
										if($scope.tempProforma.proforma.certificadoPosicionID === item.id.certificadoPosicionID && 
											$scope.tempProforma.proforma.grupoID === item.id.grupoID.toString()){
											$scope.tempProforma.proforma.cia = item.cia;
										}
							})
							if($scope.lstCertificado.length<=0){
								//alert('El grupo no tiene certificados');
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'El grupo no tiene certificados.' , okType: 'button-assertive'});	  			
							}
							/* inicio @issue JIRAV3-007:02 */
							else {
								$scope.evaluarCuotasDevengadas();
							}
							/* fon @issue JIRAV3-007:02 */
							
						});
					}					
				/*},
				function(error){
					console.log(error);
				}
			);*/
			

		};
    /**
	 * inicio @issue JIRAV3-007
	 * - setear cuotas devengadas a 0 al cambiar de grupo
     */
		$scope.cambiarGrupo = function(){
			$scope.tempProforma.proforma.numeroVacantes = null;
			$scope.tempProforma.proforma.cuotaMensual= null;
			$scope.tempProforma.proforma.cuotaIngreso= null;
			$scope.tempProforma.proforma.certificadoPosicionID= null;
			$scope.tempProforma.proforma.duracionPrograma=null;
			$scope.lstCertificado=[];

			if($scope.tempProforma.proforma.grupoID){
				var grupoSeleccionado = $filter('filter')($scope.lstGrupo, {grupoID : parseInt($scope.tempProforma.proforma.grupoID)}, true)[0];	
				$scope.tempProforma.proforma.cuotaIngreso = grupoSeleccionado.grupoNroCuota;
				$scope.tempProforma.proforma.duracionPrograma  = grupoSeleccionado.duracionPrograma;
				$scope.tempProforma.proforma.numeroVacantes  = grupoSeleccionado.vacantesDisponibles;
				$scope.tempProforma.proforma.cia  = undefined;
				$scope.cargaCertificados();
			}
            /* inicion @issue JIRAV3-007 */
            if($scope.tempProforma.proforma.cuotaIngreso && $scope.tempProforma.proforma.cuotaIngreso>1){
                if($scope.tempProforma.proforma.programaID === "C4"){
                    $scope.tempProforma.proforma.cuotasAdjudicacion = parseInt($scope.tempProforma.proforma.cuotaIngreso)-1;
                    $scope.tempProforma.proforma.cuotasProrrateo = 0;
                    $scope.tempProforma.proforma.cuotasIngresoVenta = 0;
                    $scope.noProrratear = true;
                }else{
                    $scope.tempProforma.proforma.cuotasProrrateo = parseInt($scope.tempProforma.proforma.cuotaIngreso)-1;
                    $scope.tempProforma.proforma.cuotasIngresoVenta = 0;
                    $scope.tempProforma.proforma.cuotasAdjudicacion = 0;
                    $scope.noProrratear = false;
                }
            }
            /* fin @issue JIRAV3-007 */
		};

    /**
	 *
	 *
	 *
	 * @issue JIRAV3-007
     * @param valor
     * @returns {*}
     */
    $scope.devengadoOnClick=function(valor){
        if(valor===0||valor===null||valor==undefined){
            valor = null;
        }
        return valor;
    };
    /**
	 *
	 *
	 *
	 * @issue JIRAV3-007
     * @param valor
     * @returns {*}
     */
    $scope.devengadoOnBlur=function(valor){
        if(valor===null||valor==undefined||isNaN(valor)){
            valor = 0;
            $scope.evaluarCuotasDevengadas()
        }
        return valor;
     };

    /**
	 *
	 *
	 *
	 * @issue JIRAV3-007
     * @param indiceModificado
	 * @issue JIRAV3-007:02
	 *  - actualizar los valores de los certificados
     */
    $scope.evaluarCuotasDevengadas = function(indiceModificado){
        if(	$scope.tempProforma.proforma.cuotasIngresoVenta === undefined ||
            $scope.tempProforma.proforma.cuotasIngresoVenta === null ||
            isNaN($scope.tempProforma.proforma.cuotasIngresoVenta)){
            return;
        }
        if(	$scope.tempProforma.proforma.cuotasAdjudicacion === undefined ||
            $scope.tempProforma.proforma.cuotasAdjudicacion===null ||
            isNaN($scope.tempProforma.proforma.cuotasAdjudicacion)){
            return;
        }
        if(	$scope.tempProforma.proforma.cuotasProrrateo === undefined ||
            $scope.tempProforma.proforma.cuotasProrrateo===null ||
            isNaN($scope.tempProforma.proforma.cuotasProrrateo)){
            return;
        }

        var totalCuotasDevengadas = 0;

        if($scope.tempProforma.proforma.cuotaIngreso<=1){
            $scope.tempProforma.proforma.cuotaIngreso = 1;

            $scope.tempProforma.proforma.cuotasIngresoVenta = 0;
            $scope.tempProforma.proforma.cuotasAdjudicacion = 0;
            $scope.tempProforma.proforma.cuotasProrrateo = 0;
        }

        //ajustar catidades de cuotas devengadas

        $scope.tempProforma.proforma.cuotasIngresoVenta = Math.abs(Math.trunc($scope.tempProforma.proforma.cuotasIngresoVenta));
        $scope.tempProforma.proforma.cuotasAdjudicacion = Math.abs(Math.trunc($scope.tempProforma.proforma.cuotasAdjudicacion));
        $scope.tempProforma.proforma.cuotasProrrateo = Math.abs(Math.trunc($scope.tempProforma.proforma.cuotasProrrateo));


        var contarCuotasDevengadas = function(){
            totalCuotasDevengadas = 0;
            totalCuotasDevengadas += $scope.tempProforma.proforma.cuotasIngresoVenta;
            totalCuotasDevengadas += $scope.tempProforma.proforma.cuotasAdjudicacion;
            totalCuotasDevengadas += $scope.tempProforma.proforma.cuotasProrrateo;
        };

        contarCuotasDevengadas();

        while(totalCuotasDevengadas>=$scope.tempProforma.proforma.cuotaIngreso){
            switch(indiceModificado){
                case 0:
                    $scope.tempProforma.proforma.cuotasIngresoVenta = $scope.tempProforma.proforma.cuotasIngresoVenta-1;
                    break;
                case 1:
                    $scope.tempProforma.proforma.cuotasAdjudicacion = $scope.tempProforma.proforma.cuotasAdjudicacion-1;
                    break;
                case 2:
                    $scope.tempProforma.proforma.cuotasProrrateo = $scope.tempProforma.proforma.cuotasProrrateo-1;
                    break;
            }
            contarCuotasDevengadas();
        }
        /* inicio @issue JIRAV3-007:02 */
        if($scope.tempProforma.proforma.grupoID) {
            proformaService.getCuotaCertificado($scope.tempProforma.proforma.grupoID,
                0,//certificadoPosicionId - o evalua todos los certificados
                $scope.tempProforma.proforma.cuotasProrrateo).then(
                function (data) {
                    //data = data[0];
                    //$scope.tempProforma.proforma.cuotaMensualBase = data.certificadoCuotaMensualBase;
                    //$scope.tempProforma.proforma.cuotaMensualFinal = data.certificadoCuotaMensual;
                    //$scope.tempProforma.proforma.cuotaMensual = data.certificadoCuotaMensual;
                    //$scope.tempProforma.proforma.cuotaTotalMensual = validacionService.aDosDecimales($scope.tempProforma.proforma.cuotaMensualFinal * ($scope.tempProforma.proforma.cuotasIngresoVenta + 1));
                    $scope.lstCertificado.forEach(certificado => {
                    	let d = data.find(d => d.id.grupoID === certificado.id.grupoID && d.id.certificadoPosicionID === certificado.id.certificadoPosicionID);
                    	certificado.cuotaMensual = d.certificadoCuotaMensual;
                    	if($scope.tempProforma.proforma.certificadoPosicionID === certificado.id.certificadoPosicionID){
                            $scope.tempProforma.proforma.cuotaMensual = certificado.cuotaMensual;
						}
					});
                }
            );
        }
        /* fin @issue JIRAV3-007:02 */
    };


		$scope.cargaGrupos=function(soloCargaCertificado){

			GrupoService.getByProductoId($scope.tempProforma.proforma.productoID)
			.then(function(data){
				/** INICIO card-29374486 Anderson Estela Coronel - 22-abr-2021 */
				if($scope.tempProforma.proforma.productoID == "25" && $scope.tempProforma.proforma.tipoBienID == "2")
					$scope.lstGrupo=data.filter(grupo => grupo.grupoNumero >= "8100" && grupo.grupoNumero <= "8108");
				else
					$scope.lstGrupo=data;
				/** FIN card-29374486 Anderson Estela Coronel - 22-abr-2021 */
				if($scope.lstGrupo.length<=0){
					$scope.lstTipoBien = [];
					//alert('El programa no tiene grupos');
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'El programa no tiene grupos.' , okType: 'button-assertive'});
				}
				if($scope.tempProforma.proforma.grupoID){
					if(soloCargaCertificado!== undefined && soloCargaCertificado){
						$scope.cargaCertificados();
					}else{
						$scope.cambiarGrupo();
					}
				}
			});
		};
    /**
	 *
	 *
	 * @issue JIRAV3-007
	 * - guardar programa id en $scope.tempProforma.proforma.programaID
     */
		$scope.cambiarPrograma=function(){

			$scope.tempProforma.proforma.grupoID=null;
			$scope.lstGrupo=[];

			$scope.tempProforma.proforma.duracionPrograma=null;
			$scope.tempProforma.proforma.numeroVacantes = null;
			$scope.tempProforma.proforma.cuotaMensual= null;
			$scope.tempProforma.proforma.cuotaIngreso= null;
			$scope.tempProforma.proforma.certificadoPosicionID= null;
			$scope.lstCertificado=[];
			/* inicio @issue JIRAV3-007 */
			let programaProducctoSeleccionado = $scope.lstProgramaProducto.find(pp=>pp.productoID === $scope.tempProforma.proforma.productoID);
            $scope.tempProforma.proforma.programaID = programaProducctoSeleccionado.productoNombre.split("-")[0];
            /* fin @issue JIRAV3-007 */
			$scope.cargaGrupos();
		};


		$scope.cambiarTipoBien = function(){

			$scope.tempProforma.proforma.marcaID = null;
			$scope.lstMarca=[];

			$scope.tempProforma.proforma.articuloID= null;
			$scope.lstModelo=[];

			$scope.tempProforma.proforma.productoID = null;
			$scope.lstProgramaProducto=[];

			$scope.tempProforma.proforma.grupoID=null;
			$scope.lstGrupo=[];

			$scope.tempProforma.proforma.duracionPrograma=null;
			$scope.tempProforma.proforma.numeroVacantes = null;
			$scope.tempProforma.proforma.cuotaMensual= null;
			$scope.tempProforma.proforma.cuotaIngreso= null;
			$scope.tempProforma.proforma.certificadoPosicionID= null;
			$scope.lstCertificado=[];
			if($scope.tempProforma.proforma.tipoBienID==2){//vehículo; s epiden las marcas; los modelos dependen de la marca
				$scope.lstMarca =  [] ;
				proformaService.getMarcaModeloProforma().then(function(data){
					$scope.lstMarcaModeloProducto = data;
					$scope.lstMarcaModelo = data.marcaModelo;
					$scope.lstModeloProducto = data.modeloProducto;
					for(var item in $scope.lstMarcaModelo){
						var obj ={};
						obj.marcaID = item.split('|')[0];
						obj.marcaNombre = item.split('|')[1];
						$scope.lstMarca.push(obj);
					}

				});
			}

			$scope.lstModelo = [];
			if($scope.tempProforma.proforma.tipoBienID==1){//inmueble; no se piden marcas, sino directamente los modelos
				  //TODO: jalar de servicio
				//$scope.lstModelo = shareData.lstModeloInmueble;
				proformaService.inversionesInmueble().then(function(data){
					$scope.lstModelo = data;
				});
			}
		};

		$scope.cargaModelos = function(){

			if($scope.tempProforma.proforma.tipoBienID==2){//vehículo; se piden los modelos
				$scope.lstModelo = [];

				for(var marca in $scope.lstMarca){
					if($scope.lstMarca[marca].marcaID == $scope.tempProforma.proforma.marcaID){
						var modelos = $scope.lstMarcaModelo[$scope.lstMarca[marca].marcaID + '|' + $scope.lstMarca[marca].marcaNombre];
						for(var item in modelos){
							var obj ={};
							obj.articuloID = modelos[item].split('|')[0];
							obj.articuloNombre = modelos[item].split('|')[1];
							$scope.lstModelo.push(obj);
						}
						break;
					}
				}


			}else if($scope.tempProforma.proforma.tipoBienID==1){
				  //$scope.lstModelo = shareData.lstModeloInmueble;
				  proformaService.inversionesInmueble().then(function(data){
					$scope.lstModelo = data;
				});
			}
		};

		$scope.cambiarMarca = function(){
			$scope.tempProforma.proforma.articuloID= null;
			$scope.lstModelo=[];


			$scope.tempProforma.proforma.productoID = null;
			$scope.lstProgramaProducto=[];

			$scope.tempProforma.proforma.grupoID=null;
			$scope.lstGrupo=[];

			$scope.tempProforma.proforma.duracionPrograma=null;
			$scope.tempProforma.proforma.numeroVacantes = null;
			$scope.tempProforma.proforma.cuotaMensual= null;
			$scope.tempProforma.proforma.cuotaIngreso= null;
			$scope.tempProforma.proforma.certificadoPosicionID= null;
			$scope.lstCertificado=[];
			$scope.cargaModelos();
		};
    /**
     * @issue FIDELIZACION
     * - agregar productos de tipo inmuebles a la ffvv FIDELIZACION
     * @issue C5-Inmuebles
     * - dejar solo c5-inmuebles en tipo de bien inmuebles
     */
		$scope.cargaProductos = function(){
			$scope.lstProgramaProducto = [];
			if($scope.tempProforma.proforma.tipoBienID==2){//vehículo; se piden los modelos


				for(var modelo in $scope.lstModelo){
					if($scope.lstModelo[modelo].articuloID == tempProforma.proforma.articuloID){
						var productos = $scope.lstModeloProducto[$scope.lstModelo[modelo].articuloID + '|' + $scope.lstModelo[modelo].articuloNombre];
						for(var item in productos){
							var obj ={};
							obj.productoID = productos[item].split('|')[0];
							obj.productoNombre = productos[item].split('|')[1];
							$scope.lstProgramaProducto.push(obj);
						}
						break;
					}
				}


			}else{
			    /** inicio @issue FIDELIZACION **/
				//if($rootScope.me.unidadNegocioID === '14' /*C5 - INMUEBLES*/ || $rootScope.me.unidadNegocioID === '17' /*C7_INMUEBLES*/ || $rootScope.me.unidadNegocioID === '08' /*Online - UN*/ || $rootScope.me.unidadNegocioID === '00' /*oficina*/) {
				if(['14'/*C5 - INMUEBLES*/,
					'17'/*C7_INMUEBLES*/,
					'08'/*Online - UN*/,
					'18'/*Fidelización*/,
					'00'/*oficina*/].includes($rootScope.me.unidadNegocioID)) {
                /** fin @issue FIDELIZACION **/
					//TODO: Implementar método en service
                    /** inivio @issue @issue C5-Inmuebles **/
					var obj2 ={};
					obj2.productoID = '25';
					obj2.productoNombre = 'C5-Pandero Inmuebles';
					$scope.lstProgramaProducto.push(obj2);
					
					var obj3 = {};
					obj3.productoID = '31';
					obj3.productoNombre = 'C7-Pandero Inmuebles';
					$scope.lstProgramaProducto.push(obj3);
					
					/** fin @issue @issue C5-Inmuebles **/
					/** inicio card-28943888 Anderson Estela Coronel - 02/mar/2021 */
					var obj4 = {};
					obj4.productoID = '32';
					obj4.productoNombre = 'C5- Pandero Inmuebles';
					$scope.lstProgramaProducto.push(obj4);
					/** fin card-28943888 Anderson Estela Coronel - 02/mar/2021 */
				}
			}
		};
		$scope.cambiarModelo = function(){
			$scope.tempProforma.proforma.productoID = null;
			$scope.lstProgramaProducto=[];

			$scope.tempProforma.proforma.grupoID=null;
			$scope.lstGrupo=[];

			$scope.tempProforma.proforma.duracionPrograma=null;
			$scope.tempProforma.proforma.numeroVacantes = null;
			$scope.tempProforma.proforma.cuotaMensual= null;
			$scope.tempProforma.proforma.cuotaIngreso= null;
			$scope.tempProforma.proforma.certificadoPosicionID= null;
			$scope.lstCertificado=[];
			$scope.cargaProductos();
		};

    /**
	 *
	 *
     * @param posicionId
     * @param cuotaMensual
     * @param cia
	 * @issue JIRAV3-007
	 * - agregar llamado a $scope.evaluarCuotasDevengadas
	 * @issue JIRAV3-007:02
	 *  - quitar llamada a $scope.evaluarCuotasDevengadas
     */
		$scope.elegirCertificado=function(posicionId,cuotaMensual,cia){
			$scope.certificado=posicionId;
			$scope.tempProforma.proforma.cuotaMensual = cuotaMensual;
			$scope.tempProforma.proforma.cia = cia;
			/* inicio @issue JIRAV3-007:02 */
            //$scope.evaluarCuotasDevengadas();
            /* fin @issue JIRAV3-007:02 */
		};

	  $scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
		  $scope.shownGroup = null;
		} else {
		  $scope.shownGroup = group;
		}
	  };
	  $scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	  };


		if(lstTipoBien!==null && lstTipoBien!== undefined && lstTipoBien.length===1){
		  $scope.tempProforma.proforma.tipoBienID=lstTipoBien[0].codigo;
		}


		if(dataMarcas !== null){
			$scope.lstMarcaModeloProducto = dataMarcas;
			$scope.lstMarcaModelo = dataMarcas.marcaModelo;
			$scope.lstModeloProducto = dataMarcas.modeloProducto;
			$scope.lstMarca =  [] ;
			for(var item in $scope.lstMarcaModelo){
				var obj ={};
				obj.marcaID = item.split('|')[0];
				obj.marcaNombre = item.split('|')[1];
				$scope.lstMarca.push(obj);
			}
			if($scope.tempProforma.proforma.marcaID ||
			($scope.tempProforma.proforma.articuloID && $scope.tempProforma.proforma.tipoBienID==='1')){
				$scope.cargaModelos();
			}
			if($scope.tempProforma.proforma.articuloID){
				$scope.cargaProductos();
			}
			if($scope.tempProforma.proforma.productoID){
				$scope.cargaGrupos(true);
			}

		}


		if($scope.tempProforma.proforma.tipoBienID==1){
			proformaService.inversionesInmueble().then(function(data){
				$scope.lstModelo = data;
			});
			//$scope.lstModelo = shareData.lstModeloInmueble;
		}

		$scope.guardar=function(){

			if($scope.registroAlMenosUnDatoProforma() && !$scope.esValidoTabProforma()){
				return false;
			}


			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID===$scope.TIPO_DOCUMENTO.RUC){
				if(!validacionDeTitular.validarPersonaJuridaActiva($scope.tempProforma.proforma.vtaProspecto.personaEstado)){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se puede generar la proforma, el RUC esta de baja." , okType: 'button-assertive'});
					return;
				}
				if(!validacionDeTitular.validarPersonaJuridaHabida($scope.tempProforma.proforma.vtaProspecto.pesonaFlag_22)){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se puede generar la proforma, el RUC no esta de activo." , okType: 'button-assertive'});
					return;
				}
			}

			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo;


			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled = $scope.busquedaConEscaner;
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo!=="4"){
				// inicio @issue EVALCRED-57
				//$scope.tempProforma.proforma.vtaProspecto.validado = true;
				// fin @issue EVALCRED-57
				if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID===$scope.TIPO_DOCUMENTO.RUC){
					if(!$scope.tempProforma.proforma.vtaProspecto.personaEstado && !$scope.tempProforma.proforma.vtaProspecto.pesonaFlag_22){
						$scope.tempProforma.proforma.vtaProspecto.validado = false;
					}
				}
			}

			if($scope.registroAlMenosUnDatoProforma() && $scope.esValidoTabProforma()){



				var sonIguales = true;
				if($scope.tempProforma.proformaCopia !== undefined){
					var sonIguales = $scope.comparaEdicion($scope.tempProforma.proforma, $scope.tempProforma.proformaCopia);
				}

				if($scope.tempProforma.proforma.marcaID ==="undefined"){
					delete $scope.tempProforma.proforma.marcaID;
				}
				//registro de proforma
				if($scope.tempProforma.proforma.perPuntoExhibicion!==null && $scope.tempProforma.proforma.perPuntoExhibicion!== undefined ){
					$scope.tempProforma.proforma.perPuntoExhibicion.puntoExhibicionID = $scope.tempProforma.proforma.perPuntoExhibicion.codigo;
				}

				if($scope.tempProforma.proformaCopia === undefined || ($scope.tempProforma.proformaCopia !== undefined && !sonIguales)){
					proformaService.registrarProforma($scope.tempProforma.proforma).then(function(data){
						var alertPopup = $ionicPopup.alert({
								 title: 'Mensaje',
								 template: data.message,
								 okText: 'Aceptar',
								 okType: 'button-assertive'
							   });
						alertPopup.then(function(res) {
								//$state.go('app.registroProspecto.proforma',{init: '0'});
								$state.go('app.detalleCliente',{prospectoID: data.data});
								$scope.tempProforma = {};
								$scope.limpiarListas();
								$scope.limpiarProspecto();
						});
					});
				}else{
					$state.go('app.detalleCliente',{prospectoID: $scope.tempProforma.proforma.vtaProspecto.prospectoID});
					$scope.tempProforma = {};
					$scope.limpiarListas();
					$scope.limpiarProspecto();
				}

			}
		};

		$scope.registroAlMenosUnDatoProforma = function(){
			if($scope.tempProforma.proforma.marcaID ||
				$scope.tempProforma.proforma.articuloID ||
				$scope.tempProforma.proforma.productoID ||
				$scope.tempProforma.proforma.grupoSeleccinado ||
				$scope.tempProforma.proforma.certificadoPosicionID
				){
				return true;
			}
			return false;
		};
    /**
	 *
	 *
	 *
     * @returns {boolean}
	 * @issue JIRAV3-007
	 * - bloquear por validacion de cuotas devengadas
     */
		$scope.esValidoTabProforma = function(){
			if(!$scope.tempProforma.proforma.perPuntoExhibicion){
				//alert("Por favor seleccione el punto de venta en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el punto de venta en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.tipoBienID){
				//alert("Por favor seleccione el tipo de bien en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el tipo de bien en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.marcaID && $scope.tempProforma.proforma.tipoBienID ===$scope.TIPO_BIEN.VEHICULO){
				//alert("Por favor seleccione la marca en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione la marca en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.articuloID){
				//alert("Por favor seleccione el modelo en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el modelo en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.productoID ){
				//alert("Por favor seleccione el programa en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el programa en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.grupoID ){
				//alert("Por favor seleccione el grupo en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el grupo en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.cuotaMensual){
				//alert("Certificado no posee cuota mensual");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Certificado no posee cuota mensual." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.cia){
				//alert("Certificado no posee cia");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Certificado no posee cia." , okType: 'button-assertive'});
				return false;
			}
            /* inicio @issue JIRAV3-007 */
            else if($scope.tempProforma.proforma.cuotaIngreso > 1 && !($scope.cuotasDevengadasValidas())){
                $ionicPopup.alert({ title: 'Alerta', template: "Ingrese la forma de pago de las cuotas devengadas(Debe devengar "+($scope.tempProforma.proforma.cuotaIngreso-1)+" cuotas)." , okType: 'button-assertive'});
                return false;
            }
            /* fin @issue JIRAV3-007 */
			return true;
		};

    /**
	 *
	 * @issue JIRAV3-007
     * @returns {boolean}
     */
		$scope.cuotasDevengadasValidas = function(){
			return ($scope.tempProforma.proforma.cuotasIngresoVenta+$scope.tempProforma.proforma.cuotasAdjudicacion+$scope.tempProforma.proforma.cuotasProrrateo)===($scope.tempProforma.proforma.cuotaIngreso-1);
		};

		$scope.limpiarListas = function(){						
			$rootScope.tempLstProgramaProducto = undefined;
			$rootScope.tempLstGrupo = undefined;
			$rootScope.tempLstCertificado = undefined;
			$rootScope.tempLstMarca = undefined;
			$rootScope.tempLstModelo = undefined;
			$rootScope.tempLstTipoBien = undefined;
			$rootScope.dataMarcas = undefined;
			$rootScope.porcentajeCuotaAdmin = undefined;
		};
		
		$scope.limpiarProspecto = function (){
			tempProforma.proforma.vtaProspecto = {};
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = false;
			tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
			tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
		};

		$scope.TIPO_DOCUMENTO = {
			RUC: '8',
			DNI: '4',
			PAS: '3',
			CE: '2'
		};

		$scope.comparaEdicion = function(proforma, proformaToCompare){
			if(proforma.articuloID !== proformaToCompare.articuloID
				|| proforma.productoID !== proformaToCompare.productoID
				|| proforma.grupoID !== proformaToCompare.grupoID
				|| proforma.certificadoPosicionID !== proformaToCompare.certificadoPosicionID
				|| proforma.marcaID !== proformaToCompare.marcaID
				|| proformaToCompare.perPuntoExhibicion === undefined
				|| proforma.perPuntoExhibicion.codigo !==  proformaToCompare.perPuntoExhibicion.codigo ){
				return false;
			}
			return true;
		}
	  
	})
	/*INICIO INTEGRACION EC*/
	//.controller('clienteCtrl', function($stateParams,$rootScope,$scope, $state,shareData,$ionicPopup,validacionService,validacionDeTitular,proformaService,tempProforma,dateFilter,$ionicPopover, $ionicPlatform,DATOS_RUC,PANDERO_DATA, prospectoService, USUARIO, PDF417PLUGIN, escanerService) {
/**
 *
 *
 * @issue JIRAV3-007
 * - considerar los datos de cuotas devengadas registradas en la proforma
 * @feature V4-HU005
 * - se permite siempre la edición de los campos
 * @issue V4-HU001
 * - validaciónd telefonos se realiza en el backend
 */
    .controller('clienteCtrl', function($stateParams,$rootScope,$scope, $state,shareData,$ionicPopup,validacionService,validacionDeTitular,proformaService,tempProforma,dateFilter,$ionicPopover, $ionicPlatform,DATOS_RUC,PANDERO_DATA, prospectoService, USUARIO, PDF417PLUGIN, escanerService, cuota, APP, $ionicModal) {
	/*FIN INTEGRACION EC*/
		//$scope.tempProformaDetalle = tempProformaDetalle;
		
		$scope.busquedaEscaner = true;
		$scope.busquedaConEscaner = false;
		//$scope.tempProformaDetalle = tempProformaDetalle;
		$scope.validacionService = validacionService;		
		/*INICIO INTEGRACION EC*/
		$scope.cuota = cuota;		
		/*FIN INTEGRACION EC*/

		if($stateParams.init!==undefined && $stateParams.init==='0'){
			tempProforma.proforma = {};  
		}
		
		$scope.TIPO_DOCUMENTO = {
			RUC: '8',
			DNI: '4',
			PAS: '3',
			CE: '2'
		};
		
		$scope.lstTipoDocumento = shareData.lstTipoDocumento.filter(documento => documento.codigo != $scope.TIPO_DOCUMENTO.PAS);

		$scope.TIPO_BIEN = {
			VEHICULO: 2,
			INMUEBLE: 1
		};
		$scope.limpiarProspecto = function (){
			$scope.habilitadoGuardarProforma = true;
			var tipoDocumentoSelected = undefined;
			if(tempProforma.proforma.vtaProspecto !== undefined && tempProforma.proforma.vtaProspecto.tipoDocumentoSelected !== undefined){
				tipoDocumentoSelected = tempProforma.proforma.vtaProspecto.tipoDocumentoSelected;
			}

			tempProforma.proforma.vtaProspecto = {};
			tempProforma.proforma.vtaProspecto.correo = '';

			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;

			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = false;
			if(tipoDocumentoSelected!==undefined){
				tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[tipoDocumentoSelected.codigo];
				tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
				if($scope.busquedaEscaner){
					tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
				}
			}else{
				tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
			}	
			
			tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled =  true
		};


		if(tempProforma.proforma.vtaProspecto===undefined){
			$scope.limpiarProspecto();
		}




		$scope.tempProforma=tempProforma;
		$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
		$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled = true;
		$scope.tempProforma.proforma.vtaProspecto.esEdicionProspecto = true;

		if(tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo!=="4"){
			$scope.busquedaEscaner = false;
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
		}

		if($scope.tempProforma.proforma.vtaProspecto.esEdicion!== undefined && $scope.tempProforma.proforma.vtaProspecto.esEdicion){
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled = false;
			$scope.tempProforma.proforma.vtaProspecto.esEdicionProspecto = false;
			$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
		}

		$scope.aceptarDatosContacto = () => {
			$scope.formatoContactoValido().then(
				()=>{
					$ionicPopup.confirm({
						title: 'Alerta',
						template: `¿Seguro desea guardar los datos de contacto?`,
						cancelText: 'Cancelar',
						okText: 'Aceptar'
					}).then(res => {
						if (res) {
							if ($scope.modalDatosContacto) {
								prospectoService.actualizarEvaluacionCrediticia($scope.evaluacionCrediticia, APP.MODULO.REGISTRO_PROSPECTO.ID).then(
									(data) => $ionicPopup.alert({ title: 'Alerta', template: data.message , okType: 'button-assertive'}).then(() => $scope.modalDatosContacto.hide())
								)
							}
						}
					});
				},
				(error) => {
					$ionicPopup.alert({ title: 'Alerta', template: error , okType: 'button-assertive'})
				}
			)
		};

		$scope.formatoContactoValido = () => {
			return new Promise((resolve, reject)=>{
				let telMinLength = 6;
				if(`${$scope.evaluacionCrediticia.tipoDocumento}` === '8') { //persona jurídica
                    if (!$scope.evaluacionCrediticia.razonSocial){
                        reject("Razón Social es obligatorio.");
                    }
                } else {
                    if (!$scope.evaluacionCrediticia.nombres){ // persona natural
                        reject("Nombres es obligatorio.");
                    }
                    if (!$scope.evaluacionCrediticia.apellidoPaterno){
                        reject("Apellido Paterno es obligatorio.");
                    }
                }
				let tieneAlmenosUnDato = false;
				if($scope.evaluacionCrediticia.telefono1 && $scope.evaluacionCrediticia.telefono1.length < telMinLength){
					reject("El teléfono 1 debe tener al menos 6 dígitos.");
				} else if ($scope.evaluacionCrediticia.telefono1){
                    tieneAlmenosUnDato = true;
                }
				if($scope.evaluacionCrediticia.telefono2 && $scope.evaluacionCrediticia.telefono2.length < telMinLength){
					reject("El teléfono 2 debe tener al menos 6 dígitos.");
				} else if ($scope.evaluacionCrediticia.telefono2){
                    tieneAlmenosUnDato = true;
                }
				if($scope.evaluacionCrediticia.correo && !$scope.validacionService.validarCorreElectronico($scope.evaluacionCrediticia.correo)){
					reject("El formato del Email no es válido.");
				}else if ($scope.evaluacionCrediticia.correo){
                    tieneAlmenosUnDato = true;
                }
				if(!tieneAlmenosUnDato){
                    reject("Debe ingresar al menos un dato de contacto.");
                }
				resolve();
			}) 
		};

		$scope.cancelarDatosContacto = () => {
			$ionicPopup.confirm({
				title: 'Alerta',
				template: `¿Seguro desea cancelar el registro de datos de contacto?`,
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			}).then(res => {
				if (res) {
					if ($scope.modalDatosContacto) {
						$scope.modalDatosContacto.hide();
					}
				}
			});
		};
		
		$ionicModal.fromTemplateUrl('datos-contacto-evaluacion-crediticia.html', {
			scope: $scope,
			animation: 'slide-in-up',
			backdropClickToClose: false,
			hardwareBackButtonClose: false
		}).then(function(modal) {
			$scope.modalDatosContacto = modal;
		});
		
		$scope.resetTipoDocumento = function (){
			//$scope.tipoDocumentoSelected = null;
			//TODO: CAMBIARA CONSTANTES
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
		};
        /**
         *
         *
         * @param scannerData
         * @param scannerDni
         * @returns {boolean}
         * @issue JIRAV3-001
         *  - mostrar mensajes de evaluación crediticia
		 * @feature V4-HU005
		 *  - no se bloquean los campos de nombre, apellidos y razón social
         */
		$scope.buscarProspecto = function(scannerData,scannerDni){
			if ($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected ===null || 
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected ===undefined){
				//alert('Debe seleccionar un tipo de documento');
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Debe seleccionar un tipo de documento' , okType: 'button-assertive'});
				return false;
			}
			if ($scope.tempProforma.proforma.vtaProspecto.codigoDocumento === '' || $scope.tempProforma.proforma.vtaProspecto.codigoDocumento === undefined){
				//alert('Debe ingresar un número de documento');
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Debe ingresar un número de documento' , okType: 'button-assertive'});
				return false;
			}
			/*INICIO - completar validacion de documento en proforma - john velasquez*/
			/*validar longitud documento, ngChange evita que se ingrese un formato invalido*/
			var tipoDocumento = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected;
			var codigoDocumento = $scope.tempProforma.proforma.vtaProspecto.codigoDocumento;
			if(!validacionService.validarDocumento(tipoDocumento,codigoDocumento,true,$scope.persona)){
				return false;			
			}
			/*FIN - completar validacion de documento en proforma - john velasquez*/

			var nroDocumento = $scope.tempProforma.proforma.vtaProspecto.codigoDocumento;
			var tmpTipoDocumentoSelected = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected;   		
			if(nroDocumento === PANDERO_DATA.RUC && $rootScope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA){
				//alert('No se encuentra habilitado para ingresar ventas para Pandero');
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Debe ingresar la venta por oficina.' , okType: 'button-assertive'});
				$scope.habilitadoGuardarProforma = false;
			}
			/*INICIO INTEGRACION EC*/
			//proformaService.getProspectoPorDocumento(tmpTipoDocumentoSelected.codigo,nroDocumento)
			proformaService.getProspectoPorDocumento(tmpTipoDocumentoSelected.codigo,nroDocumento, APP.MODULO.REGISTRO_PROSPECTO.ID)
			/*FIN INTEGRACION EC*/
			.then(function(data){	
				
				
				if(!data){
					$scope.limpiarProspecto();
					//alert('El documento del cliente no se encuentra registrado. Por favor llenar sus datos.');
					
					
	  					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'El documento del cliente no se encuentra registrado. Por favor llenar sus datos.' , okType: 'button-assertive'});	
	  				
					$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = nroDocumento;		
					$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
					$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
					$scope.tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;
					$scope.tempProforma.proforma.vtaProspecto.validado = false;
					if(scannerData!=""){
						$scope.tempProforma.proforma.vtaProspecto.validado = true;
						$scope.getDataPDF417(scannerData);
						/*INICIO INTEGRACION EC*/
						//$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
						//$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
						/*FIN INTEGRACION EC*/
					}
					if(scannerDni){
						/*INICIO INTEGRACION EC*/
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
                        //$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
						/*FIN INTEGRACION EC*/
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
						$scope.tempProforma.proforma.vtaProspecto.validado = true;
					}

				}else{
					/*INICIO INTEGRACION EC*/
					if(data.evaluacionCrediticia&&data.evaluacionCrediticia.cantidadDeConsultas!=null&&data.evaluacionCrediticia.cantidadDeConsultas!=undefined){
						cuota.cantidadDeConsultas = data.evaluacionCrediticia.cantidadDeConsultas;
					}
					/* inicio @issue JIRAV3-001 */
                    if(data.evaluacionCrediticia&&data.evaluacionCrediticia.mensaje){
                        $ionicPopup.confirm({
							title: 'Alerta',
							template: `${data.evaluacionCrediticia.mensaje}${data.evaluacionCrediticia.aprobado?'':', ¿Desea agregar datos de contacto?'}`,
							cancelText: 'Cancelar',
							okText: 'Aceptar'
						}).then(res => {
							if (res) {
								$scope.evaluacionCrediticia = data.evaluacionCrediticia;
								if($scope.lstTipoDocumento) {
									let tmpTipoDocumento = $scope.lstTipoDocumento.find(td => td.codigo == $scope.evaluacionCrediticia.tipoDocumento);
									if(tmpTipoDocumento) {
										$scope.evaluacionCrediticia.tipoDocumentoNombre = tmpTipoDocumento.descripcion;
									}	
								}
								$scope.modalDatosContacto.show();
							}	
						});
                    }
                    if(data.evaluacionCrediticia&&data.evaluacionCrediticia.aprobado==false){
                        return;
                    }
                    /* fin @issue JIRAV3-001 */
					/*FIN INTEGRACION EC*/
					if(data.errorServicio === 2){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});						
						prospectoService.envioCorreoOFAC(data.personaNombreCompletoOFAC,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
						$scope.limpiarProspecto();
						return;
					}
					$scope.tempProforma.proforma.vtaProspecto = data;	
                    			/*inicio @issue EVALCRED-51*/
					if(data.validado){
						/** inicio @feature V4-HU005 **/
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true; //false;
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true; //false;
						/** inicio @feature V4-HU005 **/
					}else{
		                        $scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
                		        $scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
					}
                			/*fin @issue EVALCRED-51*/
					$scope.tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;				
					/*inicio @issue EVALCRED-51*/
                    /*inicio @issue VTADIG2-498*/
                    //if(scannerData!="" && !data.esEquifax){
                    if(scannerData!=""){
                    /*fin @issue VTADIG2-498*/
						$scope.tempProforma.proforma.vtaProspecto.validado = true;
                        /*inicio @issue VTADIG2-498*/
                        if(!data.esEquifax){
                            $scope.getDataPDF417(scannerData);
                        }
                        /*fin @issue VTADIG2-498*/
					}				
                    			/*fin @issue EVALCRED-51*/
					$scope.tempProforma.proforma.vtaProspecto.buscado = true;			
					if($scope.tempProforma.proforma.vtaProspecto.prospectoID!==undefined){
					prospectoService.obtenerDetalleProspecto($scope.tempProforma.proforma.vtaProspecto.prospectoID)
			        .then(function(data){
						var tienepagos = $scope.tienePagos(data);
							if(!tienepagos && ($scope.tempProforma.proforma.vtaProspecto.validado !== undefined && !$scope.tempProforma.proforma.vtaProspecto.validado)){
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
						}
			        });	
					}else{
						if($scope.tempProforma.proforma.vtaProspecto.validado !== undefined && !$scope.tempProforma.proforma.vtaProspecto.validado){
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;						
					}		
					
					}	
					if(tmpTipoDocumentoSelected.codigo===$scope.TIPO_DOCUMENTO.RUC){
						if($scope.tempProforma.proforma.vtaProspecto.errorServicio !== undefined && $scope.tempProforma.proforma.vtaProspecto.errorServicio === 1){						
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'En estos momentos el servicio PIDE no se encuentra activo.' , okType: 'button-assertive'});	
						}	
					}
                    			/*inicio @issue EVALCRED-51*/
					if(scannerDni && $scope.tempProforma.proforma.vtaProspecto.validado){
                    			/*fin @issue EVALCRED-51*/
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
						$scope.tempProforma.proforma.vtaProspecto.validado = true;
					}

				}

				$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
				$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = tmpTipoDocumentoSelected;
				
				
			});
		};

		$scope.tienePagos = function(pagosResult){
	    	var tienepagos = false;
	    	for(var item in pagosResult.proformas){
				if(pagosResult.proformas[item].lstObligacionPagoBean.length>0){
					tienepagos = true;
					break;
				}
			}
			return tienepagos;
	    };

		$scope.changeTipoDocumentoSeleccionado = function (newValue, oldValue){
					$scope.busquedaConEscaner = false;
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected  = newValue;
					if(newValue.codigo=="4"){
						$scope.busquedaEscaner = true;
						$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;	
						$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
					}else{
						$scope.busquedaEscaner = false;
						$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsScaner;
						$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
					}
					$scope.tempProforma.proforma.vtaProspecto.codigoDocumento  = '';
		};

		$scope.registroAlMenosUnDatoProforma = function(){
			if($scope.tempProforma.proforma.marcaID ||
				$scope.tempProforma.proforma.articuloID ||
				$scope.tempProforma.proforma.productoID ||
				$scope.tempProforma.proforma.grupoSeleccinado ||
				$scope.tempProforma.proforma.certificadoPosicionID
				){
				return true;		
			}
			return false;		
		};

		$scope.esValidoTabProforma = function(){
			if(!$scope.tempProforma.proforma.perPuntoExhibicion){
				//alert("Por favor seleccione el punto de venta en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el punto de venta en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.tipoBienID){
				//alert("Por favor seleccione el tipo de bien en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el tipo de bien en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.marcaID && $scope.tempProforma.proforma.tipoBienID ===$scope.TIPO_BIEN.VEHICULO){
				//alert("Por favor seleccione la marca en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione la marca en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.articuloID){
				//alert("Por favor seleccione el modelo en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el modelo en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.productoID ){
				//alert("Por favor seleccione el programa en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el programa en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.grupoID ){
				//alert("Por favor seleccione el grupo en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el grupo en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.certificadoPosicionID ){
				//alert("Por favor seleccione el certificado en la pestaña de Proforma.");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor seleccione el certificado en la pestaña de Proforma." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.cuotaMensual){
				//alert("Certificado no posee cuota mensual");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Certificado no posee cuota mensual." , okType: 'button-assertive'});
				return false;
			}else if(!$scope.tempProforma.proforma.cia){
				//alert("Certificado no posee cia");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Certificado no posee cia." , okType: 'button-assertive'});
				return false;
			}
			return true;
		};
		/**
		 * 
		 * @returns {boolean}
		 * 
		 * @feature V4-HU001
		 *  - validación de los telefonos se realiza en el backend
		 */
		$scope.esValidoTabProspecto =function(){
			if(!$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected){
				//alert("Tipo de Documento es obligatorio");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Tipo de Documento es obligatorio." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto===undefined || 
				!$scope.tempProforma.proforma.vtaProspecto.codigoDocumento){
				//alert("Número de Documento es obligatorio");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Número de Documento es obligatorio." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.DNI){
				if(!$scope.tempProforma.proforma.vtaProspecto.nombre){
					//alert("Nombre es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Nombre es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno){
					//alert("Apellido Paterno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Paterno es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoMaterno && !$scope.tempProforma.proforma.vtaProspecto.buscado){
					//alert("Apellido Materno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Materno es obligatorio." , okType: 'button-assertive'});
					return false;
				}
			}
		
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.PAS || 
				$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.CE){

				if(!$scope.tempProforma.proforma.vtaProspecto.nombre){
					//alert("Nombre es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Nombre es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno){
					//alert("Apellido Paterno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Paterno es obligatorio." , okType: 'button-assertive'});
					return false;
				}
			}

			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				
				if(!$scope.tempProforma.proforma.vtaProspecto.razonSocial){
					//alert("Razón Social es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Razón Social es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.contacto){
					//alert("Contacto es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Contacto es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if($scope.tempProforma.proforma.vtaProspecto.codigoDocumento === PANDERO_DATA.RUC && $rootScope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA){
					//alert("Contacto es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la venta por oficina." , okType: 'button-assertive'});
					return false;
				}
		
			}
			
			
			if(!validacionService.validarCorreElectronico($scope.tempProforma.proforma.vtaProspecto.correo)){
				//alert("El correo es obligatorio, y debe tener el formato user@domain.com");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El correo es obligatorio, y debe tener el formato user@domain.com" , okType: 'button-assertive'});
				return false;
			}

			if(!$scope.tempProforma.proforma.vtaProspecto.telefono && !$scope.tempProforma.proforma.vtaProspecto.telefonoMovil){
				//alert("Ingrese al menos un telefóno");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese al menos un teléfono." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.telefono !== undefined && $scope.tempProforma.proforma.vtaProspecto.telefono.length > 0 && $scope.tempProforma.proforma.vtaProspecto.telefono.length < 6){
				//alert("El teléfono debe tener como mínimo 6 dígitos");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El teléfono debe tener como mínimo 6 dígitos." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.telefonoMovil !== undefined && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil.length > 0 && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil.length !== 9){
				//alert("El celular debe tener 9 dígitos");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El celular debe tener 9 dígitos." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaContratosLegal!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaContratosLegal){
				//alert("El prospecto tiene contratos en legal");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos en legal." , okType: 'button-assertive'});
				//proformaService.enviarCorreoContratosLegal(tipoDocumento.codigo,codigoDocumento, persona.personaNombreCompleto);
				$scope.enviarCorreoLegalProspecto();
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaContratosResueltos!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaContratosResueltos){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El titular tiene contratos resueltos." , okType: 'button-assertive'});
				$scope.enviarCorreoResueltosProspecto();
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaOFAC!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaOFAC){
				//alert("El prospecto tiene contratos en legal");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});
				$scope.enviarCorreoOFACProspecto();
				return false;
			}

			/*if($rootScope.me.usuarioTelefonos!==undefined){
				var index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefono);
				if(index>=0){
					alert("El teléfono no pertenece al titular");
					return false;
				}
				index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefonoMovil);
				/*if(index>=0){
					alert("El celular no pertenece al titular");
					return false;
				}				
			}*/
			/** inicio @feature V4-HU001 **/
			/*
			if($rootScope.me.usuarioTelefonos!==undefined){
				var index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefono);
				if(index>=0 && $scope.tempProforma.proforma.vtaProspecto.telefono!==""){
					//alert("El teléfono no pertenece al titular");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El teléfono no pertenece al titular." , okType: 'button-assertive'});
					// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					$scope.enviarCorreoTelefonoEDV($scope.tempProforma.proforma.vtaProspecto.telefono);
					// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					return false;
				}
				index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefonoMovil);
				if(index>=0 && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil!==""){
					//alert("El celular no pertenece al titular");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El celular no pertenece al titular." , okType: 'button-assertive'});
					// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					$scope.enviarCorreoTelefonoEDV($scope.tempProforma.proforma.vtaProspecto.telefonoMovil);
					// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					return false;
				}				
			}
			*/
			/** fin @feature V4-HU001 **/



			/*if($scope.tempProforma.proforma.cuotaMensual===undefined){
				alert("No existe cuota mensual");
				return false;	
			}*/
			/*
			if($scope.tempProforma.proforma.cia===undefined){
				alert("No existe cuota cia");
				return false;
			}*/

			return true;
		};	
		$scope.limpiarListas = function(){						
			$rootScope.tempLstProgramaProducto = undefined;
			$rootScope.tempLstGrupo = undefined;
			$rootScope.tempLstCertificado = undefined;
			$rootScope.tempLstMarca = undefined;
			$rootScope.tempLstModelo = undefined;
			$rootScope.tempLstTipoBien = undefined;
			$rootScope.dataMarcas = undefined;
			$rootScope.porcentajeCuotaAdmin = undefined;
		};
		// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
		$scope.enviarCorreoTelefonoEDV = function(telefono){
		// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			prospectoService.envioCorreoTelefonoNoPerteneceEDV(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento,telefono);
			// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
		};

		$scope.enviarCorreoOFACProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};

		$scope.enviarCorreoLegalProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			proformaService.enviarCorreoContratosLegal($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo,$scope.tempProforma.proforma.vtaProspecto.codigoDocumento, nombre);
			//prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};

		$scope.enviarCorreoResueltosProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			proformaService.enviarCorreoContratosResueltos($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo,$scope.tempProforma.proforma.vtaProspecto.codigoDocumento, nombre);
			//prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};



		

		$scope.guardar=function(){

			if($scope.registroAlMenosUnDatoProforma() && !$scope.esValidoTabProforma()){
				return false;
			}

			if(!$scope.esValidoTabProspecto()){
				return false;
			}
			


			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID===$scope.TIPO_DOCUMENTO.RUC){
				if(!validacionDeTitular.validarPersonaJuridaActiva($scope.tempProforma.proforma.vtaProspecto.personaEstado)){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se puede generar la proforma, el RUC esta de baja." , okType: 'button-assertive'});
					return;
				}
				if(!validacionDeTitular.validarPersonaJuridaHabida($scope.tempProforma.proforma.vtaProspecto.pesonaFlag_22)){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se puede generar la proforma, el RUC no esta de activo." , okType: 'button-assertive'});
					return;
				}				
			}

			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo;
			
			
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled = $scope.busquedaConEscaner;
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo!=="4"){
				// inicio @issue EVALCRED-57
				//$scope.tempProforma.proforma.vtaProspecto.validado = true;
				// fin @issue EVALCRED-57
				if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID===$scope.TIPO_DOCUMENTO.RUC){
					if(!$scope.tempProforma.proforma.vtaProspecto.personaEstado && !$scope.tempProforma.proforma.vtaProspecto.pesonaFlag_22){
						$scope.tempProforma.proforma.vtaProspecto.validado = false;
					}
				}
			}

			if($scope.registroAlMenosUnDatoProforma() && $scope.esValidoTabProforma()){
				if($scope.tempProforma.proforma.marcaID ==="undefined"){
					delete $scope.tempProforma.proforma.marcaID;
				}
				//registro de proforma
				if($scope.tempProforma.proforma.perPuntoExhibicion!==null && $scope.tempProforma.proforma.perPuntoExhibicion!== undefined ){
					$scope.tempProforma.proforma.perPuntoExhibicion.puntoExhibicionID = $scope.tempProforma.proforma.perPuntoExhibicion.codigo;	
				}
				proformaService.registrarProforma($scope.tempProforma.proforma).then(function(data){	
					var alertPopup = $ionicPopup.alert({
							 title: 'Mensaje',
							 template: data.message,
							 okText: 'Aceptar',
							 okType: 'button-assertive'
						   });
					alertPopup.then(function(res) {
							//$state.go('app.registroProspecto.proforma',{init: '0'});
							$state.go('app.detalleCliente',{prospectoID: data.data});
					$scope.tempProforma = {};
					$scope.limpiarListas();
					$scope.limpiarProspecto();
					});
					
					//$rootScope.proformaBack = 1;		  			
				});
			}else{
				//registro sólo de prospecto
				proformaService.registrarProspecto($scope.tempProforma.proforma.vtaProspecto).then(function(data){	
					var alertPopup = $ionicPopup.alert({
							 title: 'Mensaje',
							 template: data.message,
							 okText: 'Aceptar',
							 okType: 'button-assertive'
						   });
					alertPopup.then(function(res) {
							$state.go('app.registroProspecto.proforma',{init: '0'});
					});
					$scope.limpiarListas();
					$scope.limpiarProspecto();
					$scope.tempProforma = {};					
					//$rootScope.proformaBack = 1;		  			
				});
			}					
		};

		$ionicPopover.fromTemplateUrl('buscar-persona-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.scanerOptionsPopover = popover;
			//$scope.relacionadosOptionsPopover = popover;
		});

		$scope.doActionEscaner = function (index) {
			if(index===0){				
				if($scope.busquedaEscaner){
					$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
					$scope.busquedaScanerOptions = $scope.busquedaScanerOptionsScaner;
					$scope.busquedaConEscaner = false;
				}else{
					$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
					$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;
					$scope.busquedaConEscaner = false;
				}

				$scope.busquedaEscaner = !$scope.busquedaEscaner;
			}
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.hide();
			}
		};

		$scope.openScanerOptionsPopover = function ($event) {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.show($event);
			}
		};
		$scope.closeScanerOptionsPopover = function () {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.hide();
			}
		};
		
		$scope.$on('$destroy', function () {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.remove();
			}
		});

		$scope.isNumber = function (n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
		};

		$scope.scanBarcode = function() {
			try{				
				var currentPlatform = ionic.Platform.platform();  
				if(currentPlatform ===  PDF417PLUGIN.PLATFORM_ANDROID){
					escanerService.barcodeScanner($scope.scannerProcess);
				}else{
					$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
					escanerService.pdf417ScannerScan($scope.scannerProcess);
				}				
			}catch(e){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
				console.log(e.message + " -  " + e.stack);
			}

		};		

		$scope.scannerProcess = function(type, result){	
			if (type === "CODE39") {				
				$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = result.toString();
					$scope.buscarProspecto("",true);
			}else if (type === "PDF417") {									
				var DNI = result.substring(2,10);
				if($scope.isNumber(DNI)){
					$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = DNI;
					$scope.busquedaConEscaner = true;
					$scope.buscarProspecto(result);
				}else{
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El DNI escaneado no puede ser procesado." , okType: 'button-assertive'});
				}
			}
		};


		$scope.getDataPDF417 = function(data) {
			var colLength = 16;
			var rowLimit = 7;
			var dataToParse =  data.substring(0,colLength * rowLimit);
			var DNI = dataToParse.substring(2,10);
			var apellidoPaterno = "", apellidoMaterno = "", nombres = "";
			var colLength = 16, rowLimit = 7, initNamePosition = 10;
			for(var i=initNamePosition;i<(colLength*rowLimit);i++){
				if(i<=(colLength*3) + 1){
					apellidoPaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*3) + 1 && i<=(colLength*5) + 9){
					apellidoMaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*5) + 9 ){
					nombres+=dataToParse.charAt(i);
				}
			}
			$scope.tempProforma.proforma.vtaProspecto.nombre = nombres.trim();
			$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno = apellidoPaterno.trim();
			$scope.tempProforma.proforma.vtaProspecto.apellidoMaterno = apellidoMaterno.trim();
		}
		
		
		$scope.busquedaScanerOptions = [
			{
				id: 0,
				name: 'Ingresar Manualmente',
				label: "Ingresar Manualmente"
			}
		];

		$scope.busquedaScanerOptionsManual = $scope.busquedaScanerOptions;

		$scope.busquedaScanerOptionsScaner = [
			{
				id: 0,
				name: 'Escanear DNI',
				label: "Escanear DNI"
			}
		];
	})
	/*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
	//.controller('datosContratoCtrl', function($scope, $state, validacionService , shareData,tempProspecto,tempContrato,lstProgramaProducto,lstGrupo,lstCertificado,lstMarca,lstModelo,ProgramaProductoService,GrupoService,MarcaService,$ionicPopup,$rootScope,autocomplete,contratoService,TIPO_BIEN,$ionicNavBarDelegate,proformaService,ESTADO_HORA_ASAMBLEA) {
		.controller('datosContratoCtrl', function($scope, $state, validacionService , shareData,tempProspecto,tempContrato,lstProgramaProducto,lstGrupo,lstCertificado,lstMarca,lstModelo,ProgramaProductoService,GrupoService,MarcaService,$ionicPopup,$rootScope,autocomplete,contratoService,TIPO_BIEN,$ionicNavBarDelegate,proformaService,ESTADO_HORA_ASAMBLEA, me) {
	/*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
		/**Asignacion de constantes al scope**/

		$ionicNavBarDelegate.title("Datos del Contrato");
		$scope.TIPO_BIEN = TIPO_BIEN;
		$scope.validacionService = validacionService;
		$scope.vez="0";

		/*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
		$scope.me = me;
		/*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
		
		/*Pestaña Contrato*/				
		$scope.lstModelo = lstModelo;
		$scope.lstMarca =  lstMarca;
		$scope.lstProgramaProducto =  lstProgramaProducto;		
		$scope.lstGrupo = lstGrupo;
		$scope.lstCertificado = lstCertificado;
		//$scope.lstTipoBien =  lstTipoBien;//shareData.lstTipoBien;	  	  
		//$scope.disabledTipoBien = false;	
		$scope.mapSmvBienesServicios =  shareData.mapSmvBienesServicios;
		$scope.noProrratear = false;

		if(tempProspecto.prospecto.grupoBean.numeroAsambleVario){
			//alert("El número de la asamblea Varió, el número de asamblea actual es "+tempProspecto.prospecto.grupoBean.numeroAsambleaActual+".");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El número de la asamblea Varió, el número de asamblea actual es "+tempProspecto.prospecto.grupoBean.numeroAsambleaActual+"." , okType: 'button-assertive'});
		}		
		$scope.fixedDosDecimales = function(number){
			if(number){
				number = parseFloat(number.toFixed(2));
			}
			return number;
		};
		$scope.limpiarListas=function(){
			$rootScope.lstModelo = undefined;
			$rootScope.lstMarca =  undefined;
			$rootScope.lstProgramaProducto =  undefined;		
			$rootScope.lstGrupo = undefined;
			$rootScope.lstCertificado = undefined;
			$rootScope.porcentajeCuotaAdmin = undefined;
		}
		$scope.ciaOnClick=function(cia){
			if(cia===0||cia===null||cia==undefined){
				cia = null;				
			}
			return cia;
		}
		$scope.ciaOnBlur=function(cia){
			if(cia===null||cia==undefined||isNaN(cia)){
				cia = 0;				
			}			
			if(cia<(tempContrato.contrato.cuotaTotalMensualPagado-tempContrato.contrato.cuotaTotalMensual)){
				cia = tempContrato.contrato.cuotaTotalMensualPagado-tempContrato.contrato.cuotaTotalMensual;
			}
			return cia;
		}
		$scope.evaluarCia = function(cia){
			cia = parseFloat(cia);
			if(cia<0){
				cia = cia*(-1);
			}				
			if(cia>tempContrato.contrato.cia){
				cia = tempContrato.contrato.cia
			}
			cia = Math.trunc(validacionService.aDosDecimales(cia*100))/100;			
			//$scope.tempCia = cia;
			return cia;
		};
		$scope.cambiarPrograma=function(){
			$scope.tempContrato.contrato.coincidioPrograma=true;
			$scope.tipoBienTemporal=$scope.tempContrato.contrato.tipoBienID;

			$scope.lstGrupo=[];
			//$scope.lstTipoBien = [];
			$scope.lstCertificado = [];
			$scope.lstMarca = [];
			$scope.lstModelo = [];

			$scope.tempContrato.contrato.contratoNroAsambleaGrupo = undefined;	  		
			$scope.tempContrato.contrato.cuotaTotalMensual = undefined;
			$scope.tempContrato.contrato.cuotaTotalMensualPendiente = undefined;
			$scope.tempContrato.contrato.cia = undefined;
			$scope.tempContrato.contrato.montoCiaPagar = undefined;
			//$scope.tempCia = undefined;
			$scope.tempContrato.contrato.grupoID=undefined;
			$scope.tempContrato.contrato.certificadoPosicionID =undefined;
			$scope.tempContrato.contrato.marcaId = undefined;
			$scope.tempContrato.contrato.modeloId = undefined;
			$scope.tempContrato.contrato.cuotaMensualBase = undefined;

			GrupoService.getByProductoId($scope.tempContrato.contrato.programaProductoSeleccionado.productoID)
			.then(function(data){
				/** INICIO card-29374486 Anderson Estela Coronel - 22-abr-2021 */
				if($scope.tempContrato.contrato.programaProductoSeleccionado.productoID == 25 && $scope.tempContrato.contrato.tipoBienID == 2)
					$scope.lstGrupo=data.filter(grupo => grupo.grupoNumero >= "8100" && grupo.grupoNumero <= "8108");
				else
					$scope.lstGrupo=data;
				/** FIN card-29374486 Anderson Estela Coronel - 22-abr-2021 */	  			 
				$rootScope.tempLstGrupo = $scope.lstGrupo;
				if($scope.lstGrupo.length<=0){
					//alert('El programa no tiene grupos');
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'El programa no tiene grupos.' , okType: 'button-assertive'});	  			
				}

				/* inicio @issue CASOC7 */
				if($scope.tempContrato.contrato.tipoBienID === 2){
                    MarcaService.getMarca($scope.tempContrato.contrato.programaProductoSeleccionado.productoID)
                        .then(function(data){
                                $scope.lstMarca = data;
                                $rootScope.tempLstMarca = $scope.lstMarca;
                            },
                            function(error){
                                console.error(error);
                            });
                } else {
                    proformaService.inversionesInmueble().then(function(data){
                        $scope.lstModelo = data;
                    });
                }
                /* fin @issue CASOC7 */
			},function(error){
				console.error(error);
			});
		};
		$scope.cambiarGrupo = function(grupo){
			  if(!$scope.tempContrato.contrato.grupoSeleccionado){
				  return true;
			  }
			  $scope.tempContrato.contrato.grupoID = $scope.tempContrato.contrato.grupoSeleccionado.grupoID;
			  $scope.tempContrato.contrato.coincidioGrupo=true;
			  $scope.tempContrato.contrato.cuotaTotalMensual = undefined;
			  $scope.tempContrato.contrato.cuotaTotalMensualPendiente = undefined;
			  $scope.tempContrato.contrato.cia = undefined;
			  $scope.tempContrato.contrato.montoCiaPagar = undefined;
			  $scope.tempContrato.contrato.cuotaMensualBase = undefined;
			  if($scope.tempContrato.contrato.grupoSeleccionado){
					GrupoService.getCertificadosByGroupId($scope.tempContrato.contrato.grupoSeleccionado.grupoID)
					.then(function(data){	  			
						$scope.lstCertificado = data;
						if($scope.lstCertificado){
							if($scope.lstCertificado.length===0){
								//alert("Este grupo no tiene certificados");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Este grupo no tiene certificados." , okType: 'button-assertive'});
							}
						}
						$rootScope.tempLstCertificado = $scope.lstCertificado;			
					},function(error){
						
					});	  		
					$scope.tempContrato.contrato.contratoNroAsambleaGrupo=$scope.tempContrato.contrato.grupoSeleccionado.grupoNroCuota;
					if($scope.tempContrato.contrato.contratoNroAsambleaGrupo && $scope.tempContrato.contrato.contratoNroAsambleaGrupo>1){
						if($scope.tempContrato.contrato.programaProductoSeleccionado.programaID == "C4"){
							$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = parseInt($scope.tempContrato.contrato.contratoNroAsambleaGrupo)-1;
							$scope.tempContrato.contrato.contratoNroCuotasProrrateo = 0;
							$scope.tempContrato.contrato.contratoNroCuotasAbonado = 0;
							$scope.noProrratear = true;
						}else{
							$scope.tempContrato.contrato.contratoNroCuotasProrrateo = parseInt($scope.tempContrato.contrato.contratoNroAsambleaGrupo)-1;
							$scope.tempContrato.contrato.contratoNroCuotasAbonado = 0;
							$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = 0;
							$scope.noProrratear = false;
						}						
					}
			  }	  		
		};
		$scope.cambiarCertificado = function(){
			  $scope.tempContrato.contrato.coincidioCertificado=true;
			  $scope.tempContrato.contrato.cuotaTotalMensual = undefined;
			  $scope.tempContrato.contrato.cuotaTotalMensualPendiente = undefined;			  
			  if($scope.tempContrato.contrato.certificadoSeleccionado){
				$scope.tempContrato.contrato.certificadoPosicionID = $scope.tempContrato.contrato.certificadoSeleccionado.id.certificadoPosicionID;	  											
				$scope.evaluarCuotasDevengadas();
			  }	  		
		};


		$scope.devengadoOnClick=function(valor){
			if(valor===0||valor===null||valor==undefined){
				valor = null;				
			}
			return valor;
		}
		$scope.devengadoOnBlur=function(valor){
			if(valor===null||valor==undefined||isNaN(valor)){
				valor = 0;
				$scope.evaluarCuotasDevengadas()
			}			
			return valor;
		}
		/**
		 * method: 	evaluarCuotasDevengadas
		 * desc: 	evalua la cantidad cuotas devengadas para generar el contrato
		 * 			internamente se hace el calculo del prorrateo
		 * 			reglas:
		 * 				-deben sumar en total uno menos que el número de samblea
		 * params: 	devengados => [#ingresoDeVenta, #adjudicación, #prorrateo], en ese orden
		 * 			numeroAsamblea => numero de asamblea
		 * 			indiceModificado: => 0:#ingresoDeVenta; 1:#adjudicación; 2:#prorrateo
		 * return: 	si no pasa el indiceModificado	-> boolean(true: las cuotas devengadas con validas)
		 * 			si se pasa el indiceModificado	-> Array:[#ingresoDeVenta, #adjudicación, #prorrateo], arreglo que no excede el valor de (numeroAsamblea-1)
		 */
		$scope.evaluarCuotasDevengadas = function(indiceModificado){
			if(	$scope.tempContrato.contrato.contratoNroCuotasAbonado === undefined || 
				$scope.tempContrato.contrato.contratoNroCuotasAbonado === null ||
				isNaN($scope.tempContrato.contrato.contratoNroCuotasAbonado)){
				return;
			}
			if(	$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion === undefined || 
				$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion===null ||
				isNaN($scope.tempContrato.contrato.contratoNroCuotasAdjudicacion)){
				return;
			}
			if(	$scope.tempContrato.contrato.contratoNroCuotasProrrateo === undefined || 
				$scope.tempContrato.contrato.contratoNroCuotasProrrateo===null ||
				isNaN($scope.tempContrato.contrato.contratoNroCuotasProrrateo)){
				return;
			}
			
			var totalCuotasDevengadas = 0;

			if($scope.tempContrato.contrato.contratoNroAsambleaGrupo<=1){
				$scope.tempContrato.contrato.contratoNroAsambleaGrupo = 1;

				$scope.tempContrato.contrato.contratoNroCuotasAbonado = 0;
				$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = 0;
				$scope.tempContrato.contrato.contratoNroCuotasProrrateo = 0;
			}

			//ajustar catidades de cuotas devengadas
			
			$scope.tempContrato.contrato.contratoNroCuotasAbonado = Math.abs(Math.trunc($scope.tempContrato.contrato.contratoNroCuotasAbonado)); 
			$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = Math.abs(Math.trunc($scope.tempContrato.contrato.contratoNroCuotasAdjudicacion)); 
			$scope.tempContrato.contrato.contratoNroCuotasProrrateo = Math.abs(Math.trunc($scope.tempContrato.contrato.contratoNroCuotasProrrateo)); 
			

			var contarCuotasDevengadas = function(){
				totalCuotasDevengadas = 0;								
				totalCuotasDevengadas += $scope.tempContrato.contrato.contratoNroCuotasAbonado;
				totalCuotasDevengadas += $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion;
				totalCuotasDevengadas += $scope.tempContrato.contrato.contratoNroCuotasProrrateo;				
			}

			contarCuotasDevengadas();
			
			while(totalCuotasDevengadas>=$scope.tempContrato.contrato.contratoNroAsambleaGrupo){
				switch(indiceModificado){
					case 0:
						$scope.tempContrato.contrato.contratoNroCuotasAbonado = $scope.tempContrato.contrato.contratoNroCuotasAbonado-1;
					break;
					case 1:
						$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion-1;
					break;
					case 2:
						$scope.tempContrato.contrato.contratoNroCuotasProrrateo = $scope.tempContrato.contrato.contratoNroCuotasProrrateo-1;
					break;
				}
				contarCuotasDevengadas();
			}
			proformaService.getCuotaCertificado($scope.tempContrato.contrato.grupoID, 
												$scope.tempContrato.contrato.certificadoPosicionID,
												$scope.tempContrato.contrato.contratoNroCuotasProrrateo).then(
				function(data){
					data = data[0];
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					tempContrato.contrato.listaTipoCambios = data.listaTipoCambios;
					// FIN VTADIG2-185- José Martinez 12/02/2018
					$scope.tempContrato.contrato.cuotaMensualBase = data.certificadoCuotaMensualBase;
					$scope.tempContrato.contrato.cuotaMensualFinal = data.certificadoCuotaMensual;
					$scope.tempContrato.contrato.cuotaTotalMensual = validacionService.aDosDecimales($scope.tempContrato.contrato.cuotaMensualFinal*(tempContrato.contrato.contratoNroCuotasAbonado+1));
					$scope.tempContrato.contrato.cia = data.certificadoCIA;
					//INICIO VTADIG2-59 JOHN VELASQUEZ
					$scope.tempContrato.contrato.cuotaAdministrativaPct = data.cuotaAdministrativaPct;
					$scope.tempContrato.contrato.cuotaInscripcionPct = data.cuotaInscripcionPct;
					$scope.tempContrato.contrato.programaCantidadAsociados = data.programaCantidadAsociados;
					$scope.tempContrato.contrato.diaVencimiento = data.diaVencimiento;
					$scope.tempContrato.contrato.programaDuracionMeses = data.programaDuracionMeses;
					//FIN VTADIG2-59 JOHN VELASQUEZ
					if(	$scope.tempContrato.contrato.montoCiaPagar===undefined||
						$scope.tempContrato.contrato.montoCiaPagar===null||
						isNaN($scope.tempContrato.contrato.montoCiaPagar)){
						$scope.tempContrato.contrato.montoCiaPagar=$scope.tempContrato.contrato.cia;
					}else{
						$scope.tempContrato.contrato.montoCiaPagar = $scope.evaluarCia($scope.tempContrato.contrato.montoCiaPagar);
					}

					if(tempContrato.contrato.cuotaTotalMensualPagado>tempContrato.contrato.cuotaTotalMensual){
						$scope.tempContrato.contrato.montoCiaPagar = 0;
					}


					$scope.tempContrato.contrato.cuotaTotalMensualPendiente = validacionService.aDosDecimales($scope.tempContrato.contrato.cuotaTotalMensual-$scope.tempContrato.contrato.cuotaTotalMensualPagado);
					if($scope.tempContrato.contrato.cuotaTotalMensualPendiente<0){
						$scope.tempContrato.contrato.cuotaTotalMensualPendiente = 0;
					}
				}
			);
		};



		$scope.cambiarMarca = function(){
			$scope.tempContrato.contrato.modeloId = undefined;
			if($scope.tempContrato.contrato.tipoBienID==2){//vehículo; se piden los modelos
				$scope.lstModelo = [];
				
				MarcaService.getModelos($scope.tempContrato.contrato.marcaId)
				  .then(function(data){
						$scope.lstModelo = data;
						$rootScope.tempLstModelo = $scope.lstModelo;
					},function(error){
						
					});
			}
		};
		$scope.onlyTenDigitsFormato = function(){
			$scope.tempContrato.contrato.contratoFormato = Math.abs(parseInt($scope.tempContrato.contrato.contratoFormato));
			if($scope.tempContrato.contrato.contratoFormato>9999999999){
				$scope.tempContrato.contrato.contratoFormato = $scope.tempContrato.contrato.contratoFormato/10;
			}
			$scope.tempContrato.contrato.contratoFormato = Math.trunc($scope.tempContrato.contrato.contratoFormato);
		};

		$scope.cancelar=function(){			
			var confirmPopup = $ionicPopup.confirm({
				title: 'Cancelar',
				template: 'Se cancelará la generación del contrato',
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			});

			confirmPopup
			.then(function(res) {
				if(res) {
					$scope.limpiarListas();
					tempContrato.contrato={};
					tempContrato.titulares=[];
					$scope.vez="0";
					$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
				}
			});			
		};
            /**
			 *
			 * @issue JIRAV3-007
			 * - setear las cuots devengadas a la almacenadas en la proforma
             */
		$scope.autocomplete = function (){
			$scope.tempContrato=tempContrato;
			angular.forEach($scope.lstGrupo,function(grupo){
				if($scope.tempContrato.contrato.grupoID===grupo.grupoID){
					$scope.tempContrato.contrato.grupoSeleccionado = grupo;					
				}
			});			
			if($state.params.previous==="0"){
				
				$scope.tempContrato.contrato.cuotaMensualBase = $scope.tempContrato.contrato.cuotaTotalMensual;

				if($scope.tempContrato.contrato.contratoNroAsambleaGrupo && $scope.tempContrato.contrato.contratoNroAsambleaGrupo>1){
                    /* inicio @issue JIRAV3-007 */
					if($scope.tempContrato.contrato.programaProductoSeleccionado.programaID == "C4"){
						$scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = parseInt($scope.tempContrato.contrato.contratoNroAsambleaGrupo)-1;

						if($scope.tempContrato.proforma && $scope.tempContrato.proforma.cuotasIngresoVenta && $scope.tempContrato.proforma.cuotasIngresoVenta > 0){
                            $scope.tempContrato.contrato.contratoNroCuotasAbonado = $scope.tempContrato.proforma.cuotasIngresoVenta;
                        }else {
                            $scope.tempContrato.contrato.contratoNroCuotasAbonado = 0;
						}

                        if($scope.tempContrato.proforma && $scope.tempContrato.proforma.cuotasProrrateo && $scope.tempContrato.proforma.cuotasProrrateo > 0){
                            $scope.tempContrato.contrato.contratoNroCuotasProrrateo = $scope.tempContrato.proforma.cuotasProrrateo;
                        }else {
                            $scope.tempContrato.contrato.contratoNroCuotasProrrateo = 0;
                        }

                        $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion - $scope.tempContrato.contrato.contratoNroCuotasAbonado - $scope.tempContrato.contrato.contratoNroCuotasProrrateo;

                        if($scope.tempContrato.contrato.contratoNroCuotasAdjudicacion < 0){
                            $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = 0
						}

						$scope.noProrratear = true;

					}else{

						$scope.tempContrato.contrato.contratoNroCuotasProrrateo = parseInt($scope.tempContrato.contrato.contratoNroAsambleaGrupo)-1;

                        if($scope.tempContrato.proforma && $scope.tempContrato.proforma.cuotasIngresoVenta && $scope.tempContrato.proforma.cuotasIngresoVenta > 0){
                            $scope.tempContrato.contrato.contratoNroCuotasAbonado = $scope.tempContrato.proforma.cuotasIngresoVenta;
                        }else {
                            $scope.tempContrato.contrato.contratoNroCuotasAbonado = 0;
                        }

                        if($scope.tempContrato.proforma && $scope.tempContrato.proforma.cuotasAdjudicacion && $scope.tempContrato.proforma.cuotasAdjudicacion > 0){
                            $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = $scope.tempContrato.proforma.cuotasAdjudicacion;
                        }else {
                            $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion = 0;
                        }

                        $scope.tempContrato.contrato.contratoNroCuotasProrrateo = $scope.tempContrato.contrato.contratoNroCuotasProrrateo - $scope.tempContrato.contrato.contratoNroCuotasAbonado - $scope.tempContrato.contrato.contratoNroCuotasAdjudicacion;

                        if($scope.tempContrato.contrato.contratoNroCuotasProrrateo < 0){
                            $scope.tempContrato.contrato.contratoNroCuotasProrrateo = 0;
						}

						$scope.noProrratear = false;
					}
                    /* fin @issue JIRAV3-007 */
				}
				/*INICIALIZAR VALOR DE LA CIA*/
				$scope.tempContrato.contrato.montoCiaPagar=0;				
				//$scope.tempCia = $scope.tempContrato.contrato.montoCiaPagar;				
				$scope.vez="1";																				
			}else{
				$scope.tempContrato.contrato.coincidioCertificado=true;
				$scope.tempContrato.contrato.coincidioGrupo=true;
			}
			if($scope.tempContrato.contrato.programaProductoSeleccionado){
				if($scope.tempContrato.contrato.programaProductoSeleccionado.programaID == "C4"){
					$scope.noProrratear = true;
				}else{
					$scope.noProrratear = false;
				}
			}
			$scope.evaluarCuotasDevengadas();		
		};
		$scope.autocomplete();
	  /*Pe	staña Contrato*/	  
	})
    /**
     * @author -
     * @name debitoAutomatico
     * @issue VTADIG2-504
	 * @feature V4-HU005
	 *  - ya no se verifica la validación del documento de identidad
	 * @feature V4-HU006
	 *  - add dependecy injection [PersonaService]
	 *  - add function obtenerFilesDocumentoIdentidad to $scope
	 *  - obtener archivos de los titulares al cargar la pantalla por primera vez
     *  @feature V4-HU020
     *  - add DIRECCION
     *  - cliente no domiciliado
     */
	//.controller('datosTitularesCtrl', function($rootScope,$scope, $state ,validacionService, me, validacionDeTitular, persona, shareData, $http,tempProspecto,tempContrato,$ionicPopover,tempTitular,$ionicNavBarDelegate,TIPO_BIEN,TIPO_ASOCIADO, $ionicPopup,contratoService,USUARIO,TIPO_DOCUMENTO,ESTADO_HORA_ASAMBLEA, DIRECCION,configuracionGrupo,lstConvenios, debitoAutomaticoService, PersonaService) {
	.controller('datosTitularesCtrl', function($rootScope,$scope, $state ,validacionService, me, validacionDeTitular, persona, shareData, $http,tempProspecto,tempContrato,$ionicPopover,tempTitular,$ionicNavBarDelegate,TIPO_BIEN,TIPO_ASOCIADO, $ionicPopup,contratoService,USUARIO,TIPO_DOCUMENTO,ESTADO_HORA_ASAMBLEA, DIRECCION,configuracionGrupo,lstConvenios, debitoAutomaticoService, PersonaService, referenciasResult, PUNTOS_EXHIBICION) {
		$scope.lstConvenios = angular.copy(lstConvenios);
		$scope.mostrarConvenioInstitucional = configuracionGrupo.find(c=>c.configuracionID == 219 && c.configuracionValor == 1);
		$ionicNavBarDelegate.title("Datos del Cliente");
		$ionicNavBarDelegate.showBackButton(false);
		var str;
		var res;
		var month;
		var ubigeoDepartamento;
		var ubigeoProvincia;
		var ubigeoDistrito;
		var item;
		var j;
		/**Asignar constantes al scope para user en HTML**/
		$scope.TIPO_BIEN = TIPO_BIEN;
		$scope.me = me;
		$scope.validacionService = validacionService;
		  /*Pestaña Titular*/
		$scope.lstTipoDocumento=shareData.lstTipoDocumento;	
		if(!tempContrato.titulares){
			tempContrato.titulares = [];
		}
		$scope.lstMedioComunicacionPreferente=shareData.lstMedioComunicacionPreferente;	
		$scope.tempProspecto = tempProspecto;
		if( $scope.tempProspecto.prospecto.prospectoBean.personaTipoID == 'N' && 
			$scope.tempProspecto.prospecto.prospectoBean.tipoDocumento == 4 &&
			$scope.tempProspecto.prospecto.prospectoBean.validado){

				if(persona!=null){
					persona.personaNombre = $scope.tempProspecto.prospecto.prospectoBean.nombres;
					persona.personaApellidoPaterno = $scope.tempProspecto.prospecto.prospectoBean.apellidoPaterno
					persona.personaApellidoMaterno = $scope.tempProspecto.prospecto.prospectoBean.apellidoMaterno;
					persona.personaNombreCompleto = $scope.tempProspecto.prospecto.prospectoBean.nombreCompleto;
				}

		}
		/**
		 * 
		 * @feature V4-HU006
		 * @param persona
		 * @param conData indica si se obtinen tambien la data de los archivos en base64
		 */
		$scope.obtenerFilesDocumentoIdentidad = function (persona, conData){
			PersonaService.obtenerFilesDocumentoIdentidad(persona.tipoDocumentoID, persona.personaCodigoDocumento, conData)
				.then(response => {
					persona.archivosDocumentoIdentidad = response.documentos;
					persona.validarDocumentos = response.validarDocumentos;
				});
		};
		if(tempContrato.titulares.length<=0){
			console.warn("ves 1");
			$scope.tempContrato={};
			$scope.tempContrato.contrato={};
			$scope.tempContrato.contrato.modalidadDevolucion="";
			$scope.tempContrato.contrato.banco="";
			$scope.tempContrato.contrato.medioComunicacionPreferente=$scope.lstMedioComunicacionPreferente[0];
			
			$scope.tempContrato.contrato.personaTipoCorreoObj="";
			$scope.tempContrato.contrato.contratoUsoBienID="";			
			$scope.tempContrato.contrato.referidoID="";
			
			if(persona){
				persona.estadoCivilInicialID = persona.estadoCivilID;
				persona.esCasadoInicio = validacionService.esCasado(persona.estadoCivilInicialID);
				persona = validacionService.ajustarPersona(persona,shareData,"T");
				persona.esTitularPrincipal = true;

				if(!tempProspecto.prospecto.prospectoBean.validado){
					tempProspecto.prospecto.prospectoBean.validado = false;
				}				
				persona.personaDocumentoValidado = tempProspecto.prospecto.prospectoBean.validado;
				if(persona.tipoDocumentoID!==TIPO_DOCUMENTO.DNI&&persona.tipoDocumentoID!==TIPO_DOCUMENTO.RUC){
					persona.personaDocumentoValidado = true;
				}

				//inicio @issue EVALCRED-57
				if(persona.personaTelefonoMovil){
					persona.personaTelefono=persona.personaTelefonoMovil;
				}	
				// fin @issue EVALCRED-57			
				if(!persona.personaTelefono){
					persona.personaTelefono=tempProspecto.prospecto.prospectoBean.numeroCelular;
				}

				/** inicio @feature V4-HU020 **/
				persona.esResidente = persona.esResidente !== false;
				/** fin @feature V4-HU020 **/
				if(!persona.direcciones){
					if(persona.personaTipoID === 'J'){
						/** inicio @feature V4-HU020 **/
						persona.direcciones = [{direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO}];
						/** fin @feature V4-HU020 **/
					}else{
						/** inicio @feature V4-HU020 **/
						persona.direcciones = [{direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO},
											 {direccionTipoID: persona.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO}];
						/** fin @feature V4-HU020 **/
					}				
				}
				if(!persona.direcciones[0]){
					/** inicio @feature V4-HU020 **/
					persona.direcciones[0] = {direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO};
					/** fin @feature V4-HU020 **/
				}
				
				if(!persona.direcciones[0].direccionTelefono1){
					persona.direcciones[0].direccionTelefono1 = tempProspecto.prospecto.prospectoBean.numeroTelefono;
				}
				/** inicio @feature V4-HU020 **/
				persona.personasRelacionadas = persona.personasRelacionadas || []
				persona.personasRelacionadas.forEach(pr => {
					pr.esResidente = pr.esResidente !== false;
					pr.esResidenteOriginal = pr.esResidente
				});
				/** fin @feature V4-HU020 **/
				tempContrato.titulares[0]=persona;
			}else{			
				if(!tempProspecto.prospecto.prospectoBean.validado){
					tempProspecto.prospecto.prospectoBean.validado = false;
				}	
				tempProspecto.prospecto.prospectoBean.personaDocumentoValidado = tempProspecto.prospecto.prospectoBean.validado;
				
				tempContrato.titulares[0]=tempProspecto.prospecto.prospectoBean;
				tempContrato.titulares[0].apellidoMaterno=tempProspecto.prospecto.prospectoBean.apellidoMaterno;
				
				if(tempProspecto.prospecto.prospectoBean.personaTipoID){
					tempContrato.titulares[0].personaCorreoTrabajo = tempProspecto.prospecto.prospectoBean.personaCorreoTrabajo
				}else{
					tempContrato.titulares[0].personaCorreoPersonal=tempProspecto.prospecto.prospectoBean.correoElectronico;
				}				
				tempContrato.titulares[0].personaNombre=tempProspecto.prospecto.prospectoBean.nombres;
				if(tempContrato.titulares[0].personaIngresoMensual){
					tempContrato.titulares[0].personaIngresoNetoPrincipal = tempContrato.titulares[0].personaIngresoMensual;
				}					
				tempContrato.titulares[0].personaTelefono=tempProspecto.prospecto.prospectoBean.numeroCelular;
				tempContrato.titulares[0].personaCodigoDocumento=tempProspecto.prospecto.prospectoBean.numeroDocumento;
				// se realiza en la seccion de direcciones
				if(tempProspecto.prospecto.prospectoBean.tipoDocumento!==undefined){
					angular.forEach(shareData.lstTipoDocumento,function(tipoDocumento){
						if(parseInt(tempProspecto.prospecto.prospectoBean.tipoDocumento)===parseInt(tipoDocumento.codigo)){
							tempContrato.titulares[0].tipoDocumento = tipoDocumento;
							tempContrato.titulares[0].tipoDocumentoID = tipoDocumento.codigo;							
						}
					});
				}
				if(tempContrato.titulares[0].tipoDocumentoID!==TIPO_DOCUMENTO.DNI&&tempContrato.titulares[0].tipoDocumentoID!==TIPO_DOCUMENTO.RUC){
					tempContrato.titulares[0].personaDocumentoValidado = true;
				}

				/** inicio @feature V4-HU020 **/
				tempContrato.titulares[0].esResidente = tempContrato.titulares[0].esResidente !== false;
				/** fin @feature V4-HU020 **/
				if(tempContrato.titulares[0].personaTipoID === 'J'){
					/** inicio @feature V4-HU020 **/
					tempContrato.titulares[0].direcciones = [{direccionTipoID: tempContrato.titulares[0].esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO}];
					/** fin @feature V4-HU020 **/
				}else{
					/** inicio @feature V4-HU020 **/
					tempContrato.titulares[0].direcciones = [{direccionTipoID: tempContrato.titulares[0].esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO},
														   {direccionTipoID: tempContrato.titulares[0].esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO}];
					/** fin @feature V4-HU020 **/
				}
								
				tempContrato.titulares[0].direcciones[0].direccionTelefono1=tempProspecto.prospecto.prospectoBean.numeroTelefono;
				tempContrato.titulares[0].esTitularPrincipal = true;			
			}

			//PIDE
			if(tempContrato.titulares[0].personaTipoID === 'J'){
				if(tempProspecto.prospecto.prospectoBean.personaBean!== null &&
					tempProspecto.prospecto.prospectoBean.personaBean!== undefined){					
					if(tempContrato.titulares[0].personaRazonSocial===undefined){
						tempContrato.titulares[0].personaRazonSocial=tempProspecto.prospecto.prospectoBean.personaBean.personaRazonSocial;
					}				
					var dateFechaConstitucion = null;
					if(tempProspecto.prospecto.prospectoBean.personaBean.personaFechaConstitucion!== null &&
						tempProspecto.prospecto.prospectoBean.personaBean.personaFechaConstitucion !== undefined){
						str = tempProspecto.prospecto.prospectoBean.personaBean.personaFechaConstitucion;
						res = str.split("/");
						month = parseInt(res[1])-1;
						dateFechaConstitucion = new Date(res[2], month, res[0]);
						tempContrato.titulares[0].personaFechaConstitucion=dateFechaConstitucion;
					}
					// INICIO INTEGRACION EC
					if(tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp!=undefined){
					// FIN INTEGRACION EC
					if(tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.ubigeoIDAlt!== null && 
					tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.ubigeoIDAlt !== undefined){
						tempContrato.titulares[0].direcciones[0].ubigeoIDAlt= tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.ubigeoIDAlt;
						ubigeoJuridico = tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.ubigeoIDAlt;
						ubigeoDepartamento = ubigeoJuridico.substring(0, 2);
						ubigeoProvincia = ubigeoJuridico.substring(2, 4);
						ubigeoDistrito = ubigeoJuridico.substring(4, 6);
						tempContrato.titulares[0].direcciones[0].departamentoID = ubigeoDepartamento.trim();
						tempContrato.titulares[0].direcciones[0].provinciaID = ubigeoProvincia.trim();
						tempContrato.titulares[0].direcciones[0].distritoID = ubigeoDistrito.trim();
					}
					if(tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.direccionDetalle!=="-"){				
						tempContrato.titulares[0].direcciones[0].direccionDetalle=tempProspecto.prospecto.prospectoBean.personaBean.direccionTmp.direccionDetalle;
					}					
					// INICIO INTEGRACION EC	
					}
					// FIN INTEGRACION EC

					if(!tempContrato.titulares[0].personasRelacionadas){
						if(tempProspecto.prospecto.prospectoBean.personaBean.personasRelacionadas){
							tempContrato.titulares[0].personasRelacionadas = tempProspecto.prospecto.prospectoBean.personaBean.personasRelacionadas;
						}
						
					}						
				}
			}	


			tempContrato.titulares[0].asociadoTipoID=TIPO_ASOCIADO.TITULAR;			

			tempContrato.titulares[0].proformaID=tempProspecto.prospecto.proformaID;

			if(tempContrato.titulares[0].personaFechaCreacion){
				tempContrato.titulares[0].personaFechaCreacion = undefined;
			}
			if(tempContrato.titulares[0].personaFechaModificacion){
				tempContrato.titulares[0].personaFechaModificacion = undefined;
			}

			if(tempContrato.titulares[0].personaNombre===undefined){
				tempContrato.titulares[0].personaNombre=tempProspecto.prospecto.prospectoBean.nombres;
			}
			if(tempContrato.titulares[0].personaApellidoPaterno===undefined){
				tempContrato.titulares[0].personaApellidoPaterno=tempProspecto.prospecto.prospectoBean.apellidoPaterno;
			}
			if(tempContrato.titulares[0].personaApellidoMaterno===undefined){
				if(!tempProspecto.prospecto.prospectoBean.apellidoMaterno) tempProspecto.prospecto.prospectoBean.apellidoMaterno = '';
				tempContrato.titulares[0].personaApellidoMaterno=tempProspecto.prospecto.prospectoBean.apellidoMaterno;
			}
			if(tempContrato.titulares[0].personaNombreCompleto===undefined){
				tempContrato.titulares[0].personaNombreCompleto=tempContrato.titulares[0].personaApellidoPaterno+" "+tempContrato.titulares[0].personaApellidoMaterno+" "+tempProspecto.prospecto.prospectoBean.nombres;
			}

			if(tempContrato.titulares[0].personaCorreoPersonal===undefined){
				tempContrato.titulares[0].personaCorreoPersonal=tempProspecto.prospecto.prospectoBean.correoElectronico;
			}

			if(tempContrato.titulares[0].personaTelefono===undefined){
				tempContrato.titulares[0].personaTelefono=tempProspecto.prospecto.prospectoBean.numeroCelular;
			}

			if(tempContrato.titulares[0].personaGrupoEconomico===undefined){
				tempContrato.titulares[0].personaGrupoEconomico=tempProspecto.prospecto.prospectoBean.personaGrupoEconomico;
			}
			if(tempContrato.titulares[0].personaSujetoObligado===undefined){
				tempContrato.titulares[0].personaSujetoObligado=tempProspecto.prospecto.prospectoBean.personaSujetoObligado;
			}
			if(tempContrato.titulares[0].personaPrevLavadoActivos===undefined){
				tempContrato.titulares[0].personaPrevLavadoActivos=tempProspecto.prospecto.prospectoBean.personaPrevLavadoActivos;
			}
			if(tempContrato.titulares[0].personaActividadMinera===undefined){
				tempContrato.titulares[0].personaActividadMinera=tempProspecto.prospecto.prospectoBean.personaActividadMinera;
			}
			
			if(tempContrato.titulares[0].personaRazonSocial===undefined){
				tempContrato.titulares[0].personaRazonSocial=tempProspecto.prospecto.prospectoBean.personaRazonSocial;	
			}
			/*VALIDACION VINCULADO (START)*/
			tempContrato.titulares[0].personaEsEmpleado = tempProspecto.prospecto.prospectoBean.personaEsEmpleado;
			if(tempContrato.titulares[0].personaEsEmpleado){
				var newVinculada;
				if(!tempContrato.titulares[0].personasVinculadas){
					tempContrato.titulares[0].personasVinculadas = [];
					newVinculada = JSON.parse(JSON.stringify(tempContrato.titulares[0]));
					newVinculada.personasVinculadas = undefined;
					newVinculada.personasRelacionadas = undefined;
					newVinculada.ingresoTotalMensual = undefined;
					if(!newVinculada.personaTipoRelacionID){
						newVinculada.personaTipoRelacionID = "8";
					}
					tempContrato.titulares[0].personasVinculadas.push(newVinculada);
				}else{
					var yaVinculado = false;
					angular.forEach(tempContrato.titulares[0].personasVinculadas,function(vinculada){
						if(vinculada.tipoDocumentoID===tempContrato.titulares[0].tipoDocumentoID&&vinculada.personaCodigoDocumento===tempContrato.titulares[0].personaCodigoDocumento){
							yaVinculado=true;
						}
					});

					if(!yaVinculado){
						newVinculada = JSON.parse(JSON.stringify(tempContrato.titulares[0]));
						newVinculada.personasVinculadas = undefined;
						newVinculada.personasRelacionadas = undefined;
						newVinculada.ingresoTotalMensual = undefined;
						if(!newVinculada.personaTipoRelacionID){
							newVinculada.personaTipoRelacionID = "8";
						}	
						tempContrato.titulares[0].personasVinculadas.push(newVinculada);
					}
					
				}				
			}
			/*VALIDACION VINCULADO (END)*/			
			// INICIO VTADIG2-185- José Martinez 12/02/2018
			angular.forEach(tempContrato.titulares, function (titular) {
				
				if(titular.personaID && !titular.monedaIDIngresoMensual){
					//alert(titular.personaNombre+ ' no tiene registrado el tipo de moneda');
					titular.monedaIDIngresoMensual='2';
				}else if(titular.personaID && titular.monedaIDIngresoMensual){
					titular.monedaIDIngresoMensual = titular.monedaIDIngresoMensual.toString();	
				}				

				if(!titular.personaID){
					titular.monedaIDIngresoMensual='1';
				}

				titular.personaIngresoMensualDolares = validacionService.obtenerIngresoMensualDolares(titular.monedaIDIngresoMensual,titular.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);
				if(titular.personasRelacionadas!== undefined){
					angular.forEach(titular.personasRelacionadas, function (relacionado) {

						if(relacionado.personaID && !relacionado.monedaIDIngresoMensual){
							//alert(relacionado.personaNombre+ ' no tiene registrado el tipo de moneda');
							relacionado.monedaIDIngresoMensual='2';
						}else if(relacionado.personaID && relacionado.monedaIDIngresoMensual){
							relacionado.monedaIDIngresoMensual = relacionado.monedaIDIngresoMensual.toString();	
						}				

						if(!relacionado.personaID){
							relacionado.monedaIDIngresoMensual='1';
						}

						if(relacionado.personaIngresoMensual){
							relacionado.personaIngresoMensualDolares = validacionService.obtenerIngresoMensualDolares(relacionado.monedaIDIngresoMensual,relacionado.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);
						}
					});
				}
				/** inicio @feature V4-HU006 **/
				$scope.obtenerFilesDocumentoIdentidad(titular);
				/** fin @feature V4-HU006 **/
			});
			// FIN VTADIG2-185- José Martinez 12/02/2018
		}
		
		$scope.tempContrato=tempContrato;

		$scope.lstEstadoCivil = shareData.lstEstadoCivil;
		$scope.lstSexo=shareData.lstSexo;
		$scope.lstPais=shareData.lstPais;
		$scope.lstDepartamento=shareData.lstDepartamento;
		$scope.lstProvincia=shareData.lstProvincia;
		$scope.lstDistrito=shareData.lstDistrito;
		$scope.lstTipoVia=shareData.lstTipoVia;
		$scope.lstTipoZona=shareData.lstTipoZona;
		$scope.lstModalidadDevolucion=shareData.lstModalidadDevolucion;	
		$scope.lstBancos=shareData.lstBancos;
		
		$scope.lstActividadLaboral=shareData.lstActividadLaboral;
				//INICIO VTADIG2-185- José Martinez 12/02/2018
		$scope.lstTipoMoneda=shareData.lstTipoMoneda;
		//FIN VTADIG2-185- José Martinez 12/02/2018		
		
		$scope.lstUsosVehiculo=shareData.lstUsosVehiculo;
		$scope.referidos = referenciasResult;
		$scope.lstTiposCorreo=angular.copy(shareData.lstTiposCorreo);	
		for (var i = 0; i < $scope.lstTiposCorreo.length; i++) {
			if($scope.lstTiposCorreo[i].codigo==="3"){
				$scope.lstTiposCorreo.splice(i,1);
			}
		}

		/*HALLANDO EL TITULAR PRINCIPAL*/
		$scope.indiceTitularPrincipal = undefined;
		angular.forEach(tempContrato.titulares,function(titular,$index){
			if(titular.esTitularPrincipal){
				$scope.indiceTitularPrincipal = $index;
			}
		});
		$scope.actualizarTelefonos = function(i){
			$scope.lstTelefonos=[];
			if(tempContrato.titulares[i].personaTelefono && tempContrato.titulares[i].personaTelefono !==""){
				$scope.lstTelefonos.push({'codigo':1,'descripcion':'Celular - ' + tempContrato.titulares[i].personaTelefono});
			}
			if(tempContrato.titulares[i].direcciones[0].direccionTelefono1 && tempContrato.titulares[i].direcciones[0].direccionTelefono1 !==""){
				$scope.lstTelefonos.push({'codigo':2,'descripcion':'Fijo - '  + tempContrato.titulares[i].direcciones[0].direccionTelefono1});
			}
			if(tempContrato.titulares[i].direcciones[1] !== null && tempContrato.titulares[i].direcciones[1] !== undefined){
				if(tempContrato.titulares[i].direcciones[1].direccionTelefono1 && tempContrato.titulares[i].direcciones[1].direccionTelefono1 !==""){
					$scope.lstTelefonos.push({'codigo':3,'descripcion': 'Laboral - '  + tempContrato.titulares[i].direcciones[1].direccionTelefono1});
				}
			}
		}
		$scope.limpiarListaDatosTitular = function(){			
				$rootScope.tempLstDatosTitular = {};
		}
		$scope.limpiarListaDatosTitular();
		if($scope.indiceTitularPrincipal!==undefined){
			var i = $scope.indiceTitularPrincipal;
			if(tempContrato.titulares[i]){
				if(!tempContrato.contrato.medioComunicacionPreferente){
					tempContrato.contrato.medioComunicacionPreferente = $scope.lstMedioComunicacionPreferente[0];
					if(tempContrato.contrato.personaTipoCorreoObj === undefined){
						tempContrato.contrato.personaTipoCorreoObj = $scope.lstTiposCorreo[1];	
					}
				}
			}
			$scope.actualizarTelefonos(i);
		}
		var tempPersonaTipoCorreoObj;
		if(tempContrato.contrato){
			if(tempContrato.contrato.medioComunicacionPreferente){
				if(parseInt(tempContrato.contrato.medioComunicacionPreferente.codigo)==2){//correoElectronico
					if($scope.lstTiposCorreo){
						if(tempContrato.contrato.personaTipoCorreoObj){
							angular.forEach($scope.lstTiposCorreo,function(item){
								if(parseInt(tempContrato.contrato.personaTipoCorreoObj.codigo)==parseInt(item.codigo)){
									tempPersonaTipoCorreoObj = item;
								}
							});
							tempContrato.contrato.personaTipoCorreoObj = tempPersonaTipoCorreoObj;
						}
					}
				}
				if(parseInt(tempContrato.contrato.medioComunicacionPreferente.codigo)==3){//telefono
					if($scope.lstTelefonos){
						if(tempContrato.contrato.personaTipoCorreoObj){
							angular.forEach($scope.lstTelefonos,function(item){
								if(parseInt(tempContrato.contrato.personaTipoCorreoObj.codigo)==parseInt(item.codigo)){
									tempPersonaTipoCorreoObj = item;
								}
							});
							tempContrato.contrato.personaTipoCorreoObj = tempPersonaTipoCorreoObj;
						}		
					}	
				}			
			}
		}
		$scope.indexTitular=0;

	  /*Pestaña Titular*/	
	  		tempContrato.contrato.medioComunicacionPreferente = tempContrato.contrato.medioComunicacionPreferente || $scope.lstMedioComunicacionPreferente.find(item => item.codigo == tempContrato.contrato.medioComunicacionPreferenteId);

			angular.forEach($scope.lstModalidadDevolucion, function (item) {
				if(item.codigo == tempContrato.contrato.modalidadDevolucionID){
					tempContrato.contrato.modalidadDevolucion = item;
				}
			});	

		$scope.cambiarModDevolucion=function(){
		};

		$scope.tempTitular = tempTitular;

		for(var k=0 ; k<$scope.tempContrato.titulares.length ; k++){
			if($scope.tempContrato.titulares[k].esTitularPrincipal){
				if($scope.tempContrato.titulares[k].personaCorreoTrabajo == "" || $scope.tempContrato.titulares[k].personaCorreoTrabajo == undefined){
					$scope.lstTiposCorreo.splice(0,1);
				}
			}			
		}
		
		
		$scope.addNewTitular = function(){
			console.log("TITULARES");
			console.log(tempContrato.titulares);
			$scope.tempTitular.titular = angular.copy({});
			$scope.tempTitular.titular.indexTitular=tempContrato.titulares.length;
			console.log("pre adding titular "+$scope.tempTitular.titular.indexTitular);
			$state.go('app.datosTitular',{asociadoTipoID: TIPO_ASOCIADO.TITULAR},{reload: true, inherit: false, notify: true});		
		};
		$scope.addNewResplado=function(){
			$scope.tempTitular.titular = angular.copy({});
			$scope.tempTitular.titular.indexTitular=tempContrato.titulares.length;		
			$scope.tempTitular.titular.personaTipoID = 'N';
			$state.go('app.datosTitular',{asociadoTipoID: TIPO_ASOCIADO.RESPALDO},{reload: true, inherit: false, notify: true});		
		};
		/**
		 * 
		 * @param index
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.doAction = function (index) {
			if(index===0){			
				console.log("editing titular "+$scope.indexToEdit);
				console.log("doAction - tempContrato.titulares[$scope.indexTitular];");
				console.log(tempContrato.titulares[$scope.indexToEdit]);

				$scope.tempTitular.titular = tempContrato.titulares[$scope.indexToEdit];			
				$scope.tempTitular.titular.indexTitular=$scope.indexToEdit;
				if($scope.tempTitular.titular.paisID){
					$scope.tempTitular.titular.paisID = $scope.tempTitular.titular.paisID+'';
				}
				/** inicio @feature V4-HU020 **/
				if($scope.tempTitular.titular.paisResidenciaId){
					$scope.tempTitular.titular.paisResidenciaId = $scope.tempTitular.titular.paisResidenciaId+'';
				}
				/** fin @feature V4-HU020 **/
				tempTitular.titular = angular.copy($scope.tempTitular.titular);
				console.log("pre state go temp titular.titular");
				console.log(tempTitular.titular);
				$state.go('app.datosTitular');
			}
			if(index==1){	
				if(!tempContrato.titulares[$scope.indexToEdit].esTitularPrincipal){
					if(tempContrato.titulares[$scope.indexToEdit].asociadoTipoID == "R"){
						$scope.hayRespaldo = false;
					}
					tempContrato.titulares.splice($scope.indexToEdit,1);
				}else{
				//alert("Este titular no se puede eliminar");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Este titular no se puede eliminar." , okType: 'button-assertive'});
				}
			}
			if(index==2){
				$scope.hacerTitular($scope.indexToEdit);
			}
			$scope.titularOptionsPopover.hide();
		};
		$scope.hacerTitular=function($indexTitular){
			//angular.forEach($scope.tempContrato.titulares,function(titular,$index){
			for(var i = 0; i < $scope.tempContrato.titulares.length; i++){
				if(i === $indexTitular){
					$scope.tempContrato.titulares[i].esTitularPrincipal = true;
				}else{
					$scope.tempContrato.titulares[i].esTitularPrincipal = false;
				}
			}
			$scope.actualizarTelefonos($indexTitular);

			$scope.lstTiposCorreo=angular.copy(shareData.lstTiposCorreo);	
			for (var i = 0; i < $scope.lstTiposCorreo.length; i++) {
				if($scope.lstTiposCorreo[i].codigo==="3"){
					$scope.lstTiposCorreo.splice(i,1);
				}
				if($scope.tempContrato.contrato.personaTipoCorreoObj.codigo === $scope.lstTiposCorreo[i].codigo){
					$scope.tempContrato.contrato.personaTipoCorreoObj = $scope.lstTiposCorreo[i];
				}
			}
			
			for(var k=0 ; k<$scope.tempContrato.titulares.length ; k++){
				if($scope.tempContrato.titulares[k].esTitularPrincipal){
					if($scope.tempContrato.titulares[k].personaCorreoTrabajo == "" || $scope.tempContrato.titulares[k].personaCorreoTrabajo == undefined){
						$scope.lstTiposCorreo.splice(0,1);
					}
				}
			}
			//});		
			//tempContrato.titulares.unshift(tempContrato.titulares.splice($indexTitular,1)[0]);
		};


		$ionicPopover.fromTemplateUrl('titular-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.titularOptionsPopover = popover;
		});
		$scope.indexToEdit = 0;
		$scope.TIPO_ASOCIADO = TIPO_ASOCIADO;
		$scope.openTitularOptionsPopover = function ($event,indexTitular) {
			$scope.indexToEdit = indexTitular;
			$scope.indexTitular = indexTitular;
			$scope.tempTitularAsociadoTipoID = tempContrato.titulares[indexTitular].asociadoTipoID;
			$scope.titularOptionsPopover.show($event);
		};
		$scope.closeTitularOptionsPopover = function () {
			$scope.titularOptionsPopover.hide();
		};
		$scope.$on('$destroy', function () {
			$scope.titularOptionsPopover.remove();
		});

		$scope.titularOptions = [
			{
				id: 0,
				name: 'Editar',
				label: "Editar"
			},
			{
				id: 1,
				name: 'Eliminar',
				label: "Eliminar"
			},
			{
				id: 2,
				name: 'Titular',
				label: "Hacer titular principal"
			}
		];

		$scope.comprobar4por1 = function(valor){
			if(parseFloat(valor)>4*validacionService.aDosDecimales($scope.tempContrato.contrato.cuotaMensualFinal)){
				return true;
			}else{
				return false;
			}
		};

		$scope.hayRespaldo = false;
		//comitear lo que este en esta funcion
		$scope.validar4por1Titulares=function(validarRepresentanteLegal){
			var faltanRegistrarIngresos = false;
			var ingresoTotalMensualTitulares = 0;
			var representanteLegal = false;	
			if($scope.tempContrato.titulares[0].personaTipoID === 'J'){
				$scope.tempContrato.titulares[0].ingresoTotalMensualDolares = $scope.tempContrato.titulares[0].personaIngresoMensual;
				
				//INICIO VTAPROD-188 - Luis Amat 06/03/2019
				$scope.tempContrato.titulares[0].cumpleEl4por1 = $scope.comprobar4por1($scope.tempContrato.titulares[0].ingresoTotalMensualDolares);
				//FIN VTAPROD-188 - Luis Amat 06/03/2019

				if(validarRepresentanteLegal){
					var representanteLegal =_.find($scope.tempContrato.titulares[0].personasRelacionadas, function(persona){ return persona.personaTipoRelacionID == 17; });
					return representanteLegal;
				}

				return true;
			}			
			angular.forEach($scope.tempContrato.titulares,function(titular){
				if(titular.personaIngresoMensual === undefined||titular.personaIngresoMensual===null){
					faltanRegistrarIngresos = true;
				}
				if(titular.asociadoTipoID===TIPO_ASOCIADO.TITULAR&&titular.personaTipoID==='N'){															

					var esCasado = false;//o era casado
					var conyugeFormaPardeDeIngresosFamiliares = false;
					var conyuge;					

					if(titular.personasRelacionadas){
						angular.forEach(titular.personasRelacionadas,function(personaRelacionada){
							if(parseInt(personaRelacionada.personaTipoRelacionID)===1||parseInt(personaRelacionada.personaTipoRelacionID)===16){//esCasado
								esCasado = true;
								conyuge = personaRelacionada;
								conyuge.personaIngresoMensualDolares = validacionService.obtenerIngresoMensualDolares(conyuge.monedaIDIngresoMensual,conyuge.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);
								if(conyuge.personaIngresoMensualCalif== true && (conyuge.personaIngresoMensual===undefined||conyuge.personaIngresoMensual===null)){
									faltanRegistrarIngresos = true;	
									conyuge.personaIngresoMensualCalif = false;								
								}else {
									/**INICIO VTADIG2-59 - JOHN VELASQUEZ		      **/
									if(conyuge.personaIngresoMensualCalif == undefined||conyuge.personaIngresoMensualCalif==null){
									/**FIN VTADIG2-59 - JOHN VELASQUEZ	     	      **/
										// INICIO VTADIG2-185- José Martinez 12/02/2018
										//if(conyuge.personaIngresoMensual>0){
										//	conyuge.personaIngresoMensualCalif = true;
										//}else{
										//	conyuge.personaIngresoMensualCalif = false;
										//}
										conyuge.personaIngresoMensualCalif = false;
										// FIN VTADIG2-185- José Martinez 12/02/2018

									/**INICIO VTADIG2-59 - JOHN VELASQUEZ		      **/
									}
									/**FIN VTADIG2-59 - JOHN VELASQUEZ		          **/
								}
								if(conyuge.personaIngresoMensualCalif){
									conyugeFormaPardeDeIngresosFamiliares = true;
								}
							}
						});
					}
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					if(!esCasado){
						titular.ingresoTotalMensualDolares = validacionService.aDosDecimales(titular.personaIngresoMensualDolares);
					}
					else if(!conyugeFormaPardeDeIngresosFamiliares){
						titular.ingresoTotalMensualDolares = validacionService.aDosDecimales(titular.personaIngresoMensualDolares);
					}
					else if(conyugeFormaPardeDeIngresosFamiliares){
						titular.ingresoTotalMensualDolares = validacionService.aDosDecimales(parseFloat(titular.personaIngresoMensualDolares)+parseFloat(conyuge.personaIngresoMensualDolares));
					}
					// FIN VTADIG2-185- José Martinez 12/02/2018
					/**INICIO VTADIG2-59 - JOHN VELASQUEZ		      **/
					titular.cumpleEl4por1 = $scope.comprobar4por1(titular.ingresoTotalMensualDolares);
					/**FIN VTADIG2-59 - JOHN VELASQUEZ			      **/
					ingresoTotalMensualTitulares += parseFloat(titular.ingresoTotalMensualDolares);
					ingresoTotalMensualTitulares = validacionService.aDosDecimales(ingresoTotalMensualTitulares);
				}
				// INICIO VTADIG2-185- José Martinez 12/02/2018
				//if(titular.asociadoTipoID===TIPO_ASOCIADO.TITULAR&&titular.personaTipoID==='J'){
				//	titular.ingresoTotalMensual = parseFloat(titular.personaIngresoMensual);
				//	ingresoTotalMensualTitulares += titular.ingresoTotalMensual;
				//	ingresoTotalMensualTitulares = validacionService.aDosDecimales(ingresoTotalMensualTitulares);
				//}
				// FIN VTADIG2-185- José Martinez 12/02/2018
			});
			if(faltanRegistrarIngresos){
				return true;
			}else{
				return $scope.comprobar4por1(ingresoTotalMensualTitulares);
			}
		};
		$scope.validar4por1=function(){		
			var cumpleEl4por1;
			var ingresoTotalMensualTitulares = 0;
			var ingresoTotalMensualRespaldo = 0;
			angular.forEach($scope.tempContrato.titulares,function(titular){
				if(titular.asociadoTipoID===TIPO_ASOCIADO.TITULAR){
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					ingresoTotalMensualTitulares += parseFloat(titular.ingresoTotalMensualDolares);
					// FIN VTADIG2-185- José Martinez 12/02/2018
				}else if(titular.asociadoTipoID===TIPO_ASOCIADO.RESPALDO){
					$scope.hayRespaldo = true;
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					ingresoTotalMensualRespaldo += parseFloat(titular.personaIngresoMensualDolares);
					// FIN VTADIG2-185- José Martinez 12/02/2018
				}
			});
			if($scope.hayRespaldo){			
				$scope.tempContrato.contrato.ingresoTotalMensualAcumulado = ingresoTotalMensualRespaldo;
			}else{
				$scope.tempContrato.contrato.ingresoTotalMensualAcumulado = ingresoTotalMensualTitulares;			
			}
			$scope.tempContrato.contrato.cumpleEl4por1 = $scope.comprobar4por1($scope.tempContrato.contrato.ingresoTotalMensualAcumulado);			
		};
		$scope.validar4por1Titulares(false);
		$scope.validar4por1();
		/**
		 * @feature V4-HU005
		 *  - no se bloquea por valdiación de documento de identidad
		 */
		$scope.continuar=function(){ 
			/*DATOS CONTRATO*/
			var hayEmpleados = false;

			/*titular = Titular o Respaldo*/
			//personasRelacionadas
			//personaDocumentoValidado 
			for (const titular of $scope.tempContrato.titulares) {
				if(titular.personaTipoID == 'J'){
					if($rootScope.me.documentoDigital){
						/** inicio @feature V4-HU005 **/
						/*
						if(titular.personasRelacionadas){
							for (const respaldo of titular.personasRelacionadas) {
								if(respaldo.tipoDocumentoID==TIPO_DOCUMENTO.DNI&&!respaldo.personaDocumentoValidado){
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Hay Representantes Legales cuyos DNI no han sido validados por el escaner." , okType: 'button-assertive'});
									return;	
								}
							}							
						}
						*/
						/** fin @feature V4-HU005 **/

					}
				}else{
					if($rootScope.me.documentoDigital && !titular.personaDocumentoValidado){
						/** inicio @feature V4-HU005 **/
						/*
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Hay titulares cuyos DNI no han sido validados por el escaner." , okType: 'button-assertive'});
						return;
						*/
						/** fin @feature V4-HU005 **/
					}
				}
				
			}

			if($scope.validar4por1Titulares(true)){//no neesitan respaldo
				var indexRespaldo = -1;
				angular.forEach($scope.tempContrato.titulares,function(titular,$index){
					if(titular.asociadoTipoID===TIPO_ASOCIADO.RESPALDO){
						indexRespaldo = $index;
					}
					if(titular.personaEsEmpleado&&titular.asociadoTipoID!==TIPO_ASOCIADO.RESPALDO){
						hayEmpleados = true;
					}
				});
				if(indexRespaldo!==-1){
					$scope.tempContrato.titulares.splice(indexRespaldo,1);
				}
				$scope.hayRespaldo = false;
			}
			if(hayEmpleados&&$scope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA){
			//alert("Hay titulares vinculados a pandero, debe ingresar la venta por oficina");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Hay titulares vinculados a pandero, debe ingresar la venta por oficina." , okType: 'button-assertive'});
				return;
			}
			$scope.validar4por1();
			
			var datosTitularesValidos = true;
			var showAlerts = false;
			
			var todosTienenCorreoLaboral = true;
			var todosTienenTelefonoLaboral = true;
			$scope.indiceTitularPrincipal = undefined;
			angular.forEach($scope.tempContrato.titulares,function(titular,$index){
				var datoTitularvalido = validacionDeTitular.validardatosPersona(titular, showAlerts, $scope.tempContrato); 
				if(!datoTitularvalido){
					if(titular.personaTipoID === "N"){
						$rootScope.respuestaError.mensaje="Complete los datos del titular "+(titular.personaNombreCompleto?titular.personaNombreCompleto:titular.personaNombre);
					}else{
						$rootScope.respuestaError.mensaje="Complete los datos del titular "+titular.personaRazonSocial.trim();
					}					
				//alert($rootScope.respuestaError.mensaje);
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: $rootScope.respuestaError.mensaje , okType: 'button-assertive'});							
				}
				datosTitularesValidos = datosTitularesValidos&&datoTitularvalido;
				/*verificar si tiene correo laboral*/
				if(titular.asociadoTipoID==="T"){
					if(!titular.personaCorreoTrabajo){
						todosTienenCorreoLaboral = false;
					}
					if(titular.direcciones[1]){
						if(!titular.direcciones[1].direccionTelefono1){
							todosTienenTelefonoLaboral = false;
						}
					}else{
						todosTienenTelefonoLaboral = false;
					}
				}
				if(titular.esTitularPrincipal){
					$scope.indiceTitularPrincipal = $index;
				}
			});
			if(!datosTitularesValidos){

			}else if(!$scope.tempContrato.contrato.cumpleEl4por1){			
				if(!$scope.hayRespaldo){
					if($scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID==='N'){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Sus ingresos no cubren la condición de 4/1 para Ingresar la venta, debe acreditar mayores ingresos, mancomunar contrato o adicionar un Respaldo Familiar." , okType: 'button-assertive'});
					}
					if($scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID==='J'){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Sus ingresos no cubren la condición de 4/1 para Ingresar la venta, debe acreditar mayores ingresos o mancomunar contrato." , okType: 'button-assertive'});
					}					
				}else{
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Los ingresos de su Respaldo Familiar no cubren la condición de 4/1 para Ingresar la venta." , okType: 'button-assertive'});
				}
			}else if(!$scope.tempContrato.contrato.modalidadDevolucion){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese la modalidad de Devolución." , okType: 'button-assertive'});
			}else if(!$scope.tempContrato.contrato.banco && $scope.tempContrato.contrato.modalidadDevolucion.codigo!=='6'){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese la entidad bancaria." , okType: 'button-assertive'});
			}else if((!$scope.tempContrato.contrato.numeroCuenta||$scope.tempContrato.contrato.numeroCuenta<=0) && $scope.tempContrato.contrato.modalidadDevolucion.codigo!=='6'){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese el número de cuenta." , okType: 'button-assertive'});
			}else if(!$scope.tempContrato.contrato.medioComunicacionPreferente){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese el medio de comunicación preferente." , okType: 'button-assertive'});
			}else if($scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='2' && $scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID!='J' && !$scope.tempContrato.contrato.personaTipoCorreoObj){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccione el tipo de correo electrónico." , okType: 'button-assertive'});		
			}else if(
					parseInt($scope.tempContrato.contrato.medioComunicacionPreferente.codigo) ===2//correo 
					&& $scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID!=='J'
					&& !$scope.tempContrato.contrato.personaTipoCorreoObj){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccione el correo electrónico." , okType: 'button-assertive'});
			}else if(
					parseInt($scope.tempContrato.contrato.medioComunicacionPreferente.codigo) ===2//correo 
					&& $scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID!=='J'
					&& parseInt($scope.tempContrato.contrato.personaTipoCorreoObj.codigo) === 2
					&&!todosTienenCorreoLaboral){//correo laboral
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Si el el medio de comunicación preferente es correo laboral todos los titulares deben tener correo laboral." , okType: 'button-assertive'});
			}else if(
					parseInt($scope.tempContrato.contrato.medioComunicacionPreferente.codigo) === 3//telefono 
					&& $scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID!=='J'
					&& !$scope.tempContrato.contrato.personaTipoCorreoObj){
				//alert("Seleccion el teléfono");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccion el teléfono." , okType: 'button-assertive'});
			}else if(
					parseInt($scope.tempContrato.contrato.medioComunicacionPreferente.codigo) === 3//telefono 
					&& $scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID!=='J'
					&& parseInt($scope.tempContrato.contrato.personaTipoCorreoObj.codigo) === 3
					&&!todosTienenTelefonoLaboral){//correo laboral
			//alert("Si el el medio de comunicación preferente es teléfono laboral todos los titulares deben tener teléfono laboral");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Si el el medio de comunicación preferente es teléfono laboral todos los titulares deben tener teléfono laboral." , okType: 'button-assertive'});
			}else if($scope.tempContrato.contrato.tipoBienID === 2 && !$scope.tempContrato.contrato.contratoUsoBienID){
			//alert("Seleccione el uso del bien");
			var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccione el uso del bien." , okType: 'button-assertive'});
			}else if(($scope.tempContrato.contrato.puntoExhibicionID === PUNTOS_EXHIBICION.FRIENDS_AND_FAMILY ||
					  $scope.tempContrato.contrato.puntoExhibicionID === PUNTOS_EXHIBICION.REFERIDOS_FFVV)
					 && !$scope.tempContrato.contrato.referidoID){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Seleccione un referido." , okType: 'button-assertive'});
			}
			/*inicio @issue VTADIG2-504 */
			else if ($scope.tempContrato.contrato.debitoAutomatico && (validacion = debitoAutomaticoService.validarDebitoAutomatico($scope.tempContrato.titulares.filter(t=>t.debitoAutomatico).map(t=>t.debitoAutomatico)))&&!validacion.valido) {
		                $ionicPopup.alert({ title: 'Alerta', template: validacion.mensaje , okType: 'button-assertive'});
			}else{			
			/*fin @issue VTADIG2-504 */
				tempContrato=$scope.tempContrato;
				console.log("PASE AL VALIDAR");
				contratoService.comprobarHorasAntesAsamblea(tempContrato.contrato.grupoSeleccionado.grupoID,tempContrato.contrato.contratoNroAsambleaGrupo).then(
					function(response){
						var estado = response.data;
						if(	estado!==ESTADO_HORA_ASAMBLEA.QUEDA_SUFICIENTE_TIEMPO){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: response.message , okType: 'button-assertive'});							
							if(estado===ESTADO_HORA_ASAMBLEA.ASAMBLE_NO_CONCUERDA){
								alertPopup.then(function(val){
									$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
								});
							}							
							return;
						}
						if($scope.tempContrato.contrato.medioComunicacionPreferente!==undefined){
							if($scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID==='N'  && 
							($scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='2' ||
							$scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='3') &&
							$scope.tempContrato.contrato.personaTipoCorreoObj!==undefined){
								$scope.tempContrato.contrato.personaTipoCorreoTelefono=$scope.tempContrato.contrato.personaTipoCorreoObj.codigo;
								if($scope.tempContrato.contrato.personaTipoCorreoTelefono==="1"){
									$scope.tempContrato.contrato.personaCorreoPersonal=$scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaCorreoPersonal;
								}
								if($scope.tempContrato.contrato.personaTipoCorreoTelefono==="2"){
									$scope.tempContrato.contrato.personaCorreoPersonal=$scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaCorreoTrabajo;
								}
							}
							else if($scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaTipoID==='J' && 
							($scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='2' ||
							$scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='3')){
								$scope.tempContrato.contrato.personaCorreoPersonal=$scope.tempContrato.titulares[$scope.indiceTitularPrincipal].personaCorreoPersonal;
								//$scope.tempContrato.titulares[0].personaCorreoTrabajo = $scope.tempContrato.titulares[0].personaCorreoPersonal;
								if($scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='2'){
									$scope.tempContrato.contrato.personaTipoCorreoTelefono="2";
								}
								if($scope.tempContrato.contrato.medioComunicacionPreferente.codigo==='3' &&  $scope.tempContrato.contrato.personaTipoCorreoObj!==undefined){
									$scope.tempContrato.contrato.personaTipoCorreoTelefono=$scope.tempContrato.contrato.personaTipoCorreoObj.codigo;
								}										
							}
							tempContrato.titulares.unshift(tempContrato.titulares.splice($scope.indiceTitularPrincipal,1)[0]);
							console.log("contrato Completo ->%o" ,$scope.tempContrato);
							var confirmPopup = $ionicPopup.confirm({
								title: 'Datos del Cliente',
								template: 'Continue solo si está seguro de que los datos ingresados son correctos.',
								cancelText: 'Cancelar',
									okText: 'Continuar'
							});
				
							confirmPopup
							.then(function(res) {
								if(res) {
									$state.go("app.resumenContrato");
								}
							});
							
						}
					}
				);
			}
		};
		
		$scope.checkDebitoAutomatico = () => {
			if ($scope.tempContrato.contrato.debitoAutomatico){
				$scope.tempContrato.contrato.debitoAutomaticoVisanet = false;
			}
		};

		$scope.checkDebitoAutomaticoVisanet = () => {
			if ($scope.tempContrato.contrato.debitoAutomaticoVisanet){
				$scope.tempContrato.contrato.debitoAutomatico = false;
			}
		};
		
	})

	/**
	 * @feature V4-HU005
	 *  - la validación del documento de identidad no genera bloqueo
	 * @feature V4-HU020
	 *  - add injections [DIRECCION]
	 *  - cliente no domiciliado
	 */
	.controller('agregarPersonaCtrl', function($scope, $rootScope, $state, $stateParams, validacionDeTitular, validacionService, shareData, $http,tempProspecto,tempContrato,$ionicPopover,tempTitular,numContrato,departamento,BACK_END_URL,dateFilter,lstEmpleados,PersonaService,ESTADO_CIVIL,TIPO_RELACION_ID,TIPO_DOCUMENTO, PERSONA_TIPO,$ionicNavBarDelegate, $ionicPopup, PDF417PLUGIN, escanerService,APP, DIRECCION) {
		var j;
		var item;
		$scope.validacionService = validacionService;
		$scope.today = new Date();
		$scope.TIPOS = {
			VINCULADA: 0,
			RELACIONADA: 1
		};
		$scope.METODO_INGRESO = {
			BUSCAR: 0,
			MANUAL: 1
		}; 
		$scope.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
		$scope.tipo = $stateParams.tipo;
		$scope.persona=tempTitular.titular;
		$scope.buscado = false;
		$scope.busquedaEscaner = true;
		$scope.modoEdicion = false;
		$scope.validacionEscaner = false;

		//comitear
		$scope.onblurPersonaIngresoMensual = function () {
			if($scope.newRelacionada.personaIngresoMensual){
				var temp = parseFloat($scope.newRelacionada.personaIngresoMensual);
				if(temp&&!isNaN(temp)){
					$scope.newRelacionada.personaIngresoMensual = ''+temp;
				}				
			}			
		}
		// INICIO VTADIG2-185- José Martinez 12/02/2018
		$scope.actualizaIngresoMensuales = function () {
			
			if($scope.newRelacionada.personaIngresoMensual){
				var termina_en_punto = false;
				if($scope.newRelacionada.personaIngresoMensual.endsWith('.')){
					termina_en_punto = true;
				};
				var temp = parseFloat($scope.newRelacionada.personaIngresoMensual);
				if(temp&&!isNaN(temp)){
					$scope.newRelacionada.personaIngresoMensual = ''+temp+(termina_en_punto?'.':'');
				}
			}

			$scope.newRelacionada.personaIngresoMensual = validacionService.formatearDosDecimales($scope.newRelacionada.personaIngresoMensual);
			console.log($scope.newRelacionada.personaIngresoMensual);

			$scope.newRelacionada.personaIngresoMensualDolares =validacionService.obtenerIngresoMensualDolares($scope.newRelacionada.monedaIDIngresoMensual,$scope.newRelacionada.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);
			console.log('personaIngresoMensualDolares:'+$scope.newRelacionada.personaIngresoMensualDolares);

		}	
		// FIN VTADIG2-185- José Martinez 12/02/2018			
		//fin commitear
		/*SCAN START*/
		$scope.scanBarcode = function() {
			try{				
				var currentPlatform = ionic.Platform.platform();  
				if(currentPlatform ===  PDF417PLUGIN.PLATFORM_ANDROID){
					escanerService.barcodeScanner($scope.scannerProcess);
				}else{
					escanerService.pdf417ScannerScan($scope.scannerProcess);
						}
			}catch(e){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
				console.log(e.message + " -  " + e.stack);
			}
	        
	    };

		$scope.scannerProcess = function(type, result){	
			if (type === "CODE39") {	
				$scope.newRelacionada.personaCodigoDocumento = result.toString();
				$scope.buscar($scope.newRelacionada.personaCodigoDocumento,false,"",true);
				//$scope.inicializaPersona($scope.newRelacionada.tipoDocumento,$scope.newRelacionada.personaCodigoDocumento);			
			}else if (type === "PDF417") {		
				var DNI = result.substring(2,10);
				$scope.newRelacionada.personaCodigoDocumento = DNI;
				$scope.buscar(DNI,true,result);
				$scope.busquedaConEscaner = true;
			}
		};

	    $scope.getDataPDF417 = function(data) {
	        var colLength = 16;
	        var rowLimit = 7;
	        var dataToParse =  data.substring(0,colLength * rowLimit);
	        var DNI = dataToParse.substring(2,10);
	        var apellidoPaterno = "", apellidoMaterno = "", nombres = "";
			var colLength = 16, rowLimit = 7, initNamePosition = 10;
			for(var i=initNamePosition;i<(colLength*rowLimit);i++){
				if(i<=(colLength*3) + 1){
					apellidoPaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*3) + 1 && i<=(colLength*5) + 9){
					apellidoMaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*5) + 9 ){
					nombres+=dataToParse.charAt(i);
				}
			}
			$scope.newRelacionada.personaNombre = nombres.trim();
			$scope.newRelacionada.personaApellidoPaterno = apellidoPaterno.trim();
			$scope.newRelacionada.personaApellidoMaterno = apellidoMaterno.trim();
	    };
		/*SCAN END*/
		$scope.busquedaScanerOptions = [
			{
				id: 0,
				name: 'Ingresar Manualmente',
				label: "Ingresar Manualmente"
			}
		];
		$scope.busquedaScanerOptionsScaner = [
			{
				id: 0,
				name: 'Escanear DNI',
				label: "Escanear DNI"
			}
		];		
		$scope.busquedaScanerOptionsManual = $scope.busquedaScanerOptions;
		$ionicPopover.fromTemplateUrl('buscar-persona-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.scanerOptionsPopover = popover;
			//$scope.relacionadosOptionsPopover = popover;
		});

		$scope.openScanerOptionsPopover = function ($event) {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.show($event);
			}
		};
		$scope.doActionEscaner = function (index) {
			if(index===0){				
				if($scope.busquedaEscaner){
					$scope.tipoNumeroDocumentoDisabled = true;
					$scope.busquedaScanerOptions = $scope.busquedaScanerOptionsScaner;
					if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'J'){
						$scope.validacionEscaner = false;
					}
				}else{
					$scope.tipoNumeroDocumentoDisabled = false;
					$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;
					if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'J'){
						$scope.validacionEscaner = true;
					}
				}
				$scope.busquedaEscaner = !$scope.busquedaEscaner;
			}
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.hide();
			}
			$scope.inicializaPersona($scope.newRelacionada.tipoDocumento);
		};

		$scope.tempListTipoDocumento = angular.copy(shareData.lstTipoDocumento);
		$scope.lstTipoDocumento = [];
		for (const tipoDoc of $scope.tempListTipoDocumento) {
			if(tipoDoc.codigo!=$scope.TIPO_DOCUMENTO.RUC && tipoDoc.codigo != $scope.TIPO_DOCUMENTO.PAS){
				$scope.lstTipoDocumento.push(tipoDoc);
			}
		}	

		$scope.mapTipoDocumentos = {};
		
		angular.forEach($scope.lstTipoDocumento, function (item) {
			$scope.mapTipoDocumentos[item.codigo] = item;
		});

		$scope.lstPais=shareData.lstPais;
		$scope.lstDepartamentoLaboralRelacionada = shareData.lstDepartamento;
		$scope.lstSexo = shareData.lstSexo;

		/** INICIO APPVENTA-70 - Anderson Estela Coronel - 02/dic/2020 */
		$scope.lstPaisResidencia = shareData.lstPais.filter(pais => pais['codigo'] != "189");
		/** FIN APPVENTA-70 - Anderson Estela Coronel - 02/dic/2020 */

		if(lstEmpleados!==null && lstEmpleados!== undefined && 
		shareData.lstEmpleados !==null && shareData.lstEmpleados!== undefined){
			shareData.lstEmpleados = lstEmpleados;
		}
			
		$scope.lstEmpleados = shareData.lstEmpleados;


		$scope.lstTipoVia = shareData.lstTipoVia;
		$scope.lstTipoZona = shareData.lstTipoZona;

		$scope.lstProvinciaLaboralRelacionada=[];
		$scope.lstDistritoLaboralRelacionada=[];	
		$scope.lstPersonaOcupacion = shareData.lstPersonaOcupacion;	
		//INICIO VTADIG2-185- José Martinez 12/02/2018
		$scope.lstTipoMoneda=shareData.lstTipoMoneda;
		//FIN VTADIG2-185- José Martinez 12/02/2018

		

		$scope.esNuevaPersona = false;		
		/**/
		$scope.tempRelacionada = {};
		


		/**/

		$scope.isShowCheckIngresoFamiliares =  function(){
			if($scope.tipo == $scope.TIPOS.RELACIONADA&&$scope.persona.personaTipoID==="N"){
				return true;
			}
			return false;
		};
		$scope.isShowCheckPEP =  function(){
			if($scope.tipo == $scope.TIPOS.RELACIONADA && $scope.persona.personaTipoID==="J"){
				return true;
			}
			return false;
		};		
		
		$scope.cambiarDepartamentoLaboralRelacionada = function(departamentoLaboralID,limpiarDistrito){
			$scope.url=BACK_END_URL+"provincia/"+departamentoLaboralID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstProvinciaLaboralRelacionada=response.data.data;
				if(limpiarDistrito){
					$scope.lstDistritoLaboralRelacionada=[];
				}
			}, function errorCallback(response) {
				
			});
		};
		$scope.cambiarProvinciaLaboralRelacionada=function(departamentoLaboralID,provinciaLaboralID){			
			$scope.url=BACK_END_URL+"distrito/"+departamentoLaboralID+"/"+provinciaLaboralID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstDistritoLaboralRelacionada=response.data.data;
			}, function errorCallback(response) {
				
			});
		};		


		$scope.cambiarDepartamentoDomiciliadaRelacionada = function(departamentoID,limpiarDistrito){
			$scope.url=BACK_END_URL+"provincia/"+departamentoID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstProvinciaDomiciliadaRelacionada=response.data.data;
				if(limpiarDistrito){
					$scope.lstDistritoDomiciliadaRelacionada = [];
				}
			}, function errorCallback(response) {
				
			});
		};
		$scope.cambiarProvinciaDomiciliadaRelacionada=function(departamentoID,provinciaID){			
			$scope.url=BACK_END_URL+"distrito/"+departamentoID+"/"+provinciaID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstDistritoDomiciliadaRelacionada=response.data.data;
			}, function errorCallback(response) {
				
			});
		};		
		
		$scope.cambiarTipoRelacion = function(){
			$scope.newRelacionada.personaIngresoMensualCalif = 
				($scope.persona.personaTipoID==='J') ? false:
					(($scope.newRelacionada.personaIngresoMensual)?true:false)

			if($scope.newRelacionada.personaTipoRelacionID != 17){
				$scope.validacionEscaner = false;
			}else{
				if(!$scope.newRelacionada.personaDocumentoValidado){
					$scope.validacionEscaner = true;
				}
			}		
		}
		
		$scope.changeTipoRelacion= function(){

			if ($scope.persona.personaTipoID==="J"){
				$scope.newRelacionada.personaIngresoMensualCalif = false;
				//$scope.changeBolean(false);
				//$scope.formaPardeDeIngresosFamiliares = false;
			}
		};

		$scope.cancelarRelacionado = function(){
			$scope.newRelacionada = {};
			$state.go('app.datosTitular',{previous:'agregarPersona'});
		};
		
		//$scope.changeBolean = function(varBolean){
		//	$scope.formaPardeDeIngresosFamiliares = varBolean;
		//};	

		$scope.showPersonasVinculadaOptions = function(){
			
		};

		$scope.elementoErroneo = function (form){

					var error = form.$error;
					console.log("LISTA DE ELEMENTOS");
					console.log(error.length);
		    		angular.forEach(error.validar, function(field){
		        		if(field.$invalid){
		            	var fieldName = field.$name;
		            	document.getElementById(fieldName).scrollIntoView();
		        	}
		    		});
		}
		/**
		 * 
		 * @param form
		 * @feature V4-HU005
		 *  - la valicación del documento de identidad no genera bloqueo
		 */
		$scope.aceptarRelacionado = function(form){
			/*requireds*/
			// Inicio John Velasquez OBS-C6 28/05/2018
			if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'J'){
			// Fin John Velasquez OBS-C6 28/05/2018
				/** inicio @feature V4-HU005 **//*
				if($scope.newRelacionada.tipoDocumentoID==TIPO_DOCUMENTO.DNI && $scope.newRelacionada.personaTipoRelacionID == 17 && !$scope.newRelacionada.personaDocumentoValidado){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Es necesario escanear el DNI" , okType: 'button-assertive'});
					return;
				}
				*//** fin @feature V4-HU005 **/
			}

			if($scope.tipo==$scope.TIPOS.RELACIONADA && tempTitular.titular.personaTipoID==="N"){
				$scope.newRelacionada.estadoCivilID=tempTitular.titular.estadoCivilID;	
			}

			if(!$scope.persona.personasRelacionadas){
				$scope.persona.personasRelacionadas = [];
			}
			if(!$scope.persona.personasVinculadas){
				$scope.persona.personasVinculadas = [];
			}
			
			if(!$scope.newRelacionada.personasRelacionadas){
				$scope.newRelacionada.personasRelacionadas = [];
			}
			
			var showAlerts = true;
			/*INICIO INTEGRACION EC*/
			/*
			var valid = validacionDeTitular.validarDatosPersonaRelacionada($scope.newRelacionada,showAlerts,$scope.tipo,form, tempContrato);
			if(!valid){
				//alert($rootScope.respuestaError.mensaje);
				$scope.elementoErroneo(form);
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: $rootScope.respuestaError.mensaje , okType: 'button-assertive'});
				return;
			}
			if(valid){
				$scope.newRelacionada.tipoDocumentoID = $scope.newRelacionada.tipoDocumento.codigo;
			*/
			validacionDeTitular.validarDatosPersonaRelacionada($scope.newRelacionada,showAlerts,$scope.tipo,form, tempContrato, APP.MODULO.INGRESO_DE_VENTA.ID)
			.then(function(valid){
				if(!valid){
					//alert($rootScope.respuestaError.mensaje);
					$scope.elementoErroneo(form);
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: $rootScope.respuestaError.mensaje , okType: 'button-assertive'});
				}else {	
					$scope.agregarRelacionadaValidado();
					$state.go('app.datosTitular',{previous:'agregarPersona',asociadoTipoID:tempTitular.titular.asociadoTipoID ,esEmpleadoPandero:$scope.newRelacionada.personaEsEmpleado, esCasado: $scope.newRelacionada.personaTipoRelacionID});
				}
			})
		};

		$scope.agregarRelacionadaValidado = function(){
			$scope.newRelacionada.tipoDocumentoID = $scope.newRelacionada.tipoDocumento.codigo;
		/*FIN INTEGRACION EC*/
				$scope.newRelacionada.personaTipoDocumentoNombre  = $scope.newRelacionada.tipoDocumento.descripcion;
				if($scope.newRelacionada.personaIngresoMensualCalif===undefined||
				   $scope.newRelacionada.personaIngresoMensualCalif===null){
					$scope.newRelacionada.personaIngresoMensualCalif = false;
				}
				if($scope.newRelacionada.personaOcupacionObj){
						$scope.newRelacionada.personaOcupacion = $scope.newRelacionada.personaOcupacionObj.codigo;
				}
				
				angular.forEach($scope.lstTipoRelacion, function (item) {
					if(item.codigo == $scope.newRelacionada.personaTipoRelacionID){
						$scope.newRelacionada.tipoRelacionNombre = item.descripcion;
						if($scope.newRelacionada.personaTipoRelacionID.trim){
							if(parseInt($scope.newRelacionada.personaTipoRelacionID.trim())==17){
								$scope.newRelacionada.tipoRelacionNombre = "REP. LEGAL";
							}
						}else if(parseInt($scope.newRelacionada.personaTipoRelacionID)==17){
							$scope.newRelacionada.tipoRelacionNombre = "REP. LEGAL";
						}					
					}       
				});

				switch(parseInt($scope.tipo)){
					case $scope.TIPOS.RELACIONADA : 
						$scope.nombreTemporal($scope.newRelacionada);						
						if($scope.esNuevaPersona){
							tempTitular.titular.personasRelacionadas[tempTitular.titular.indexRelacionado] = $scope.newRelacionada;
							if($scope.newRelacionada.personaEsEmpleado){
								$scope.copiaRelacionada = angular.copy($scope.newRelacionada);
								var indiceVinculada = $scope.buscarIndiceVinculado();
								tempTitular.titular.personasVinculadas[indiceVinculada] = $scope.copiaRelacionada;
							//	tempTitular.titular.personasVinculadas.push($scope.copiaRelacionada);
							}				
						}else{
							var yaExistente = tempTitular.titular.personasRelacionadas.find(p=>p.personaCodigoDocumento === $scope.newRelacionada.personaCodigoDocumento && p.tipoDocumentoID === $scope.newRelacionada.tipoDocumentoID);
							if(yaExistente){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ya existe una persona relacionada con "+ $scope.newRelacionada.personaTipoDocumentoNombre +" "+$scope.newRelacionada.personaCodigoDocumento , okType: 'button-assertive'});
								return ;
							}

							if($scope.newRelacionada.estadoCivilID == 'N'){
								$scope.newRelacionada.personaTipoRelacionID = "16";
							}
							tempTitular.titular.personasRelacionadas.push($scope.newRelacionada);
							//si el titular es persona natural y el relacionado es vinculado
							if($scope.newRelacionada.personaEsEmpleado&&tempTitular.titular.personaTipoID === PERSONA_TIPO.NATURAL){
								$scope.copiaRelacionada = angular.copy($scope.newRelacionada);
								$scope.copiaRelacionada.personaTipoRelacionID = 1;
								$scope.copiaRelacionada.tipoRelacionNombre = "Cónyuge";
								tempTitular.titular.personasVinculadas.push($scope.copiaRelacionada);
							}				
						}
						break;
					case $scope.TIPOS.VINCULADA : 
						if($scope.esNuevaPersona){											//ESTA AL REVES
							tempTitular.titular.personasVinculadas[tempTitular.titular.indexVinculado] = $scope.newRelacionada;
								if(parseInt($scope.newRelacionada.personaTipoRelacionID) == TIPO_RELACION_ID.CONYUGUE){
									$scope.copiaRelacionada = angular.copy($scope.newRelacionada);
									//$scope.nombreTemporal($scope.copiaRelacionada);
									var indiceRelacionado = $scope.buscarIndiceRelacionado();
									tempTitular.titular.estadoCivilID = "C";
									//tempTitular.titular.personasRelacionadas.splice(indiceRelacionado,1);
									if(indiceRelacionado!==undefined){
										var tempRelacionado = tempTitular.titular.personasRelacionadas[indiceRelacionado];
										//sobre escribir conyuge anterior, si es el mismo no se sobreescribe
										if(	tempRelacionado.tipoDocumentoID != $scope.copiaRelacionada.tipoDocumentoID ||
											tempRelacionado.personaCodigoDocumento != $scope.copiaRelacionada.personaCodigoDocumento){
											tempTitular.titular.personasRelacionadas[indiceRelacionado] = $scope.copiaRelacionada;
										}										
									}else{
										if(!$scope.copiaRelacionada.personaTipoID){
											$scope.copiaRelacionada.personaTipoID = PERSONA_TIPO.NATURAL;
										}
										tempTitular.titular.personasRelacionadas = [];
										tempTitular.titular.personasRelacionadas[0] = $scope.copiaRelacionada;
									}
								}else{
									//si el titular es un persona natural y la persona vinculada esta en la lista de relacionados se le debe quitar ya que solo debe tener como relacionada a conyuge o conviviente
									if(tempTitular.titular.personaTipoID === PERSONA_TIPO.NATURAL){
										//buscamos al vinculado en la liste de erelacionados
										var indiceVinculadoEnRelacionados = undefined;
										angular.forEach(tempTitular.titular.personasRelacionadas ,function(relacionada, indiceRelacionada){
											if(	relacionada.tipoDocumentoID === $scope.newRelacionada.tipoDocumentoID &&
												relacionada.personaCodigoDocumento === $scope.newRelacionada.personaCodigoDocumento){
													indiceVinculadoEnRelacionados = indiceRelacionada;
											}
										});
										//si lo encontramos lo eliminamos
										if(indiceVinculadoEnRelacionados!==undefined){
											tempTitular.titular.personasRelacionadas.splice(indiceVinculadoEnRelacionados,1);
										}
									}
								}
						}else{
							
							if(!$scope.existeDNI($scope.newRelacionada.personaCodigoDocumento)){
								tempTitular.titular.personasVinculadas.push($scope.newRelacionada);
								$scope.newRelacionada.personaEsEmpleado = true;
								if(parseInt($scope.newRelacionada.personaTipoRelacionID) == TIPO_RELACION_ID.CONYUGUE){
									$scope.copiaRelacionada = angular.copy($scope.newRelacionada);
									$scope.nombreTemporal($scope.copiaRelacionada);
									$scope.copiaRelacionada.personaTipoRelacionID = "1";
									tempTitular.titular.personasRelacionadas =[];
									tempTitular.titular.estadoCivilID = "C";
									if(!$scope.copiaRelacionada.personaTipoID){
										$scope.copiaRelacionada.personaTipoID = PERSONA_TIPO.NATURAL;
									}
									tempTitular.titular.personasRelacionadas.push($scope.copiaRelacionada);
								}	
							}else{
								//alert("Ya existe una persona vinculada con DNI " + $scope.newRelacionada.personaCodigoDocumento);
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ya existe una persona vinculada con DNI." + $scope.newRelacionada.personaCodigoDocumento , okType: 'button-assertive'});
								return ;
							}
							
						}					
						break;
				}
		/*INICIO INTEGRACION EC*/
		/*
				console.log($scope.newRelacionada.personaEsEmpleado);
				$state.go('app.datosTitular',{previous:'agregarPersona',asociadoTipoID:tempTitular.titular.asociadoTipoID ,esEmpleadoPandero:$scope.newRelacionada.personaEsEmpleado, esCasado: $scope.newRelacionada.personaTipoRelacionID});
			}
		*/
		/*FIN INTEGRACION EC*/
		};

		$scope.existeDNI = function (codigoDocumento){
	
			if(tempTitular.titular.personasVinculadas.length > 0){
				for(j = 0; j<tempTitular.titular.personasVinculadas.length ; j++){
					if(tempTitular.titular.personasVinculadas[j].personaCodigoDocumento == codigoDocumento){
						return true;
					}
				}
			}
			return false;
		};

		$scope.buscarIndiceVinculado = function(){
			for(var i = 0 ; i<tempTitular.titular.personasVinculadas.length ;i++){
				if(parseInt(tempTitular.titular.personasVinculadas[i].personaTipoRelacionID) == TIPO_RELACION_ID.CONYUGUE){
					return i;
				}
			}
		};

		$scope.buscarIndiceRelacionado = function(){
			for(var i = 0 ; i<tempTitular.titular.personasRelacionadas.length ;i++){
				if(parseInt(tempTitular.titular.personasRelacionadas[i].personaTipoRelacionID) === TIPO_RELACION_ID.CONYUGUE){
					return i;
				}
			}
		};
		/**
		 * 
		 * @param newRelacionada
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.nombreTemporal = function(newRelacionada){

			if(newRelacionada.personaIngresoMensualCalif){
				newRelacionada.personaOcupacion = newRelacionada.personaOcupacionObj.codigo;
			}					
			
			/** inicio @feature V4-HU020 **/
			newRelacionada.esResidente = newRelacionada.esResidente !== false;
			/** fin @feature V4-HU020 **/
			
			if(!newRelacionada.direcciones){
				newRelacionada.direcciones =[];
			}

			if (tempTitular.titular.personaTipoID==="J"){
				/** inicio @feature V4-HU020 **/
				newRelacionada.direccionDomiciliada.direccionTipoID = newRelacionada.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO;
				/** fin @feature V4-HU020 **/
				newRelacionada.direcciones[0] = newRelacionada.direccionDomiciliada;
			}else if($scope.newRelacionada.personaIngresoMensualCalif){
				/** inicio @feature V4-HU020 **/
				newRelacionada.direccionLaboral.direccionTipoID = newRelacionada.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO;
				/** fin @feature V4-HU020 **/
				newRelacionada.direcciones[1] = newRelacionada.direccionLaboral;
			}

			newRelacionada.personaFechaNacimiento= dateFilter(newRelacionada.personaFechaNacimiento, 'dd/MM/yyyy');	  			
			newRelacionada.personaTipoRelacionID = parseInt(newRelacionada.personaTipoRelacionID);

			newRelacionada.tipoDocumentoID = newRelacionada.tipoDocumento.codigo;

			newRelacionada.asociadoTipoID = 'L';
					
			newRelacionada.personaSujetoObligado=false;
			newRelacionada.personaPrevLavadoActivos=false;
			newRelacionada.personaActividadMinera=false;
			newRelacionada.personaGrupoEconomico=false;
			if(!newRelacionada.personaPEP||$scope.newRelacionada.personaPEP===null){
				newRelacionada.personaPEP=false;
			}else{
				newRelacionada.personaPEP = true;
			}
			
			if (newRelacionada.tipoDocumento.codigo!== undefined && newRelacionada.tipoDocumento.codigo ==='8'){
					newRelacionada.personaTipoID = 'J';				
			} else if (newRelacionada.tipoDocumento.codigo!== undefined){
				newRelacionada.personaTipoID = 'N';
			}
			if(tempTitular.titular.personaTipoID=='N'){
				if(tempTitular.titular.estadoCivilID==='C'){
					newRelacionada.personaTipoRelacionID = "1";
				}else if(tempTitular.titular.estadoCivilID==='N'){
					newRelacionada.personaTipoRelacionID = "16";
				}
			}
		};
		/** inicio @feature V4-HU020 **/
		$scope.tempDireccionDomiciliada = {};
		$scope.tempDireccionNoDomiciliada = {};
		$scope.tempDireccionLaboral = {};
		$scope.tempDireccionLaboralNoDomiciliada = {};
		$scope.changeResidente = ($value) => {
			console.log($value);
			if($scope.newRelacionada.esResidente) {
				$scope.tempDireccionNoDomiciliada = $scope.newRelacionada.direccionDomiciliada;
				$scope.newRelacionada.direccionDomiciliada = $scope.tempDireccionDomiciliada;
				$scope.tempDireccionLaboralNoDomiciliada = $scope.newRelacionada.direccionLaboral;
				$scope.newRelacionada.direccionLaboral = $scope.tempDireccionLaboral;
			} else {
				$scope.tempDireccionDomiciliada = $scope.newRelacionada.direccionDomiciliada;
				$scope.newRelacionada.direccionDomiciliada = $scope.tempDireccionNoDomiciliada;
				$scope.tempDireccionLaboral = $scope.newRelacionada.direccionLaboral;
				$scope.newRelacionada.direccionLaboral = $scope.tempDireccionLaboralNoDomiciliada;
			}
			console.log($scope.newRelacionada.direccionDomiciliada);
			console.log($scope.newRelacionada.direccionLaboral);
		};
		$scope.bloquearInputPorNoResidente = (persona) => {
			return false;
			//return persona && persona.personaID && !persona.esResidenteOriginal;
		};
        /** fin @feature V4-HU020 **/
		/**
		 * 
		 * @param data
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.ajustarPersonaRelacionada = function (data){
			$scope.newRelacionada=data;
			// INICIO VTADIG2-185- José Martinez 12/02/2018
			if($scope.newRelacionada.personaID && !$scope.newRelacionada.monedaIDIngresoMensual){
				//alert($scope.newRelacionada.personaNombre+ ' no tiene registrado el tipo de moneda');
				$scope.newRelacionada.monedaIDIngresoMensual='2';
			}
			
			if($scope.newRelacionada.personaID && $scope.newRelacionada.monedaIDIngresoMensual){
				$scope.newRelacionada.monedaIDIngresoMensual = $scope.newRelacionada.monedaIDIngresoMensual.toString();	
			}

			$scope.newRelacionada.personaIngresoMensualDolares = validacionService.obtenerIngresoMensualDolares($scope.newRelacionada.monedaIDIngresoMensual,$scope.newRelacionada.personaIngresoMensual, tempContrato.contrato.listaTipoCambios);

			// FIN VTADIG2-185- José Martinez 12/02/2018

			if(data.paisID){
				$scope.newRelacionada.paisID=data.paisID+'';
			}else{
				$scope.newRelacionada.paisID = "189";
			}
			/** inicio @feature V4-HU020 **/
			$scope.newRelacionada.esResidente = $scope.newRelacionada.esResidente !== false;
			if(data.paisResidenciaId){
				$scope.newRelacionada.paisResidenciaId=data.paisResidenciaId+'';
			}
			/** fin @feature V4-HU020 **/
			if($scope.newRelacionada.personaFechaNacimiento){
				var str = $scope.newRelacionada.personaFechaNacimiento;
				var res = str.split(" ")[0].split("/");
				var month = parseInt(res[1])-1;
				$scope.newRelacionada.personaFechaNacimiento = new Date(res[2], month, res[0]);								
			}

			angular.forEach($scope.lstPersonaOcupacion,function(ocupacion){
				if($scope.newRelacionada.personaOcupacion==ocupacion.codigo){
					$scope.newRelacionada.personaOcupacionObj = ocupacion;
				}
			});


			//$scope.newRelacionada.personaFechaIngresoTrabajo=dateFilter($scope.newRelacionada.personaFechaIngresoTrabajo, 'dd/MM/yyyy');	  			
			$scope.newRelacionada.tipoDocumento = $scope.mapTipoDocumentos[data.tipoDocumentoID];

			if(!$scope.newRelacionada.paisID){
				$scope.newRelacionada.paisID = "189";
			}

			//INICIO VTADIG2-185- José Martinez 12/02/2018
			if($scope.newRelacionada.personaId){
				$scope.newRelacionada.monedaIDIngresoMensual=1;
			}
			//FIN VTADIG2-185- José Martinez 12/02/2018					

			if($rootScope.me.documentoDigital && tempTitular.titular.personaTipoID == 'J'){
				if(!$scope.newRelacionada.personaDocumentoValidado){
					$scope.validacionEscaner = true ;
				}
			}

			if(tempTitular.titular.personaTipoID=='N'){
				if(tempTitular.titular.estadoCivilID==='C'){
					$scope.newRelacionada.personaTipoRelacionID = "1";
				}else if(tempTitular.titular.estadoCivilID==='N'){
					$scope.newRelacionada.personaTipoRelacionID = "16";
				}								
			}else if(tempTitular.titular.personaTipoID=='J'){
				$scope.newRelacionada.personaTipoRelacionID = "17";
			}

			angular.forEach($scope.lstPersonaOcupacion,function(ocupacion){
				if(ocupacion.codigo===data.personaOcupacion){
					$scope.newRelacionada.personaOcupacionObj=ocupacion;
				}
			});
			if(tempTitular.titular.personaTipoID=='J'){
					$scope.newRelacionada.sexoID=data.sexoID;
			}else{
				/* if($scope.newRelacionada.personaIngresoMensual&&$scope.persona.personaTipoID==="N"){
					$scope.newRelacionada.personaIngresoMensualCalif = true;
				} */
			}
			if(!$scope.newRelacionada.personaTelefono){
				$scope.newRelacionada.personaTelefono = $scope.newRelacionada.personaTelefonoMovil
			}
			if(data.direcciones){
				if(data.direcciones[0]){
					$scope.newRelacionada.direccionDomiciliada=data.direcciones[0];
					$scope.newRelacionada.direccionDomiciliada.departamentoID=$scope.newRelacionada.direccionDomiciliada.departamentoID+'';
					$scope.newRelacionada.direccionDomiciliada.provinciaID=$scope.newRelacionada.direccionDomiciliada.provinciaID+'';
					$scope.newRelacionada.direccionDomiciliada.distritoID=$scope.newRelacionada.direccionDomiciliada.distritoID+'';
					if($scope.newRelacionada.direccionDomiciliada.tipoViaID){
						$scope.newRelacionada.direccionDomiciliada.tipoViaID = $scope.newRelacionada.direccionDomiciliada.tipoViaID.trim();
					}
					if($scope.newRelacionada.direccionDomiciliada.tipoZonaID){
						$scope.newRelacionada.direccionDomiciliada.tipoZonaID = $scope.newRelacionada.direccionDomiciliada.tipoZonaID.trim();
					}
					if(!$scope.newRelacionada.personaTelefono){
						$scope.newRelacionada.personaTelefono = $scope.newRelacionada.direccionDomiciliada.direccionTelefono1;
					}
					$scope.cambiarDepartamentoDomiciliadaRelacionada($scope.newRelacionada.direccionDomiciliada.departamentoID);
					$scope.cambiarProvinciaDomiciliadaRelacionada($scope.newRelacionada.direccionDomiciliada.departamentoID,$scope.newRelacionada.direccionDomiciliada.provinciaID);
				}
				if(data.direcciones[1]){
					$scope.newRelacionada.direccionLaboral=data.direcciones[1];
					$scope.newRelacionada.direccionLaboral.departamentoID=$scope.newRelacionada.direccionLaboral.departamentoID+'';
					$scope.newRelacionada.direccionLaboral.provinciaID=$scope.newRelacionada.direccionLaboral.provinciaID+'';
					$scope.newRelacionada.direccionLaboral.distritoID=$scope.newRelacionada.direccionLaboral.distritoID+'';
					if($scope.newRelacionada.direccionLaboral.tipoViaID){
						$scope.newRelacionada.direccionLaboral.tipoViaID = $scope.newRelacionada.direccionLaboral.tipoViaID.trim();
					}
					if($scope.newRelacionada.direccionLaboral.tipoZonaID){
						$scope.newRelacionada.direccionLaboral.tipoZonaID = $scope.newRelacionada.direccionLaboral.tipoZonaID.trim();
					}
					$scope.cambiarDepartamentoLaboralRelacionada($scope.newRelacionada.direccionLaboral.departamentoID);
					$scope.cambiarProvinciaLaboralRelacionada($scope.newRelacionada.direccionLaboral.departamentoID,$scope.newRelacionada.direccionLaboral.provinciaID);
				}									
			}
			$scope.buscado = true;
			$scope.modoEdicion = false;
		};
		$scope.buscar = function (dni,scanner,dataScanner,scannerDni) {
			if(scanner == undefined){
				scanner = false;
			}
			console.log($scope.newRelacionada.tipoDocumento.codigo);

			if(!$scope.newRelacionada.tipoDocumento){
				//alert("Por favor eliga el tipo de documento");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor eliga el tipo de documento." , okType: 'button-assertive'});
			}else if(!dni || (dni && dni.length===0)){
				//alert("Por favor ingrese un número de documento");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Por favor ingrese un número de documento." , okType: 'button-assertive'});
			}else if(scanner=== false && $scope.newRelacionada.tipoDocumento.codigo === "4" && dni.length !== 8){
				//alert("Ingrese un DNI adecuado (8 dígitos)");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese un DNI adecuado (8 dígitos)." , okType: 'button-assertive'});
			}else{
				var personaCodigoDocumentoIsValid = true;

				angular.forEach(tempContrato.titulares,function(item){
					if(dni==item.personaCodigoDocumento){
						//alert("La persona identificada con Dni "+dni+" ya ha sido agregado como Titular.");
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "La persona identificada con  "+ $scope.newRelacionada.tipoDocumento.descripcion + " " + $scope.newRelacionada.tipoDocumento.codigo+" ya ha sido agregado como Titular." , okType: 'button-assertive'});
						personaCodigoDocumentoIsValid = false;
					}
				});

				if(personaCodigoDocumentoIsValid){
				/*INICIO EVAL CRED*/
					PersonaService.obtenerPersona($scope.newRelacionada.tipoDocumento.codigo,dni,APP.MODULO.INGRESO_DE_VENTA.ID)
				/*FIN EVAL CRED*/
					.then(function(data){
						if(data){
							$scope.ajustarPersonaRelacionada(data);
                            /*inicio @issue EVALCRED-51*/
                            /*inicio @issue VTADIG2-507 / VTADIG2-527*/
							//if(scanner && !data.esEquifax){
                            if(scanner){
								$scope.newRelacionada.personaDocumentoValidado = true;
                                if(!data.esEquifax){
                                    $scope.getDataPDF417(dataScanner);
									$scope.modoEdicion = false;
                                }
							}
                            /*fin @issue VTADIG2-507 / VTADIG2-527*/
                            /*fin @issue EVALCRED-51*/
							if(scannerDni){
								$scope.modoEdicion = true;
								$scope.buscado = true;
								$scope.busquedaEscaner = false;
								$scope.deSaf = false;
								$scope.newRelacionada.personaDocumentoValidado = true;
							}
							/*inicio @issue EVALCRED-57*/
							$scope.validado = data.personaDocumentoValidado?true:false;
							/*fin @issue EVALCRED-57*/
						}else{
							//alert("Persona no encontrada, ingrese los datos manualmente");
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Persona no encontrada, ingrese los datos manualmente." , okType: 'button-assertive'});							
							$scope.modoEdicion = true;
							$scope.inicializaPersona($scope.newRelacionada.tipoDocumento,$scope.newRelacionada.personaCodigoDocumento); 							
							if(scanner){
								$scope.getDataPDF417(dataScanner);
								$scope.newRelacionada.personaDocumentoValidado = true;
								$scope.modoEdicion = false;
							}
							if(scannerDni){
								$scope.modoEdicion = true;
								$scope.buscado = true;
								$scope.busquedaEscaner = false;
								$scope.deSaf = false;
								$scope.newRelacionada.personaDocumentoValidado = true;
							}
							$scope.buscado = true;
							/*inicio @issue EVALCRED-57*/
                            				$scope.validado = false;
				                        /*fin @issue EVALCRED-57*/
                            /** inicio @feature V4-HU020 **/
							$scope.newRelacionada.esResidente = true;
							/** fin @feature V4-HU020 **/
						}
					},function(error){
		                $scope.buscado = false;
						$scope.modoEdicion = true;
		            });
				}
			}										
		};

		$scope.changeTipoDocumentoSeleccionado= function(tipoDocumento){
			if(tipoDocumento.codigo=="4"){
				$scope.busquedaEscaner = true;
				$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;	
			}else{
				$scope.busquedaEscaner = false;
				$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsScaner;
			}
			$scope.inicializaPersona(tipoDocumento);
		};
		/**
		 * 
		 * @param tipoDocumento
		 * @param codigoDocumento
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.inicializaPersona = function(tipoDocumento, codigoDocumento){
			$scope.buscado = false;
			$scope.modoEdicion = true;
			$scope.newRelacionada = {};
			$scope.newRelacionada.personaIngresoMensualCalif=false;	
			// INICIO VTADIG2-185- José Martinez 12/02/2018					
			$scope.newRelacionada.monedaIDIngresoMensual='1';
			// FIN VTADIG2-185- José Martinez 12/02/2018
			if(tipoDocumento){
				$scope.newRelacionada.tipoDocumento = tipoDocumento;
				$scope.newRelacionada.tipoDocumentoID = tipoDocumento.codigo;				
			}else{
				$scope.newRelacionada.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
				$scope.newRelacionada.tipoDocumentoID = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo;				
			}									
			$scope.newRelacionada.personaCodigoDocumento = codigoDocumento;
			$scope.newRelacionada.personaTipoID='N';

			$scope.newRelacionada.paisID = "189";
			if(tempTitular.titular.personaTipoID=='N'){
				$scope.newRelacionada.personaTipoRelacionID = "1";
			}
			if(tempTitular.titular.personaTipoID=='J'){
				$scope.newRelacionada.personaTipoRelacionID = "17";
			}
			/** inicio @feature V4-HU020 **/
			$scope.newRelacionada.esResidente = true;
			/** fin @feature V4-HU020 **/
		}
		$scope.vinculadoSeleccionado = function (newValue, oldValue){
			$scope.newRelacionada  = newValue;
			$scope.newRelacionada.tipoDocumento = $scope.mapTipoDocumentos[''+$scope.newRelacionada.tipoDocumentoID];
		};
		$scope.shoeOptions = function(){
			var hideSheet = $ionicActionSheet.show({
			buttons: [
				{ text: '<b><i class="icon ion-information-circled"></i>Buscar</b>' },
				{ text: '<b><i class="icon ion-card"></i>Agregar Manualmente</b>' }
			],
			titleText: 'Opciones',
			cancelText: 'Cancelar',
			cancel: function() {},
			buttonClicked: function(index) 	{
				if(index===0){
					   $scope.metodoIngreso = $scope.METODO_INGRESO.BUSCAR;
				}
				if(index===1){
					   $scope.metodoIngreso = $scope.METODO_INGRESO.MANUAL;
				}
				return true;
				}
			});
		};
		
		if(parseInt($scope.tipo)===$scope.TIPOS.VINCULADA){
			//console.log("lstTipoVinculada");
			$ionicNavBarDelegate.title("Datos del Vinculado");

			$scope.lstTipoRelacion = JSON.parse(JSON.stringify(shareData.lstTipoVinculada));						

			if(!tempTitular.titular.personaEsEmpleado){
				if(tempTitular.titular.personaTipoID == "N"){
					for(j = 0; j<=3 ; j++){
						for(i = 0; i < $scope.lstTipoRelacion.length;i++){
							item = $scope.lstTipoRelacion[i];
							if(item.tipoPersona!=="N")
							{
								$scope.lstTipoRelacion.splice(i,1);
							}
						}
					}		
				}else{
					for(j = 0; j<=3 ; j++){
						for(i = 0; i < $scope.lstTipoRelacion.length;i++){
							item = $scope.lstTipoRelacion[i];
							if(item.tipoPersona!=="J")
							{
								$scope.lstTipoRelacion.splice(i,1);
							}
						}
					}
				}
			}
		}else if(parseInt($scope.tipo)===$scope.TIPOS.RELACIONADA){
			//console.log("lstTipoRelacionada");
			$ionicNavBarDelegate.title("Datos del Relacionado");
			$scope.lstTipoRelacion = JSON.parse(JSON.stringify(shareData.lstTipoRelacionada));
			if(tempTitular.titular.personaTipoID==="N"){
				var templstTipoRelacion;
				if(tempTitular.titular.estadoCivilID.trim()==="C"){					
					angular.forEach($scope.lstTipoRelacion,function(item,$index){
						if(parseInt(item.codigo)===1){
							templstTipoRelacion = [item];
						}
					});					
				}
				if(tempTitular.titular.estadoCivilID.trim()==="N"){
					angular.forEach($scope.lstTipoRelacion,function(item,$index){
						if(parseInt(item.codigo)===16){
							templstTipoRelacion = [item];
						}
					});	
				}
				$scope.lstTipoRelacion = templstTipoRelacion;
			}
			if(tempTitular.titular.personaTipoID==="J"){
				for(j = 0; j<=3 ; j++){
					for(i = 0; i < $scope.lstTipoRelacion.length;i++){
						item = $scope.lstTipoRelacion[i];
						if(item.tipoPersona!=="J")
						{
							$scope.lstTipoRelacion.splice(i,1);
						}
					}
				}				
			}
		}
		if(!$scope.persona.personasRelacionadas){
			$scope.persona.personasRelacionadas = [];
		}
		if(!$scope.persona.personasVinculadas){
			$scope.persona.personasVinculadas = [];
		}
		if($scope.tipo==$scope.TIPOS.RELACIONADA){
			if($scope.persona.personasRelacionadas){
				if($scope.persona.personasRelacionadas.length>tempTitular.titular.indexRelacionado){
					//console.log("editando persona relacionada");
					//console.log($scope.persona.personasRelacionadas[tempTitular.titular.indexRelacionado]);
					$scope.modoEdicion = false;
					$scope.busquedaEscaner = false;
					$scope.buscado = true;
					
					if($scope.persona.personasRelacionadas[tempTitular.titular.indexRelacionado].personaID != undefined){
						$scope.deSaf = true;	
					}
					$scope.newRelacionada = JSON.parse(JSON.stringify($scope.persona.personasRelacionadas[tempTitular.titular.indexRelacionado]));

					if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'J'){
							if(!$scope.newRelacionada.personaDocumentoValidado){
								$scope.validacionEscaner = true;
							}
					}

					// INICIO SPRINT-2
					/*
					if($scope.newRelacionada.personaFechaNacimiento){
						$scope.newRelacionada.personaFechaNacimiento = new Date($scope.newRelacionada.personaFechaNacimiento);
						//$scope.newRelacionada.personaFechaNacimiento = new Date($scope.newRelacionada.personaFechaNacimiento);
					}
					*/
					// FIN SPRINT-2
					if($scope.newRelacionada.paisID){
						$scope.newRelacionada.paisID = ''+$scope.newRelacionada.paisID;
					}else{						
						$scope.newRelacionada.paisID = "189";
					}
					/** inicio @feature V4-HU020 **/
					if($scope.newRelacionada.paisResidenciaId){
						$scope.newRelacionada.paisResidenciaId = ''+$scope.newRelacionada.paisResidenciaId;
					}
					/** fin @feature V4-HU020 **/
					if($scope.persona.personaTipoID==="N"){
						if($scope.newRelacionada.personaIngresoMensualCalif===undefined){
							if($scope.newRelacionada.personaIngresoMensual){
								$scope.newRelacionada.personaIngresoMensualCalif=true;	
							}else{
								$scope.newRelacionada.personaIngresoMensualCalif=false;	
							}
						}
					}else{
						$scope.newRelacionada.personaIngresoMensualCalif=undefined;
					}
					if($scope.newRelacionada.tipoDocumentoID){
						angular.forEach($scope.lstTipoDocumento,function(documento){
							if(parseInt($scope.newRelacionada.tipoDocumentoID)===parseInt(documento.codigo)){
								$scope.newRelacionada.tipoDocumento = documento;
							}
						}); 
					}					
					if($scope.newRelacionada.personaTelefono===undefined){
						$scope.newRelacionada.personaTelefono = $scope.newRelacionada.personaTelefono;
					}

					$scope.newRelacionada.personaTipoRelacionID = $scope.newRelacionada.personaTipoRelacionID+'';
					$scope.esNuevaPersona = true;
					if($scope.newRelacionada){
						if($scope.newRelacionada.direcciones){
							if($scope.newRelacionada.direcciones[0]){		
								$scope.cambiarDepartamentoDomiciliadaRelacionada($scope.newRelacionada.direcciones[0].departamentoID);
								$scope.cambiarProvinciaDomiciliadaRelacionada($scope.newRelacionada.direcciones[0].departamentoID, $scope.newRelacionada.direcciones[0].provinciaID);
								$scope.newRelacionada.direccionDomiciliada = $scope.newRelacionada.direcciones[0];
								if($scope.newRelacionada.direccionDomiciliada.tipoViaID){
									$scope.newRelacionada.direccionDomiciliada.tipoViaID = $scope.newRelacionada.direccionDomiciliada.tipoViaID.trim();
								}
								if($scope.newRelacionada.direccionDomiciliada.tipoZonaID){
									$scope.newRelacionada.direccionDomiciliada.tipoZonaID = $scope.newRelacionada.direccionDomiciliada.tipoZonaID.trim();
								}
							}
							if($scope.newRelacionada.direcciones[1]){
								$scope.cambiarDepartamentoLaboralRelacionada($scope.newRelacionada.direcciones[1].departamentoID);
								$scope.cambiarProvinciaLaboralRelacionada($scope.newRelacionada.direcciones[1].departamentoID, $scope.newRelacionada.direcciones[1].provinciaID);
								$scope.newRelacionada.direccionLaboral = $scope.newRelacionada.direcciones[1];
								if($scope.newRelacionada.direccionLaboral.tipoViaID){
									$scope.newRelacionada.direccionLaboral.tipoViaID = $scope.newRelacionada.direccionLaboral.tipoViaID.trim();
								}
								if($scope.newRelacionada.direccionLaboral.tipoZonaID){
									$scope.newRelacionada.direccionLaboral.tipoZonaID = $scope.newRelacionada.direccionLaboral.tipoZonaID.trim();
								}								
							}
						}						
					}												
					if($scope.newRelacionada.personaOcupacionObj===undefined){
						angular.forEach($scope.lstPersonaOcupacion,function(ocupacion){
							if($scope.newRelacionada.personaOcupacion==ocupacion.codigo){
								$scope.newRelacionada.personaOcupacionObj = ocupacion;
							}
						}); 
					}
					if($scope.newRelacionada.personaFechaNacimiento){
						if(!$scope.newRelacionada.personaFechaNacimiento.getDate){
							var str = $scope.newRelacionada.personaFechaNacimiento;
							var res = str.split(" ")[0].split("/");
							var month = parseInt(res[1])-1;
							$scope.newRelacionada.personaFechaNacimiento = new Date(res[2], month, res[0]);
							//$scope.newRelacionada.personaFechaNacimiento = new Date(res[2], month, res[0]);
						}
						else{
							$scope.newRelacionada.personaFechaNacimiento = $scope.newRelacionada.personaFechaNacimiento;
						}
					}	
				}else{
					//console.log("nueva persona relacionada");
					$scope.newRelacionada = {};				
					$scope.newRelacionada.personaIngresoMensualCalif=false;						

					$scope.newRelacionada.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
					
					$scope.newRelacionada.tipoDocumentoID = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo;

					$scope.newRelacionada.personaTipoID='N';
					$scope.newRelacionada.paisID = "189";
                        if(tempTitular.titular.personaTipoID=='N'){
						    $scope.newRelacionada.personaTipoRelacionID = "16";
                        }
                        if(tempTitular.titular.personaTipoID=='J'){
                            $scope.newRelacionada.personaTipoRelacionID = "17";//bien
                        }
					$scope.modoEdicion = true;
				}
			}else{
				//console.log("primera persona relacionada");
				$scope.persona.personasRelacionadas = [];
				$scope.newRelacionada = {};
				$scope.newRelacionada.personaIngresoMensualCalif=false;
				$scope.newRelacionada.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
				$scope.newRelacionada.tipoDocumentoID = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo;
				$scope.newRelacionada.personaTipoID='N';
				$scope.newRelacionada.paisID = "189";
				if(tempTitular.titular.personaTipoID=='N'){
					$scope.newRelacionada.personaTipoRelacionID = "1";
				}
				if(tempTitular.titular.personaTipoID=='J'){
					$scope.newRelacionada.personaTipoRelacionID = "17";//bien
				}
				$scope.modoEdicion = true;
			}	
		}
		if($scope.tipo==$scope.TIPOS.VINCULADA){
			if($scope.persona.personasVinculadas){
				if($scope.persona.personasVinculadas.length>tempTitular.titular.indexVinculado){
					//console.log("editando persona vinvulada");
					//console.log($scope.persona.personasVinculadas[tempTitular.titular.indexVinculado]);
					$scope.modoEdicion = false;
					$scope.newRelacionada = JSON.parse(JSON.stringify($scope.persona.personasVinculadas[tempTitular.titular.indexVinculado]));				

					/**CRAEMOS UNA NUEVA VARIABLE PARA PODER DETERMINAR SI EL VINCULADO ERA CASADO**/
					/**También se puede almacenar tipoRelacionNombre**/
					$scope.tempTipoRelacionId = $scope.newRelacionada.personaTipoRelacionID;

					$scope.newRelacionada.personaIngresoMensualCalif=undefined;						
					$scope.esNuevaPersona = true;					
					$scope.newRelacionada.personaTipoRelacionID = $scope.newRelacionada.personaTipoRelacionID+'';
					if($scope.newRelacionada.tipoDocumentoID){
						angular.forEach($scope.lstTipoDocumento,function(documento){
							if(parseInt($scope.newRelacionada.tipoDocumentoID)===parseInt(documento.codigo)){
								$scope.newRelacionada.tipoDocumento = documento;
							}
						}); 
					}
				}else{
					//console.log("nueva persona relacionada");
					$scope.newRelacionada = {};				
					$scope.newRelacionada.personaIngresoMensualCalif=undefined;
					/*$scope.newRelacionada.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
					$scope.newRelacionada.tipoDocumentoID = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo;*/
					$scope.newRelacionada.tipoDocumento = undefined;
					$scope.newRelacionada.personaTipoID='N';
					$scope.newRelacionada.paisID = "189";
					$scope.modoEdicion = true;
					//if($scope.lstTipoRelacion.length>0){
					//	$scope.newRelacionada.personaTipoRelacionID = $scope.lstTipoRelacion[0].codigo;
					//}				
				}
			}else{
				//console.log("primera persona relacionada");
				$scope.persona.personasVinculadas = [];
				$scope.newRelacionada = {};
				$scope.newRelacionada.personaIngresoMensualCalif=undefined;			
				$scope.newRelacionada.tipoDocumento = undefined;
				/*$scope.newRelacionada.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
				$scope.newRelacionada.tipoDocumentoID = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo;*/
				$scope.newRelacionada.personaTipoID='N';
				$scope.newRelacionada.paisID = "189";
				$scope.modoEdicion = true;
				//if($scope.lstTipoRelacion.length>0){
				//	$scope.newRelacionada.personaTipoRelacionID = $scope.lstTipoRelacion[0].codigo;
				//}	
			}
			
			/** SI LA RELACION ES VALIDA Y DIFERENTE DE  CASADO QUITAMOS EL PRIMER ELEMENTO (CASADO) **/								
			if(tempTitular.titular.personaTipoID === PERSONA_TIPO.NATURAL){
				var tempEstadoCivilID = tempTitular.titular.estadoCivilID;
				if(tempEstadoCivilID != ESTADO_CIVIL.CASADO && tempEstadoCivilID != ESTADO_CIVIL.CONVIVIENTE){
					var tempLstTipoRelacion = [];
					if($scope.lstTipoRelacion){
						angular.forEach($scope.lstTipoRelacion,function(relacion){
							if(	parseInt(relacion.codigo)!==1&&
								parseInt(relacion.codigo)!==25&
								parseInt(relacion.codigo)!==24){
									tempLstTipoRelacion.push(relacion);
								}
						});
					}
					$scope.lstTipoRelacion = tempLstTipoRelacion;
				}else{//casado o conviviente
					var vinculadoEnRelacionados = false;
					if(!tempTitular.titular.personasRelacionadas){
						tempTitular.titular.personasRelacionadas = [];					
					}
					if(tempTitular.titular.personasRelacionadas.length<=0){
						vinculadoEnRelacionados = true;
					}else{
						//ver si el vinculado ya esta en relacionados
						if($scope.newRelacionada.tipoDocumentoID!==undefined&&$scope.newRelacionada.personaCodigoDocumento!==undefined){
							angular.forEach(tempTitular.titular.personasRelacionadas, function(relacionada){
								if(	relacionada.tipoDocumentoID == $scope.newRelacionada.tipoDocumentoID &&
									relacionada.personaCodigoDocumento == $scope.newRelacionada.personaCodigoDocumento){
										vinculadoEnRelacionados = true;
								}
							});
						}
					}
					//si el vinculado ya está en relacionados se muestra la opcion de conyuge
					//sino se quita
					if(!vinculadoEnRelacionados){
						var tempLstTipoRelacion = [];
						if($scope.lstTipoRelacion){
							angular.forEach($scope.lstTipoRelacion,function(relacion){
								if(	parseInt(relacion.codigo)!==1){
									tempLstTipoRelacion.push(relacion);
								}
							});
						}
						$scope.lstTipoRelacion = tempLstTipoRelacion;
					}				
				}
			}
			
		}
		/**
		 * AGREGAR PERSONA DELACIONADA BLOCK_END
		 */
	})
	/*INICIO INTEGRACION EC*/
	/**
	 * @feature V4-HU006
	 * - add injection dependecy [$ionicScrollDelegate, camaraService, base64Utils, configuracionArchivos]
	 * - add attrs personaDocumentos, archivosDocumentoIdentidad, archivosDocumentoIdentidadEliminar, formPersona to $scope
	 * - add functions obtenerFilesDocumentoIdentidad, desplegarArchivos, quitarImagen, tomarFoto, cargarImagen, cargarArchivos to $scope
     * @feature V4-HU020
     *  - add inject [DIRECCION]
     *  - cliente no residente
	 */
	.controller('datosTitularCtrl', function($rootScope,$scope, $state,$stateParams, $ionicScrollDelegate, PersonaService, validacionDeTitular, validacionService , shareData, $http,tempProspecto,tempContrato,$ionicPopover,tempTitular,numContrato,departamento,BACK_END_URL,moment,dateFilter,$ionicModal,$q, $cordovaGeolocation, $ionicLoading,$ionicPlatform,$timeout, TABAJADOR,TIPO_ASOCIADO,TIPO_DOCUMENTO,ESTADO_CIVIL,TIPO_RELACION_ID,PERSONA_TIPO,lstDepartamento,lstPais,lstProvincia,lstDistrito,lstProvinciaNacimiento,lstDistritoNacimiento,lstProvinciaLaboral,lstDistritoLaboral,$ionicNavBarDelegate, $ionicPopup,PDF417PLUGIN, escanerService,proformaService,LST_TIPO_DOCUMENTO,APP,DIRECCION, camaraService, base64Utils, configuracionArchivos) {
	/*INICIO INTEGRACION EC*/
		
		if(tempTitular.titular.asociadoTipoID == "T"){
			$ionicNavBarDelegate.title("Datos del Titular");
		}else if ($stateParams.asociadoTipoID == "R"){
			$ionicNavBarDelegate.title("Datos del Respaldo");
		}else if ($stateParams.asociadoTipoID == "T"){
			$ionicNavBarDelegate.title("Datos del Titular");
		}
		/** inicio @feature V4-HU020 **/
		$scope.mapTipoDireccion = {};
		/** fin @feature V4-HU020 **/
		$scope.validacionService = validacionService;
		$scope.today = new Date();
		$scope.tempTitular = tempTitular;
		
		$scope.persona=$scope.tempTitular.titular;
		/** inicio @feature V4-HU006 **/
		$scope.personaDocumentos = [];
		$scope.archivosDocumentoIdentidad = $scope.persona.archivosDocumentoIdentidad;
		$scope.archivosDocumentoIdentidadEliminar = [];

		$scope.formPersona = {
			documentoIdentidad: {
				archivo: {
					desplegado: false,
					scroll:{
						handle: "documentoidentidadfilehandle"
					}
				}	
			}
		};
		/** fin @feature V4-HU006 **/

		$scope.validacionEscaner = ($scope.persona.personaTipoID==undefined);
		console.log($scope.validacionEscaner);

		// INICIO VTADIG2-185- José Martinez 12/02/2018
		if($scope.persona.monedaIDIngresoMensual){
			$scope.persona.monedaIDIngresoMensual = $scope.persona.monedaIDIngresoMensual.toString();	
		}
		// FIN VTADIG2-185- José Martinez 12/02/2018
		$scope.TIPO_TABAJADOR = TABAJADOR.TIPO;

		$scope.onblurPersonaIngresoMensual = function () {
			if($scope.persona.personaIngresoMensual){
				var temp = parseFloat($scope.persona.personaIngresoMensual);
				if(temp&&!isNaN(temp)){
					$scope.persona.personaIngresoMensual = ''+temp;
				}				
			}		
		}
		/**
		 * 
		 * Asigna los campos [archivosDocumentoIdentidad, validarDocumentos] de la persona
		 * - archivosDocumentoIdentidad: arreglo con los datos de los archivos del documento de identidad de la persona
		 * - indica si se debe validar la la cantidad de archivos para continuar
		 * @feature V4-HU006
		 * @param persona
		 * @param conData indica si se incluye la data de los archivos en base 64
		 * @returns {Promise<any>}
		 */
		$scope.obtenerFilesDocumentoIdentidad = function (persona, conData){
			return new Promise((resolver, rechazar) => {
				PersonaService.obtenerFilesDocumentoIdentidad(persona.tipoDocumentoID, persona.personaCodigoDocumento, conData)
					.then(response => {
						persona.archivosDocumentoIdentidad = response.documentos;
						persona.validarDocumentos = response.validarDocumentos;
						$scope.archivosDocumentoIdentidad = persona.archivosDocumentoIdentidad;
						resolver();
					});
			});
		};
		// INICIO VTADIG2-185- José Martinez 12/02/2018
		$scope.actualizaIngresoMensuales = function () {
			
			if($scope.persona.personaIngresoMensual){
				var termina_en_punto = false;
				if($scope.persona.personaIngresoMensual.endsWith('.')){
					termina_en_punto = true;
				};
				var temp = parseFloat($scope.persona.personaIngresoMensual);
				if(temp&&!isNaN(temp)){
					$scope.persona.personaIngresoMensual = ''+temp+(termina_en_punto?'.':'');
				}
			}

			$scope.persona.personaIngresoMensual = validacionService.formatearDosDecimales($scope.persona.personaIngresoMensual);
			console.log('personaIngresoMensual Formulario:'+$scope.persona.personaIngresoMensual);

			$scope.persona.personaIngresoMensualDolares =validacionService.obtenerIngresoMensualDolares($scope.persona.monedaIDIngresoMensual,$scope.persona.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);
			console.log('personaIngresoMensualDolares:'+$scope.persona.personaIngresoMensualDolares);

		}
		// FIN VTADIG2-185- José Martinez 12/02/2018
		/**
		 * 
		 * @feature V4-HU020
		 * @param $value
		 */
		$scope.changeResidente = ($value) => {
			console.log($value);
			// actualizamos el map de tipo_dirección_id -> dirección  
			$scope.persona.direcciones = $scope.persona.direcciones || [];
			$scope.persona.direcciones.forEach(direccion => $scope.mapTipoDireccion[direccion.direccionTipoID] = direccion);
			
			// recuperamos o creamos la dirección personal de tipo según la persona es o no residente
			let tipoDireccionPersonal = $scope.persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO;
			let direccionPersonal = $scope.mapTipoDireccion[tipoDireccionPersonal] || {direccionTipoID: tipoDireccionPersonal};
			
			// recuperamos o creamos la dirección laboral de tipo según la persona es o no residente
			let tipoDireccionLaboral = $scope.persona.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO;
			let direccionLaboral = $scope.mapTipoDireccion[tipoDireccionLaboral] || {direccionTipoID: tipoDireccionLaboral};
			
			// asignamos las dirección
			$scope.persona.direcciones = [];
			$scope.persona.direcciones.push(direccionPersonal);
			if($scope.persona.personaTipoID === PERSONA_TIPO.NATURAL){
				$scope.persona.direcciones.push(direccionLaboral);
			}
			
			console.log($scope.persona);
		};
		$scope.testCasado = function(){
			if($scope.persona.estadoCivilID != ESTADO_CIVIL.CASADO && $scope.persona.estadoCivilID != ESTADO_CIVIL.CONVIVIENTE){
				var indiceConyuge = undefined;

				angular.forEach($scope.persona.personasVinculadas,function(value,index){
					if(parseInt(value.personaTipoRelacionID) == TIPO_RELACION_ID.CONYUGUE){
						indiceConyuge = index;	
					}
				});

				if(indiceConyuge != undefined){
					$scope.persona.personasVinculadas.splice(indiceConyuge,1);					
					if($scope.persona.personasVinculadas.length < 1){
						$scope.vinculadosOptionsPopover.hide();
					}
				}
			}else{
				if(!$scope.persona.personasRelacionadas){
					$scope.persona.personasRelacionadas = [];
				}
				if(!$scope.persona.personasVinculadas){
					$scope.persona.personasVinculadas = [];
				}		
				angular.forEach($scope.persona.personasRelacionadas, function(relacionada){
					if(relacionada.personaTipoID !== PERSONA_TIPO.NATURAL){
						return;
					}
					if($scope.persona.estadoCivilID == ESTADO_CIVIL.CASADO){
						relacionada.tipoRelacionNombre = "Cónyugue";
						relacionada.personaTipoRelacionID = TIPO_RELACION_ID.CONYUGUE;
					}else if ($scope.persona.estadoCivilID == ESTADO_CIVIL.CONVIVIENTE) {
						relacionada.tipoRelacionNombre = "Conviviente";
						relacionada.personaTipoRelacionID = TIPO_RELACION_ID.CONVIVIENTE;
					}				
					if(relacionada.personaEsEmpleado===true){//si la persona relacionada tambien es una persona vinculada se debe agregar las persona vinculada
						//ver si ya pertenece a la lista de vindulados
						var yaVinculado = false;
						angular.forEach($scope.persona.personasVinculadas, function(vinculada, indiceVinculada){								
							if(	vinculada.tipoDocumentoID === relacionada.tipoDocumentoID && 
								vinculada.personaCodigoDocumento === relacionada.personaCodigoDocumento){
								yaVinculado = true;
								//si ya esta vinculado se actualiza la relación
								vinculada.personaTipoRelacionID = 1;	
								vinculada.tipoRelacionNombre = "Cónyuge";
							}														
						});
						if(!yaVinculado){//si no está vinculada se agrega a la lista
							var copiaRelacionada = angular.copy(relacionada);
							copiaRelacionada.personaTipoRelacionID = 1;	
							copiaRelacionada.tipoRelacionNombre = "Cónyuge";
							$scope.persona.personasVinculadas.push(copiaRelacionada);
						}
					}
				});
			}
		}

		$scope.lstTipoDocumentoFilter = shareData.lstTipoDocumento.filter(documento => documento.codigo != LST_TIPO_DOCUMENTO.find(t=>t.codigo 
			== 3).codigo);

		$scope.lstTipoDocumento = $scope.lstTipoDocumentoFilter;	
		$scope.lstTipoRelacion = shareData.lstTipoRelacionada;		
		$scope.lstActividadLaboral = shareData.lstActividadLaboral;
		$scope.lstPersonaOcupacion = shareData.lstPersonaOcupacion;
		$scope.indexTitular=$scope.tempTitular.titular.indexTitular;
		$scope.lstEstadoCivil = shareData.lstEstadoCivil;

		//INICIO VTADIG2-185- José Martinez 12/02/2018
		$scope.lstTipoMoneda=shareData.lstTipoMoneda;
		//FIN VTADIG2-185- José Martinez 12/02/2018

		$scope.lstSexo=shareData.lstSexo;

		$scope.lstPais=lstPais;

		/** INICIO APPVENTA-70 - Anderson Estela Coronel - 02/dic/2020 */
		$scope.lstPaisResidencia = lstPais.filter(pais => pais['codigo'] != "189");
		/** FIN APPVENTA-70 - Anderson Estela Coronel - 02/dic/2020 */

		$scope.lstDepartamento=lstDepartamento;
		/*ubigeo domicilio*/		
		$scope.lstProvincia=lstProvincia;
		$scope.lstDistrito=lstDistrito;
		/*ubigeo nacimiento*/		
		$scope.lstProvinciaNacimiento = lstProvinciaNacimiento;
		$scope.lstDistritoNacimiento = lstDistritoNacimiento;
		/*ubigeo laboral*/
		$scope.lstProvinciaLaboral = lstProvinciaLaboral;
		$scope.lstDistritoLaboral = lstDistritoLaboral;

		$scope.lstTipoVia=shareData.lstTipoVia;
		$scope.lstTipoZona=shareData.lstTipoZona;	
		$scope.lstTiposTrabajador=shareData.lstTiposTrabajador;
		$scope.lstActividadLaboral=shareData.lstActividadLaboral;
		
		//$scope.persona.personasRelacionadas=[];
		$scope.contrato = tempContrato.contrato;
				
		$scope.buscado = false;
		$scope.busquedaEscaner = true;
		$scope.indexToEdit = 0;
		$scope.lstTipoDocumento = [];
		/*prueba titulares*/
		$scope.nuevoTitular = false;
		if(tempContrato.titulares[$scope.indexTitular]===undefined){//nuevo titular			
			if($stateParams.previous==='agregarPersona'){
				$scope.buscado = true;
			}
			$scope.modoEdicion=false;
			$scope.nuevoTitular = true;
		}else{//edicion de titular
			$scope.modoEdicion=true;
			$scope.busquedaEscaner = false;
		}
		
		
		var geocoder = new google.maps.Geocoder();

		$scope.addressSelected = undefined;
		$scope.TIPO_ASOCIADO = TIPO_ASOCIADO;
		$scope.TIPO_DOCUMENTO = TIPO_DOCUMENTO;

		$scope.getAddressSuggestions=function(queryString){
			var defer = $q.defer();            
			geocoder.geocode(
				{
					componentRestrictions: {
						country: 'PE'
					},
					address: queryString
				},
				function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) { 
						$scope.listAddress = results;
						defer.resolve(results); 
					}else { 
						
						defer.reject(results); 
					}
				}
			);
			return defer.promise;	    
		};
		$scope.busquedaScanerOptions = [
			{
				id: 0,
				name: 'Ingresar Manualmente',
				label: "Ingresar Manualmente"
			}
		];
		$scope.busquedaScanerOptionsScaner = [
			{
				id: 0,
				name: 'Escanear DNI',
				label: "Escanear DNI"
			}
		];		
		$scope.busquedaScanerOptionsManual = $scope.busquedaScanerOptions;
		$ionicPopover.fromTemplateUrl('buscar-persona-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.scanerOptionsPopover = popover;
			//$scope.relacionadosOptionsPopover = popover;
		});

		$scope.openScanerOptionsPopover = function ($event) {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.show($event);
			}
		};
        /**
         * @feature V4-HU020
         * @param persona
         * @returns {boolean}
         */
		$scope.bloquearInputPorNoResidente = (persona) => {
			return false;
			//return persona && persona.personaID && !persona.esResidenteOriginal;
		};
		$scope.doActionEscaner = function (index) {
			if(index===0){				
				if($scope.busquedaEscaner){
					$scope.tipoNumeroDocumentoDisabled = true;
					$scope.busquedaScanerOptions = $scope.busquedaScanerOptionsScaner;
					$scope.validacionEscaner = false;
				}else{
					$scope.tipoNumeroDocumentoDisabled = false;
					$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;
					if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'N'){
						$scope.validacionEscaner = true;
					}
				}
				$scope.busquedaEscaner = !$scope.busquedaEscaner;
			}
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.hide();
			}
			$scope.inicializaPersona($scope.persona.tipoDocumento);
		};
		
		$scope.openScanerOptionsPopover = function ($event) {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.show($event);
			}
		};
		$scope.closeScanerOptionsPopover = function () {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.hide();
			}
		};
		
		$scope.$on('$destroy', function () {
			if($scope.scanerOptionsPopover){
				$scope.scanerOptionsPopover.remove();
			}
		});

		$scope.triggerClick = function () {
			$timeout(function() {
				angular.element(document.querySelector("#autocomplete")).triggerHandler('click');
			}, 0);
		};

		$scope.triggerClickLaboral = function () {
			$timeout(function() {
				angular.element(document.querySelector("#autocompleteLaboral")).triggerHandler('click');
			}, 0);
		};

		$scope.cancelButtonClickedMethod = function (callback,posicionDireccion) {
			$scope.obtenerDireccion(callback,posicionDireccion);
		};

		$scope.clickedMethod = function (callback,posicionDireccion) {
			$scope.obtenerDireccion(callback, posicionDireccion);
		};

		$scope.obtenerDireccion = function(callback, posicionDireccion){
			var address_components;
			if(callback.item === undefined){
				address_components = callback.selectedItems.address_components;
			}else{
				address_components = callback.item.address_components;
			}
			var distrito="", provincia="", departamento="", tipoVia="";
			for(var item in address_components){
				if(address_components[item].types.length==2 || address_components[item].types.length==1){
					var esTipoVia = address_components[item].types.filter(function(el){return el=="route";});					
					var esDistrito = address_components[item].types.filter(function(el){return el=="locality" || el=="political";});
					var esProvincia = address_components[item].types.filter(function(el){return el=="administrative_area_level_2" || el=="political";});
					var esDepartamento = address_components[item].types.filter(function(el){return el=="administrative_area_level_1" || el=="political";});
					if(esDistrito.length==2){
						distrito = address_components[item].long_name.toLowerCase().replace("provincia de ","").replace("distrito de ","").replace("departamento de ","");
						console.log(distrito);
					}
					
					if(esProvincia.length==2){
						provincia = address_components[item].long_name.toLowerCase().replace("provincia de ","").replace("distrito de ","").replace("departamento de ","");
						console.log(provincia);
					}
					
					if(esDepartamento.length==2){
						departamento = address_components[item].long_name.toLowerCase().replace("provincia de ","").replace("distrito de ","").replace("departamento de ","");
						console.log(departamento);
					}

					if(esTipoVia.length>0){
						var tiposVia = $scope.lstTipoVia.filter(function(el){return address_components[item].long_name.toUpperCase().indexOf(el.descripcionLarga)!==-1;});
						if(tiposVia.length>0){
							$scope.persona.direcciones[posicionDireccion].tipoViaID = tiposVia[0].codigo;
				}
			}
				}
			}


			if(departamento!==""){
				departamento = $scope.accentFold(departamento);
				for(item in $scope.lstDepartamento){
					if($scope.lstDepartamento[item].descripcion==departamento.toUpperCase()){
						$scope.persona.direcciones[posicionDireccion].departamentoID = $scope.lstDepartamento[item].codigo;						
						$scope.cambiarDepartamento($scope.persona.direcciones[posicionDireccion].departamentoID,true);
						break;
					}
				}
			}
			$timeout(function () {
				if(provincia!==""){
					provincia = $scope.accentFold(provincia);
					for(item in $scope.lstProvincia){
						if($scope.lstProvincia[item].descripcion==provincia.toUpperCase()){							
							$scope.persona.direcciones[posicionDireccion].provinciaID = $scope.lstProvincia[item].codigo;
							$scope.cambiarProvincia($scope.persona.direcciones[posicionDireccion].departamentoID,$scope.persona.direcciones[0].provinciaID);                        
							break;
						}
					}
				}
			}, 1500);

			$timeout(function () {
				if(distrito!==""){
					distrito = $scope.accentFold(distrito);
					for(item in $scope.lstDistrito){
						if($scope.lstDistrito[item].descripcion==distrito.toUpperCase()){							
							$scope.persona.direcciones[posicionDireccion].distritoID = $scope.lstDistrito[item].codigo; 
							break;
						}
					}
				}
			}, 3000);
		};

		$scope.accentFold = function(inStr) {
			return inStr.replace(/([àáâãäå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g, function(str,a,c,e,i,n,o,s,u,y,ae) { if(a) return 'a'; else if(c) return 'c'; else if(e) return 'e'; else if(i) return 'i'; else if(n) return 'n'; else if(o) return 'o'; else if(s) return 's'; else if(u) return 'u'; else if(y) return 'y'; else if(ae) return 'ae'; });
		};

		$scope.cambiarDepartamentoNacimiento=function(departamentoNacimientoID,limpiarDistrito){
			$scope.url=BACK_END_URL+"provincia/"+departamentoNacimientoID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstProvinciaNacimiento=response.data.data;
				if(limpiarDistrito){
					$scope.lstDistritoNacimiento = [];
				}
			}, function errorCallback(response) {
				
			});
		};
		$scope.changeOcupacionSeleccionado = function (newValue, oldValue){
					$scope.persona.personaOcupacion  = newValue.codigo;
		};
		$scope.cambiarProvinciaNacimiento=function(departamentoNacimientoID,provinciaNacimientoID){
			
			$scope.url=BACK_END_URL+"distrito/"+departamentoNacimientoID+"/"+provinciaNacimientoID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstDistritoNacimiento=response.data.data;
			}, function errorCallback(response) {
				
			});
		};
		$scope.cambiarDepartamentoLaboral=function(departamentoLaboralID,limpiarDistrito){
			$scope.url=BACK_END_URL+"provincia/"+departamentoLaboralID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstProvinciaLaboral=response.data.data;
				if(limpiarDistrito){
					$scope.lstDistritoLaboral=[];
				}
			}, function errorCallback(response) {
				
			});
				
		};

		$scope.cambiarProvinciaLaboral=function(departamentoLaboralID,provinciaLaboralID){
			
			$scope.url=BACK_END_URL+"distrito/"+departamentoLaboralID+"/"+provinciaLaboralID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {
				
				
				$scope.lstDistritoLaboral=response.data.data;
			}, function errorCallback(response) {
				
			});
		};

		$scope.cambiarDepartamento=function(departamentoID,limpiarDistrito){
			$scope.url=BACK_END_URL+"provincia/"+departamentoID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {				
				
				$scope.lstProvincia=response.data.data;
				if(limpiarDistrito){
					$scope.lstDistrito=[];
				}
			}, function errorCallback(response) {
				
			});			    
		};

		$scope.cambiarProvincia=function(departamentoID,provinciaID){
			
			$scope.url=BACK_END_URL+"distrito/"+departamentoID+"/"+provinciaID;
			
			$http({
			  method: 'GET',
			  url: $scope.url
			}).then(function successCallback(response) {
				
				$scope.lstDistrito=response.data.data;				
			}, function errorCallback(response) {
				
			});
		};
		$scope.cambiarLstEstadoCivilSegunEstadoCivil = function(estadoCivilID){
			if(!estadoCivilID){
				$scope.lstEstadoCivil = shareData.lstEstadoCivil;	
			}
			else{
				$scope.lstEstadoCivil = [];
				estadoCivilID = estadoCivilID.trim();
				angular.forEach(shareData.lstEstadoCivil,function(item){
					if(estadoCivilID === "C"){//si está casada
						if(item.codigo==="V"||item.codigo==="D"||item.codigo==="N"||item.codigo==="C"){
							$scope.lstEstadoCivil.push(item);
						}
					}
					else if(estadoCivilID === "N"){//si está casada
						if(item.codigo==="N"||item.codigo==="C"||item.codigo==="D"||item.codigo==="V"){
							$scope.lstEstadoCivil.push(item);
						}
					}
					else if(estadoCivilID === "V"){//si está casada
						if(item.codigo==="V"||item.codigo==="C"||item.codigo==="D"||item.codigo==="N"){
							$scope.lstEstadoCivil.push(item);
						}
					}
					else if(estadoCivilID === "D"){//si está casada
						if(item.codigo==="D"||item.codigo==="C"||item.codigo==="V"||item.codigo==="N"){
							$scope.lstEstadoCivil.push(item);
						}
					}
					else if(estadoCivilID === "S"){//si está casada						
							$scope.lstEstadoCivil.push(item);						
					}
				});
			}			
		}			
		/*contraton con o sin persona juridica*/
		$scope.contratoConPersonaJuridica = false;
		angular.forEach(tempContrato.titulares,function(titular){
			if(titular.personaTipoID==="J"){
				$scope.contratoConPersonaJuridica = true;
			}
		});
		if($scope.nuevoTitular&&!$scope.contratoConPersonaJuridica){
			angular.forEach($scope.lstTipoDocumentoFilter,function(tipoDocumento){
				if(tipoDocumento.codigo !== TIPO_DOCUMENTO.RUC){
					$scope.lstTipoDocumento.push(tipoDocumento);
				}
			});
		}
		if($scope.nuevoTitular&&$scope.contratoConPersonaJuridica){
			angular.forEach($scope.lstTipoDocumentoFilter,function(tipoDocumento){
				if(tipoDocumento.codigo === TIPO_DOCUMENTO.RUC){
					$scope.lstTipoDocumento.push(tipoDocumento);
				}
			});
		}
		if($scope.persona){			
			/*listas de estado civil, se filtrar solo si la persona ya está registrada*/			
			if($scope.persona.personaID){
				$scope.cambiarLstEstadoCivilSegunEstadoCivil($scope.persona.estadoCivilInicialID);
			}
			if($scope.nuevoTitular){
				if($scope.contratoConPersonaJuridica){
					$scope.persona.personaTipoID = "J";
					$scope.persona.tipoDocumentoID = TIPO_DOCUMENTO.RUC;

				}
				if(!$scope.contratoConPersonaJuridica){
					$scope.persona.personaTipoID = "N";
					if(!$scope.persona.tipoDocumentoID){
						$scope.persona.tipoDocumentoID = TIPO_DOCUMENTO.DNI;
					}					
				}
				$scope.modoEdicion=false;			
			}else{
				$scope.modoEdicion=true;			
			}
			if(!$scope.persona.paisID){
				$scope.persona.paisID = "189";
			}
			$scope.persona = validacionService.ajustarPersona($scope.persona,shareData,$stateParams.asociadoTipoID);						
		}else{	
			$scope.inicializaPersona();
		}
		
		$scope.changeTipoDocumentoSeleccionado = function (newValue, oldValue){

			$scope.persona.personaTipoID = 'N';
			$scope.persona.tipoDocumentoID = newValue.codigo;
			$scope.persona.tipoDocumento = newValue;
			if(newValue.codigo===$scope.TIPO_DOCUMENTO.RUC){
                /*inicio @issue EVALCRED-FINAL*/
                $scope.validacionEscaner = false;
                /*fin @issue EVALCRED-FINAL*/
				$scope.persona.personaTipoID = 'J';
			}
			if(newValue.codigo===$scope.TIPO_DOCUMENTO.DNI){
                /*inicio @issue EVALCRED-FINAL*/
                $scope.validacionEscaner = true;
                /*fin @issue EVALCRED-FINAL*/
				$scope.busquedaEscaner = true;
				$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsManual;	
			}else{
                /*inicio @issue EVALCRED-FINAL*/
                $scope.validacionEscaner = false;
                /*fin @issue EVALCRED-FINAL*/
				$scope.busquedaEscaner = false;
				$scope.busquedaScanerOptions=$scope.busquedaScanerOptionsScaner;
			}
			$scope.persona.personaCodigoDocumento  = undefined;
			$scope.modoEdicion=false;			
		};

		/**
		 * 
 		 * @param tipoDocumento
		 * @param numeroDocumento
		 * @feature V4-HU020
		 *  - cliente no residente
		 */
		$scope.inicializaPersona = function (tipoDocumento,numeroDocumento){
			$scope.persona = {};
			$scope.persona.asociadoTipoID = $stateParams.asociadoTipoID;
			$scope.modoEdicion = false;
			$scope.buscado = false;
			if(tipoDocumento){
				$scope.persona.tipoDocumento = tipoDocumento;
				$scope.persona.tipoDocumentoID = tipoDocumento.codigo;
			}else{
				if(!$scope.contratoConPersonaJuridica){
					$scope.persona.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
					$scope.persona.tipoDocumentoID = $scope.TIPO_DOCUMENTO.DNI;
				}else{
					$scope.persona.tipoDocumento = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.RUC];//$scope.lstTipoDocumento[3];
					$scope.persona.tipoDocumentoID = $scope.TIPO_DOCUMENTO.RUC;
				}			
			}
			if(numeroDocumento){
				$scope.persona.personaCodigoDocumento = numeroDocumento;				
			}

			$scope.persona.personaTipoID = 'N';			
			if(tipoDocumento && tipoDocumento.codigo==="8"){
				$scope.persona.personaTipoID = 'J';
			}

			$scope.persona.paisID = "189";

			/** inicio @feature V4-HU020 **/
			$scope.persona.esResidente = true;
			if($scope.persona.personaTipoID === 'J'){
				$scope.persona.direcciones = [{direccionTipoID: $scope.persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO}];
			}else{
				$scope.persona.direcciones = [{direccionTipoID: $scope.persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO},
											  {direccionTipoID: $scope.persona.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO}];
			}
			/** fin @feature V4-HU020 **/

			if($scope.persona.tipoDocumentoID!==shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI].codigo){
				$scope.busquedaEscaner = false;
			}
			$scope.lstEstadoCivil = shareData.lstEstadoCivil;
			//INICIO VTADIG2-185- José Martinez 12/02/2018
			$scope.persona.monedaIDIngresoMensual='1';			
			//INICIO VTADIG2-185- José Martinez 12/02/2018
			
		}

		$scope.buscarPersona=function(tipoDocumento,codigoDocumento,scanner,dataScanner,scannerDNI){
			
			if(scanner == undefined){
				scanner = false;
			}
			/*validar longitud documento, ngChange evita que se ingrese un formato invalido*/
			if(!validacionService.validarDocumento(tipoDocumento,codigoDocumento,true,$scope.persona.personaTipoID)){
				return false;
			}
			$scope.modoEdicion = false;
			$scope.buscado = true;
			if(PersonaService.existeComoTitular(tipoDocumento.codigo,codigoDocumento)){
				$scope.inicializaPersona(tipoDocumento);
				return false;
			}
			/**
			 * 
			 * 
			 * @feature V4-HU006
			 * - se agregan datos sobre los archivos del documento de identida de la persona
			 */
			PersonaService.validaPersona(tipoDocumento.codigo, codigoDocumento,tempContrato.contrato.grupoSeleccionado.grupoID,tempContrato.contrato.certificadoSeleccionado.id.certificadoPosicionID)
				.then(
					function (persona1) {						
						if(persona1){

							if(persona1.personaEsEmpleadoEspecial&&tempContrato.contrato.contratoNroAsambleaGrupo>1){								
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Operación cancelada. Los clientes que poseen personas vinculadas a la empresa solo podrán ingresar al sistema si participan desde la asamblea inaugural. La cuota de ingreso actual es la número " + tempContrato.contrato.contratoNroAsambleaGrupo + ".", okType: 'button-assertive'});
								$scope.inicializaPersona(tipoDocumento)
								return;
							}
							if(persona1.personaOFAC){								
								$scope.inicializaPersona(tipoDocumento);								
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El titular se encuentra en la lista OFAC." , okType: 'button-assertive'});
								proformaService.enviarCorreoOFAC(persona1.tipoDocumentoID,persona1.personaCodigoDocumento,persona1.personaNombreCompletoOFAC);
								return;
							}							
							/*INICIO INTEGRACION EC*/
							//PersonaService.obtenerPersona(tipoDocumento.codigo,codigoDocumento).then(
							PersonaService.obtenerPersona(tipoDocumento.codigo,codigoDocumento,APP.MODULO.INGRESO_DE_VENTA.ID).then(
							/*INICIO INTEGRACION EC*/
							function(persona){	
								if(persona){
									/*INICIO INTEGRACION EC*/
									if(persona.evaluacionCrediticia&&persona.evaluacionCrediticia.aprobado==false){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: persona.evaluacionCrediticia.mensaje , okType: 'button-assertive'});
										$scope.inicializaPersona(tipoDocumento);
										return;
									}
									/*FIN INTEGRACION EC*/
									if(persona.personaContratosResueltos){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El titular tiene contratos resueltos." , okType: 'button-assertive'});
										proformaService.enviarCorreoContratosResueltos(tipoDocumento.codigo,codigoDocumento, persona.personaNombreCompleto);
										/*INICIO INTEGRACION EC*/
										$scope.inicializaPersona(tipoDocumento);
										/*FIN INTEGRACION EC*/
										return;
									}
									if(persona.personaContratosLegal){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El titular tiene contratos en legal." , okType: 'button-assertive'});
										proformaService.enviarCorreoContratosLegal(tipoDocumento.codigo,codigoDocumento, persona.personaNombreCompleto);
										/*INICIO INTEGRACION EC*/
										$scope.inicializaPersona(tipoDocumento);
										/*INICIO INTEGRACION EC*/
										return;
									}									
									/*INICIO INTEGRACION EC*/
									if(persona1.empresasConContratosLegales||persona1.empresasConContratosResueltos){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No es posible ingresar venta a la persona, debido a que cuenta con contrato(s) en situación legal o resueltos con Pandero." , okType: 'button-assertive'});
										if(persona1.empresasConContratosResueltos){
											//proformaService.enviarCorreoContratosResueltos(tipoDocumento.codigo,codigoDocumento, persona.personaNombreCompleto);
											$scope.inicializaPersona(tipoDocumento);
										}
										if(persona1.empresasConContratosLegales){
											//proformaService.enviarCorreoContratosLegal(tipoDocumento.codigo,codigoDocumento, persona.personaNombreCompleto);
											$scope.inicializaPersona(tipoDocumento);
										}
										conforme = false;
									}
									/*FIN INTEGRACION EC*/
									persona.estadoCivilInicialID = persona.estadoCivilID;												 
									$scope.cambiarLstEstadoCivilSegunEstadoCivil(persona.estadoCivilInicialID);
									
									persona.esCasadoInicio = validacionService.esCasado(persona.estadoCivilInicialID);
									persona = validacionService.ajustarPersona(persona,shareData,$stateParams.asociadoTipoID);
									if(persona.personaNacimientoUbigeo){
										$scope.cambiarDepartamentoNacimiento(persona.departamentoNacimientoID);
										$scope.cambiarProvinciaNacimiento(persona.departamentoNacimientoID,persona.provinciaNacimientoID);
									}

									if(persona.direcciones[0]!==undefined){
										$scope.cambiarDepartamento(persona.direcciones[0].departamentoID);
										$scope.cambiarProvincia(persona.direcciones[0].departamentoID,persona.direcciones[0].provinciaID);
									}

									if(persona.direcciones[1]!==undefined){
										$scope.cambiarDepartamentoLaboral(persona.direcciones[1].departamentoID);
										$scope.cambiarProvinciaLaboral(persona.direcciones[1].departamentoID,persona.direcciones[1].provinciaID);
									}

									if($rootScope.me.documentoDigital && $scope.persona.personaTipoID == 'N'){
										if(!persona.personaDocumentoValidado){
											$scope.validacionEscaner = true;
										}else{
											$scope.validacionEscaner = false;
										}
									}

									//$scope.ajustarPersona(persona,true);
									tempTitular.titular = persona;
									tempTitular.titular.indexTitular = $scope.indexTitular;
									$scope.persona = tempTitular.titular;
									$scope.modoEdicion = true;
									$scope.buscado = true;					
									/*inicio @issue EVALCRED-57*/
									$scope.validado = persona.personaDocumentoValidado?true:false;
									/*fin @issue EVALCRED-57*/
									if(!tempTitular.titular.personaDocumentoValidado){
										tempTitular.titular.personaDocumentoValidado = false;
									}
									if($scope.persona.tipoDocumento===$scope.TIPO_DOCUMENTO.RUC){
										if(!$scope.persona.personaEstado && !$scope.persona.pesonaFlag_22){
											tempTitular.titular.personaDocumentoValidado = false;
										}else{
											tempTitular.titular.personaDocumentoValidado = true;
										}
									}
									if(tempTitular.titular.tipoDocumentoID!==TIPO_DOCUMENTO.DNI&&tempTitular.titular.tipoDocumentoID!==TIPO_DOCUMENTO.RUC){
										tempTitular.titular.personaDocumentoValidado = true;
									}
				                                    	/*inicio @issue EVALCRED-51*/
									if(scanner){
                                        /*inicio @issue VTADIG2-507 / VTADIG2-527*/
                                        if(!persona.esEquifax){
                                            $scope.getDataPDF417(dataScanner);
                                        }
                                        /*fin @issue VTADIG2-507 / VTADIG2-527*/
										/*tempTitular.titular.personaNombre = dataScanner.nombres;
										tempTitular.titular.personaApellidoPaterno = dataScanner.apellidoPaterno;
										tempTitular.titular.personaApellidoMaterno = dataScanner.apellidoMaterno;*/
										$scope.persona.personaDocumentoValidado = true;
									}
                                    					/*fin @issue EVALCRED-51*/
									if(scannerDNI){
										$scope.modoEdicion = false;
										$scope.buscado = true;
										$scope.persona.personaDocumentoValidado = true
									}
								}else{
									$scope.inicializaPersona($scope.persona.tipoDocumento,$scope.persona.personaCodigoDocumento);
									tempTitular.titular = $scope.persona;
									tempTitular.titular.indexTitular = $scope.indexTitular;
									if(tempTitular.titular.tipoDocumentoID!==TIPO_DOCUMENTO.DNI&&tempTitular.titular.tipoDocumentoID!==TIPO_DOCUMENTO.RUC){
										tempTitular.titular.personaDocumentoValidado = true;
									}else{
										tempTitular.titular.personaDocumentoValidado = false;
									}									
									$scope.persona = tempTitular.titular;
									//alert("Persona con el "+$scope.persona.tipoDocumento.descripcion+" "+$scope.persona.personaCodigoDocumento+" no encontrado, ingrese sus datos manualmente");
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Persona con el "+$scope.persona.tipoDocumento.descripcion+" "+$scope.persona.personaCodigoDocumento+" no encontrado, ingrese sus datos manualmente." , okType: 'button-assertive'});
									$scope.modoEdicion = false;
									$scope.buscado = true;
									if(scanner){
										$scope.getDataPDF417(dataScanner);
										/*tempTitular.titular.personaNombre = dataScanner.nombres;
										tempTitular.titular.personaApellidoPaterno = dataScanner.apellidoPaterno;
										tempTitular.titular.personaApellidoMaterno = dataScanner.apellidoMaterno;*/
										$scope.persona.personaDocumentoValidado = true;
										$scope.modoEdicion = true;
									}		
									if(scannerDNI){
										$scope.modoEdicion = false;
										$scope.buscado = true;
										$scope.persona.personaDocumentoValidado = true
									}							
																				
								}
								/** inicio @feature V4-HU006 **/
								$scope.obtenerFilesDocumentoIdentidad(tempTitular.titular);
								/** fin @feature V4-HU006 **/
							},
							function(error){
								$scope.buscado = false;
							})
						}
					},
					function(error){
						//console.log("ERRORRRRRRRRR");
						$scope.inicializaPersona(tipoDocumento)
					}
				);
			};		

		$scope.comprobar4por1 = function(valor){
			if(parseFloat(valor)>4*validacionService.aDosDecimales($scope.contrato.cuotaMensualFinal)){
				return true;
			}else{
				return false;
			}
		};
		/**
		 * 
		 * despliega la sección de carga de archivos para el documento de identidad
		 * y actualiza la data de los archivos incluyendo la data en base64
		 * 
		 * @feature V4-HU006
		 */
		$scope.desplegarArchivos = function(){
			$scope.formPersona.documentoIdentidad.archivo.desplegado = !$scope.formPersona.documentoIdentidad.archivo.desplegado
			if(!$scope.persona.archivosDocumentoIdentidad || $scope.persona.archivosDocumentoIdentidad.find(adi => !adi.base64)){
				$scope.obtenerFilesDocumentoIdentidad($scope.persona, true);
			}
		};
		/**
		 * 
		 * quita un elemento de la lista de archivos de la persona 
		 * y lo incluye en la lista de archivos a eliminar
		 * 
		 * @feature V4-HU006
		 * @param index
		 */
		$scope.quitarImagen = function(index){
			$ionicPopup.confirm({
				title: 'Archivo',
				template: '¿Seguro que desea quitar este archivo?',
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			}).then(function (res) {
				if (res) {

					adi = $scope.archivosDocumentoIdentidad[index];
					if (adi) {
						$scope.archivosDocumentoIdentidad.splice(index, 1);
						if (adi.nombre) {
							$scope.archivosDocumentoIdentidadEliminar.push(adi);
						}
					}
				}
			});
		};
		/**
		 * 
		 * abre la captura e imagenes
		 * y agrega el resultado los archivos del documento de identidad de la persona
		 * 
		 * @feature V4-HU006
		 */
		$scope.tomarFoto = function(){
			camaraService.hacerCaptura().then(function(imagen){
				var maxTamanio = configuracionArchivos.maxTamanio;
				if(base64Utils.lengthInKB(imagen) <= maxTamanio) {
					$scope.archivosDocumentoIdentidad.push({base64: imagen});
					$ionicScrollDelegate.$getByHandle($scope.formPersona.documentoIdentidad.archivo.scroll.handle).scrollBottom(true);
				} else{
					$ionicPopup.alert({ title: 'Alerta', template: "El tamaño del archivo debe ser menor a "+maxTamanio/1024 +"MB.", okType: 'button-assertive'});
				}
			});
		};
		/**
		 *
		 * abre la selección de archivos
		 * y agrega el resultado los archivos del documento de identidad de la persona 
		 * 
		 * @feature V4-HU006
		 */
		$scope.cargarImagen = function(){
			camaraService.seleccionarImagen().then(function(imagen){
				var maxTamanio = configuracionArchivos.maxTamanio;
				if(base64Utils.lengthInKB(imagen) <= maxTamanio) {
					$scope.archivosDocumentoIdentidad.push({base64: imagen});
					$ionicScrollDelegate.$getByHandle($scope.formPersona.documentoIdentidad.archivo.scroll.handle).scrollBottom(true);
				} else{
					$ionicPopup.alert({ title: 'Alerta', template: "El tamaño del archivo debe ser menor a "+maxTamanio/1024 +"MB.", okType: 'button-assertive'});
				}
			});
		};
		$scope.validar4por1 = function(){
			$scope.persona.cumpleEl4por1 = true;
			if($scope.persona.personaTipoID==='N'){
				var esCasado = false;// o era casado
				var conyugeFormaPardeDeIngresosFamiliares = false;
				var conyuge;
				if($scope.persona.personasRelacionadas){
					angular.forEach($scope.persona.personasRelacionadas,function(personaRelacionada){
						if(parseInt(personaRelacionada.personaTipoRelacionID)===1||parseInt(personaRelacionada.personaTipoRelacionID)===1){//esCasado
							esCasado = true;
							conyuge = personaRelacionada;
							if(conyuge.personaIngresoMensual && conyuge.personaIngresoMensualCalif){
								conyugeFormaPardeDeIngresosFamiliares = true;
							}
						}
					});
				}
				if(!esCasado){
					console.log("no es casado");
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					$scope.persona.ingresoTotalMensualDolares = validacionService.aDosDecimales(parseFloat($scope.persona.personaIngresoMensualDolares));
					// FIN VTADIG2-185- José Martinez 12/02/2018
				}
				else if(!conyugeFormaPardeDeIngresosFamiliares){
					console.log("es casado pero su conyuge no forma parte de los ingresos familiares");
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					$scope.persona.ingresoTotalMensualDolares = validacionService.aDosDecimales(parseFloat($scope.persona.personaIngresoMensualDolares));
					// FIN VTADIG2-185- José Martinez 12/02/2018
				}
				else if(conyugeFormaPardeDeIngresosFamiliares){
					console.log("es casado pero su conyuge forma parte de los ingresos familiares");
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					$scope.persona.ingresoTotalMensualDolares = validacionService.aDosDecimales(parseFloat($scope.persona.personaIngresoMensualDolares)+parseFloat(conyuge.personaIngresoMensualDolares));
					// FIN VTADIG2-185- José Martinez 12/02/2018
				}
			}else if($scope.persona.personaTipoID==='J'){
				// INICIO VTADIG2-185- José Martinez 12/02/2018
				$scope.persona.ingresoTotalMensualDolares = validacionService.aDosDecimales(parseFloat($scope.persona.personaIngresoMensualDolares));
				// FIN VTADIG2-185- José Martinez 12/02/2018
			}
			// INICIO VTADIG2-185- José Martinez 12/02/2018
			$scope.persona.cumpleEl4por1 = $scope.comprobar4por1($scope.persona.ingresoTotalMensualDolares);
			// FIN VTADIG2-185- José Martinez 12/02/2018
		};

		$scope.elementoErroneo = function (form){

			var error = form.$error;
			console.log("LISTA DE ELEMENTOS ");
			console.log(error.length);
    		angular.forEach(error.validar, function(field){
        		if(field.$invalid){
            	var fieldName = field.$name;
            	document.getElementById(fieldName).scrollIntoView();
        	}
    		});
		}
		/**
		 * 
		 * @param form
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.continuar=function(form){		
			console.log(form);
			$scope.valido=false;
			var showAlerts = true;

			if(!$scope.persona.personasRelacionadas){
				$scope.persona.personasRelacionadas=[];
			}

			/** inicio @feature V4-HU020 **/
			$scope.persona.esResidente = $scope.persona.esResidente !== false;
			/** fin @feature V4-HU020 **/
			$scope.valido = validacionDeTitular.validardatosPersona($scope.persona,showAlerts,tempContrato,form);
			if(!$scope.valido){
				//alert($rootScope.respuestaError.mensaje);
				$scope.elementoErroneo(form); 
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: $rootScope.respuestaError.mensaje , okType: 'button-assertive'});
				if($scope.persona.personaTipoID === PERSONA_TIPO.NATURAL){
					$rootScope.respuestaError.mensaje=($scope.persona.personaNombreCompleto?$scope.persona.personaNombreCompleto:$scope.persona.personaNombre)+": "+$rootScope.respuestaError.mensaje;					
				}else{
					$rootScope.respuestaError.mensaje=$scope.persona.personaRazonSocial.trim()+": "+$rootScope.respuestaError.mensaje;				
				}			
			}else{
				var personaCodigoDocumentoIsValid = true;
				if($scope.nuevoTitular){
					angular.forEach(tempContrato.titulares,function(item){
						if($scope.persona.personaCodigoDocumento==item.personaCodigoDocumento){
							//alert("Persona con documento "+$scope.persona.personaCodigoDocumento+" ya está agregado");
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Persona con documento "+$scope.persona.personaCodigoDocumento+" ya está agregado." , okType: 'button-assertive'});
							personaCodigoDocumentoIsValid = false;
						}
					});
				}
				if(personaCodigoDocumentoIsValid){
					$scope.persona.personaPEP=$scope.persona.personaPEP ? true : false;
					$scope.persona.personaSujetoObligado=$scope.persona.personaSujetoObligado ? true : false;
					$scope.persona.personaPrevLavadoActivos=$scope.persona.personaPrevLavadoActivos ? true : false;
					$scope.persona.personaActividadMinera=$scope.persona.personaActividadMinera ? true : false;
					$scope.persona.personaGrupoEconomico=$scope.persona.personaGrupoEconomico ? true : false;
					$scope.persona.paisID = parseInt($scope.persona.paisID);
					/** inicio @feature V4-HU020 **/
					if($scope.persona.paisResidenciaId){
						$scope.persona.paisResidenciaId = parseInt($scope.persona.paisResidenciaId);
					}
					/** fin @feature V4-HU020 **/
					$scope.persona.personaTipoRelacionID = parseInt($scope.persona.personaTipoRelacionID);
					
					if($scope.persona.personaGiroNegocioObj){
						$scope.persona.actividadLaboralID = $scope.persona.personaGiroNegocioObj.codigo+'';
					}
					if($scope.persona.personaOcupacionObj){
						$scope.persona.personaOcupacion = $scope.persona.personaOcupacionObj.codigo+'';
					}

					$scope.persona.tipoDocumentoID = $scope.persona.tipoDocumento.codigo;			

					if($scope.persona.personaTipoID==='J'){
						$scope.persona.personaRUC = $scope.persona.personaCodigoDocumento;
						if($scope.persona.direcciones){
							if($scope.persona.direcciones[0]){
								if(!$scope.persona.direcciones[0].direccionTelefono1){
									$scope.persona.direcciones[0].direccionTelefono1 = $scope.persona.direccionTelefono1;
								}
								/** inicio @feature V4-HU020 **/
								$scope.persona.direcciones[0].direccionTipoID = $scope.persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO;
								/** fin @feature V4-HU020 **/
							}
						}
					}else{
						//$scope.persona.personaRUC = "";
						if($scope.persona.direcciones){
							if($scope.persona.direcciones[0]){
								/** inicio @feature V4-HU020 **/
								$scope.persona.direcciones[0].direccionTipoID = $scope.persona.esResidente ? DIRECCION.TIPO.PERSONAL : DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO;
								/** fin @feature V4-HU020 **/
							}
							if($scope.persona.direcciones[1]){
								/** inicio @feature V4-HU020 **/
								$scope.persona.direcciones[1].direccionTipoID = $scope.persona.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO;
								/** fin @feature V4-HU020 **/
							}
						}
					}
					if(!$scope.persona.personaApellidoMaterno){ $scope.persona.personaApellidoMaterno='';}
					$scope.persona.personaNombreCompleto = $scope.persona.personaApellidoPaterno + " "+ $scope.persona.personaApellidoMaterno+" "+$scope.persona.personaNombre;                
					$scope.persona.tipoDocumentoID = $scope.persona.tipoDocumento.codigo;
					$scope.persona.personaTipoDocumentoNombre  = $scope.persona.tipoDocumento.descripcion;
					if($scope.persona.personaID===undefined){
						$scope.persona.personaID = null;
					}

					$scope.validar4por1();
					/** inicio @feature V4-HU006 **/
					//$scope.confirmarTitular();
					$scope.cargarArchivos().then($scope.confirmarTitular);
					/** fin @feature V4-HU006 **/
				}	
			}		
		};
		/**
		 * 
		 * actualiza los archivos del documento de identidad
		 * de la persona en el repositorio
		 * 
		 * @feature V4-HU00
		 * @returns {Promise<any>}
		 */
		$scope.cargarArchivos = function(){

			return new Promise((resolver, rechazar) => {
				var promesas = []

                $scope.archivosDocumentoIdentidad = $scope.archivosDocumentoIdentidad || [];
				
				$scope.archivosDocumentoIdentidadAgregar = $scope.archivosDocumentoIdentidad.filter(adi => !adi.nombre);

				$scope.archivosDocumentoIdentidadEliminar
					.forEach(el => {
						promesas.push(PersonaService.eliminarFileDocumentoIdentidad($scope.persona.tipoDocumentoID, $scope.persona.personaCodigoDocumento, el.nombre));
					});

				$scope.archivosDocumentoIdentidadAgregar
					.forEach(adi => {
						promesas.push(PersonaService.guardarFileDocumentoIdentidad($scope.persona.tipoDocumentoID, $scope.persona.personaCodigoDocumento, adi.base64));
					});

				Promise.all(promesas).then(values=>resolver(values), reason => rechazar(reazon));
			});
		};
		/**
		 * 
		 * 
		 * @feature V4-HU00
		 *  - se agrega la actualización de los datos de los archivos
		 *    como paso previo para continuar
		 */
		$scope.confirmarTitular = function(){
			/** inicio @feature V4-HU00 **/
			$scope.obtenerFilesDocumentoIdentidad($scope.persona).then(()=>{
				tempContrato.titulares[$scope.indexTitular]=angular.copy($scope.persona);
				$state.go('app.generarContrato.datosPersona',{previous:'titular',vez:"n"});
			});
			/** fin @feature V4-HU00 **/
		};
		/**
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		$scope.regresar=function(){
			$scope.persona.paisID = parseInt($scope.persona.paisID);
			/** inicio @feature V4-HU020 **/
			if($scope.persona.paisResidenciaId){
				$scope.persona.paisResidenciaId = parseInt($scope.persona.paisResidenciaId);
			}
			/** fin @feature V4-HU020 **/
			$state.go('app.generarContrato.datosPersona',{previous:'titular',vez:"n"});
		};

		$scope.agregarRelacionada = function(){
			tempTitular.titular = $scope.persona;
			tempTitular.titular.indexRelacionado = 999999;
			$state.go('app.agregarPersona',{tipo:1});
		};
		$scope.agregarVinculada = function(){
			tempTitular.titular = $scope.persona;
			tempTitular.titular.indexVinculado = 999999;
			$state.go('app.agregarPersona',{tipo:0 , estadoCivil:$scope.persona.estadoCivilID});	
		};

		/**START - POPOVER RELACIONADOS**/
		
		$ionicPopover.fromTemplateUrl('relacionados-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.relacionadosOptionsPopover = popover;
		});

		$scope.doActionRelacionados = function (index) {
			if(index===0){
				//tempContrato.titulares[tempTitular.titular.indexTitular]=angular.copy($scope.persona);			
				/*$scope.tempTitular.titular=tempContrato.titulares[$scope.indexToEdit];*/			
				tempTitular.titular.indexRelacionado=$scope.indexToEdit;
				$state.go('app.agregarPersona',{tipo:1});
			}
			if(index==1){
				$scope.persona.personasRelacionadas.splice($scope.indexRelacionado,1);
				//SI ES CONYUGUE QUIERE DECIR QUE ES PERSONA NATURAL
				if($scope.persona.personaTipoID == "N"){
					//BUSCAMOS INDICE DE CONYUGUE EN VINCULADOS
					var indiceTemporal = undefined;
					for(var i = 0 ; i<$scope.persona.personasVinculadas.length ;i++){
						if($scope.persona.personasVinculadas[i].personaTipoRelacionID == TIPO_RELACION_ID.CONYUGUE){
							indiceTemporal = i;
						}
					}

					if(indiceTemporal != undefined){
						$scope.persona.personasVinculadas.splice(indiceTemporal,1);
					}
				}
				//ELIMINAMOS. ESE VALOR.
			}

			if($scope.relacionadosOptionsPopover){
				$scope.relacionadosOptionsPopover.hide();
			}
		};

		$scope.openRelacionadosOptionsPopover = function ($event,indexRelacionado) {
			$scope.indexToEdit = indexRelacionado;
			$scope.indexRelacionado=indexRelacionado;
			if($scope.relacionadosOptionsPopover){
				$scope.relacionadosOptionsPopover.show($event);
			}
		};
		$scope.closeRelacionadosOptionsPopover = function () {
			if($scope.relacionadosOptionsPopover){
				$scope.relacionadosOptionsPopover.hide();
			}
		};
		$scope.$on('$destroy', function () {
			if($scope.relacionadosOptionsPopover){
				$scope.relacionadosOptionsPopover.remove();
			}
		});
		/**END - POPOVER RELACIONADOS**/
		/**START - POPOVER VINCULADOS**/
		$ionicPopover.fromTemplateUrl('vinculados-options-popover.html', {
			scope: $scope
		}).then(function (popover) {
			$scope.vinculadosOptionsPopover = popover;
		});

		$scope.doActionVinculados = function (index) {
			if(index===0){			
				//$scope.persona.personasVinculadas.push($scope.newRelacionada)
				/*$scope.tempTitular.titular=tempContrato.titulares[$scope.indexToEdit];			
				$scope.tempTitular.titular.indexTitular=$scope.indexToEdit;*/
				tempTitular.titular.indexVinculado=$scope.indexToEdit;
				$state.go('app.agregarPersona',{tipo:0});
			}
			if(index==1){
				if($scope.persona.personasVinculadas[$scope.indexVinculado].personaTipoRelacionID == TIPO_RELACION_ID.CONYUGUE){
					$scope.persona.estadoCivilID = undefined;	
					$scope.persona.personasRelacionadas = [];				
				}

				$scope.persona.personasVinculadas.splice($scope.indexVinculado,1);
				
				//CAMBIAR ESTADO A UNDEFINED
				//CAMBIAR ESTADO A 

			}
			$scope.vinculadosOptionsPopover.hide();
		};
			
		$scope.openVinculadosOptionsPopover = function ($event,indexVinculado) {
			$scope.indexToEdit = indexVinculado;
			$scope.indexVinculado=indexVinculado;
			$scope.vinculadosOptionsPopover.show($event);
		};
		$scope.closeVinculadosOptionsPopover = function () {
			$scope.vinculadosOptionsPopover.hide();
		};
		$scope.$on('$destroy', function () {
			$scope.vinculadosOptionsPopover.remove();
		});
		/**END - POPOVER VINCULADOS**/
		$scope.personaOptions = [
			{
				id: 0,
				name: 'Editar',
				label: "Editar"
			},
			{
				id: 1,
				name: 'Eliminar',
				label: "Eliminar"
			}
		];
		/* MODAL 4/1 */
		$scope.omitir4por1 = function(){
			$scope.closeModal4por1();
			$scope.confirmarTitular();
		};
		$ionicModal.fromTemplateUrl('criterio41-modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal4po1 = modal;
		});
		$scope.openModal4por1 = function() {
			$scope.modal4po1.show();
		};
		$scope.closeModal4por1 = function() {
			$scope.modal4po1.hide();
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal4po1.remove();
		});
		/*SCAN START*/
		$scope.scanBarcode = function() {
			try{				
				var currentPlatform = ionic.Platform.platform();  
				if(currentPlatform ===  PDF417PLUGIN.PLATFORM_ANDROID){
					escanerService.barcodeScanner($scope.scannerProcess);
				}else{
					escanerService.pdf417ScannerScan($scope.scannerProcess);
						}
			}catch(e){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
				console.log(e.message + " -  " + e.stack);
			}
	        
	    };

		$scope.scannerProcess = function(type, result){	
			if (type === "CODE39") {	
				$scope.persona.personaCodigoDocumento = result.toString();
				$scope.buscarPersona($scope.persona.tipoDocumento,$scope.persona.personaCodigoDocumento,false,"",true);
			}else if (type === "PDF417") {		
				var DNI = result.substring(2,10);
				$scope.persona.personaCodigoDocumento = DNI;
				$scope.buscarPersona($scope.persona.tipoDocumento,DNI,true,result);
				$scope.busquedaConEscaner = true;
			}
		};

	    

	    $scope.getDataPDF417 = function(data) {
			
	        var colLength = 16;
	        var rowLimit = 7;
	        var dataToParse =  data.substring(0,colLength * rowLimit);
	        var DNI = dataToParse.substring(2,10);
	        var apellidoPaterno = "", apellidoMaterno = "", nombres = "";
			var colLength = 16, rowLimit = 7, initNamePosition = 10;
			for(var i=initNamePosition;i<(colLength*rowLimit);i++){
				if(i<=(colLength*3) + 1){
					apellidoPaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*3) + 1 && i<=(colLength*5) + 9){
					apellidoMaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*5) + 9 ){
					nombres+=dataToParse.charAt(i);
				}
			}
			$scope.persona.personaNombre = nombres.trim();
			$scope.persona.personaApellidoPaterno = apellidoPaterno.trim();
			$scope.persona.personaApellidoMaterno = apellidoMaterno.trim();
	    }
		/*SCAN END*/
		//SI ES CONYUGUE CAMBIAR EL SELECT DE ESTADO CIVIL A CASADO SI NO 
		if(parseInt($stateParams.esCasado) == TIPO_RELACION_ID.CONYUGUE){
			if($scope.persona.estadoCivilID != "N"){
				$scope.persona.estadoCivilID = ESTADO_CIVIL.CASADO;
			}
		}else if($stateParams.esCasado != "" ){
			//ENVIA SI ES SOLTERO VIUDO DIVORCIADO
			if($scope.persona.personasRelacionadas.length < 1){
					//$scope.persona.estadoCivilID = "";
					$scope.doActionRelacionados(1);	
			}
		}


		// SI ES EMPLEADO DE PANDERO
		if($stateParams.esEmpleadoPandero == 'true'){
			/*CAMBIAMOS EL FLAG HA VERDADERO*/
			$scope.persona.personaTieneVinculado = ($stateParams.esEmpleadoPandero == 'true'); 
		}else{
			if($scope.persona.personasVinculadas.length > 0){
				$scope.persona.personaTieneVinculado = true; 	
			}			
		}

	})	
	/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
	//.controller('resumenContratoCtrl', function(validacionService, $scope,$rootScope, $state , shareData,tempContrato,$http,numContrato,dateFilter,BACK_END_URL,TIPO_ASOCIADO, $ionicPopup,DIRECCION,ubigeoService,PERSONA_TIPO,MOCK_CONTRATO, TIPO_RELACION_ID) {
    /**
     * @issue JIRAV3-006
     *  - quitar inject MOCK_CONTRATO
	 * @feature V4-HU020
	 *  - cliente no domiciliado
     */
	.controller('resumenContratoCtrl', function(validacionService, $scope,$rootScope, $state , shareData,tempContrato,$http,numContrato,dateFilter,BACK_END_URL,TIPO_ASOCIADO, $ionicPopup,DIRECCION,ubigeoService,PERSONA_TIPO, TIPO_RELACION_ID, me,$ionicLoading) {
	/*FIN - JOHN VELASQUEZ - CAMINOS APP*/
		$scope.TIPO_ASOCIADO = TIPO_ASOCIADO;
		var str;
		var res;
		var month;

		//tempContrato = MOCK_CONTRATO;
		/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
		$scope.me = me;
		/*FIN - JOHN VELASQUEZ - CAMINOS APP*/

		//INICIO VTADIG2-288 JOHN VELASQUEZ
		$scope.TIPO_RELACION_ID = TIPO_RELACION_ID;
		//FIN VTADIG2-288 JOHN VELASQUEZ

		$scope.contratoForm=tempContrato;

		$scope.contratoForm.contrato.medioComunicacionPreferenteId = tempContrato.contrato.medioComunicacionPreferente.codigo;

		$scope.contratoForm.contratoBean=tempContrato.contrato;				

		//INICIO VTADIG2-59 JOHN VELASQUEZ
		$scope.lstTipoVia=shareData.lstTipoVia;
		$scope.lstTipoZona=shareData.lstTipoZona;
		$scope.personasFirmaDigital = [];
		$scope.contratoForm.contrato.titulares = $scope.contratoForm.titulares;
		//FIN VTADIG2-59 JOHN VELASQUEZ

		if(!$scope.contratoForm.contrato.montoCiaPagar){
			$scope.contratoForm.contrato.montoCiaPagar = 0.00;
		}
		
		$scope.contratoForm.contrato.cuotaTotalMensualPendiente = tempContrato.contrato.cuotaTotalMensualPendiente		
		$scope.hayRespaldo = false;
		angular.forEach($scope.contratoForm.titulares,function(titular){
			/** inicio @feature V4-HU020 **/
			titular.esResidente = titular.esResidente !== false;
			/** fin @feature V4-HU020 **/
			if(titular.asociadoTipoID===TIPO_ASOCIADO.RESPALDO){
				$scope.hayRespaldo = true;
				//INICIO VTADIG2-59 JOHN VELASQUEZ
				$scope.personasFirmaDigital.push(titular);
				//FIN VTADIG2-59 JOHN VELASQUEZ	
			}
			//INICIO VTADIG2-59 JOHN VELASQUEZ
			else if(titular.asociadoTipoID===TIPO_ASOCIADO.TITULAR){
				if(titular.personaTipoID == PERSONA_TIPO.NATURAL){
					$scope.personasFirmaDigital.push(titular);
				}else{
					//INICIO VTADIG2-288 JOHN VELASQUEZ
					//$scope.personasFirmaDigital = $scope.personasFirmaDigital.concat(titular.personasRelacionadas);
					titular.personasRelacionadas = titular.personasRelacionadas || [];
					$scope.personasFirmaDigital = $scope.personasFirmaDigital.concat(titular.personasRelacionadas.filter(pr=>pr.personaTipoRelacionID == TIPO_RELACION_ID.REPRESENTANTE_LEGAL));
					//FIN VTADIG2-288 JOHN VELASQUEZ
				}
			}
			;
			//INICIO VTADIG2-59 JOHN VELASQUEZ
			/*obligatorios*/
			/**DIRECCION PERSONAL**/
			if(!titular.direcciones){console.error("El titular "+titular.personaCodigoDocumento+" "+titular.personaNombreCompleto+" no tiene ninguna direccion");return;}
			/** inicio @feature V4-HU020 **/
			let direccionPersonal = titular.esResidente ? titular.direcciones.find(d=>d.direccionTipoID===DIRECCION.TIPO.PERSONAL) : titular.direcciones.find(d=>d.direccionTipoID===DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO);
			/** fin @feature V4-HU020 **/
			if(!direccionPersonal){console.error("El titular "+titular.personaCodigoDocumento+" "+titular.personaNombreCompleto+" no tiene una dirección personal");return;}
			/**DIRECCION LABORAL**/
			/** inicio @feature V4-HU020 **/
			var direccionLaboral = titular.esResidente ? titular.direcciones.find(d=>d.direccionTipoID===DIRECCION.TIPO.LABORAL) : titular.direcciones.find(d=>d.direccionTipoID===DIRECCION.TIPO.LABORAL_NO_DOMICILIADO);
			/** fin @feature V4-HU020 **/
			/**direccion personal TIPO VIA**/
			/** inicio @feature V4-HU020 **/
			if(!direccionPersonal.tipoViaID){
				console.error("La direccion del titular "+titular.personaCodigoDocumento+" "+titular.personaNombreCompleto+" no tiene un tipo de via asignado");
			} else{
				var tipoVia = $scope.lstTipoVia.find(t=>t.codigo === direccionPersonal.tipoViaID);
				direccionPersonal.tipoViaNombre = tipoVia.descripcionLarga;
			}
			if(titular.paisResidenciaId){
				let pais = shareData.lstPais.find(pais => `${pais.codigo}` === `${titular.paisResidenciaId}`);
				titular.paisResidenciaNombre=pais.descripcion;
			}
			/** fin @feature V4-HU020 **/
			/**direccion personal DEPARTAMENTO - PROVINCIA - DISTRITO**/
			if(direccionPersonal.departamentoID){
				ubigeoService.getDepartamento(direccionPersonal.departamentoID)
				.then(d=>direccionPersonal.departamentoNombre = d.descripcion);
				if(direccionPersonal.provinciaID){
					ubigeoService.getProvincia(direccionPersonal.departamentoID,direccionPersonal.provinciaID)
					.then(d=>direccionPersonal.provinciaNombre = d.descripcion);
					if(direccionPersonal.distritoID){
						ubigeoService.getDistrito(direccionPersonal.departamentoID,direccionPersonal.provinciaID,direccionPersonal.distritoID)
						.then(d=>direccionPersonal.distritoNombre = d.descripcion);
					}
				}
			}
			/**TELEFONOS**/
			titular.telefonos = [];
			if(titular.personaTelefono){
				titular.telefonos.push(titular.personaTelefono);
			}
			if(direccionPersonal.direccionTelefono1){
				titular.telefonos.push(direccionPersonal.direccionTelefono1);
			}
			
			if(direccionLaboral){
				if(direccionLaboral.direccionTelefono1){
					titular.telefonos.push(direccionLaboral.direccionTelefono1);
				}
			}
			/*opcionales*/
			/**ZONA**/
			if(direccionPersonal.tipoZonaID){
				var tipoZona = $scope.lstTipoZona.find(t=>t.codigo==direccionPersonal.tipoZonaID);
				if(tipoZona){
					direccionPersonal.tipoZonaNombre = tipoZona.descripcionLarga;
				}else{
					console.log("La direccion del titular "+titular.personaCodigoDocumento+" "+titular.personaNombreCompleto+" no tiene un tipo de zona asignado");
				}
			}
			titular.direccionPersonal = direccionPersonal;
			//FIN VTADIG2-59 JOHN VELASQUEZ
		});
		$scope.regresar = function(){

			/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
			if($scope.me.documentoDigital){
				var confirmPopup = $ionicPopup.confirm({
					title: 'Firma digital',
					template: 'Si regresa tendrá que volver a realizar todas las grabaciones.',
					cancelText: 'Cancelar',
						okText: 'Aceptar'
				});
	
				confirmPopup
				.then(function(res) {
					if(res) {
						$rootScope.$emit("digitalSignatureCanceled");
						$state.go('app.generarContrato.datosPersona');
					}
				});
			}else{
				$state.go('app.generarContrato.datosPersona');
			}			
			/*FIN - JOHN VELASQUEZ - CAMINOS APP*/
		}
		 $scope.showConfirm = function() {
			if($scope.me.documentoDigital){
				$rootScope.$emit("digitalSignatureAccepted");
			}
			//INICIO VTADIG2-59 JOHN VELASQUEZ
			$scope.firmaDigitalCompleta = true;
			$scope.personasFirmaDigital.forEach(t=>{
				if(!t.speechs){
					$scope.firmaDigitalCompleta = false;
				}else if(t.speechs.length<=0){
					$scope.firmaDigitalCompleta = false;
				}
			});

			if(!$scope.firmaDigitalCompleta&&$scope.me.documentoDigital){
				$ionicPopup.alert({
					title: 'Firma Digital',
					template: 'Debe completar todas las firmas digitales',
					okText: 'Aceptar'
				});
				return;
			}			
			//INICIO VTADIG2-59 JOHN VELASQUEZ

			var confirmPopup = $ionicPopup.confirm({
				title: 'Generar Contrato',
				template: 'Se generará el contrato',
				cancelText: 'Cancelar',
        			okText: 'Aceptar'
			});

			confirmPopup
			.then(function(res) {
				if(res) {
					$scope.generarContrato();
				}
			});
		};
		$scope.generarContrato=function(){
			$scope.contratoForm.contratoBean.articuloID=$scope.contratoForm.contratoBean.modeloId+'';
			$scope.contratoForm.contratoBean.programaProductoID = tempContrato.contratoBean.programaProductoSeleccionado.productoID;
			$scope.contratoForm.contratoBean.grupoID = tempContrato.contratoBean.grupoSeleccionado.grupoID;
			$scope.contratoForm.contratoBean.certificadoPosicionID = tempContrato.contratoBean.certificadoSeleccionado.id.certificadoPosicionID;
			$scope.contratoForm.contrato.cuotaTotalMensualPendiente =  validacionService.aDosDecimales($scope.contratoForm.contrato.cuotaTotalMensual - $scope.contratoForm.contrato.cuotaTotalMensualPagado);
			$scope.contratoForm.contratoBean.tipoCuentaID = tempContrato.contratoBean.modalidadDevolucion.codigo;
			if(tempContrato.contratoBean.banco!==undefined){
				   $scope.contratoForm.contratoBean.bancoId = tempContrato.contratoBean.banco.codigo;
			}	  		

			$scope.contratoForm.contratoBean.origenVentaDigital = 1;
			$scope.contratoForm.contratoBean.documentoDigital = $scope.me.documentoDigital;
			$scope.contratoForm.contratoBean.usuarioIDCreacion = 1;
			$scope.contratoForm.contratoBean.referidoID = (tempContrato.contratoBean.referidoID !== "") ? parseInt(tempContrato.contratoBean.referidoID): null;
			

			//for (var i = 0; i < $scope.contratoForm.titulares.length; i++) {						

			angular.forEach($scope.contratoForm.titulares,function(objTitular){
				objTitular.tipoDocumentoID=objTitular.tipoDocumento.codigo;
				objTitular.personaIngresoMensual = parseFloat(objTitular.personaIngresoMensual);
				objTitular.personaFechaNacimiento= dateFilter( objTitular.personaFechaNacimiento, 'dd/MM/yyyy');	  			
				objTitular.personaFechaIngresoTrabajo= dateFilter( objTitular.personaFechaIngresoTrabajo, 'dd/MM/yyyy');	  			
				if(objTitular.personaTipoID==='J'){ 			
					objTitular.personaFechaConstitucion= dateFilter( objTitular.personaFechaConstitucion, 'dd/MM/yyyy');
					objTitular.personaNombre='';
					objTitular.personaApellidoPaterno='';
					objTitular.personaApellidoMaterno='';
					objTitular.personaNombreCompleto='';
					
				}
				if(objTitular.departamentoNacimientoID&&objTitular.provinciaNacimientoID&&objTitular.distritoNacimientoID){
					objTitular.personaNacimientoUbigeo = 
					objTitular.departamentoNacimientoID +
					objTitular.provinciaNacimientoID +
					objTitular.distritoNacimientoID;
				}else{
					objTitular.personaNacimientoUbigeo = undefined;
				}
				
				objTitular.personaIngresoMensual = parseFloat(objTitular.personaIngresoMensual);
				if(objTitular.personasRelacionadas){
					angular.forEach(objTitular.personasRelacionadas,function(relacionada){
						if(relacionada.personaIngresoMensual){
							relacionada.personaIngresoMensual = parseFloat(relacionada.personaIngresoMensual);
						}
						if(relacionada.personaFechaNacimiento){
							if(relacionada.personaFechaNacimiento.getDate){
								relacionada.personaFechaNacimiento= dateFilter(relacionada.personaFechaNacimiento, 'dd/MM/yyyy');
								console.log(relacionada.personaFechaNacimiento);
							}						
						}
						if(relacionada.personaFechaIngresoTrabajo){
							if(relacionada.personaFechaIngresoTrabajo.getDate){
								relacionada.personaFechaIngresoTrabajo= dateFilter(relacionada.personaFechaIngresoTrabajo, 'dd/MM/yyyy');
								console.log(relacionada.personaFechaIngresoTrabajo);
							}						
						}
						if(relacionada.direcciones){
							//tiene ambas direcciones
							if(relacionada.direcciones.length===2){
								if(!relacionada.direcciones[0]){//si la direccion de domicilio es null o undeficed
									relacionada.direcciones.splice(0,1);									
								}
							}
							//si solo tiene una direccion o si la que queda(laboral) tambien es null o undefined
							if(relacionada.direcciones.length===1){
								if(!relacionada.direcciones[0]){
									relacionada.direcciones.splice(0,1);
								}
							}	
						}
					});				
				}
				if(objTitular.personasVinculadas){
					angular.forEach(objTitular.personasVinculadas,function(vinculada){						
						vinculada.personaFechaNacimiento = undefined;
						vinculada.personaFechaIngresoTrabajo = undefined;
					});
				}
			});			

			$scope.url=BACK_END_URL+'contrato';						

			$http({
			  method: 'POST',
			  url: $scope.url,
			  data:{
				contratoBean: $scope.contratoForm.contratoBean
			  }
			}).then(
				function (response) {
					if(response.data.code===0){
						console.log("GENERACION CONTRATO CORRECTO");
						console.log("contrato="+response.data.data.contratoNumero);
						console.log("obligacionID="+response.data.data.obligacionID+" - montoObligacion="+response.data.data.montoObligacion);
						numContrato.numContrato=response.data.data.contratoNumero;
						numContrato.obligacionID=response.data.data.obligacionID;
						numContrato.montoObligacion=response.data.data.montoObligacion;
						numContrato.contratoID=response.data.data.contratoID;
						numContrato.debitoAutomatico = tempContrato.contrato.debitoAutomatico;
						numContrato.debitoAutomaticoVisanet = tempContrato.contrato.debitoAutomaticoVisanet;
						$rootScope.tempLstProgramaProducto = undefined;
						$rootScope.tempLstTipoBien = undefined;
						$rootScope.tempLstGrupo = undefined;
						$rootScope.tempLstCertificado = undefined;
						$rootScope.tempLstMarca = undefined;
						$rootScope.tempLstModelo = undefined;			
						$rootScope.tempLstProformasProspecto = undefined;
						$rootScope.tempLstSeguimientoProspecto = undefined;		
						tempContrato.contrato={};
						tempContrato.titulares=[];
						$state.go("app.mensajeConfirmacionContrato");					
                        /*inicio @issue VTADIG2-504 */
                        if(response.data.message !== '' && response.data.type=='warning'){
                            $ionicPopup.alert({ title: 'Alerta', template: response.data.message , okType: 'button-assertive'});
                        }
                        /*fin @issue VTADIG2-504 */
					}else{
						console.log(response);
						//for (var i = 0; i < $scope.contratoForm.titulares.length; i++) {
						angular.forEach($scope.contratoForm.titulares,function(objTitular){

							if(objTitular.personaFechaNacimiento){
								str = objTitular.personaFechaNacimiento;
								res = str.split(" ")[0].split("/");
								month = parseInt(res[1])-1;
								objTitular.personaFechaNacimiento = new Date(res[2], month, res[0]);
							}
							if(objTitular.personaFechaIngresoTrabajo){
								str = objTitular.personaFechaIngresoTrabajo;
								res = str.split(" ")[0].split("/");
								month = parseInt(res[1])-1;
								objTitular.personaFechaIngresoTrabajo = new Date(res[2], month, res[0]);
							}

							if(objTitular.personaTipoID==='J'){ 											
								str = objTitular.personaFechaConstitucion;
								res = str.split(" ")[0].split("/");
								month = parseInt(res[1])-1;
								objTitular.personaFechaConstitucion = new Date(res[2], month, res[0]);
							}
							if(objTitular.personasRelacionadas){
								angular.forEach(objTitular.personasRelacionadas,function(relacionada){
									/** inicio @feature V4-HU020 **/
									relacionada.esResidente = relacionada.esResidente !== false;
									/** fin @feature V4-HU020 **/
									if(relacionada.personaFechaNacimiento){
										str = relacionada.personaFechaNacimiento;
										res = str.split(" ")[0].split("/");
										month = parseInt(res[1])-1;
										relacionada.personaFechaNacimiento = new Date(res[2], month, res[0]);	
										console.log(relacionada.personaFechaNacimiento);				
									}
									if(relacionada.direcciones){
										if(relacionada.direcciones.length===1){//si solo tien una direccion debe ser de domicilio
											/** inicio @feature V4-HU020 **/
											if(relacionada.direcciones.direccionTipoID === relacionada.esResidente ? DIRECCION.TIPO.LABORAL : DIRECCION.TIPO.LABORAL_NO_DOMICILIADO){
											/** fin @feature V4-HU020 **/
												relacionada.direcciones[1] = relacionada.direcciones[0];
												relacionada.direcciones[0] = null;
											}
										}
									}			
								});				
							}							
						});
						//alert("Ocurrió un error al registrar el contrato");
					}
				}, 
				function (response) {
					//alert('Error al generar el contrato');
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: response.data.message , okType: 'button-assertive'});
				}
			);
			
		};
	})


	.controller('estaditicosCtrl', function($scope, $state , shareData) {
		
		$scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
		$scope.series = ['Series A', 'Series B'];

		$scope.data = [
		[65, 59, 80, 81, 56, 55, 40],
		[28, 48, 40, 19, 86, 27, 90]
		];
	  
	  
	})

	.controller('registroProspectoCtrl', function($scope, $state , shareData,tempContrato, tempProforma) {
		$scope.ocultaProspecto = function(){
			if(tempProforma.proforma!== undefined 
				&& tempProforma.proforma.vtaProspecto!== undefined 
				&& tempProforma.proforma.vtaProspecto.esEdicion!== undefined 
				&& tempProforma.proforma.vtaProspecto.esEdicion){
				return "ng-hide";
			}else{
				return "ng-show";
			}
		}
		
		$scope.irAProforma=function(){
			$state.go('app.registroProspecto.proforma',{init: '1'});
		};

						
		
	})

	/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
	//.controller('generarContratoCtrl', function($scope, $state , shareData,tempContrato,contratoService, $ionicPopup, PersonaService, tempProspecto,ESTADO_HORA_ASAMBLEA) {
		.controller('generarContratoCtrl', function($scope, $state , shareData,tempContrato,contratoService, $ionicPopup, PersonaService, tempProspecto,ESTADO_HORA_ASAMBLEA, me) {
	/*FIN - JOHN VELASQUEZ - CAMINOS APP*/

		/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
		$scope.me = me;
		/*FIN - JOHN VELASQUEZ - CAMINOS APP*/

		$scope.irAContrato=function(){
			$state.go('app.generarContrato.datosContrato',{previous: 'titular', vez: 'n'});
		};

		$scope.irATitulares=function(){			  
			console.log("tempContrato.contrato.contratoFormato->"+tempContrato.contrato.contratoFormato);
			
			if(!tempContrato.contrato.programaProductoSeleccionado){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Programa-Producto es obligatorio." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.grupoSeleccionado){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Grupo es obligatorio." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.certificadoSeleccionado){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Certificado de Bien es obligatorio." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.tipoBienID){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Tipo de Bien es obligatorio." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.marcaId&&tempContrato.contrato.tipoBienID!=1){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Marca es obligatorio." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.modeloId){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Modelo es obligatorio." , okType: 'button-assertive'});
			}/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/else if(!tempContrato.contrato.contratoFormato&&!$scope.me.documentoDigital){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Formato es obligatorio." , okType: 'button-assertive'});
			}/*FIN - JOHN VELASQUEZ - CAMINOS APP*/else if(tempContrato.contrato.contratoNroAsambleaGrupo>1 && (tempContrato.contrato.contratoNroCuotasAbonado+tempContrato.contrato.contratoNroCuotasAdjudicacion+tempContrato.contrato.contratoNroCuotasProrrateo)!==tempContrato.contrato.contratoNroAsambleaGrupo-1){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese la forma de pago de las cuotas devengadas(Debe devengar "+(tempContrato.contrato.contratoNroAsambleaGrupo-1)+" cuotas)." , okType: 'button-assertive'});
			}else if(!tempContrato.contrato.cia){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El certificado seleccionado no tiene registrado la cuota mensual y la cia." , okType: 'button-assertive'});
			}else{
				var tipoDocumentoID;
				if(tempProspecto.prospecto.prospectoBean.tipoDocumento.codigo){
                    			tipoDocumentoID = tempProspecto.prospecto.prospectoBean.tipoDocumento.codigo;
				}else{
                    			tipoDocumentoID = tempProspecto.prospecto.prospectoBean.tipoDocumento;
				}
				PersonaService.validaPersona(tipoDocumentoID, tempProspecto.prospecto.prospectoBean.numeroDocumento,tempContrato.contrato.grupoSeleccionado.grupoID,tempContrato.contrato.certificadoSeleccionado.id.certificadoPosicionID)
				.then(
					function (persona) {						
						if(persona){
							if(persona.personaEsEmpleadoEspecial&&tempContrato.contrato.contratoNroAsambleaGrupo>1){								
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Los vinculados solo pueden ingresar en la cuota 1." , okType: 'button-assertive'});
								return;
							}
							/*if(persona.personaEsEmpleado&&persona.excedePorcentajeMaximoVinculados){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El grupo ha completado la cantidad de asociados vinculados sobre el número teórico de asociados." , okType: 'button-assertive'});
								return;
							}
							if(persona.excedeVmcaAsociados){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El asociado ha superado el Valor Teórico del Grupo (VMCA)." , okType: 'button-assertive'});
								return;
							}*/
						}
						contratoService.comprobarHorasAntesAsamblea(tempContrato.contrato.grupoSeleccionado.grupoID,tempContrato.contrato.contratoNroAsambleaGrupo).then(
							function(response){
								var estado = response.data;
								if(	estado!==ESTADO_HORA_ASAMBLEA.QUEDA_SUFICIENTE_TIEMPO){
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: response.message , okType: 'button-assertive'});							
									if(estado===ESTADO_HORA_ASAMBLEA.ASAMBLE_NO_CONCUERDA){
										alertPopup.then(function(val){
											$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
										});
									}
									return;
								}
								/*INICIO - JOHN VELASQUEZ - CAMINOS APP*/

								if(!me.documentoDigital){
									contratoService.contarFormatoContrato(tempContrato.contrato.contratoFormato, tempContrato.contrato.programaProductoSeleccionado.productoID,tempContrato.contrato.tipoBienID)
									.then(
										function(data){
											if(data>0){
												var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Nro. de formato ya registrado." , okType: 'button-assertive'});
											}else{
													tempContrato = tempContrato;
													$state.go('app.generarContrato.datosPersona',{previous:'contrato',vez:'1'});						
											}
										}
									);
								}else{
									tempContrato = tempContrato;
									$state.go('app.generarContrato.datosPersona',{previous:'contrato',vez:'1'});						
								}
								

								//tempContrato = tempContrato;
								//console.log("pre ir titlares");
								//console.log(tempContrato);			
								//$state.go('app.generarContrato.datosPersona',{previous:'contrato',vez:'1'});						
								
								/*FIN - JOHN VELASQUEZ - CAMINOS APP*/
							}
						);						
					},
					function(error){
						//console.log("SPVMASIVAP");
					}
				)				
			}									
		};
		
	})
	//inicio - @feature/MasterCard
	//.controller('mensajeConfirmacionCtrl', function($scope, $rootScope, $state, $ionicPlatform, shareData,tempContrato, tempProspecto,numContrato,obligacionPagoService,pagoService,blockUI,$ionicNavBarDelegate, $ionicPopup, debitoAutomaticoService) {
	//.controller('mensajeConfirmacionCtrl', function($scope, $rootScope, $state, $ionicPlatform, shareData,tempContrato, tempProspecto,numContrato,obligacionPagoService,pagoService,blockUI,$ionicNavBarDelegate, $ionicPopup, debitoAutomaticoService,pagoMastercardService) {
	//fin - @feature/MasterCard	
	//inicio - @feature/pagoLink
	.controller('mensajeConfirmacionCtrl', function($scope, $rootScope, $state, $ionicPlatform, shareData,tempContrato, tempProspecto,numContrato,obligacionPagoService,pagoService,blockUI,$ionicNavBarDelegate, $ionicPopup, debitoAutomaticoService,pagoMastercardService,pagoLinkService){
	//fin - @feature/pagoLink
		$ionicNavBarDelegate.showBackButton(false);
		$scope.deshabilitaPago=false;
		//$scope.numContrato = numContrato.numContrato;
		$scope.tetempProspecto = tempProspecto;
		var obligacionPagoID = numContrato.obligacionID;
		var montoObligacion = numContrato.montoObligacion;
		var contratoID = numContrato.contratoID;
		$scope.numContrato = numContrato;
		$scope.numContrato.debitoAutomaticoVisanetCorreoEnviado = $scope.numContrato.debitoAutomaticoVisanetCorreoEnviado || false;
		$scope.tempContrato = tempContrato;
		
		$scope.finalizar = function(){
			$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
		}
		var onSuccess = function (mposResponseBeanObject) { 
			console.log("success: "+mposResponseBeanObject);
			var responseBeanObject = JSON.parse(mposResponseBeanObject);
			if(responseBeanObject.statusType==="AUTHORIZED")
			{			
				//pagoService.pagarDirectoObligacionMPOS(responseBeanObject,obligacionPagoID)	
				pagoService.pagarObligacionMPOS(responseBeanObject,obligacionPagoID)				
				.then(function(data){
					console.log("response-pagarObligacion:: "+data);
					$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
				}, function errorCallback(response) {
					console.log("ERROR response-pagarObligacion");
					$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
				});					
			}else{
				$scope.deshabilitaPago=false;
			}
			blockUI.stop();	
		}; 

		var onError = function (mposErrorObject) { 
			console.log("error: "+mposErrorObject);
			var errorObject = JSON.parse(mposErrorObject);
			alert('ERROR. '+errorObject.message);
			blockUI.stop();
			$scope.deshabilitaPago=false;
		};

		$scope.pagarObligacionMPOS = function(){
			$ionicPlatform.ready(function () {
				console.log("en pagarObligacionMPOS");	
				var amount = montoObligacion;
				var reference = obligacionPagoID;
				var email = "";	

				// Obtener datos de la obligacion de pago
				var estadoObligacion = "";
				pagoService.getObligacionPago(obligacionPagoID)
				.then(function(data){
					estadoObligacion = data['estado'];

					if(estadoObligacion!="0"){
						alert("No existe obligación pendiente de pago");

					}else{
						// Ejecutar el pago con MPOS
						$scope.deshabilitaPago=true;
						
						/*blockUI.start();
						window.visanet.authorize(amount, reference, email, onSuccess, onError);
						/**/
						
						blockUI.start();
						var jsonMPOSBean={
							"responseCode":"00",
							"transactionId":102,
							"bank":"INTERBANK",
							"transactionLocalDate":"28/05/17",
							"transactionLocalTime":"19:20",
							"currencyLabel":"$",
							"transactionAmount":montoObligacion,
							"voucherType":"CREDITO",
							"currencyCode":"840",
							"traceNumber":"101001",
							"statusType":"AUTHORIZED",
							"voucherEmail":"usuario@gmail.com"
						};

						// Llamar al servicio de pagos SAF
						//pagoService.pagarDirectoObligacionMPOS(jsonMPOSBean,obligacionPagoID)
						pagoService.pagarObligacionMPOS(jsonMPOSBean,obligacionPagoID)
						.then(function(data){
							console.log("response-pagarObligacion:: "+data);	
                                                        // INICIO SPRINT-2 03/01/2018
							blockUI.stop();
							//alert("El pago ha sido realizado correctamente");
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El pago ha sido realizado correctamente." , okType: 'button-assertive'});	
							alertPopup.then(function(data){
								$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
							});				
                                                        // FIN SPRINT-2 03/01/2018										
						}, function errorCallback(response) {
							alert("Se produjo un error");
							blockUI.stop();
							$scope.deshabilitaPago=false;
						});/**/
					}


				}, function errorCallback(response) {
					alert("No se puede realizar el pago");
				});
				
			});
		};

		$scope.enviarCorreoDebitoAutomaticoVisanet = function() {
			debitoAutomaticoService.enviarFormularioVisaNet(contratoID).then(function(data){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.message , okType: 'button-assertive'});
				$scope.numContrato.debitoAutomaticoVisanetCorreoEnviado = true;
			});
		};

		if ($scope.numContrato.debitoAutomaticoVisanet === true) {
			$scope.enviarCorreoDebitoAutomaticoVisanet();
		}
		//inicio - @feature/MasterCard		
		$scope.contratoMastercardObligacionPagoID=obligacionPagoID;	
		$scope.pagarObligacionMastercard=function(){
			console.log("pago por contrato");
			var validarAsamblea="";
			pagoMastercardService.validaDiasAsamblea(obligacionPagoID)
			.then(function(data){
				if(data.data){
					let origenId=1 //ingreso de ventas
					$state.go('app.registrarPagoMastercardObligacion',{obligacionID:obligacionPagoID,origenObligacionID:origenId});					
				}
				console.log('validaAsamblea'+data);
			})
			.catch(function(err){
				console.log("error::",err);
				$ionicPopup.alert({ title: 'Alerta', template: err.message , okType: 'button-assertive'});
			}) ;

		}
		//fin - @feature/MasterCard
		/**
		 * inicio -@feature/pagoLink
		*/
		$scope.pagarObligacionPagoLink=function(){
			console.log("Pago por Ingreso de ventas");
			console.log(obligacionPagoID);
			pagoLinkService.generarPagoLink(obligacionPagoID)
			.then(function(data){
				if(data) {
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Correo enviado con éxito." , okType: 'button-assertive'});	
							alertPopup.then(function(res){
								$state.go('app.detalleCliente',{prospectoID: tempProspecto.prospecto.prospectoBean.prospectoID});
							});	
				}	
						console.log("pagolink en obligacion"+data);
			})
			.catch(function(err){
						console.log("error::",err);
						$ionicPopup.alert({ title: 'Alerta', template:"No se pudo enviar el link de pago." , okType: 'button-assertive'});
			});
		}
		/**
		 * fin -@feature/pagoLink
		*/
	})
	
	 /**INICIO - NOTEROC - 01/04/2020 */
	//.controller('mensajeConfirmacionObligacionCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPlatform, shareData, obligacionPagoService, pagoService, blockUI, $ionicPopup, VISTAS_ID) {
	//.controller('mensajeConfirmacionObligacionCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPlatform, shareData, obligacionPagoService, pagoService, blockUI, $ionicPopup, VISTAS_ID,pagoMastercardService) {
	 /**FIN - NOTEROC - 01/04/2020 */
	 //inicio - @feature/pagoLink
	.controller('mensajeConfirmacionObligacionCtrl', function($scope, $rootScope, $state, $stateParams, $ionicPlatform, shareData, obligacionPagoService, pagoService, blockUI, $ionicPopup, VISTAS_ID,pagoMastercardService,pagoLinkService){
	//fin - @feature/pagoLink
		console.log("en controller: mensajeConfirmacionObligacionCtrl");
		$scope.deshabilitaPago=false;
		var obligacionPagoID = $rootScope.obligacionPagoID;
		$scope.prospectoID = $rootScope.prospectoID;

		$scope.continuar = function(){
			console.log("id origen:",$stateParams.origenID)
			switch(parseInt($stateParams.origenID)){
				case VISTAS_ID.NUEVA_OBLIGACION_CIA:
					$state.go('app.pagoCia');
				break;
				default:
					$state.go('app.detalleCliente',{prospectoID: $scope.prospectoID});
				break;
			}
		}

		var montoObligacion = $rootScope.montoPago;
		console.log("obligacionPagoID="+obligacionPagoID+' - montoObligacion='+montoObligacion);

		var onSuccess = function (mposResponseBeanObject) { 
			console.log("success: "+mposResponseBeanObject);
			var responseBeanObject = JSON.parse(mposResponseBeanObject);
			if(responseBeanObject.statusType==="AUTHORIZED")
			{				
				//pagoService.pagarDirectoObligacionMPOS(responseBeanObject,obligacionPagoID)
				pagoService.pagarObligacionMPOS(responseBeanObject,obligacionPagoID)				
				.then(function(data){
					console.log("response-pagarObligacion:: "+data);  		
					$scope.continuar();
				}, function errorCallback(response) {
					console.log("ERROR response-pagarObligacion");
					$scope.continuar();
				});	
				//$state.go('app.registroProspecto.proforma',{init: '0'});				
			}else{
				$scope.deshabilitaPago=false;
			}	
			blockUI.stop();
		}; 		

		var onError = function (mposErrorObject) { 
			console.log("error: "+mposErrorObject);
			var errorObject = JSON.parse(mposErrorObject);
			alert('ERROR. '+errorObject.message);
			blockUI.stop();
			$scope.deshabilitaPago=false;
		};

		$scope.pagarObligacionMPOS = function(){
			$ionicPlatform.ready(function () {
				console.log("en pagarObligacionMPOS");		
				//Llamar al servicio de pagos MPOS				
				var amount = montoObligacion;
				var reference = obligacionPagoID;
				var email = "";	


				// Obtener datos de la obligacion de pago
				var estadoObligacion = "";
				pagoService.getObligacionPago(obligacionPagoID)
				.then(function(data){
					estadoObligacion = data['estado'];

					if(estadoObligacion!="0"){
						alert("No existe obligación pendiente de pago");

					}else{
						// Ejecutar el pago con MPOS
						$scope.deshabilitaPago=true;
						
						/*blockUI.start();
						window.visanet.authorize(amount, reference, email, onSuccess, onError);
						*/

						blockUI.start();
						var jsonMPOSBean={
							"responseCode":"00",
							"transactionId":102,
							"bank":"INTERBANK",
							"transactionLocalDate":"28/05/17",
							"transactionLocalTime":"19:20",
							"currencyLabel":"$",
							"transactionAmount":montoObligacion,
							"voucherType":"CREDITO",
							"currencyCode":"840",
							"traceNumber":"101001",
							"statusType":"AUTHORIZED",
							"voucherEmail":"usuario@gmail.com"
						};

						// Llamar al servicio de pagos SAF
						//pagoService.pagarDirectoObligacionMPOS(jsonMPOSBean,obligacionPagoID)
						pagoService.pagarObligacionMPOS(jsonMPOSBean,obligacionPagoID)
						.then(function(data){
							console.log("response-pagarObligacion:: "+data);	
							//alert("El pago ha sido realizado correctamente");
							blockUI.stop();
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El pago ha sido realizado correctamente." , okType: 'button-assertive'});
							alertPopup.then(
								function(res){
									$scope.continuar();
								}
							);							
						}, function errorCallback(response) {
							alert("Se produjo un error");
							blockUI.stop();
							$scope.deshabilitaPago=false;
						});/**/

					}


				}, function errorCallback(response) {
					alert("No se puede realizar el pago");
				});

			});
		};

		$scope.pagarObligacionMastercard=function(){
			pagoMastercardService.validaDiasAsamblea(obligacionPagoID)
			.then(function(data){
				if(data.data){
					let origenId=2 //cia
					$state.go('app.registrarPagoMastercardObligacion',{obligacionID:obligacionPagoID,origenObligacionID:origenId,vistaID:$stateParams.origenID});					
				}
				console.log('validaAsamblea'+data.data);
			})
			.catch(function(err){
				console.log("error::",err);
				$ionicPopup.alert({ title: 'Alerta', template: err.message , okType: 'button-assertive'});
			}) ;

		}
	// //inicio - @feature/MasterCard		
	// $scope.pagoMastercardObligacionPagoID=obligacionPagoID;
	// $scope.vistaIdNuevaObligacionCia=$stateParams.origenID;
	// //fin - @feature/MasterCard
	/**
	 * inicio -@feature/pagoLink
	*/
		$scope.pagarObligacionPagoLink=function(){
			console.log("Pago por CIA-ACuenta");
			console.log(obligacionPagoID);
			pagoLinkService.generarPagoLink(obligacionPagoID)
			.then(function(data){
				if(data) {
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Correo enviado con éxito." , okType: 'button-assertive'});	
							alertPopup.then(function(res){
								$scope.continuar();
							});	
				}	
							console.log("pagolink en obligacion"+data);
			})
			.catch(function(err){
							console.log("error::",err);
							$ionicPopup.alert({ title: 'Alerta', template:"No se pudo enviar el link de pago." , okType: 'button-assertive'});
			});
		}
	/**
	 * fin -@feature/pagoLink
	*/
	})

	.controller('crearObligacionCtrl', function(validacionService, $scope, $rootScope, $state, me, shareData,tempContrato,numContrato,tempProspecto,obligacionPagoService,ProformaDetailService,GrupoService,blockUI,$ionicPopup,configuracionObligacionPago,proformaService,contratoService,OBLIGACION_PAGO,$ionicNavBarDelegate,USUARIO){

		$ionicNavBarDelegate.showBackButton(false);

		$scope.flag=false;
		$scope.valido=false;
		$scope.obligacionPagoBean={};
		$scope.me = me;
		
		$scope.obligacionPagoBean.bdImporte=0;
		
		$scope.obligacionPagoBean.cia=0;		
		$scope.obligacionPagoBean.montoCiaPagar=0;
		$scope.obligacionPagoBean.cuotaTotalMensual=0;
		$scope.cuotaTotalMensual = 0;
		$scope.CONCEPTO = {
			CUOTA_MENSUAL: 0,
			CIA: 1
		}

		$scope.prospecto={};
		$scope.prospecto=tempProspecto.prospecto;

		$scope.obligacionPagoBean.strNombre=$scope.prospecto.prospectoBean.nombres;
		$scope.obligacionPagoBean.strApellidoPaterno=$scope.prospecto.prospectoBean.apellidoPaterno;
		$scope.obligacionPagoBean.strApellidoMaterno=$scope.prospecto.prospectoBean.apellidoMaterno;
		$scope.obligacionPagoBean.strTipoDocumentoID=$scope.prospecto.prospectoBean.tipoDocumento;
		$scope.obligacionPagoBean.strCodigoDocumento=$scope.prospecto.prospectoBean.numeroDocumento;
		$scope.obligacionPagoBean.strPersonaTipoID=$scope.prospecto.prospectoBean.personaTipoID;
		$scope.obligacionPagoBean.strRazonSocial=$scope.prospecto.prospectoBean.personaRazonSocial;
		$scope.obligacionPagoBean.strTelefono=$scope.prospecto.prospectoBean.numeroTelefono;
		$scope.obligacionPagoBean.strTelefonoMovil=$scope.prospecto.prospectoBean.numeroCelular;
		$scope.obligacionPagoBean.strCorreoPersonal=$scope.prospecto.prospectoBean.correoElectronico;		
		$scope.obligacionPagoBean.proformaID=$scope.prospecto.proformaID;
		$scope.obligacionPagoBean.lngNroPagos=$scope.prospecto.lngNroPagos;
		$scope.lstObligacionesDePago = [];

		//$scope.conceptos[0]: indica evaluar la cuota mensual
		//$scope.conceptos[1]: indica evaluar el monto de la cia
		$scope.conceptos = configuracionObligacionPago[0].conceptos.split("/");//para el primer pago		
		if(!$scope.prospecto.lstObligacionPagoBean){
			$scope.prospecto.lstObligacionPagoBean = [];
		}

		angular.forEach($scope.prospecto.lstObligacionPagoBean, function(obligacion){
			if(obligacion.lngEstadoObligacion===OBLIGACION_PAGO.ESTADO.PAGADO){
				$scope.lstObligacionesDePago.push(obligacion);
			}
		});
		
		var clientIsValidated = false;
		if($scope.prospecto.prospectoBean.validado!== undefined && $scope.prospecto.prospectoBean.validado){
			clientIsValidated = true;
		}
		var cuotaMensual;
		var porcentaje;
		var nmSeparation;	
		proformaService.getCuotaCertificado($scope.prospecto.grupoID, $scope.prospecto.certificadoPosicionID)
		.then(
			function(data){
				
				$scope.obligacionPagoBean.cuotaTotalMensualPendiente=$scope.prospecto.cuotaTotalMensualPendiente;
				$scope.obligacionPagoBean.cuotaTotalMensualPagado=$scope.prospecto.cuotaTotalMensualPagado;
				$scope.obligacionPagoBean.cuotaTotalMensual = validacionService.aDosDecimales($scope.prospecto.cuotaTotalMensual);
				cuotaMensual = $scope.obligacionPagoBean.cuotaTotalMensual;
				
				var pagos = 0;
				for(var pago in  $scope.prospecto.lstObligacionPagoBean){
					if($scope.prospecto.lstObligacionPagoBean[pago].lngEstadoObligacion === OBLIGACION_PAGO.ESTADO.PAGADO ){
						pagos += $scope.prospecto.lstObligacionPagoBean[pago].bdImporte;
					}			
				}							
				porcentaje = Math.trunc((pagos/cuotaMensual) * 10000)/100  ;
				porcentaje = porcentaje>100?100:porcentaje;		
				porcentaje = porcentaje?porcentaje:0;
				nmSeparation = pagos;
					
			}
		);	

		$scope.changeImportePago = function(){
			$scope.obligacionPagoBean.bdImporte = parseFloat($scope.obligacionPagoBean.bdImporte);
			$scope.cuotaTotalMensual = $scope.obligacionPagoBean.bdImporte;			
			if($scope.cuotaTotalMensual<0){
				$scope.cuotaTotalMensual = $scope.cuotaTotalMensual*(-1);
			}						
			$scope.cuotaTotalMensual = Math.trunc(validacionService.aDosDecimales($scope.cuotaTotalMensual*100))/100;
			$scope.obligacionPagoBean.bdImporte = $scope.cuotaTotalMensual;
		};
		$scope.onBlurImportePago=function(){
			if($scope.obligacionPagoBean.bdImporte===null||$scope.obligacionPagoBean.bdImporte==undefined||isNaN($scope.obligacionPagoBean.bdImporte)){
				$scope.obligacionPagoBean.bdImporte = 0;				
			}
			$scope.changeImportePago();
		}

		$scope.clickImportePago = function(){
			if($scope.obligacionPagoBean.bdImporte===0){
				$scope.obligacionPagoBean.bdImporte = null;
			}			
		};

		$scope.regresar = function(){
			$state.go("app.detalleCliente",{prospectoID: $scope.prospecto.prospectoBean.prospectoID});
		}
		$scope.generarObligacionPago=function(){
			console.log("en generarObligacionPago");
			if($scope.obligacionPagoBean.bdImporte===undefined || $scope.obligacionPagoBean.bdImporte<=0){
				//alert("Ingrese un monto de separación adecuado");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese un monto de separación adecuado." , okType: 'button-assertive'});
				return;
			}	
			var diff = validacionService.aDosDecimales($scope.obligacionPagoBean.cuotaTotalMensual-$scope.obligacionPagoBean.cuotaTotalMensualPagado);
			if($scope.obligacionPagoBean.bdImporte>=diff){
				//alert("El importe ingresado es mayor/igual a la cuota total mensual, debe ir a la opción de INGRESO DE VENTA para poder registrar su venta");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El importe ingresado es mayor/igual a la cuota total mensual, debe ir a la opción de INGRESO DE VENTA para poder registrar su venta." , okType: 'button-assertive'});
				//$scope.irGenerarContrato();
				return;
			}
			//primero obligación de pago
			if($scope.lstObligacionesDePago.length===0){
				var montoMinimo = validacionService.aDosDecimales($scope.obligacionPagoBean.cuotaTotalMensual*configuracionObligacionPago[0].porcentaje);
				if($scope.obligacionPagoBean.bdImporte<montoMinimo){
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El  monto mínimo de separación es US$"+
						($scope.obligacionPagoBean.cuotaTotalMensual*configuracionObligacionPago[0].porcentaje).toFixed(2)+
						" ("+(configuracionObligacionPago[0].porcentaje*100).toFixed(2)+"% de la Cuota Mensual) para la primera obligación de pago." , okType: 'button-assertive'});					
					return;
				}
			}
			//segunda obligación de pago
			if($scope.lstObligacionesDePago.length>=configuracionObligacionPago.length-1){//para realizar el ultimo pago se debe generar contrato				
				if($scope.obligacionPagoBean.bdImporte<diff){
					//alert("Alcanzó el limite maximo de "+(configuracionObligacionPago.length-1)+" obligaciones de pago");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Alcanzó el límite máximo de "+(configuracionObligacionPago.length-1)+" obligaciones de pago. debe ir a la opción de INGRESO DE VENTA para poder registrar su venta." , okType: 'button-assertive'});
					return;
				}
			}
			/*todo correcto*/
			console.info("Generando obligacion de pago");				
			obligacionPagoService.generarObligacion($scope.obligacionPagoBean)
			.then(function(data){				
				$rootScope.obligacionPagoID = data;				
				$rootScope.prospectoID = $scope.prospecto.prospectoBean.prospectoID;
				$rootScope.montoPago=$scope.obligacionPagoBean.bdImporte; // No borrar, se usa para pago de mpos
				$state.go("app.mensajeConfirmacionObligacion");
			});					
		};
		$scope.irGenerarContrato = function () {
			/*if(!clientIsValidated){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Para generar un contrato TIENE que escanear el DNI." , okType: 'button-assertive'});
				return;
			}*/
			/*INICIO INTEGRACION EC*/
			//ProformaDetailService.obtenerDetalleProformaContrato($scope.prospecto.proformaID)
			ProformaDetailService.obtenerDetalleProformaContrato($scope.prospecto.proformaID, APP.MODULO.INGRESO_DE_VENTA.ID)
			/*FIN INTEGRACION EC*/
					.then(function successCallback(data) {						                        
						/*INICIO INTEGRACION EC*/
						if(data.evaluacionCrediticia&&data.evaluacionCrediticia.aprobado==false){
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: data.evaluacionCrediticia.mensaje , okType: 'button-assertive'});
							return;
						}
						/*FIN INTEGRACION EC*/
						if(data.prospectoBean!== null && data.prospectoBean !== undefined){
							if(!data.prospectoBean.personaGrupoEconomico||data.prospectoBean.personaGrupoEconomico===null){data.prospectoBean.personaGrupoEconomico = false;}
							if(!data.prospectoBean.personaSujetoObligado||data.prospectoBean.personaSujetoObligado===null){data.prospectoBean.personaSujetoObligado = false;}
							if(!data.prospectoBean.personaPrevLavadoActivos||data.prospectoBean.personaPrevLavadoActivos===null){data.prospectoBean.personaPrevLavadoActivos = false;}
							if(!data.prospectoBean.personaActividadMinera||data.prospectoBean.personaActividadMinera===null){data.prospectoBean.personaActividadMinera = false;}
							if(data.prospectoBean.errorServicio !== undefined && data.prospectoBean.errorServicio === 1){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'En estos momentos el servicio PIDE no se encuentra activo.' , okType: 'button-assertive'});	
							}
						}
						//console.log(data.prospectoBean); 
						tempProspecto.prospecto = data;
						//console.log("separación % : "+$scope.prospecto.proformas[indice].nmSeparationPct);
						if(porcentaje>=configuracionObligacionPago[0].porcentaje*100){
							//console.log("mayor al 30%");
							tempProspecto.prospecto.cuotaTotalMensualPagado = nmSeparation;
							tempContrato.contrato.cuotaTotalMensual = parseFloat($scope.obligacionPagoBean.cuotaTotalMensual.toFixed(2));
						}else{
							//console.log("menor al 30%");
						}
						tempContrato.titulares = [];
						if($scope.prospecto){
							var conforme = true;	
							if($scope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA&&$scope.prospecto.personaEsEmpleado){
								//alert("Debe ingresar la venta por oficina");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la venta por oficina." , okType: 'button-assertive'});
								conforme = false;
							}else if($scope.prospecto.personaEsEmpleado && (tempProspecto.prospecto.grupoBean.numeroAsambleaActual!=1 || tempProspecto.prospecto.grupoBean.numeroAsambleaActual!=68 || tempProspecto.prospecto.grupoBean.numeroAsambleVario)){
								//alert("Los vinculados solo pueden ingresar en la cuota 1");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Los vinculados solo pueden ingresar en la cuota 1." , okType: 'button-assertive'});
								conforme = false;
							}else if($scope.prospecto.personaOFAC){
								//alert("El prospecto se encuentra en la lista OFAC");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});
								conforme = false;
							}else if($scope.prospecto.personaContratosResueltos){
								//alert("El prospecto tiene contratos resueltos");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos resueltos." , okType: 'button-assertive'});
								conforme = false;
							}else if($scope.prospecto.personaContratosLegal){
								//alert("El prospecto tiene contratos en legal");
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos en legal." , okType: 'button-assertive'});
								conforme = false;
							}
							/*INICIO INTEGRACION EC*/
							else if($scope.prospecto.empresasConContratosLegales||$scope.prospecto.empresasConContratosResueltos){
								var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No es posible ingresar venta a la persona, debido a que cuenta con contrato(s) en situación legal o resueltos con Pandero." , okType: 'button-assertive'});
								if($scope.prospecto.empresasConContratosResueltos){
									//proformaService.enviarCorreoContratosResueltos($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								}
								if($scope.prospecto.empresasConContratosLegales){
									//proformaService.enviarCorreoContratosLegal($scope.prospecto.tipoDocumento,$scope.prospecto.numeroDocumento,tempProspecto.prospecto.prospectoBean.nombreCompleto);
								}
								conforme = false;
							}
							/*FIN INTEGRACION EC*/
							if(conforme){
								tempProspecto.prospecto.prospectoBean.personaEsEmpleado = $scope.prospecto.personaEsEmpleado;
								$scope.limpiarListas();
								$state.go('app.generarContrato.datosContrato',{previous : 0});
							}else{
								$scope.limpiarListas();
							}
						}else{
							tempProspecto.prospecto.prospectoBean.personaEsEmpleado = false;
							$scope.limpiarListas();
							$state.go('app.generarContrato.datosContrato',{previous : 0});
						}
						//console.log(tempProspecto.prospecto.prospectoBean);                        
					}, function errorCallback(response) {
						$scope.limpiarListas();
					});
		}
		$scope.limpiarListas = function(){
			$rootScope.tempLstProgramaProducto = undefined;
			$rootScope.tempLstGrupo = undefined;
			$rootScope.tempLstCertificado = undefined;
			$rootScope.tempLstMarca = undefined;
			$rootScope.tempLstModelo = undefined;
			$rootScope.tempLstTipoBien = undefined;
			$rootScope.dataMarcas = false;
			$rootScope.porcentajeCuotaAdmin = undefined;
		};
	})
	/**
	 * @feature V4-HU005
	 *  - no se bloquean los campos de nombre, apellidos y razón social
	 * @feature V4-HU001
	 * - la validación de los teéfonos se realiza en el backend
	 */
	.controller('editarProspectoCtrl', function($stateParams,$rootScope,$scope, $state,shareData,$ionicPopup,validacionService,proformaService,dateFilter, $ionicPlatform, prospectoResult, pagosResult, prospectoService, PANDERO_DATA, USUARIO, PDF417PLUGIN, FUENTE_DE_INFORMACION, escanerService,blockUI,$ionicHistory) {
		//$scope.tempProformaDetalle = tempProformaDetalle;

		$scope.busquedaEscaner = true;
		$scope.busquedaEscaner = true;
		$scope.busquedaConEscaner = false;

		
		//$scope.tempProformaDetalle = tempProformaDetalle;
		$scope.validacionService = validacionService;		

	    var tempProforma = {};
	    tempProforma.proforma = {};  

		$scope.lstTipoDocumento = shareData.lstTipoDocumento;
		$scope.TIPO_DOCUMENTO = {
			RUC: '8',
			DNI: '4',
			PAS: '3',
			CE: '2'
		};	
		$scope.limpiarProspecto = function (){

			var tipoDocumentoSelected = undefined;
			if(tempProforma.proforma.vtaProspecto !== undefined && tempProforma.proforma.vtaProspecto.tipoDocumentoSelected !== undefined){
				tipoDocumentoSelected = tempProforma.proforma.vtaProspecto.tipoDocumentoSelected;
			}
			tempProforma.proforma.vtaProspecto = {};
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = false;
			if(tipoDocumentoSelected!==undefined){
				tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[tipoDocumentoSelected.codigo];
				tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
				tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =true;
				if(tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo === $scope.TIPO_DOCUMENTO.DNI){
					tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =false;
				}
			}else{
				tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
			}	
			
			tempProforma.proforma.vtaProspecto.tipoDocumentoDisabled =  true
		};

		$scope.tienePagos = function(pagosResult){
	    	var tienepagos = false;
	    	for(var item in pagosResult.proformas){
				if(pagosResult.proformas[item].lstObligacionPagoBean.length>0){
					tienepagos = true;
					break;
				}
			}
			return tienepagos;
		};


	    if(tempProforma.proforma.vtaProspecto===undefined){
			tempProforma.proforma.vtaProspecto = prospectoResult;
			/*inicio @issue EVALCRED-57*/
			/*
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = false;
			*/
			/** inicio @feature V4-HU005 **/
			tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;//prospectoResult.validado ? false : true;
			tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true; //prospectoResult.validado ? false : true;
			/** fin @feature V4-HU005 **/
			/*fin @issue EVALCRED-57*/
			tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[tempProforma.proforma.vtaProspecto.tipoDocumentoID];;
			tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
			tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =true;
			if(tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo === $scope.TIPO_DOCUMENTO.DNI){
				tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =false;
			}
			
	    }
	    tempProforma.proforma.vtaProspecto = prospectoResult;
		$scope.tempProforma=tempProforma;	
		/*inicio @issue EVALCRED-57*/
		/*
		$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
		$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;
		*/
		/** inicio @feature V4-HU005 **/
		$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;//prospectoResult.validado ? false : true;
		$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true; //prospectoResult.validado ? false : true;
		/** fin @feature V4-HU005 **/
		/*fin @issue EVALCRED-57*/
		$scope.tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;
		$scope.numeroDocumentoInit = $scope.tempProforma.proforma.vtaProspecto.codigoDocumento;
		if(tempProforma.proforma.vtaProspecto.tipoDocumentoID=="4"){
			$scope.busquedaEscaner = true;
		}else{
			$scope.busquedaEscaner = false;
		}
		var tienepagos = $scope.tienePagos(pagosResult);
		
		/*INICIO - modificado por John Velasquez - VTAOBS1-57 - 28.09.2017*/
		/*if(!tienepagos && ($scope.tempProforma.proforma.vtaProspecto.validado !== undefined && !$scope.tempProforma.proforma.vtaProspecto.validado)){
			$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
		}

		if($scope.tempProforma.proforma.vtaProspecto.validado!== undefined && $scope.tempProforma.proforma.vtaProspecto.validado && tienepagos){
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =false;
			$scope.busquedaConEscaner = $scope.tempProforma.proforma.vtaProspecto.validado;
			$scope.tempProforma.proforma.vtaProspecto.buscado = $scope.tempProforma.proforma.vtaProspecto.validado;
		}*/
		if(!tienepagos&&$scope.tempProforma.proforma.vtaProspecto.validado!==true){
			if(	
				(
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID!==$scope.TIPO_DOCUMENTO.CE&&
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID!==$scope.TIPO_DOCUMENTO.PAS
				)||
				$scope.tempProforma.proforma.vtaProspecto.fuenteInformacion !== FUENTE_DE_INFORMACION.SAF
			){
				$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
				$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =false;
				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
				$scope.busquedaConEscaner = $scope.tempProforma.proforma.vtaProspecto.validado;			
				$scope.tempProforma.proforma.vtaProspecto.buscado = $scope.tempProforma.proforma.vtaProspecto.validado;					
			}			
		}
		/*FIN - modificado por John Velasquez - VTAOBS1-57 - 28.09.2017*/
		/*Inicio VTADIG2-495*/
		/*inicio @issue EVALCRED*/
		if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID == $scope.TIPO_DOCUMENTO.DNI ||
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID == $scope.TIPO_DOCUMENTO.CE ||
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID == $scope.TIPO_DOCUMENTO.RUC){
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
			$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro = false;
		}
		/*fin @issue EVALCRED*/
		/*Fin VTADIG2-495*/


		$scope.resetTipoDocumento = function (){
			//$scope.tipoDocumentoSelected = null;
			//TODO: CAMBIARA CONSTANTES
			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = shareData.mapTipoDocumento[$scope.TIPO_DOCUMENTO.DNI];
		};
		$scope.buscarProspecto = function(scannerData,scannerDni){
			if ($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected ===null || 
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected ===undefined){
				//alert('Debe seleccionar un tipo de documento');
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Debe seleccionar un tipo de documento.' , okType: 'button-assertive'});
				return false;
			}
			if ($scope.tempProforma.proforma.vtaProspecto.codigoDocumento === '' || $scope.tempProforma.proforma.vtaProspecto.codigoDocumento === undefined){
				//alert('Debe ingresar un número de documento');
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Debe ingresar un número de documento.' , okType: 'button-assertive'});
				return false;
			}

			var nroDocumento = $scope.tempProforma.proforma.vtaProspecto.codigoDocumento;
	  		var tmpTipoDocumentoSelected = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected;   		
	  		proformaService.getProspectoPorDocumento(tmpTipoDocumentoSelected.codigo,nroDocumento)
	  		.then(function(data){		  			
	  			
	  			if(!data){

					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'Número de DNI no coincide con el DNI escaneado.' , okType: 'button-assertive'});
					return false;
					/*$scope.limpiarProspecto();  
	  				//alert('El documento del cliente no se encuentra registrado. Por favor llenar sus datos.');
	  				
	  				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'El documento del cliente no se encuentra registrado. Por favor llenar sus datos.' , okType: 'button-assertive'});
	  				
	  				
	  				$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = nroDocumento;		
	  				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
	  				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
	  				$scope.tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;*/
	  				
	  			}else{
	  				if(data.errorServicio === 2){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});						
						prospectoService.envioCorreoOFAC(data.personaNombreCompletoOFAC,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
						$scope.limpiarProspecto();
						return;
					}
	  				$scope.tempProforma.proforma.vtaProspecto = data;	
	  				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = false;
	  				$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
	  				$scope.tempProforma.proforma.vtaProspecto.dataSecundariaDisabled = true;
	  				$scope.tempProforma.proforma.vtaProspecto.buscado = true;
	  				if($scope.tempProforma.proforma.vtaProspecto.prospectoID!==undefined){
	  				prospectoService.obtenerDetalleProspecto($scope.tempProforma.proforma.vtaProspecto.prospectoID)
			        .then(function(data){
						var tienepagos = $scope.tienePagos(data);
							if(!tienepagos && ($scope.tempProforma.proforma.vtaProspecto.validado !== undefined && !$scope.tempProforma.proforma.vtaProspecto.validado)){
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
						}
			        });
					}else{
						if($scope.tempProforma.proforma.vtaProspecto.validado !== undefined && !$scope.tempProforma.proforma.vtaProspecto.validado){
						$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;						
						}
						
					}
					if(tmpTipoDocumentoSelected.codigo===$scope.TIPO_DOCUMENTO.RUC){
						if($scope.tempProforma.proforma.vtaProspecto.errorServicio !== undefined && $scope.tempProforma.proforma.vtaProspecto.errorServicio === 1){						
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: 'En estos momentos el servicio PIDE no se encuentra activo.' , okType: 'button-assertive'});	
						}
					}
	  				
	  			}
	  			if(scannerData!=""){
  					$scope.getDataPDF417(scannerData);
					  $scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = false;
					  $scope.tempProforma.proforma.vtaProspecto.validado = true;
				}

				if(scannerDni){
					$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabled = true;
					$scope.tempProforma.proforma.vtaProspecto.dataPrincipalDisabledCE = true;
					$scope.tempProforma.proforma.vtaProspecto.validado = true;
				}

				$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = false;
				$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabledNro =false;
	  			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected = tmpTipoDocumentoSelected;
				
	  		});
		};

		$scope.changeTipoDocumentoSeleccionado = function (newValue, oldValue){
					$scope.busquedaConEscaner = false;
					$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected  = newValue;
					if(newValue.codigo=="4"){
						$scope.busquedaEscaner = true;	
					}else{
						$scope.busquedaEscaner = false;
					}
					$scope.tempProforma.proforma.vtaProspecto.codigoDocumento  = '';
					$scope.limpiarProspecto();
		};
		/**
		 * 
		 * @returns {boolean}
		 * @feature V4-HU001
		 * - la validación de los telefonos se realiza en el backend
		 * 
		 */
		$scope.esValidoTabProspecto =function(){
			if(!$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected){
				//alert("Tipo de Documento es obligatorio");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Tipo de Documento es obligatorio." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto===undefined || 
				!$scope.tempProforma.proforma.vtaProspecto.codigoDocumento){
				//alert("Número de Documento es obligatorio");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Número de Documento es obligatorio." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.DNI){
				if(!$scope.tempProforma.proforma.vtaProspecto.nombre){
					//alert("Nombre es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Nombre es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno){
					//alert("Apellido Paterno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Paterno es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoMaterno && !$scope.tempProforma.proforma.vtaProspecto.buscado){
					//alert("Apellido Materno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Materno es obligatorio." , okType: 'button-assertive'});
					return false;
				}
			}
		
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.PAS || 
				$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.CE){

				if(!$scope.tempProforma.proforma.vtaProspecto.nombre){
					//alert("Nombre es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Nombre es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno){
					//alert("Apellido Paterno es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Apellido Paterno es obligatorio." , okType: 'button-assertive'});
					return false;
				}
			}

			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				
				if(!$scope.tempProforma.proforma.vtaProspecto.razonSocial){
					//alert("Razón Social es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Razón Social es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if(!$scope.tempProforma.proforma.vtaProspecto.contacto){
					//alert("Contacto es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Contacto es obligatorio." , okType: 'button-assertive'});
					return false;
				}else if($scope.tempProforma.proforma.vtaProspecto.codigoDocumento === PANDERO_DATA.RUC && $rootScope.me.tipoUsuarioVentas !== USUARIO.TIPO.OFICINA){
					//alert("Contacto es obligatorio");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Debe ingresar la venta por oficina." , okType: 'button-assertive'});
					return false;
				}
		
			}
			
			
			if(!validacionService.validarCorreElectronico($scope.tempProforma.proforma.vtaProspecto.correo)){
				//alert("El correo es obligatorio, y debe tener el formato user@domain.com");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El correo es obligatorio, y debe tener el formato user@domain.com" , okType: 'button-assertive'});
				return false;
			}

			if(!$scope.tempProforma.proforma.vtaProspecto.telefono && !$scope.tempProforma.proforma.vtaProspecto.telefonoMovil){
				//alert("Ingrese al menos un telefóno");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese al menos un teléfono." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.telefono !== undefined && $scope.tempProforma.proforma.vtaProspecto.telefono.length > 0 && $scope.tempProforma.proforma.vtaProspecto.telefono.length < 6){
				//alert("El teléfono debe tener como mínimo 6 dígitos");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El teléfono debe tener como mínimo 6 dígitos." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.telefonoMovil !== undefined && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil.length > 0 && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil.length !== 9){
				//alert("El celular debe tener 9 dígitos");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El celular debe tener 9 dígitos." , okType: 'button-assertive'});
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaContratosLegal!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaContratosLegal){
				//alert("El prospecto tiene contratos en legal");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto tiene contratos en legal." , okType: 'button-assertive'});
				$scope.enviarCorreoLegalProspecto();
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaOFAC!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaOFAC){
				//alert("El prospecto tiene contratos en legal");
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El prospecto se encuentra en la lista OFAC." , okType: 'button-assertive'});
				$scope.enviarCorreoOFACProspecto();
				return false;
			}

			if($scope.tempProforma.proforma.vtaProspecto.personaContratosResueltos!==undefined && $scope.tempProforma.proforma.vtaProspecto.personaContratosResueltos){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El titular tiene contratos resueltos." , okType: 'button-assertive'});
				$scope.enviarCorreoResueltosProspecto();
				return false;
			}


			/** inicio @feature V4-HU001 **/
			/*
			if($rootScope.me.usuarioTelefonos!==undefined){
				var index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefono);
				if(index>=0 && $scope.tempProforma.proforma.vtaProspecto.telefono!==""){
					//alert("El teléfono no pertenece al titular");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El teléfono no pertenece al titular." , okType: 'button-assertive'});
					// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					$scope.enviarCorreoTelefonoEDV($scope.tempProforma.proforma.vtaProspecto.telefono);
					// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					return false;
				}
				index = $rootScope.me.usuarioTelefonos.indexOf($scope.tempProforma.proforma.vtaProspecto.telefonoMovil);
				if(index>=0 && $scope.tempProforma.proforma.vtaProspecto.telefonoMovil!==""){
					//alert("El celular no pertenece al titular");
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El celular no pertenece al titular." , okType: 'button-assertive'});
					// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					$scope.enviarCorreoTelefonoEDV($scope.tempProforma.proforma.vtaProspecto.telefonoMovil);
					// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
					return false;
				}				
			}
			*/
			/** fin @feature V4-HU001 **/
			
			return true;
		};	
		$scope.limpiarListas = function(){						
			$rootScope.tempLstProgramaProducto = undefined;
			$rootScope.tempLstGrupo = undefined;
			$rootScope.tempLstCertificado = undefined;
			$rootScope.tempLstMarca = undefined;
			$rootScope.tempLstModelo = undefined;
			$rootScope.tempLstTipoBien = undefined;
			$rootScope.dataMarcas = undefined;
			$rootScope.porcentajeCuotaAdmin = undefined;
		};
		// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
		$scope.enviarCorreoTelefonoEDV = function(telefono){
		// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			prospectoService.envioCorreoTelefonoNoPerteneceEDV(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento,telefono);
			// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
		};

		$scope.enviarCorreoOFACProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};


		$scope.enviarCorreoLegalProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			proformaService.enviarCorreoContratosLegal($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo,$scope.tempProforma.proforma.vtaProspecto.codigoDocumento, nombre);
			//prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};

		$scope.enviarCorreoResueltosProspecto = function(){
			var nombre = "";
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo  === $scope.TIPO_DOCUMENTO.RUC){
				nombre = $scope.tempProforma.proforma.vtaProspecto.razonSocial;
			}else{
				nombre = $scope.tempProforma.proforma.vtaProspecto.nombre + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoPaterno + " " + $scope.tempProforma.proforma.vtaProspecto.apellidoMaterno;
			}
			proformaService.enviarCorreoContratosResueltos($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo,$scope.tempProforma.proforma.vtaProspecto.codigoDocumento, nombre);
			//prospectoService.envioCorreoOFAC(nombre,$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.descripcion, $scope.tempProforma.proforma.vtaProspecto.codigoDocumento);
		};

		$scope.guardar=function(){
			
			if(!$scope.esValidoTabProspecto()){
				return false;
			}

			$scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID = $scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo;
			//$scope.tempProforma.proforma.vtaProspecto.validado=$scope.busquedaConEscaner;
			if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoSelected.codigo!=="4"){
				// inicio @issue EVALCRED-57
				//$scope.tempProforma.proforma.vtaProspecto.validado = true;
				// fin @issue EVALCRED-57
				if($scope.tempProforma.proforma.vtaProspecto.tipoDocumentoID===$scope.TIPO_DOCUMENTO.RUC){
					if(!$scope.tempProforma.proforma.vtaProspecto.personaEstado && !$scope.tempProforma.proforma.vtaProspecto.pesonaFlag_22){
						$scope.tempProforma.proforma.vtaProspecto.validado = false;
					}
				}


			}

			var esNuevo = false;
			if($scope.numeroDocumentoInit !== $scope.tempProforma.proforma.vtaProspecto.codigoDocumento){
				$scope.tempProforma.proforma.vtaProspecto.prospectoID = null;
				esNuevo = true;
			}
			
			//registro sólo de prospecto
	  		proformaService.registrarProspecto($scope.tempProforma.proforma.vtaProspecto).then(function(data){	
				var alertPopup = $ionicPopup.alert({
						     title: 'Mensaje',
						     template: data.message,
						     okText: 'Aceptar',
						     okType: 'button-assertive'
						   });
					var prospectoID = null;
					if(esNuevo){
						prospectoID = data.data;						
					}else{
						prospectoID = $scope.tempProforma.proforma.vtaProspecto.prospectoID;
					}
					alertPopup.then(function(res) {
						$state.go('app.detalleCliente',{prospectoID: prospectoID});
					});
				$scope.limpiarListas();
				$scope.limpiarProspecto();
				$scope.tempProforma = {};					
					//$rootScope.proformaBack = 1;		  			
	  		});
							
		};		

		$scope.isNumber = function (n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
		};


		$scope.scanBarcode = function() {
			try{				
				var currentPlatform = ionic.Platform.platform();  
				if(currentPlatform ===  PDF417PLUGIN.PLATFORM_ANDROID){
					escanerService.barcodeScanner($scope.scannerProcess);
				}else{
					$scope.tempProforma.proforma.vtaProspecto.tipoNumeroDocumentoDisabled = true;
					escanerService.pdf417ScannerScan($scope.scannerProcess);
				}	
			}catch(e){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
				console.log(e.message + " -  " + e.stack);
			}

	    };

		$scope.scannerProcess = function(type, result){	
			if (type === "CODE39") {	
				$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = result.toString();
				$scope.buscarProspecto("",true);			
			}else if (type === "PDF417") {		
				var DNI = result.substring(2,10);
				if($scope.isNumber(DNI)){
					$scope.tempProforma.proforma.vtaProspecto.codigoDocumento = DNI;
					$scope.buscarProspecto(result);
					$scope.busquedaConEscaner = true;
				}else{
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El DNI escaneado no puede ser procesado." , okType: 'button-assertive'});
				}
			}
	    };

	    $scope.getDataPDF417 = function(data) {
	        var colLength = 16;
	        var rowLimit = 7;
	        var dataToParse =  data.substring(0,colLength * rowLimit);
	        var DNI = dataToParse.substring(2,10);
	        var apellidoPaterno = "", apellidoMaterno = "", nombres = "";
			var colLength = 16, rowLimit = 7, initNamePosition = 10;
			for(var i=initNamePosition;i<(colLength*rowLimit);i++){
				if(i<=(colLength*3) + 1){
					apellidoPaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*3) + 1 && i<=(colLength*5) + 9){
					apellidoMaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*5) + 9 ){
					nombres+=dataToParse.charAt(i);
				}
			}
			$scope.tempProforma.proforma.vtaProspecto.nombre = nombres.trim();
			$scope.tempProforma.proforma.vtaProspecto.apellidoPaterno = apellidoPaterno.trim();
			$scope.tempProforma.proforma.vtaProspecto.apellidoMaterno = apellidoMaterno.trim();
	    }

        /*inicio @issue VTADIG2-496*/
        $scope.regresar = function(){
            $ionicHistory.goBack();
        }
        /*fin @issue VTADIG2-496*/
        
		blockUI.stop();	    
		
		
	})
	.controller("detallePagoCtrl", function($scope, tempProspecto,configuracionObligacionPago,me, contratoService, validacionService,OBLIGACION_PAGO,proformaService){
		$scope.proforma = tempProspecto.proforma;
		$scope.prospectoID = tempProspecto.prospectoID;
		$scope.ESTADO_OBLIGACION = OBLIGACION_PAGO.ESTADO;
		$scope.hayContrato = false;
		$scope.contrato;
		$scope.cia = $scope.proforma.certificadoBean.cia;		
		//inicializar cuotas devengadas 0
		$scope.cuotasProrrateo = 0;
		$scope.cuotasIngreso = 0;
		$scope.cuotasAdjudicacion = 0;
		//inicializar cuota mensual por defecto a lo indicado en el certificado
		$scope.coutaMensual = $scope.proforma.certificadoBean.cuotaMensual;
		$scope.coutaMensualTotal = validacionService.aDosDecimales($scope.coutaMensual*($scope.cuotasIngreso+1));
		//inicialisar pagos por conceptos de cuota mensual y CIA
		$scope.pagoPorCuotaMensual = 0;		
		$scope.contratoCia = {
			importePago: $scope.proforma.certificadoBean.cia,
			ciaPagada: 0,
			ciaPendiente: $scope.proforma.certificadoBean.cia
		}
		$scope.proforma.lstObligacionPagoBean = $scope.proforma.lstObligacionPagoBean.filter(function(obligacion){
			return obligacion.lngEstadoObligacion !== $scope.ESTADO_OBLIGACION.ELIMINADO
		});
		angular.forEach($scope.proforma.lstObligacionPagoBean,function(obligacion){			
			//pago por concepto de cuota mensual consideranro prorrateos e ingresos de venta 
			if(obligacion.lngEstadoObligacion !== $scope.ESTADO_OBLIGACION.ELIMINADO){
				if(obligacion.fechaRegistro){
					obligacion.fechaRegistro = new Date(obligacion.fechaRegistro);
				}
				if(obligacion.fechaPago){
					obligacion.fechaPago = new Date(obligacion.fechaPago);
				}
				if(obligacion.contratoNumero){
					$scope.numeroContrato = obligacion.contratoNumero;
					$scope.hayContrato = true;
				}
				if(obligacion.lngEstadoObligacion === $scope.ESTADO_OBLIGACION.PAGADO){
					if(obligacion.cuotaMensual){
						$scope.pagoPorCuotaMensual = validacionService.aDosDecimales($scope.pagoPorCuotaMensual + obligacion.cuotaMensual);
					}
					//pago por concepto de CIA
					if(obligacion.cia){
						$scope.pagoPorCia = validacionService.aDosDecimales($scope.pagoPorCia + obligacion.cia);
					}
				}				
			}			
		});
		if($scope.hayContrato){
			contratoService.obtenerContratoPorNumero($scope.numeroContrato)
			.then(
				function(data){
					$scope.contrato = data;
					//reasignacion de cuotas devengadas
					$scope.cuotasProrrateo  = data.contratoNroCuotasProrrateo?data.contratoNroCuotasProrrateo:0;
					$scope.cuotasIngreso  = data.contratoNroCuotasIngreso?data.contratoNroCuotasIngreso:0;
					$scope.cuotasAdjudicacion = data.contratoNroCuotasAdjudicacion?data.contratoNroCuotasAdjudicacion:0;
					//reasignacion de cuota mensual
					$scope.coutaMensual = validacionService.aDosDecimales(data.cuotaMensualFinal);
					$scope.coutaMensualTotal = validacionService.aDosDecimales($scope.coutaMensual*($scope.cuotasIngreso+1));					
					if(data.grupoID&&data.certificadoPosicionID){
						proformaService.getCuotaCertificado(data.grupoID,data.certificadoPosicionID).then(
							function(data2){
								data2 = data2[0];
								$scope.cia = data2.certificadoCIA;
							}
						);	
					}					
				}
			);
			contratoService.buscarContratosCiaPendientes(4,$scope.numeroContrato.replace(/-/g,''))
			.then(function(data){
				$scope.contratoCia = data[0];				
			});
		}
	})
	.controller('pagoCiaCtrl',function($scope,$rootScope,$state,$stateParams/*,$ionicNavBarDelegate*/,$ionicPopup,contratoService,$ionicPopover, shareData){
		//$ionicNavBarDelegate.title("Pago CIA")
		//$ionicNavBarDelegate.showBackButton(false);
		$scope.buscarContrato = function($event){			
			$scope.busqueda = $event
			$rootScope.temp.busquedaContratoCiaPendiente = $scope.busqueda;
			contratoService.buscarContratosCiaPendientes($event.tipo,$event.valor1,$event.valor2).then(
				function(contratos){
					$scope.contratos = contratos;
					angular.forEach($scope.contratos,function(contrato){
						contrato.tipoDocumentoNombre = shareData.mapTipoDocumento[contrato.tipoDocumentoID].descripcion;
					});
				},
				function(error){
					$rootScope.temp.busquedaContratoCiaPendiente = {};
					$scope.busqueda = $rootScope.temp.busquedaContratoCiaPendiente;
				}
			)
		}
        /**START - POPOVER VINCULADOS**/
        var OPTIONS = {
        	CONSULTA_PAGO: 1,
			PAGO_A_CUENTA: 2,
			REENVIAR_CORREO_PAGO_PENDIENTE: 3,
			REENVIAR_CORREO_PAGO_REALIZADO: 4
		};
        $scope.temps = {};
        $scope.contratoCiaOptions = [
			{codigo: OPTIONS.PAGO_A_CUENTA, label:"Pago a cuenta", mostrar: true},
			{codigo: OPTIONS.REENVIAR_CORREO_PAGO_PENDIENTE, label:"Enviar pago pendiente", mostrar: true},
			{codigo: OPTIONS.REENVIAR_CORREO_PAGO_REALIZADO, label:"Enviar pago realizado", mostrar: true}
		];
		$scope.optionsMap = {};
        $ionicPopover.fromTemplateUrl('contrato-cia-popover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.contratoCiaOptionsPopover = popover;
        });
		$scope.onOpenContratoCiaOption = function($event,$contrato){
			console.log("$event: %o",$event);
			if($contrato){
				$scope.temps.contrato = $contrato;
				
				if($scope.temps.contrato.nroObligacionesPagadas>0){
					$scope.optionsMap[OPTIONS.REENVIAR_CORREO_PAGO_REALIZADO].mostrar = true;
				}else{
					$scope.optionsMap[OPTIONS.REENVIAR_CORREO_PAGO_REALIZADO].mostrar = false;
				}
			}
		};
        $scope.selectContratoCiaOption = function (option) {
        	console.log(option);
			switch (option.codigo){
				case OPTIONS.CONSULTA_PAGO:
					$state.go('app.consultaPagoCia',{numeroContrato:$scope.temps.contrato.numeroContrato,contratoID:$scope.temps.contrato.contratoId});
					break;
				case OPTIONS.PAGO_A_CUENTA:
					$state.go('app.nuevaObligacionPagoCia',{numeroContrato:$scope.temps.contrato.numeroContrato});
					break;
				case OPTIONS.REENVIAR_CORREO_PAGO_PENDIENTE:
					contratoService.enviarCorreopagoPendiente($scope.temps.contrato.contratoId,$scope.temps.contrato.numeroContrato).then(function(){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se envió el correo de pago pendiente." , okType: 'button-assertive'});
					});
				break;
				case OPTIONS.REENVIAR_CORREO_PAGO_REALIZADO:
					contratoService.enviarCorreopagoRealizado($scope.temps.contrato.contratoId).then(function(){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se envió el correo de pago realizado." , okType: 'button-assertive'});
					});
				break;
			}
            $scope.contratoCiaOptionsPopover.hide();
        };

        $scope.$on('$destroy', function () {
            $scope.contratoCiaOptionsPopover.remove();
		});
		
		$scope.init = function(){
			$scope.contratos = [];
			if(!$rootScope.temp){
				$rootScope.temp = {};
			}
			if(!$rootScope.temp.busquedaContratoCiaPendiente){
				$rootScope.temp.busquedaContratoCiaPendiente = {};
			}
			if($rootScope.temp.busquedaContratoCiaPendiente.tipo&&!$stateParams.new===true){
				$scope.busqueda = $rootScope.temp.busquedaContratoCiaPendiente;
				$scope.buscarContrato($scope.busqueda);
			}
			angular.forEach($scope.contratoCiaOptions, function(option){
				$scope.optionsMap[option.codigo] = option;
			})
		}
		$scope.init();
	})
	.controller('consultaPagoCiaCtrl',function($scope,$stateParams,$state,$ionicNavBarDelegate,contratoService,obligaciones,contrato,OBLIGACION_PAGO){
		$ionicNavBarDelegate.title("Detalle Pago CIA")
		$ionicNavBarDelegate.showBackButton(false);
		$scope.contrato = contrato;
        $scope.ESTADO_OBLIGACION = OBLIGACION_PAGO.ESTADO;
        $scope.init = function(){
            if(obligaciones){
                $scope.obligaciones = obligaciones.filter(function(obligacion){
                    return obligacion.lngEstadoObligacion != $scope.ESTADO_OBLIGACION.ELIMINADO;
                });
            }
		}
        $scope.regresar = function(){
            $state.go("app.pagoCia");
        };
        $scope.init();
	})
	.controller('nuevaObligacionPagoCiaCtrl',function($scope,$state,$rootScope,$stateParams,$ionicPopup,$ionicNavBarDelegate,obligacionPagoService,validacionService,contrato,VISTAS_ID){
		$ionicNavBarDelegate.title("Nuevo Pago CIA")
		$ionicNavBarDelegate.showBackButton(false);
		$scope.contrato = contrato;
		$scope.obligacionPagoBean = {
            indPagoCia: true,
            bdImporte: 0,
			strTipoDocumentoID: contrato.tipoDocumentoID,
            strCodigoDocumento: contrato.codigoDocumento,
            strPersonaTipoID: contrato.personaTipoID,
			contratoID: contrato.contratoId,
            contratoNumero: contrato.numeroContrato,
			strCorreoPersonal: contrato.correoAsociado,
			strNombre: contrato.nombreAsociado,
			productoNombre: contrato.programaProducto
		};
        $scope.generarObligacionPago=function(){
            if($scope.obligacionPagoBean.bdImporte===undefined || $scope.obligacionPagoBean.bdImporte<=0){
                //alert("Ingrese un monto de separación adecuado");
                var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Ingrese un monto de CIA adecuado." , okType: 'button-assertive'});
                return;
            }
            /*todo correcto*/
			var confirmPopup = $ionicPopup.confirm({
				title: 'Obligación de CIA',
				template: 'Se generará el pago a cuenta de CIA',
				cancelText: 'Cancelar',
				okText: 'Aceptar'
			});

			confirmPopup
			.then(function(res) {
				if(res) {
					console.info("Generando obligacion de pago");
					obligacionPagoService.generarObligacion($scope.obligacionPagoBean)
					.then(function(data){
						$rootScope.obligacionPagoID = data;
						$rootScope.prospectoID = $scope.contrato.proformaBean.prospectoBean.prospectoID;
						$rootScope.montoPago=$scope.obligacionPagoBean.bdImporte; // No borrar, se usa para pago de mpos
						$state.go("app.mensajeConfirmacionObligacion",{origenID: VISTAS_ID.NUEVA_OBLIGACION_CIA});
					});
				}
			});
        };
        $scope.regresar = function(){
            $state.go("app.pagoCia");
		};
        $scope.changeImportePago = function(){
            $scope.obligacionPagoBean.bdImporte = Math.abs(parseFloat($scope.obligacionPagoBean.bdImporte));
            $scope.obligacionPagoBean.bdImporte = Math.trunc(validacionService.aDosDecimales($scope.obligacionPagoBean.bdImporte*100))/100;
            if($scope.obligacionPagoBean.bdImporte>$scope.contrato.ciaPendiente){
                $scope.obligacionPagoBean.bdImporte = $scope.contrato.ciaPendiente;
			}
        };
        $scope.onBlurImportePago = function(){
            if($scope.obligacionPagoBean.bdImporte===null||$scope.obligacionPagoBean.bdImporte==undefined||isNaN($scope.obligacionPagoBean.bdImporte)){
                $scope.obligacionPagoBean.bdImporte = 0;
            }
            $scope.changeImportePago();
		}
		$scope.clickImportePago = function(){
            if($scope.obligacionPagoBean.bdImporte===0){
                $scope.obligacionPagoBean.bdImporte = null;
            }
        }
	})
	/**
	 * @feature V4-HU010
	 */
	.controller('NotificacionPagoCiaCtrl', function($scope, $ionicPopup, contratosCiaPendientes, configuracionRecordatorioPago, contratoService, TIPO_BUSQUEDA_CONTRATO) {
		$scope.tipoPago = "CIA";
		$scope.contratos = [];
		$scope.configuracionRecordatorioPago = configuracionRecordatorioPago;
		$scope.toggleAllActions = {
			SELECT_ALL: 1,
			DESELECT_ALL: 2
		};
		
		$scope.toggleAllAction = $scope.toggleAllActions.SELECT_ALL;
		
		$scope.enviar = () => {
			contratoService.enviarRecordatorioPagoCia($scope.contratos.filter(contrato => contrato.seleccionado).map(contrato => contrato.contratoId)).then(data => {
				console.log(data);
				$ionicPopup.alert({ title: 'Alerta', template: data.message , okType: 'button-assertive'})
					.then($scope.actualizar);
			});
		};
		
		$scope.toggleAll = () => {
			switch ($scope.toggleAllAction) {
				case $scope.toggleAllActions.SELECT_ALL:
					$scope.contratoHabiles.forEach(contrato => contrato.seleccionado = true);
					$scope.toggleAllAction = $scope.toggleAllActions.DESELECT_ALL;
					break;
				case $scope.toggleAllActions.DESELECT_ALL:
					$scope.contratoHabiles.forEach(contrato => contrato.seleccionado = false);
					$scope.toggleAllAction = $scope.toggleAllActions.SELECT_ALL;
					break;
			}
		};
		
		$scope.changeSelect = (contrato) => {
			if(!contrato.seleccionado){
				$scope.toggleAllAction = $scope.toggleAllActions.SELECT_ALL;
			} else if(!$scope.contratoHabiles.find(contrato => !contrato.seleccionado)){
				$scope.toggleAllAction = $scope.toggleAllActions.DESELECT_ALL;
			}
		};
		
		$scope.actualizar = () => {
			contratoService.buscarContratosCiaPendientes(TIPO_BUSQUEDA_CONTRATO.TODOS)
				.then(function (data) {
					$scope.setContratos(data);
				});
		};
		
		$scope.setContratos = (contratos) => {
			$scope.contratos = contratos || [];
			$scope.contratos = $scope.contratos.filter(contrato => contrato.ciaPendiente > 1);
			$scope.contratos.forEach(contrato => contrato.seleccionado = contrato.envioRecordatorioActivo || false);
			console.log($scope.contratos);
			$scope.contratoHabiles = $scope.contratos.filter(contrato => contrato.envioRecordatorioActivo) || [];
			$scope.toggleAllAction = $scope.contratoHabiles.find(contrato => !contrato.seleccionado) ? $scope.toggleAllActions.SELECT_ALL : $scope.toggleAllActions.DESELECT_ALL;
		};
		
		$scope.init = ()=> {
			$scope.setContratos(contratosCiaPendientes);
		};
		
		$scope.init();
	})
	.controller('misVentasCtrl', function($scope,$state,misVentas,$ionicNavBarDelegate, proformaService, me, USUARIO){
		$ionicNavBarDelegate.title("Mis Ventas");
		$scope.misVentas = misVentas;
		$scope.me = me;
		$scope.tipoVenderor = USUARIO.TIPO;
		angular.forEach($scope.misVentas,function(proforma){
			proforma.mMensjae = proforma.contratoNumero
		});
		$scope.regresar = function(){
		    $state.go("app.pagoCia");
		};
	})
	.controller('misSeparacionesCtrl', function($scope,misSeparaciones,$ionicNavBarDelegate, proformaService, me, USUARIO){
		$ionicNavBarDelegate.title("Mis Separaciones");
		$scope.me = me;
		$scope.tipoVenderor = USUARIO.TIPO;
		$scope.misSeparaciones = misSeparaciones;
		angular.forEach($scope.misSeparaciones,function(proforma){
			proforma.mMensjae = proforma.proformaID
		});
	})
	 /*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
	.controller('EditProfileCtrl',function($scope,$rootScope,$http,$ionicPopup,profileService,loginService,validacionService,me,config,telefonos){
		$scope.me = me;
		$scope.config = config;
		$scope.telefonos = telefonos;
		$scope.validacionService = validacionService;
		$scope.telefonosSonValidos = function(){			
			var t = $scope.telefonos.find(function(tel){
				return !$scope.validacionService.validarTelefono(tel.numero);
			})
			if(t){
				return {value: t, message: t.descripcion+" debe tener 9 dígitos."}
			}
			var l = $scope.telefonos.reduce((m1,m2) => {
				return (m1.numero == "" && m2.numero == "");
			})
			if(l){
				return {value: {descripcion:"Alerta"}, message: "Debe digitar por lo menos un número celular"}
			}
			return true;
		}
		$scope.cambiarImagenDeUsuario = function(){
			if(filechooser){
				filechooser.open(
					{}, 
					function(uri){
						console.log(uri);
						/* INICIO - VTADIG2-232 - JOHN VELASQUEZ */
						plugins.crop.promise(uri.url, {default: 100})
						.then(function success (newPath) {
							console.log(newPath)
							$http.get(newPath,{responseType: "blob"}).then(function(data){
								var blob = data.data;	
								if(!blob){
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se pudo localizar el archivo, o está dañado." , okType: 'button-assertive'});
									return;	
								}							
								var url = uri.url;
								var filename = url.substring(url.lastIndexOf("/")+1,url.length);
								/*VALIDAR FORMATO*/
								var formato = filename.substring(filename.lastIndexOf(".")+1,filename.length).toLowerCase();
								var formatos = $scope.config.formatosImagen.toLowerCase().split(",");
								if(formatos.indexOf(formato)<0){
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Solo se permiten archivos en formato: "+formatos.join(",") , okType: 'button-assertive'});
									return;
								}
								/*VALIDAR TAMAÑO*/
								var tamanoKB = blob.size/1024;
								var tamanoMaximo = $scope.config.maximoTamanoImagen;
								if(tamanoKB>tamanoMaximo){
									var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El tamaño del archivo debe ser menor a "+tamanoMaximo/1024 +"MB.", okType: 'button-assertive'});
									return;
								}
	
								blobToBase64(data.data,function(base64){
									profileService.changeAvatarFromBase64(filename, base64).then(function (response) {
										me.imagenUsuario = response.data;
										var alertPopup = $ionicPopup.alert({ title: 'Perfil', template: response.message , okType: 'button-assertive'});									
									})
								});									
							});
						})
						.catch(function fail (err) {

						})
						/* FIN	  - VTADIG2-232 - JOHN VELASQUEZ */
						
					}, 
					function(msg){
						console.log(msg);							
					});						
			}
		}
		$scope.guardar = function(){
			if((reason = $scope.telefonosSonValidos())!==true){
				var alertPopup = $ionicPopup.alert({ title: reason.value.descripcion, template: reason.message , okType: 'button-assertive'});
				return;
			}
			profileService.updatePhones($scope.telefonos).then(function(){
				loginService.me().then(function (data){
					$rootScope.me = data;
					var alertPopup = $ionicPopup.alert({ title: 'Perfil', template: "Se actualizó la información." , okType: 'button-assertive'});
				});
			})
		}
	})
	/*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
	.controller('misIncidenciasCtrl',function($scope, $state, $ionicHistory, incidenciaService, INCIDENCIA, TIPO_BUSQUEDA_INCIDENCIAS, STRINGS, generadorDeMapas,incidencias){
		$scope.incidencias = angular.copy(incidencias);		
		var INCIDENCIA_ESTADO = INCIDENCIA.ESTADO;
		$scope.filtros = [
			{codigo: INCIDENCIA_ESTADO.EN_PROGRESO, label: STRINGS.INCIDENCIA.ESTADO.EN_PROGRESO, mostrar: true},
			{codigo: INCIDENCIA_ESTADO.PENDIENES, label: STRINGS.INCIDENCIA.ESTADO.PENDIENTE, mostrar:true},
			{codigo: INCIDENCIA_ESTADO.RESUELTAS, label: STRINGS.INCIDENCIA.ESTADO.RESUELTO, mostrar: true}
		]

		$scope.filtro = $scope.filtros[1];
		$scope.filtroDefault = {};
		
		$scope.bucarIncidenciasPorEstado = function(estadoId){
			//Tipo de busqueda: TIPO_BUSQUEDA_INCIDENCIAS.ESTADO (PENDIENTE)
			incidenciaService.bucarIncidencias(estadoId).then(function(incidencias){
				$scope.incidencias = incidencias;
			})
		}
		$scope.irDetalleIncidencia = function(indicencia){
			$state.go('app.incidenciaDetalle',{incidenciaID: indicencia.incidenciaID});
		}
		
		$scope.init=function(){		
			$ionicHistory.clearHistory();		
			/**watches */
			$scope.$watch('filtro.codigo',function(newVal, oldVal){
				//$scope.cambiarFiltro(newVal);
				console.log(newVal)
			})
			$scope.mapFiltros = generadorDeMapas.generarMapaDesdeArreglo($scope.filtros,'codigo');
			//$scope.filtroDefault = $scope.mapFiltros[INCIDENCIA_ESTADO.PENDIENES].codigo
			//$scope.filtro.codigo = $scope.filtroDefault;
			//$scope.bucarIncidenciasPorEstado($scope.filtro.codigo);
		}
		$scope.init();
	})
	.controller('nuevaIncidenciaCtrl',function($scope, $ionicPopup, $sessionStorage, usuarioService, incidenciaService, lstFuerzaDeVenta, lstPuntoDeExhibicion, lstIncidenciaTipoCategoria,CONFIGURACION_APP, USUARIO,$state){
		$scope.imagenesMaximas = CONFIGURACION_APP.INCIDENCIAS.MAXIMO_IMAGENES_ADJUNTAS	
		$scope.puntosDeVentas = lstPuntoDeExhibicion;
		$scope.tiposCategoria = lstIncidenciaTipoCategoria;
		$scope.fuerzasDeVenta = lstFuerzaDeVenta;
		$scope.tipoVenderor = USUARIO.TIPO;
		$scope.categorias = [];
		$scope.mensajeMaxLength = 700;
		$scope.incidencia = {			
			puntoVentaID: undefined,
			categoriaID: undefined,
			asunto: undefined,
			mensaje: undefined
		};		

		//PARA QUE APARESCA EL COMBO
		/*$scope.esVendedor = function(){
			return usuarioService.esVendedor();
		}*/

		$scope.cambiarFuerzaDeVenta = function(fuerzaDeVenta){		
			if(fuerzaDeVenta){
				$scope.incidencia.fuerzaDeVentaID = fuerzaDeVenta.personaID;
			}				
		}
		$scope.cambiarPuntoVenta = function(puntoDeVenta){
			if(puntoDeVenta){
				$scope.incidencia.puntoVentaID = puntoDeVenta.codigo;
			}
			//console.log('$scope.incidencia.putoDeVentaId: '+$scope.incidencia.puntoVentaId);
		}
		$scope.limpiarCategoria = function(){
			$scope.incidencia.categoria = undefined
			$scope.incidencia.categoriaID = undefined;
		}
		$scope.cambiarTipoCategoria = function (tipoCategoria) {
			$scope.limpiarCategoria();
			if(tipoCategoria){
				$scope.incidencia.tipoCategoriaID = tipoCategoria.codigo;
				incidenciaService.getCategoria($scope.incidencia.tipoCategoriaID).then(function(data){
					$scope.categorias = data;
				});
				//console.log('$scope.incidencia.tipoCategoriaID: '+$scope.incidencia.tipoCategoriaId);
			}			
		}
		$scope.cambiarCategoria = function(categoria){
			if(categoria){
				$scope.incidencia.categoriaID = categoria.categoriaId;
				//console.log('$scope.incidencia.tipoCategoriaId: '+$scope.incidencia.CategoriaId);
			}			
		}
		$scope.enviarIncidencia = function(form){
			var validacion = incidenciaService.validarIncidencia($scope.incidencia,form);
			if(validacion.valido===true){
				incidenciaService.enviarIncidencia($scope.incidencia).then(function(){
					 var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se envió exitosamente." , okType: 'button-assertive'});
					 alertPopup.then(function(res) {
						if(res) {
							$state.go('app.misIncidencias');
						 }
					  });
			
				});
			}else{
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: validacion.mensaje , okType: 'button-assertive'});
			}
		}
		$scope.init = function(){
			/*WATCHERS*/
			$scope.$watch('incidencia.fuerzaDeVenta',function(newVal, oldVal){
				$scope.cambiarFuerzaDeVenta(newVal);
			});

			$scope.$watch('incidencia.puntoVenta',function(newVal, oldVal){
				$scope.cambiarPuntoVenta(newVal);
			});
	
			$scope.$watch('incidencia.tipoCategoria',function(newVal, oldVal){
				$scope.cambiarTipoCategoria(newVal);
			});
	
			$scope.$watch('incidencia.categoria',function(newVal, oldVal){
				$scope.cambiarCategoria(newVal);
			});
			/*Defaults values*/
			if($sessionStorage.defaults){
				if($sessionStorage.defaults.puntoExhibicion){
					$scope.incidencia.puntoVenta = $sessionStorage.defaults.puntoExhibicion;
					$scope.incidencia.puntoVentaID = $sessionStorage.defaults.puntoExhibicion.codigo;
				}
				//PARA QUE APARESCA EL COMBO
				/*if($scope.esVendedor()===true){*/
					$scope.incidencia.fuerzaDeVentaID = $scope.me.vendedor.personaID;
					$scope.incidencia.fuerzaDeVentaNombre = $scope.me.vendedor.personNombreCompleto;
				/*}*/
			}
		}
		$scope.init();
	})
	.controller('incidenciaDetalleCtrl',function($state,$ionicPopup,$scope,$state,incidencia, usuarioService,INCIDENCIA,STRINGS,incidenciaService){
		var INCIDENCIA_ESTADO = INCIDENCIA.ESTADO;
		$scope.estados = [
			{codigo: INCIDENCIA_ESTADO.EN_PROGRESO, label: STRINGS.INCIDENCIA.ESTADO.EN_PROGRESO, mostrar: true},
			{codigo: INCIDENCIA_ESTADO.PENDIENES, label: STRINGS.INCIDENCIA.ESTADO.PENDIENTE, mostrar:true},
			{codigo: INCIDENCIA_ESTADO.RESUELTAS, label: STRINGS.INCIDENCIA.ESTADO.RESUELTO, mostrar: true}
		]
		$scope.incidencia = incidencia;
		$scope.incidencia.estado = ($scope.estados.filter(estado => estado.codigo == $scope.incidencia.estadoID))[0];
		$scope.puedeEditarEstado = function(){
			return usuarioService.esDeMarketing();
		}

		$scope.irChat = function(chatId){
			$state.go('app.incidenciaChat',{incidenciaChatId: $scope.incidencia.incidenciaID})
		};
		$scope.actualizarEstado = function(){
			incidenciaService.actualizarIncidencia($scope.incidencia.incidenciaID,$scope.incidencia.estado.codigo).then(function(){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se actualizó exitosamente." , okType: 'button-assertive'});
				alertPopup.then(function(res) {
				   if(res) {
					   $state.go('app.misIncidencias');
					}
				 });
	   
		   });
		};
		$scope.init = function(){		
			
		}
		$scope.init();
	})
	/*INICIO JOHN VELASQUEZ SEGUIMIENTO EDV*/
	.controller('seguimientoEdvCtrl',function($scope,$state,$ionicPopup,seguimientoEdvService,$filter){
		var dateFilter = $filter('date');
		$scope.filtro = {};
		$scope.inputsFiltro = [
			{
				nombre: "Fecha Desde",
				label: "Desde",
				tipo: "date",
				valor: undefined,
				valorMaximo: new Date(),
				placeholder: "dd/MM/yyyy",
				validar: function(){
					console.log(dateFilter(this.valor, 'dd/MM/yyyy'));
					this.error = undefined;
					if(!this.valor){
						this.error = {mensaje: this.nombre+" es necesaria"};
					}else if($scope.filtro.fechaFin&&this.valor>$scope.filtro.fechaFin){
						this.error = {mensaje: "La " + this.nombre+" debe ser menor a la fecha Hasta."};
					}
					return this;
				},
				eventoCambio: function(){
					if(!this.validar().error){
						$scope.filtro.fechaInicio = this.valor;
					}
				},
				error: undefined//{mensaje: text}
			},
			{
				nombre: "Fecha Hasta",
				label: "Hasta",
				tipo: "date",
				valor: undefined,
				valorMaximo: new Date(),
				placeholder: "dd/MM/yyyy",
				validar: function(){
					this.error = undefined;
					if(!this.valor){
						this.error = {mensaje: this.nombre+" es necesaria"};
					}else if($scope.filtro.fechaInicio&&this.valor<$scope.filtro.fechaInicio){
						this.error = {mensaje: "La " + this.nombre+" debe ser mayor a la fecha Desde."};
					}
					return this;
				},
				eventoCambio: function(){
					if(!this.validar().error){
						$scope.filtro.fechaFin = this.valor;
					}
				},
				error: undefined//{mensaje: text}
			}
		];

		$scope.validarFiltros = function(){
			var valido = true;
			$scope.inputsFiltro.forEach(f => {
				if((error = f.validar().error)&&valido){
					var alertPopup = $ionicPopup.alert({ title: "Alerta", template: error.mensaje , okType: 'button-assertive'});
					valido = false;
				}
			})
			return valido;
		};
		
		$scope.enviar = function(){
			if($scope.validarFiltros()){
				seguimientoEdvService.enviarSeguimientoPorCorreo({fechaInicio: dateFormat($scope.filtro.fechaInicio), fechaFin: dateFormat($scope.filtro.fechaFin)})
				.then(function(data){
					var alertPopup = $ionicPopup.alert({ title: "Envío exitoso", template: 'Los reportes fueron enviados a su correo electrónico.' , okType: 'button-assertive'});
					alertPopup.then(r=>$state.go('app.profile'));
				});
			}
		}
	})
	/*INICIO JOHN VELASQUEZ SEGUIMIENTO EDV*/
	/**
	 * @feature V4-HU016
	 */
	.controller('estadisticasRematesCtrl',function($scope,estadisticasAsmbleaRematesService, programaProducto){
		$scope.productos = programaProducto;
		$scope.datasetcolors = ['#1F458E','#AFDDDF','#5587A5','#F2FAEF','#BA2F2C'];
		$scope.tiempoCampo={
			meses: {
				valor: "m",
				descripcion: "meses"
			},
			anos: {
				valor: "a",
				descripcion: "años"
			}
		};
		$scope.optionesUltimos = [
			{cantidad: "3", campo: $scope.tiempoCampo.meses},
			{cantidad: "6", campo: $scope.tiempoCampo.meses},
			{cantidad: "9", campo: $scope.tiempoCampo.meses},
			{cantidad: "12", campo: $scope.tiempoCampo.meses},
			//{cantidad: "20", campo: $scope.tiempoCampo.anos}
		];
		$scope.formData = {};
		$scope.data = null;
		$scope.mainChart = null

		$scope.init = () => {
			$scope.configMainChart();
			if ($scope.productos && $scope.productos.length > 0) {
                $scope.formData.producto = $scope.productos[0];
            }
			$scope.cargar();
		};
		
		$scope.configMainChart = () => {
			var ctx = document.getElementById('mainChart').getContext('2d');
			$scope.mainChart = new Chart(ctx, {
				type: 'line',
				options: {
					plugins: {
						zoom: {
							// Container for pan options
							pan: {
								// Boolean to enable panning
								enabled: true,

								// Panning directions. Remove the appropriate direction to disable
								// Eg. 'y' would only allow panning in the y direction
								mode: 'xy',

								rangeMin: {
									// Format of min pan range depends on scale type
									x: null,
									y: null
								},
								rangeMax: {
									// Format of max pan range depends on scale type
									x: null,
									y: null
								},

								// Function called while the user is panning
								onPan: function({chart}) { console.log(`I'm panning!!!`); },
								// Function called once panning is completed
								onPanComplete: function({chart}) { console.log(`I was panned!!!`); }
							},

							// Container for zoom options
							zoom: {
								// Boolean to enable zooming
								enabled: true,

								// Enable drag-to-zoom behavior
								drag: true,

								// Drag-to-zoom rectangle style can be customized
								// drag: {
								// 	 borderColor: 'rgba(225,225,225,0.3)'
								// 	 borderWidth: 5,
								// 	 backgroundColor: 'rgb(225,225,225)'
								// },

								// Zooming directions. Remove the appropriate direction to disable
								// Eg. 'y' would only allow zooming in the y direction
								mode: 'xy',

								rangeMin: {
									// Format of min zoom range depends on scale type
									x: null,
									y: null
								},
								rangeMax: {
									// Format of max zoom range depends on scale type
									x: null,
									y: null
								},
								speed: 0.1,
								onZoom: function({chart}) { console.log(`I'm zooming!!!`); },
								// Function called once zooming is completed
								onZoomComplete: function({chart}) { console.log(`I was zoomed!!!`); }
							}
						}
					},
					showAllTooltips: true,
					//responsive: false,
					aspectRatio: 9,
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Asamblea',
								fontStyle: "bold"
							},
							ticks: {
								beginAtZero: true
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Remates',
								fontStyle: "bold"
							},
							ticks: {
								beginAtZero: true
							}
						}]
					},
					legend: {
						display: false,
						position: 'top',
						onClick: () => {},
						fullWidth: true,
						labels: {
							usePointStyle: true,
							boxWidth: 15,
							filter: function(legendItem, data) {
								return legendItem.text 
							}
						}
					},
					tooltips: {
						position: 'custom',
						xAlign: 'center',
						//yAlign: 'bottom',
						displayColors: false,
						enabled: false,
						backgroundColor:"rgba(0,0,0,0)",
						cornerRadius: 10,
						filter: function (tooltipItem) {
							return tooltipItem.yLabel > 0;
						},
						callbacks: {
							title: function(tooltipItems, data) {
								return "";
							},
							label: function(tooltipItem, data) {
								let datasetLabel = "";
								let label = data.labels[tooltipItem.index];
								let dataItem = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								return typeof dataItem === "object" ? dataItem.y : dataItem
							},
							labelTextColor: (tooltipItem, chart) => {return "#2d2d2d"},
						}
					},
					layout: {
						padding: {
							left: 10,
							right: 10,
							top: 30,
							bottom: 10
						}
					}
				}
			});
			console.log("main chart created");
		};
		$scope.control = {};
		$scope.control.noconsultado = true;
		$scope.cargar = (opcion) => {
			//console.log(`cargando los últimos ${opcion.cantidad} ${opcion.campo.descripcion}`);
			//estadisticasAsmbleaRematesService.promedioUltimos(`${opcion.cantidad}${opcion.campo.valor}`)
			estadisticasAsmbleaRematesService.promedioUltimos()
				.then((data) => {
					$scope.data = data;
					$scope.control.noconsultado = false;
					//$scope.mainChart.data = $scope.data.data;
					if ($scope.productos && $scope.productos.length > 0) {
						$scope.formData.producto = $scope.formData.producto || $scope.productos[0];
						$scope.mostrarProducto($scope.formData.producto);
					}
				}, (error) => {
					console.log(error);
					$scope.data = null;
				});
		};
		
		$scope.mostrarProducto = (producto) => {
			if($scope.data.data.datasets.length <= 0) {
				return;
			}
			let datasets = $scope.data.data.datasets.filter(ds => ds.group === producto.productoNombre);
			if(datasets && datasets.length > 0) {
				$scope.mainChart.data.datasets = datasets.map((dataset, index) => {
					dataset.borderColor = $scope.datasetcolors[index];
					dataset.backgroundColor = 'rgba(0,0,0,0)';
					return dataset;
				});
				$scope.mainChart.data.labels = datasets[0].xAxisLabels;
			} else {
				$scope.mainChart.data.datasets = [{data: (new Array($scope.data.data.labels.length)).fill(0)}];
				$scope.mainChart.data.labels = $scope.data.data.labels;
			}
			$scope.mainChart.update();
			$scope.mainChart.update();
		};

		$scope.init();
	})
	.controller('incidenciaChatCtrl',function($scope, mensajes,$stateParams){
		$scope.chat = {};
		$scope.mensajeMaxLength = 255;
		$scope.chat.mensajes = mensajes;
		console.log($stateParams.incidenciaChatId);
		$scope.chat.chatID = parseInt($stateParams.incidenciaChatId);
	})
	.controller('noConnectionCtrl',function($scope,$window,$state){
		$scope.actualizar = function() {
			$state.go('auth.walkthrough');
			//$window.location.reload();
		}
	})
	.controller('versionNotSupportedCtrl',function($scope,$sessionStorage,$window,$state){
		$scope.mensaje = $sessionStorage.ultimoError.message;
	})
	//INICIO VTADIG2-18 John Velasquez 03-10-2017
	.controller('documentoContratoCtrl',function($scope,$sessionStorage,$state, $stateParams,documentoContrato,LST_PERSONA_TIPO,LST_TIPO_DOCUMENTO,LST_TIPO_ASOCIADO,DOCUMENTOS_CONTRATO){
		$scope.documentoContrato = documentoContrato;
		$scope.personaTipo = LST_PERSONA_TIPO;
		$scope.documentoTipo = LST_TIPO_DOCUMENTO;
		$scope.asociadoTipo = LST_TIPO_ASOCIADO;		
		$scope.estadoDocumentos = DOCUMENTOS_CONTRATO.LST_ESTADOS;
		$scope.estadoDocumento = DOCUMENTOS_CONTRATO.ESTADOS;
		
		$scope.irPersona = function(personaId){
			$state.go('app.documentoContratoPersona',{contratoId: $stateParams.contratoId, personaId: personaId});
		}

		$scope.init = function(){
			$scope.documentoContrato.listaAsociados.forEach(function(asociado){
				asociado.tipoPersonaLabel = $scope.personaTipo.find(function(tipo){return tipo.codigo === asociado.tipoPersonaId}).label;
				asociado.tipoDocumentoLabel = $scope.documentoTipo.find(function(tipo){return parseInt(tipo.codigo) === parseInt(asociado.tipoDocumento)}).label;
				asociado.tipoAsociadoLabel = $scope.asociadoTipo.find(function(tipo){return tipo.codigo === asociado.tipoTitularId}).label;
				asociado.documentoEstadoLabel = $scope.estadoDocumentos.find(function(tipo){return parseInt(tipo.codigo) === parseInt(asociado.estadoDocumento)}).label;				
			});
		}

		$scope.init();
	})
	.controller('documentoContratoPersonaCtrl',function($scope,$sessionStorage,$stateParams, $http, $ionicPopup, contratoService, documentoContratoPersona, configuracionArchivos,LST_PERSONA_TIPO,LST_TIPO_DOCUMENTO,LST_TIPO_ASOCIADO,DOCUMENTO_CONTRATO,$ionicLoading){
		$scope.documentoContratoPersona = documentoContratoPersona;
		$scope.lstPersonaTipo = LST_PERSONA_TIPO;
		$scope.lstDocumentoTipo = LST_TIPO_DOCUMENTO;
		$scope.tipoAsociado = LST_TIPO_ASOCIADO;
		$scope.lstEstadoDocumento = DOCUMENTO_CONTRATO.LST_ESTADOS;
		$scope.estadoDocumento = DOCUMENTO_CONTRATO.ESTADO;
		$scope.configuracionArchivos = configuracionArchivos;

		$scope.init = function(){
			$scope.documentoContratoPersona.tipoPersonaLabel = $scope.lstPersonaTipo.find(function(tipo){return tipo.codigo === $scope.documentoContratoPersona.personaTipoID}).label;
			$scope.documentoContratoPersona.tipoDocumentoLabel = $scope.lstDocumentoTipo.find(function(tipo){return parseInt(tipo.codigo) === parseInt($scope.documentoContratoPersona.tipoDocumentoID)}).label;
			
			$scope.documentoContratoPersona.lstDocument.forEach(function(documento){
				documento.estadoDocumentoLabel = $scope.lstEstadoDocumento.find(function(tipo){return parseInt(tipo.codigo) === parseInt(documento.estadoDocumento)}).label;
				if(!documento.lstArchivo){
					documento.lstArchivo = [];
				}
				documento.edicionActiva = function(){
					return documento.estadoDocumento === $scope.estadoDocumento.NO_ENVIADO || documento.estadoDocumento === $scope.estadoDocumento.RECHAZADO;
				}
				documento.enviar = function(){					
					if(documento.lstArchivo){
						if(documento.lstArchivo.length>0){
							var confirmPopup = $ionicPopup.confirm({
								title: 'Cancelar',
								template: '¿Está seguro de enviar este documento?',
								cancelText: 'Cancelar',
								okText: 'Aceptar'
							});
				
							confirmPopup
							.then(function(res) {
								if(res){
									contratoService.enviarDocumento(documento.documentoContratoId).then(function(data){
										documento.estadoDocumento = $scope.estadoDocumento.ENVIADO;
										documento.estadoDocumentoLabel = $scope.lstEstadoDocumento.find(function(tipo){return parseInt(tipo.codigo) === parseInt(documento.estadoDocumento)}).label;
										documento.desplegado = true;
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El documento ha sido enviado." , okType: 'button-assertive'});
									});
								}								
							});								
						}else{
							var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No hay nada que enviar." , okType: 'button-assertive'});
						}
					}else{
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No hay nada que enviar." , okType: 'button-assertive'});
					}
				}
				documento.removerArchivo = function(indice){
					var archivo = documento.lstArchivo[indice];
					if(archivo){
						var confirmPopup = $ionicPopup.confirm({
							title: 'Cancelar',
							template: '¿Está seguro de eliminar este archivo?',
							cancelText: 'Cancelar',
							okText: 'Aceptar'
						});
			
						confirmPopup
						.then(function(res) {
							if(res) {
								contratoService.eliminarArchivo($stateParams.contratoId,$stateParams.personaId,archivo.archivoId)
								.then(function(data){
									documento.lstArchivo.splice(indice,1);
								});	
							}
						});							
					}									
				}
				documento.descargarArchivo = function(indice){
					var archivo = documento.lstArchivo[indice];
					if(archivo){
						contratoService.descargarArchivo($stateParams.contratoId,$stateParams.personaId,archivo)
						.then(function(data){
							
						});	
					}									
				}
				documento.validarArchivo = function(archivo){
					var validacion = {
						archivoValido: true,
						mensaje: "Archivo válido"
					}
					if(!archivo){
						validacion.archivoValido = false;
						validacion.mensaje = "No hay archivo";
					}
					var tamanioKB = archivo.size/1024;
					var minTamanio = $scope.configuracionArchivos.minTamanio;
					var maxTamanio = $scope.configuracionArchivos.maxTamanio;
					var formatosValidos = $scope.configuracionArchivos.listaTipoArchivo;
					formatosValidos.forEach(function(formato,index){
						formatosValidos[index] = formato.toLowerCase();
					})
					var formato = archivo.name.substring(archivo.name.lastIndexOf(".")+1,archivo.name.length);
					if(tamanioKB>maxTamanio){
						validacion.archivoValido = false;
						validacion.mensaje = "El archivo debe tener un tamaño máximo de "+maxTamanio/1024+"MB";
					}
					if(formatosValidos.indexOf(formato.toLowerCase())<0){
						formatosValidos = formatosValidos.filter(function(el) {
							return el !== 'mp3' && el !== 'p7b';
						});
						validacion.archivoValido = false;
						validacion.mensaje = "Solo se permiten archivos en formato: "+formatosValidos.join(",");
					}
					return validacion;
				}
				documento.agregarArchivo = function(){					
					if(filechooser){
						filechooser.open(
							{}, 
							function(uri){
								console.log(uri);
								$http.get(uri.url,{responseType: "blob"}).then(function(data){	
									if(!data.data){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "No se pudo localizar el archivo, o no tiene data." , okType: 'button-assertive'});
										return;	
									}
									var url = uri.url;
									var filename = url.substring(url.lastIndexOf("/")+1,url.length);
									var format = url.substring(url.lastIndexOf(".")+1,url.length);
									//var file = new File([data.data], filename);			
									//var file = data.data;
									file = data.data;									
									//var file = new File (filename, url, data.type, new Date(), data.size);
									file.lastModifiedDate = new Date();    								
									file.name = filename;	
									var validacion = documento.validarArchivo(file);
									if(!validacion.archivoValido){
										var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: validacion.mensaje , okType: 'button-assertive'});
										return;
									}else{
										contratoService.agregarArchivo($stateParams.contratoId,$stateParams.personaId,DOCUMENTO_CONTRATO.TIPO.NO_ADICIONAL, documento.documentoContratoId,documento.documentoId,file).then(
											function(data){
												console.log(data)
												documento.lstArchivo.push(data);											
											}
										);
									}									
								});
							}, 
							function(msg){
								console.log(msg);							
							});						
					}
				}
			});			
		}
		$scope.init();
	})
	//FIN VTADIG2-18 John Velasquez 03-10-2017
	//INIT GBENITES - VD2-RQ30 - 07/08/2018
    /**
     * @issue JIRAV3-002
     *  - inject $stateParams
     */
	.controller('detalleRestriccionCtrl', function ($scope, $state, $stateParams, detalleEventoContrato, documentosSinAprobacion, requisitosPendientes, observaciones) {
		var vm = this;
        /* inicio @issue JIRAV3-002 */
		$scope.contratoId =  $stateParams.contratoID;
		/* fin @issue JIRAV3-002 */

		$scope.detalleEventoContrato = detalleEventoContrato;
		$scope.documentosSinAprobacion = documentosSinAprobacion;
		$scope.requisitosPendientes = requisitosPendientes;
		$scope.observaciones = observaciones;

		//funcion para filtrar por nombre de asociado en requisitos pendientes y documentos sin aprobacion
		$scope.obtenerPersonaPorId = function (arr, keyname) {
			var personaConID = [];
			var nombrePersona;
			for (x in arr) {
				if (arr[x].personaNombreCompleto != null && arr[x].personaNombreCompleto != nombrePersona) {
					var miobj = new Object();

					miobj.personaNombreCompleto = arr[x].personaNombreCompleto;
					// Documentos del Asociado
					miobj.asociadoId = arr[x].asociadoId;
					miobj.asociadoTipoId = arr[x].asociadoTipoId;
					miobj.asociadoTipoID = arr[x].asociadoTipoID;
					miobj.asociadoNumero = arr[x].asociadoNumero;
					miobj.documentoNombre = arr[x].documentoNombre;
					// Para Requisitos Eva. Pendiente
					miobj.personaID = arr[x].personaID;
					miobj.estadoRequisito = arr[x].estadoRequisito;
					miobj.requisitoNombre = arr[x].requisitoNombre;
					if(!!arr[x].asociadoNumero) {
						personaConID[arr[x].asociadoNumero] = miobj;
					}
					personaConID.push(miobj);
				}
				nombrePersona = arr[x].personaNombreCompleto;
			}
		    var output = [], 
		    keys = [];
		      
		    // we utilize angular's foreach function
		    // this takes in our original collection and an iterator function
		    angular.forEach(personaConID, function(item) {
		        // we check to see whether our object exists
		        var key = item[keyname];
		        // if it's not already part of our keys array
		        if(keys.indexOf(key) === -1) {
		            // add it to our keys array
		            keys.push(key); 
		            // push this item to our final output array
		            output.push(item);
		        }

		    });
		      // return our array which should be devoid of
		      // any duplicates
		      return output;

		}

		//Documentos del titular o respaldo
		$scope.documentoTitularORespaldo = $scope.obtenerPersonaPorId(documentosSinAprobacion,'asociadoId')
		//Persona en RequisitosEvalPendiente
		$scope.personaReqEvalPendiente = $scope.obtenerPersonaPorId(requisitosPendientes, 'personaID')

		var documentoContrato = [];
		var documentoPersona = [];

		angular.forEach($scope.documentosSinAprobacion, function (value, key) {
			if (value.personaNombreCompleto == null) {
				documentoContrato.push(value);
			} else {
				documentoPersona.push(value);
			}
		})

		$scope.documentosContrato = documentoContrato;
		$scope.documentosPersona = documentoPersona;

		/*Texto Documentos del titular o Respaldo  */
		$scope.mostrarTitularOrespaldo = function (id) {
			var result = '';

			if(id === 'T') {
				result = ' - TITULAR'
			} else if (id === 'R') {
				result = ' - RESPALDO'
			}
			return result;
		}

		/*Requisitos Eval Pendiente condicionales */
		$scope.mostrarEvalPendiente = function (id) {
			return (id === 'Edad del Titular (>=18 y <=70 años)' || id === "Ingresos Netos Mensuales (4/1)" ||
				id === 'Llamada de Bienvenida' || id === 'Firma conforme a documentos' || id === 'Multa Electoral');
		}

		/*Requisitos Eval Pendiente reemplaza nombre requisito */
		$scope.mostrarTipoObservacion = function (obs) {
			resultTipoObservacion = null;
			if (obs == 'ASIST_INGVTA') {
				var resultTipoObservacion = "Asistente ing. ventas : ";
			} else if (obs == 'ASIST_LEGAL') {
				resultTipoObservacion = "Asistente Legal : ";
			} else if (obs == 'LLAMADA_BIENV') {
				resultTipoObservacion = "Llamada de Bienvenida : ";
			} else if (obs == 'REV_INGVTA') {
				resultTipoObservacion = "Revisión ingreso de venta : ";
			}
			return resultTipoObservacion;
		}

		/*Observaciones condicionales*/
		$scope.mostrarObservaciones = function () {
			return (detalleEventoContrato[0].eventoID > 3 && observaciones != null);
		}

		$scope.regresar = function () {
			$state.go('app.misVentas');
		}

	})
//FIN GBENITES - VD2-RQ30 - 07/08/2018

// INICIO -@feature/MasterCard  
.controller('registrarPagoMastercardObligacionCtrl', function($scope,$state,$rootScope,$stateParams,$ionicPopup,validacionService,obligacionesPorPagarService,pagoMastercardService,REGISTRO_TRANSACCION_MASTERCARD){
	var vm=this;
	var montoPago='';
	var obligacionId =  $stateParams.obligacionID;
	$scope.validacionService = validacionService;
	var vistaIdInit=$stateParams.vistaID;
	var initOrigen=$stateParams.origenObligacionID;
	var fecha=new Date();
	$scope.fechaActual=fecha;
	 var date = new Date(),d = date.getDate(), y = date.getFullYear(), m = date.getMonth(),h= date.getHours(),min = date.getMinutes(),s = date.getSeconds();
	 var fechaTransaccion = new Date(y, m, d,h,min,s);
	 $scope.transacion = {};
	 $scope.transacion.fecha=fechaTransaccion

	 let  _mes = (m + 1).toString();
	 let ms = checkZero(_mes);
	 let _dia=(d-1).toString()
	 let dia = checkZero(_dia);
	 
	 function checkZero(data){
		if(data.length == 1){
		  data = "0" + data;
		}
		return data;
	  }
	 $scope.fechaminima= y +'-'+ ms +'-'+ dia; 
	 // fecha maxima
	 document.getElementById('dt').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
	
	 obligacionesPorPagarService.getDetalleObligacion(obligacionId)
	.then(function(data){
		montoPago=data['importePago'];
		$scope.montoPagoObligacion=montoPago;
		$scope.prospectoID=data['prospectoID'];
	}
	,function(reason){console.log(reason);});
	$scope.regresar=function(){
		switch (parseInt(initOrigen)) {
			case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_INGRESO_VENTAS:
			  $state.go('app.mensajeConfirmacionContrato');
			  break;
			case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_PAGO_CIA_Y_ACUENTA: 
			  $state.go('app.mensajeConfirmacionObligacion',{obligacionID:$stateParams.obligacionID,origenID: $stateParams.vistaID});
			  break;
			case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_OBLIGACION_POR_PAGAR: 
			   $state.go('app.detalleObligacion',{obligacionPagoID: $stateParams.obligacionID});
			  break; 
			default:
			  console.log('default');
		  }
	}

	$scope.guardar=function(){
		var id=this.transacion.id;
		var fecha=this.transacion.fecha;
		console.log("fecha",this.transacion.fecha);
		if(id==undefined || id==""){
			$ionicPopup.alert({ title: 'Alerta', template: "ID de Transacción es obligatorio." , okType: 'button-assertive'});
			   return false;
		   }
		if(id.length!=15){
		 $ionicPopup.alert({ title: 'Alerta', template: "El ID de Transacción  debe tener 15 dígitos." , okType: 'button-assertive'});
			return false;
		}
		if(fecha==undefined){
			$ionicPopup.alert({ title: 'Alerta', template: "Fecha de transacción es obligatoria." , okType: 'button-assertive'});
			   return false;
		   }


		let _fecha= new Date(fecha);
		var day = _fecha.getDate() + "";
		var month = (_fecha.getMonth() + 1) + "";
		var year = _fecha.getFullYear() + "";
		var hour = _fecha.getHours() + "";
		var minutes = _fecha.getMinutes() + "";
		var seconds = _fecha.getSeconds() + "";
		
		day = checkZero(day);
		month = checkZero(month);
		year = checkZero(year);
		hour = checkZero(hour);
		mintues = checkZero(minutes);
		seconds = checkZero(seconds);
		 var fechatransaccion=day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds
		function checkZero(data){
		  if(data.length == 1){
			data = "0" + data;
		  }
		  return data;
		}

		var data={
			"obligacionPagoID": parseInt(obligacionId),
	         "transaccionPOSID" : id,
	         "fechaTransaccionPOS" :fechatransaccion
		}

		var confirmPopup = $ionicPopup.confirm({
			title: 'Registro de Transacción',
			cssClass: '#000',
			template: 'Continúe solo si está seguro de que los datos ingresados son correctos.',
			cancelText: 'Cancelar',
			okText: 'Continuar'
		});
		confirmPopup
		.then(function(res) {
			if(res) {
				pagoMastercardService.guardarPagoMastercard(data)
				.then(function(response){
					if(response.data && response.code==0){
						var alertPopup = $ionicPopup.alert({ title: 'Mensaje', template: "Se ha registrado la transacción con éxito." ,okText: 'Aceptar', okType: 'button-assertive'});	
						alertPopup.then(function(res){
							switch (parseInt(initOrigen)) {
								case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_INGRESO_VENTAS:
									$state.go('app.detalleCliente',{prospectoID: $scope.prospectoID});
								  break;
								case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_PAGO_CIA_Y_ACUENTA: 
								  let vistaID=parseInt($stateParams.vistaID)
								  if(vistaID==1){
									$state.go('app.pagoCia');
								  }else{
									$state.go('app.detalleCliente',{prospectoID: $scope.prospectoID});
								  }
								  break;
								case REGISTRO_TRANSACCION_MASTERCARD.ORIGENID_OBLIGACION_POR_PAGAR: 
								   $state.go('app.obligacionesPorPagar');
								   break;
								default:
								  console.log('default');
							  }
						});	
					}else{
						$ionicPopup.alert({ title: 'Alerta', template: response.message , okType: 'button-assertive'});
					}
				}
				)
				.catch(function(err){
					console.log("error::",err);
					$ionicPopup.alert({ title: 'Alerta', template: err.message , okType: 'button-assertive'});
				}) 
			}
		});
	}
})
;
//  fin @feature/MasterCard
