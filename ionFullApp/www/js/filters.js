angular.module('your_app_name.filters', [])

.filter('rawHtml', function($sce){
  return function(val) {
    return $sce.trustAsHtml(val);
  };
})

.filter('parseDate', function() {
  return function(value) {
      return Date.parse(value);
  };
})
.filter('secondsToDateTime', [function() {
  /**
   * This code returns a date string formatted manually.
   * Code "new Date(1970, 0, 1).setSeconds(seconds)" returns malformed output on days.
   * Eg. 4 days, magically becomes 5, 15 becomes 16 and so on...;
   * */
  return function(seconds) {
  var days = Math.floor(seconds/86400);
  if(isNaN(days)){days=0;}
  var hours = Math.floor((seconds % 86400) / 3600);
  if(isNaN(hours)){hours=0;}
  var mins = Math.floor(((seconds % 86400) % 3600) / 60);
  if(isNaN(mins)){mins=0;}
  var secs = Math.floor(((seconds % 86400) % 3600) % 60);
  if(isNaN(secs)){secs=0;}
  return (days > 0 ? days+'d ' : '') + ('00'+hours).slice(-2) +':' + ('00'+mins).slice(-2)+':' + ('00'+secs).slice(-2);
  };
}]);
;
