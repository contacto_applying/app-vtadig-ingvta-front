/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |VTAOBS1-3                    |1.0      |John Velasquez   |20.09.2017 |                                  |
 * |VTADIG2-18                   |1.0      |John Velasquez   |3.10.2017  |                                  |
 * |JIRAV3-002					 |1.0.1    |John Velasquez   |16.11.2018 | set cache: false fos state app.detalleRestriccion   |
 * |JIRAV3-004					 |1.0.2    |John Velasquez   |23.11.2018 | login no requiere punto de exhibicion y se agrega state 'auth.seleccionPuntoDeVenta' como paso extra entre login y perfil  |
 * |JIRAV3-005					 |1.0.3    |John Velasquez   |26.11.2018 | add resolve 'me' to 'app.consultaCliente' |
 * |V4-HU002                     |1.0.4    |John Velasquez   |04.07.2019 | obtener información del dispositivo; plugins extras requeridos: {@link https://github.com/pbakondy/cordova-plugin-sim}, {@link https://github.com/apache/cordova-plugin-device}|
 * |V4-HU006					 |1.2.0    |John Velasquez   |23-07-2019 | agregar base64Utils y resolves[configuracionArchivos] en app.datostitular|
 * |V4-HU010 					 |1.3.0    |John Velasquez   |30.07.2019 | add states 'app.notificacionPagoCia' |
 * |V4-HU011                     |1.4.0    |John Velasquez |16/08/2019 | add states 'app.simuladorPremiosComisiones' & 'app.simulacionPremiosComisiones'|
 * |V4-HU012                     |1.5.0    |John velasquez   |25/07/2019 | agregar state 'app.reporteComisiones'  |
 * |V4-HU016                     |1.0.0    |John velasquez   |25/07/2019 | agregar state 'app.estadisticasRemates'  |
 * |V4-HU020                     |1.0.1    |John Velasquez   |12-07-2019 | cliente no domiciliado |
 * |VTADIG2-549					 |1.0.3    |John Velasquez   |26.11.2018 | avitar que la pantalla se apague y funcoinamiento en segundo plano |
 * |V4-HU030                     |1.0.0    |John velasquez   |07/09/2019 | agregar state 'app.viajeNuevoVendedorSup' y 'app.viajeNuevoVendedorObservacion'  |
 * |feature/Mastercard            |1.0     |Nataly Otero     |03/04/2020  | agregar state 'app.registrarPagoMastercardContrato'
 * |--------------------------------------------------------------------------------------------------------|
 */

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('your_app_name', [
  //'youtube-embed',
  'ionic',
  'angularMoment',
  'your_app_name.controllers',
  'your_app_name.directives',
  'your_app_name.filters',
  'your_app_name.services',
  'your_app_name.factories',
  'your_app_name.config',
  'your_app_name.views',
  'your_app_name.constants',
  'your_app_name.components',
  'your_app_name.strings',
  'underscore',
  'ngMap',
  'ngResource',
  'ngCordova',
  'slugifier',
  'ionic.contrib.ui.tinderCards',
  'ionic-modal-select',
  'ngStorage',
  'ion-autocomplete'
  ])

  

/**
 * @feature V4-HU002
 *  - add function getDeviceInfo y los permisos permissions.READ_PHONE_STATE, para obtener información del dispositivo
 * @issue VTADIG2-549
 */
    .run(function($ionicPlatform,$http, $sessionStorage,$injector, $rootScope, $ionicConfig, $timeout,WEB_VERSION_ID,$cordovaFile,DIRECTORIES) {
  $ionicPlatform.ready(function() {
    /** inicio @issue VTADIG2-549 **/
	if(window.plugins && window.plugins.insomnia){
        	window.plugins.insomnia.keepAwake(); 
    	}
    	if(cordova.plugins && cordova.plugins.backgroundMode) {
    		cordova.plugins.backgroundMode.enable();
    	}
    /** fin @issue VTADIG2-549 **/
    $ionicPlatform.registerBackButtonAction(function () {
      //console.log("no   no no no no ....");
    }, 100);
    
      if(window.cordova &&  window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      }
      try{
        Keyboard.hideFormAccessoryBar(false);
        Keyboard.setKeyboardStyle('dark');
      } catch (e)  {
        console.error(e);
      }
      if(window.StatusBar) {
          StatusBar.styleDefault();
          StatusBar.styleBlackTranslucent();
          StatusBar.styleBlackTranslucent();
      }
      var $state = $injector.get('$state');      
      try{
        cordova.getAppVersion(function(version) {        
          $sessionStorage.appVersion = version;
          //$state.go('auth.walkthrough');          
        });
      } catch(e){
          $sessionStorage.appVersion = WEB_VERSION_ID;          
          //$state.go('auth.walkthrough');          				
      }       
      /*INICIO VTADIG2-59 JOHN VELASQUEZ 28-12-2017*/
      createAppDirectory();
      function createAppDirectory(){
        try{
          if (cordova.file.documentsDirectory) {
            $rootScope.baseDirectory = cordova.file.documentsDirectory//+'AppIngvta'// + "IngVta/records/temps/speech";
          } else {				
            $rootScope.baseDirectory = cordova.file.externalRootDirectory//+'AppIngvta'// + "IngVta/records/temps/speech";
          }
          $rootScope.appDirectory = $rootScope.baseDirectory+DIRECTORIES.APP_DIRECTORY_NAME+"/";
          $rootScope.appTempDirectory = $rootScope.appDirectory+DIRECTORIES.APP_TEMP_DIRECTORY_NAME+"/";
          
          $cordovaFile.checkDir($rootScope.baseDirectory, DIRECTORIES.APP_DIRECTORY_NAME)
          .then(k=>createAppDirectories(),e=>{     
            $cordovaFile.createDir($rootScope.baseDirectory, DIRECTORIES.APP_DIRECTORY_NAME, false).then(d=>{
              createAppDirectories();
            });
          });
        } catch(e){

        }
      }
      function createAppDirectories(){
        createTempDirectory()
      }
      function createTempDirectory(){
        $cordovaFile.checkDir($rootScope.appDirectory, DIRECTORIES.APP_TEMP_DIRECTORY_NAME)
        .then(k=>createTempDirectories(),e=>{          
          $cordovaFile.createDir($rootScope.appDirectory, DIRECTORIES.APP_TEMP_DIRECTORY_NAME, false).then(d=>{
            createTempDirectories();
          });      
        })
      }
      function createTempDirectories(){

      }
      /*FIN VTADIG2-59 JOHN VELASQUEZ 28-12-2017*/      
      /*INICIO JOHN VELASQUEZ EDITAR PERFIL*/
      /*PERMISOS*/
      try{
        var permissions = cordova.plugins.permissions;
        var listPermissions = [
          permissions.CAMERA,
          permissions.WRITE_EXTERNAL_STORAGE,
          permissions.READ_EXTERNAL_STORAGE,
          permissions.CAMERA,
          permissions.CALL_PHONE,
          permissions.CAPTURE_AUDIO_OUTPUT,
          permissions.RECORD_AUDIO,
          /** inicio @feature V4-HU002 **/
          permissions.READ_PHONE_STATE
          /** fin @feature V4-HU002 **/
        ];
        if(permissions){
          permissions.requestPermissions(listPermissions,
            function(){
              /** inicio @feature V4-HU002 **/
              getDeviceInfo()
              /** fin @feature V4-HU002 **/
            }, 
            function () {
              
            });
        }
      }catch (e) {
        
      }      
      /*FIN JOHN VELASQUEZ EDITAR PERFIL*/
    /**
     *
     * obtiene inforamción de dispositivo(simInfo, deviceInfo) y la almacena en $sessiónStorage 
     * 
     * @feature V4-HU002
     */
      function getDeviceInfo(){
        // SIM INFO
        var getSeimInfo = () => {
          window.plugins.sim.getSimInfo(simInfo => {
            $sessionStorage.simInfo = simInfo;
            console.log("simInfo");
            console.log($sessionStorage.simInfo); // {deviceID[IMEI]: 362...26}
          }, error=>console.log(error));
        };
        window.plugins.sim.hasReadPermission(data => {
          if(data === true){
            getSeimInfo()
          }else{
            window.plugins.sim.requestReadPermission(getSeimInfo, error => console.log(error));
          }
        }, error => console.log(error));
        // DEVICE INFO
        $sessionStorage.deviceInfo = device;
        console.log("deviceInfo");
        console.log($sessionStorage.deviceInfo); 
      }
  });

  $ionicPlatform.on("deviceready", function(){
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    }
    if(window.StatusBar) {
      //StatusBar.styleDefault();
    }
  });

  // This fixes transitions for transparent background views
  $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
      if (toState.name.indexOf('auth.walkthrough') > -1) {
          // set transitions to android to avoid weird visual effect in the walkthrough transitions
          $timeout(function () {
              $ionicConfig.views.transition('android');
              $ionicConfig.views.swipeBackEnabled(false);//false
              console.log("setting transition to android and disabling swipe back");
          }, 0);
      }
  });
  $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
      if (toState.name.indexOf('app.feeds-categories') > -1) {
          // Restore platform default transition. We are just hardcoding android transitions to auth views.
          $ionicConfig.views.transition('platform');
          // If it's ios, then enable swipe back again
          if (ionic.Platform.isIOS()) {
              $ionicConfig.views.swipeBackEnabled(true);//true
          }
          console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
      }
  });


})


.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider,$httpProvider) {
  $httpProvider.interceptors.push('generalInterceptor');
  $ionicConfigProvider.views.swipeBackEnabled(false);
  $stateProvider

  //INTRO
  .state('auth', {
    url: "/auth",
    templateUrl: "views/auth/auth.html",
    abstract: true,
    cache: false,
    controller: 'AuthCtrl',
    /*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017
    resolve:{      
      appVersionNumber:['$q','versionService','$sessionStorage',function($q,versionService,$sessionStorage){
        var delay = $q.defer();
        versionService.getCurrentVersionNumber()
        .then(function (data) {
          $sessionStorage.appVersionNumber = data;
          delay.resolve(data);  
        });         
        return delay.promise;        
      }],
      appVersionCode:['$q','versionService','$sessionStorage',function($q,versionService,$sessionStorage){
        var delay = $q.defer();
        versionService.getCurrentVersionCode()
        .then(function (data) {
          $sessionStorage.appVersionCode = data;
          delay.resolve(data);  
        });         
        return delay.promise;        
      }]  
    }
    FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
  })
  /**
   * @issue JIRAV3-004
   *    - quitar resolve.lstPuntoExhibicion
   */
  .state('auth.walkthrough', {
    url: '/walkthrough',
    templateUrl: "views/auth/walkthrough.html",
    cache: false,
    resolve: {
      /*lstPuntoExhibicion: ['$rootScope','loginService','ListaGeneralService','$q','blockUI','$sessionStorage',function($rootScope,loginService,ListaGeneralService,$q,blockUI,$sessionStorage){
        var delay = $q.defer();
        blockUI.start();
        $sessionStorage.token = undefined;
        ListaGeneralService.getPuntosDeVenta()
        .then(
          function (response) {
            blockUI.stop();          
            var data = JSON.parse(response);
            $rootScope.puntosDeVenta = data;
            delay.resolve(data);  
          },
          function(error){
            console.log("CONEXION ERROR");
          }
        );
        return delay.promise;
      }] */
    }
  })
  /**
   * @issue JIRAV3-004
   *    - quitar resolve.lstPuntoExhibicion
   */
  .state('auth.login', {
    url: '/login',
    templateUrl: "views/auth/login.html",
    controller: 'LoginCtrl',
    resolve: {
      /*lstPuntoExhibicion: ['$rootScope','loginService','ListaGeneralService','$q','blockUI',function($rootScope,loginService,ListaGeneralService,$q,blockUI){
        if($rootScope.puntosDeVenta){
          return $rootScope.puntosDeVenta;
        }
        var delay = $q.defer();
        blockUI.start();
        ListaGeneralService.getPuntosDeVenta()
        .then(function (response) {
          blockUI.stop();
          var data = JSON.parse(response);
          delay.resolve(data);  
        });         
        return delay.promise;
      }] ,*/
    }
  })
  /**
   *
   * Seleccion del punto de venta por defecto para la sesión actual
   *
   * @issue JIRAV3-004
   */
  .state('auth.seleccionPuntoDeVenta',{
      url: '/login/PuntoDeVenta',
      templateUrl: "views/auth/selectPuntosVenta.html",
      controller: 'SeleccionPuntoDeVentaCtrl',
      cache: false,
      resolve: {
          lstPuntoExhibicion: ['$rootScope','ListaGeneralService','$q','blockUI',function($rootScope,ListaGeneralService,$q,blockUI){
            var delay = $q.defer();
            blockUI.start();
            ListaGeneralService.getPuntosDeVenta()
            .then(function (response) {
              blockUI.stop();
              var data = JSON.parse(response);
              $rootScope.puntosDeVenta = data;
              delay.resolve(data);
            });
            return delay.promise;
          }]
      }
  })
  .state('auth.signup', {
    url: '/signup',
    templateUrl: "views/auth/signup.html",
    controller: 'SignupCtrl'
  })

  .state('auth.forgot-password', {
    url: "/forgot-password",
    templateUrl: "views/auth/forgot-password.html",
    controller: 'ForgotPasswordCtrl'
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/app/side-menu.html",
    controller: 'AppCtrl',
    resolve: {
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
      listaGeneral: ['$rootScope','loginService','ListaGeneralService','$q',function($rootScope,loginService,ListaGeneralService,$q){
        if($rootScope.listaGeneral!==null && $rootScope.listaGeneral!==undefined){
          return $rootScope.listaGeneral;
        }
        var delay = $q.defer();
        ListaGeneralService.getListaGeneral().then(function successCallback(response) {
          var data = JSON.parse(response);
          delay.resolve(data);      
          $rootScope.listaGeneral = data;    
        });         
        return delay.promise;
      }],
      lstEmpleados:['shareData','VinculadoService','$q',function(shareData,VinculadoService,$q){        
        var delay = $q.defer();
        if(!shareData.lstEmpleados){
          VinculadoService.obtenerEmpleados()
          .then(function (lstEmpleados) {
            shareData.lstEmpleados = lstEmpleados;
            delay.resolve(shareData.lstEmpleados);
          });
        }else{
          delay.resolve(shareData.lstEmpleados);
        }
        return delay.promise;
      }]
    }
  })
  .state('app.connectionRefused', {
    url: '/ConnectionRefused',
    templateUrl: "views/app/connectionRefused.html",
    controller: 'connectionRefusedCtrl'
  })
  .state('app.error', {
    url: '/error',
    templateUrl: "views/app/error.html",
    controller: 'errorCtrl'
  })
  .state('app.accessDenied', {
    url: '/accessDenied',
    templateUrl: "views/app/accessDenied.html",
    controller: 'accessDeniedCtrl'
  })
  .state('noConnection', {
    url: '/noConnection',
    templateUrl: "views/app/noConnection.html",
    controller: 'noConnectionCtrl'
  })
  /*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
  .state('versionNotSupported', {
    url: '/versionNotSupported',
    templateUrl: "views/app/versionNotSupported.html",
    controller: 'versionNotSupportedCtrl'
  })
  /*FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/




  //LAYOUTS
  .state('app.layouts', {
    url: "/layouts",
    views: {
      'menuContent': {
        templateUrl: "views/app/layouts/layouts.html"
      }
    }
  })

  .state('app.tinder-cards', {
    url: "/layouts/tinder-cards",
    views: {
      'menuContent': {
        templateUrl: "views/app/layouts/tinder-cards.html",
        controller: 'TinderCardsCtrl'
      }
    }
  })

  .state('app.slider', {
    url: "/layouts/slider",
    views: {
      'menuContent': {
        templateUrl: "views/app/layouts/slider.html"
      }
    }
  })



  //OTHERS
  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "views/app/settings.html",
        controller: 'SettingsCtrl'
      }
    }
  })


  .state('app.registroClienteProspecto', {
    url: "/registroClienteProspecto",
    views: {
      'menuContent': {
        templateUrl: "views/app/registroClienteProspecto.html",
        controller: 'registerClientCtrl'
      }
    }
  })



  .state('app.confirmacionCliente', {
    url: "/confirmacionCliente/:previous",
    views: {
      'menuContent': {
        templateUrl: "views/app/confirmacionCliente.html",
      }
    }
  })


  .state('app.crearObligacion', {
    url: "/crearObligacion",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/crearObligacion.html",
        controller: 'crearObligacionCtrl'
      }
    },
    resolve: { 
      configuracionObligacionPago: ["obligacionPagoService","$q",function(obligacionPagoService,$q){
        var delay = $q.defer();
        obligacionPagoService.obtenerConfiguracionObligacionPago()
        .then(function(data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }]
    }
  })


 .state('app.crearObligacion2', {
    url: "/crearObligacion2",
    views: {
      'menuContent': {
      templateUrl: "views/app/crearObligacion2.html"
      }
    }
  })

.state('app.detallePago', {
    url: "/detallePago",
    cache: false,
    views: {
      'menuContent': {
      templateUrl: "views/app/detallePago.html",
      controller: "detallePagoCtrl"
      }
    },
    resolve: { 
      configuracionObligacionPago: ["obligacionPagoService","$q",function(obligacionPagoService,$q){
        var delay = $q.defer();
        obligacionPagoService.obtenerConfiguracionObligacionPago()
        .then(function(data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }]
    }
  })

 .state('app.generarContrato', {
  url: "/generarContrato",
  abstract : true,
  cache: true,
  views: {
    'menuContent': {
      templateUrl: "views/app/generarContrato.html",
      controller: 'generarContratoCtrl'
    }
  },
  /*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
  resolve: {    
    me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
      if($rootScope.me!==null && $rootScope.me!==undefined){
        return $rootScope.me;
      }
      var delay = $q.defer();
      loginService.me().then(function (data){
        delay.resolve(data);
        $rootScope.me = data;
      });
      return delay.promise;
    }]    
  }
  /*FIN - JOHN VELASQUEZ - CAMINOS APP*/
 })

.state('app.generarContrato.datosContrato', {
    url: "/datosContrato/:previous",
    cache: false,
    views: {
      'datosContrato': {
        templateUrl: "views/app/confirmacionBien.html",
        controller: 'datosContratoCtrl'
      }
    },
    resolve: { 
      autocomplete:['$stateParams','tempProspecto','tempContrato','$q','blockUI','$sessionStorage',function($stateParams,tempProspecto,tempContrato,$q,blockUI,$sessionStorage){
        //console.log("init autocomplete"); 
        var delay = $q.defer();
        blockUI.start();
        setTimeout( function() {
          //console.log("init autocomplete 1"); 
          if($stateParams.previous=="0"){          
            tempContrato.contrato={};          

            tempContrato.contrato.proformaID=tempProspecto.prospecto.proformaID+'';
            tempContrato.contrato.programaProductoID=tempProspecto.prospecto.programaProductoID;
            tempContrato.contrato.grupoID=tempProspecto.prospecto.grupoID;
            tempContrato.contrato.marcaId=tempProspecto.prospecto.marcaId?tempProspecto.prospecto.marcaId+'':undefined;
            
            tempContrato.contrato.modeloId=tempProspecto.prospecto.modeloId+'';
            tempContrato.contrato.certificadoPosicionID=tempProspecto.prospecto.certificadoPosicionID;

            /** INICIO card-27923717 - ANderson Estela Coronel - 08/mar/2021 */
            if(tempContrato.proforma.puntoExhibicionID != null)
              tempContrato.contrato.puntoExhibicionID = tempContrato.proforma.puntoExhibicionID;
            else
              tempContrato.contrato.puntoExhibicionID=$sessionStorage.defaults.puntoExhibicion.codigo+'';
            /** FIN card-27923717 - ANderson Estela Coronel - 08/mar/2021 */
            tempContrato.contrato.contratoOrigenID="1";

            tempContrato.contrato.cuotaTotalMensual=tempProspecto.prospecto.cuotaTotalMensual;
            tempContrato.contrato.contratoNroAsambleaGrupo=tempProspecto.prospecto.contratoNroAsambleaGrupo;
            tempContrato.contrato.tipoBienID=tempProspecto.prospecto.tipoBienId;
            tempContrato.contrato.cuotaTotalMensualPendiente=tempProspecto.prospecto.cuotaTotalMensual-tempProspecto.prospecto.cuotaTotalMensualPagado;
            tempContrato.contrato.cuotaTotalMensualPendiente=tempContrato.contrato.cuotaTotalMensualPendiente?tempContrato.contrato.cuotaTotalMensualPendiente:0;
            tempContrato.contrato.cuotaTotalMensualPagado=tempProspecto.prospecto.cuotaTotalMensualPagado;
            tempContrato.contrato.cuotaTotalMensualPagado=tempContrato.contrato.cuotaTotalMensualPagado?tempContrato.contrato.cuotaTotalMensualPagado:0;
            tempContrato.contrato.cia=tempProspecto.prospecto.cia;
          
            tempContrato.contrato.contratoNroCuotasProrrateo=0;
            tempContrato.contrato.contratoNroCuotasAbonado=0;
            tempContrato.contrato.contratoNroCuotasAdjudicacion=0;
            
            tempContrato.contrato.contratoNroCuotasIngreso=1+tempContrato.contrato.contratoNroCuotasProrrateo;

            tempContrato.contrato.montoCiaPagar=0;    

            if(tempContrato.contrato.contratoNroAsambleaGrupo && tempContrato.contrato.contratoNroAsambleaGrupo>1){
              if(tempContrato.contrato.programaProductoSeleccionado.programaID == "C4"){
                tempContrato.contrato.contratoNroCuotasAdjudicacion = parseInt(tempContrato.contrato.contratoNroAsambleaGrupo)-1;
              }else{
                tempContrato.contrato.contratoNroCuotasProrrateo = parseInt(tempContrato.contrato.contratoNroAsambleaGrupo)-1;						
              }					
            }
            tempContrato.contrato.montoCiaPagar=tempContrato.contrato.cia;

            delay.resolve(true);
          }

          delay.resolve(true);

        }, 1000 );      
        blockUI.stop();  
        //console.log("init autocomplete 2"); 
        return delay.promise;
      }],
      lstProgramaProducto:['tempProspecto','tempContrato','ProgramaProductoService','$rootScope','$q','autocomplete', function (tempProspecto,tempContrato,ProgramaProductoService,$rootScope,$q,autocomplete) {          
          //console.log("init lstProgramaProducto"); 
          var coincidioPrograma=false;
          if($rootScope.tempLstProgramaProducto){
            //console.log("FROM ROOTSCOPE");
            //console.log("MY ID -> "+tempContrato.contrato.programaProductoID);
            angular.forEach($rootScope.tempLstProgramaProducto, function (item) {
              //console.log("     item Id -> "+item.productoID);
                if(!tempContrato.contrato.programaProductoSeleccionado && (tempContrato.contrato.programaProductoID && item.productoID == tempContrato.contrato.programaProductoID)){
                  tempContrato.contrato.programaProductoSeleccionado = item;
                  coincidioPrograma=true;
                }
                else if(tempContrato.contrato.programaProductoSeleccionado){
                  if(item.productoID ==tempContrato.contrato.programaProductoSeleccionado.productoID){
                    coincidioPrograma=true;
                    tempContrato.contrato.programaProductoSeleccionado = item;

                  }else{

                  }
                }
            });
            
            if(!coincidioPrograma){
              tempContrato.contrato.grupoSeleccionado=undefined;
              tempContrato.contrato.certificadoSeleccionado=undefined;
              tempContrato.contrato.certificadoPosicionID=undefined;
              tempContrato.contrato.grupoID=undefined;
              
            } 
            tempContrato.contrato.coincidioPrograma=coincidioPrograma;


            return $rootScope.tempLstProgramaProducto;
          }else{
            //console.log("FROM SERVICE");
            var delay = $q.defer();
            /** INICIO APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */
            //ProgramaProductoService.getProgramaProducto().then(function(data){
            ProgramaProductoService.getProgramaProducto(tempProspecto.prospecto.tipoBienId).then(function(data){
            /** FIN APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */
              $rootScope.tempLstProgramaProducto = data;
              //console.log("MY ID -> "+tempContrato.contrato.programaProductoID);
              angular.forEach($rootScope.tempLstProgramaProducto, function (item) {       
                //console.log("     item Id -> "+item.productoID);
                if(!tempContrato.contrato.programaProductoSeleccionado && item.productoID == tempContrato.contrato.programaProductoID){
                  tempContrato.contrato.programaProductoSeleccionado = item;
                  coincidioPrograma=true;
                }else if(tempContrato.contrato.programaProductoSeleccionado){
                  if(item.productoID ==tempContrato.contrato.programaProductoSeleccionado.productoID){
                    coincidioPrograma=true;
                    tempContrato.contrato.programaProductoSeleccionado = item;
                  }
                }
              });
                             
              if(!coincidioPrograma){
                tempContrato.contrato.grupoSeleccionado=undefined;
                tempContrato.contrato.certificadoSeleccionado=undefined;
                tempContrato.contrato.certificadoPosicionID=undefined;
                tempContrato.contrato.grupoID=undefined;
              } 
              tempContrato.contrato.coincidioPrograma=coincidioPrograma;
              delay.resolve($rootScope.tempLstProgramaProducto);
            });            
            return delay.promise;
          }
      }],
      lstGrupo:['tempProspecto','tempContrato','GrupoService','$rootScope','$q','autocomplete','lstProgramaProducto', function (tempProspecto,tempContrato,GrupoService,$rootScope,$q,autocomplete,lstProgramaProducto) {          
          var coincidioGrupo=false;
          if($rootScope.tempLstGrupo){
            angular.forEach($rootScope.tempLstGrupo, function (item) {
              if(item.grupoID == tempContrato.contrato.grupoID && tempContrato.contrato.programaProductoSeleccionado){
                tempContrato.contrato.grupoSeleccionado = item;
                tempContrato.contrato.contratoNroAsambleaGrupo = item.grupoNroCuota;
                coincidioGrupo=true;
              }
            });   
            tempContrato.contrato.coincidioGrupo=coincidioGrupo;
            return $rootScope.tempLstGrupo;
          }else{
            var delay = $q.defer();
            GrupoService.getByProductoId(tempProspecto.prospecto.programaProductoID).then(function(data){
              if(data!== undefined && data !== null && data.length>0){
                $rootScope.tempLstGrupo = data;
                angular.forEach($rootScope.tempLstGrupo, function (item) {
                  if(item.grupoID == tempContrato.contrato.grupoID && tempContrato.contrato.programaProductoSeleccionado){
                    tempContrato.contrato.grupoSeleccionado = item;
                    tempContrato.contrato.contratoNroAsambleaGrupo = item.grupoNroCuota;
                    coincidioGrupo=true;
                  }
                }); 
                tempContrato.contrato.coincidioGrupo=coincidioGrupo;
                delay.resolve($rootScope.tempLstGrupo);
              }else{
                alert('No existen grupos disponibles según el producto seleccionado');                
                tempContrato.contrato.coincidioGrupo=false;
                delay.reject(data);
              }              
            });
            tempContrato.contrato.coincidioGrupo=coincidioGrupo;
            return delay.promise;
          }
      }],        
      lstCertificado:['tempProspecto','tempContrato','GrupoService','$rootScope','$q','autocomplete','contratoService','lstGrupo', 'proformaService', function (tempProspecto,tempContrato,GrupoService,$rootScope,$q,autocomplete,contratoService,lstGrupo,proformaService) {
        if(!tempContrato.contrato.grupoSeleccionado){
          tempContrato.contrato.cuotaTotalMensual = undefined;
          return null;
        }
        var coincidioCertificado=false;
        if($rootScope.tempLstCertificado){
          angular.forEach($rootScope.tempLstCertificado, function (item) {
            if(item.id.certificadoPosicionID == tempContrato.contrato.certificadoPosicionID){
              if(!tempContrato.contrato.contratoNroCuotasProrrateo){
                tempContrato.contrato.contratoNroCuotasProrrateo = 0;
              }
              tempContrato.contrato.certificadoSeleccionado = item;
              coincidioCertificado=true;
            }
          });
          
          tempContrato.contrato.coincidioCertificado=coincidioCertificado;
          return $rootScope.tempLstCertificado;
        }else{
          var delay = $q.defer();
          GrupoService.getCertificadosByGroupId(tempContrato.contrato.grupoID).then(function(data){
            $rootScope.tempLstCertificado = data;
            angular.forEach($rootScope.tempLstCertificado, function (item) {              
              if(item.id.certificadoPosicionID == tempContrato.contrato.certificadoPosicionID){
                if(!tempContrato.contrato.contratoNroCuotasProrrateo){
                  tempContrato.contrato.contratoNroCuotasProrrateo = 0;
                }
                tempContrato.contrato.certificadoSeleccionado = item;
                coincidioCertificado=true;
              }
            });
            tempContrato.contrato.coincidioCertificado=coincidioCertificado;            
            delay.resolve($rootScope.tempLstCertificado);
          });
          return delay.promise;
        }
      }],
      lstMarca:['tempProspecto','tempContrato','MarcaService','$rootScope','$q','autocomplete', function (tempProspecto,tempContrato,MarcaService,$rootScope,$q,autocomplete) {                                          
          //console.log("GETTING LstMarca");
          
          if($rootScope.tempLstMarca){
            //console.log("FROM ROOTSCOPE");
            
            return $rootScope.tempLstMarca;
          }else{
            if(tempContrato.contrato.tipoBienID==2){//vehículo
              var delay = $q.defer();
              //console.log("FROM SERVICE");
              MarcaService.getMarca(tempProspecto.prospecto.programaProductoID).then(function(data){
                $rootScope.tempLstMarca = data;
                //console.log("lst marca cargado de service");    
                //console.log($rootScope.tempLstMarca);
                delay.resolve($rootScope.tempLstMarca);
              });
              return delay.promise;
            }else{
              
              return [];
            }
          }
          //return MarcaService.getMarca(tempProspecto.prospecto.programaProductoID);        
      }],
      lstModelo:['tempProspecto','tempContrato','MarcaService','$rootScope','$q','shareData','autocomplete','proformaService', function (tempProspecto,tempContrato,MarcaService,$rootScope,$q,shareData,autocomplete,proformaService) {                  
        //console.log("GETTING LstModelo");        
          
          if($rootScope.tempLstModelo){            
            return $rootScope.tempLstModelo;
          }else{
            if(tempContrato.contrato.tipoBienID==2){//vehículo
              var delay = $q.defer();
              //console.log("FROM SERVICE");
              MarcaService.getModelos(tempProspecto.prospecto.marcaId).then(function(data){
                $rootScope.tempLstModelo = data;        
                //console.log("lst modelo cargado de service");
                //console.log($rootScope.tempLstModelo);
                delay.resolve($rootScope.tempLstModelo);                
              });
              return delay.promise;
            }else{
              var delay = $q.defer();
              proformaService.inversionesInmueble().then(function(data){
                delay.resolve(data);        
              }); 
              //return shareData.lstModeloInmueble;
              return delay.promise;
            }
          }
      }],
      /*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }]
      /*FIN - JOHN VELASQUEZ - CAMINOS APP*/
    }
  })

  .state('app.generarContrato.datosPersona', {
    url: "/datosPersona/:previous/:vez",
    cache: false,
    views: {
      'datosPersona': {
        templateUrl: "views/app/datosClientes.html",
        controller: 'datosTitularesCtrl'
      }
    },
    resolve: {
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
      persona: ["PersonaService","tempProspecto","$q","$state","tempContrato",function(PersonaService,tempProspecto,$q,$state,tempContrato){        
        if(!tempContrato.titulares){
          tempContrato.titulares = []; 
        }
        if(tempContrato.titulares.length<=0){
          var delay = $q.defer();
          PersonaService.obtenerPersona(tempProspecto.prospecto.prospectoBean.tipoDocumento,tempProspecto.prospecto.prospectoBean.numeroDocumento)
          .then(function(data){
              delay.resolve(data);
          });                   
          return delay.promise;
        }else{
          return null;
        }
      }],
      configuracionGrupo: ["contratoService","$q","$state",function(contratoService,$q,$state){        
          var delay = $q.defer();
          contratoService.obtenerConfiguracionGrupo("APP MOVIL ING VENTA")
          .then(function(data){
              delay.resolve(data);
          });                   
          return delay.promise;
      }],
      lstConvenios: ["contratoService","$q","$state",function(contratoService,$q,$state){        
        var delay = $q.defer();
        contratoService.obtenerConveniosInstitucionales()
        .then(function(data){
            delay.resolve(data);
        });                   
        return delay.promise;
      }],
      referenciasResult: function(referidoService, $q,$stateParams,blockUI,$rootScope){
        blockUI.start();
        var delay = $q.defer();
        referidoService.getReferidoList($rootScope.me.personaID)
        .then(function(data){
          blockUI.stop();
          delay.resolve(data);
        });
        return delay.promise;
      }
    }
  })
  /**
   * @feature V4-HU011
   */
   .state('app.simuladorPremiosComisiones', {
    url: '/simulador/pyc?cuota&certificadosVendidos',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/simuladorPremiosComisiones.html",
        controller: 'simuladorPremiosComisionesCtrl',
        resolve: {}
      }
    }
  })
  /**
   * @feature V4-HU011
   */
  .state('app.simulacionPremiosComisiones', {
    url: '/simulacion/pyc?cuota&certificadosVendidos',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/simulacionPremiosComisiones.html",
        controller: 'simulacionPremiosComisionesCtrl',
        resolve: {
          simulacion: ["$q","$stateParams","simuladorPremiosComisionesService", function($q,$stateParams, simuladorPremiosComisionesService){
            var delay = $q.defer();
            simuladorPremiosComisionesService.obtenerReporte($stateParams.cuota, $stateParams.certificadosVendidos)
                .then(function (data) {
                  delay.resolve(data);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
      
  /**
   * @feature V4-HU030
   */
  .state('app.viajeNuevoVendedorSup', {
    url: '/viajeNuevoVendedor/supervisor',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/viajeNuevoVendedor/supervisor.html",
        controller: 'viajeNuevoVendedorSupervisorCtrl',
        cache: false,
        resolve: {
          me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
            if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
            }
            var delay = $q.defer();
            loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
            });
            return delay.promise;
          }],
          vendedores: ['$q', 'ViajeNuevoVendedorService', "me", function($q, ViajeNuevoVendedorService, me){
            var delay = $q.defer();
            ViajeNuevoVendedorService.obtenerVendedores()
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
  /**
   * @feature V4-HU030
   */
  .state('app.viajeNuevoVendedorObservacion', {
    url: '/viajeNuevoVendedor/:personaId/observacion',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/viajeNuevoVendedor/observacion.html",
        controller: 'viajeNuevoVendedorObservacionCtrl',
        cache: false,
        resolve: {
          me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
            if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
            }
            var delay = $q.defer();
            loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
            });
            return delay.promise;
          }],
        vendedor: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", function($q, ViajeNuevoVendedorService, me, $stateParams){
          var delay = $q.defer();
          ViajeNuevoVendedorService.obtenerVendedor($stateParams.personaId)
              .then(function (data) {
                delay.resolve(data.data);
              }, function(error){
                console.log(error);
              });
          return delay.promise;
        }],
        formulario: ['$q', 'FormularioService', "me", "FORMULARIO", function($q, FormularioService, me, FORMULARIO){
            var delay = $q.defer();
            FormularioService.obtenerFormulario(FORMULARIO.FORMATO_ID_5_TO_5)
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
  /**
   * @feature V4-HU030
   */
  .state('app.viajeNuevoVendedorDetalleVendedor', {
    url: '/viajeNuevoVendedor/:personaId',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/viajeNuevoVendedor/vendedor.html",
        controller: 'viajeNuevoVendedorDetalleVendedorCtrl',
        cache: false,
        resolve: {
          me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
            if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
            }
            var delay = $q.defer();
            loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
            });
            return delay.promise;
          }],
          vendedor: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", function($q, ViajeNuevoVendedorService, me, $stateParams){
            var delay = $q.defer();
            ViajeNuevoVendedorService.obtenerVendedor($stateParams.personaId ? $stateParams.personaId : me.personaId)
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
  /**
   * @feature V4-HU030
   */
  .state('app.viajeNuevoVendedorDetalleSemana', {
    url: '/viajeNuevoVendedor/:personaId/semana/:semana',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/viajeNuevoVendedor/semana.html",
        controller: 'viajeNuevoVendedorDetalleSemanaCtrl',
        cache: false,
        resolve: {
          me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
            if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
            }
            var delay = $q.defer();
            loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
            });
            return delay.promise;
          }],
          reporte: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", function($q, ViajeNuevoVendedorService, me, $stateParams){
            var delay = $q.defer();
            ViajeNuevoVendedorService.obtenerReporteSemanal($stateParams.personaId, $stateParams.semana)
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }],
          tipofeedbackLst: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", function($q, ViajeNuevoVendedorService, me, $stateParams){
            var delay = $q.defer();
            ViajeNuevoVendedorService.obtenerTiposFeedback()
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
  /**
   * @feature V4-HU030
   */
  .state('app.viajeNuevoVendedorPasaporte', {
    url: '/viajeNuevoVendedor/:personaId/pasaporte',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/viajeNuevoVendedor/pasaporte.html",
        controller: 'viajeNuevoVendedorPasaporteCtrl',
        cache: false,
        resolve: {
          me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
            if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
            }
            var delay = $q.defer();
            loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
            });
            return delay.promise;
          }],
          pasaporte: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", "FORMULARIO", function($q, ViajeNuevoVendedorService, me, $stateParams, FORMULARIO){
            var delay = $q.defer();
            if(!$stateParams.personaId){
                $stateParams.personaId = me.personaID;
            }
            ViajeNuevoVendedorService.obtenerPasaporte($stateParams.personaId)
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
            }],
          vendedor: ['$q', 'ViajeNuevoVendedorService', "me", "$stateParams", function($q, ViajeNuevoVendedorService, me, $stateParams){
            var delay = $q.defer();
            ViajeNuevoVendedorService.obtenerVendedor($stateParams.personaId ? $stateParams.personaId : me.personaId)
                .then(function (data) {
                  delay.resolve(data.data);
                }, function(error){
                  console.log(error);
                });
            return delay.promise;
          }]
        }
      }
    }
  })
  .state('app.agregarPersona', {
    url: "/datosTitular/juridico/agregarPersona/:tipo/:estadoCivil",//[0:vinculada];[1:relacionada]
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/agregarPersona.html",
        controller: 'agregarPersonaCtrl',
        resolve: {           
          lstEmpleados:['shareData','VinculadoService','$q',function(shareData,VinculadoService,$q){        
            var delay = $q.defer();
            if(!shareData.lstEmpleados){
              VinculadoService.obtenerEmpleados()
              .then(function (lstEmpleados) {
                shareData.lstEmpleados = lstEmpleados;
                delay.resolve(shareData.lstEmpleados);
              });
            }else{
              /* INICIO ticket 44101 Anderson Estela Coronel 08/oct/2020 */
              VinculadoService.obtenerEmpleadosVinculado()
              .then(function (lstEmpleados) {
                shareData.lstEmpleados = lstEmpleados;
                delay.resolve(shareData.lstEmpleados);
              });
              //delay.resolve(shareData.lstEmpleados);
              /* FIN ticket 44101 Anderson Estela Coronel 08/oct/2020 */
            }
            return delay.promise;
          }]
        }
      }
    }
  })
  /**
   * @feature V4-HU006
   *  - add resolves[configuracionArchivos]
   */
  .state('app.datosTitular', {
    url: "/datosTitular/:asociadoTipoID/:esEmpleadoPandero/:esCasado/:previous",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/datosTitular.html",
        controller: 'datosTitularCtrl'
      }
    },
    resolve: {
      lstPais: ['ubigeoService','$q','shareData','$rootScope',function(ubigeoService, $q,shareData,$rootScope){
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }        
        var delay = $q.defer();         
        if($rootScope.tempLstDatosTitular.lstPais){
          shareData.lstPais = $rootScope.tempLstDatosTitular.lstPais;
          delay.resolve(shareData.lstPais);
        }else{
          ubigeoService.getPais()
          .then(function (response) {                      
            $rootScope.tempLstDatosTitular.lstPais = response;
            shareData.lstPais = $rootScope.tempLstDatosTitular.lstPais;
            delay.resolve(shareData.lstPais);
          });
        }      
        return delay.promise;
      }],
      lstDepartamento: ['departamento','$q','shareData','$rootScope',function(departamento, $q,shareData,$rootScope){
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstDepartamento){
          shareData.lstDepartamento = $rootScope.tempLstDatosTitular.lstDepartamento;
          delay.resolve(shareData.lstDepartamento);
        }else{
          departamento.getDepartamento()
          .then(function (response) {	
            $rootScope.tempLstDatosTitular.lstDepartamento = response;
            shareData.lstDepartamento = $rootScope.tempLstDatosTitular.lstDepartamento;			
            delay.resolve(shareData.lstDepartamento);
          });
        }        
        return delay.promise;
      }],
      lstProvincia:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){
        if(!tempTitular.titular.direcciones){
          return [];
        }

        if(!tempTitular.titular.direcciones[0]){
          return [];
        }

        if(!tempTitular.titular.direcciones[0].departamentoID){
          return [];
        }
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstProvincia){          
          delay.resolve($rootScope.tempLstDatosTitular.lstProvincia);
        }else{
          var url=BACK_END_URL+"provincia/"+tempTitular.titular.direcciones[0].departamentoID;			
          $http({
            method: 'GET',
            url: url
          }).then(function(response) {
            $rootScope.tempLstDatosTitular.lstProvincia = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstProvincia);
          });
        }                           
        return delay.promise;                  
      }],
      lstDistrito:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){
        if(!tempTitular.titular.direcciones){
          return [];
        }

        if(!tempTitular.titular.direcciones[0]){
          return [];
        }
          
        if(!tempTitular.titular.direcciones[0].departamentoID||!tempTitular.titular.direcciones[0].provinciaID){
          return [];
        }
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstDistrito){
          delay.resolve($rootScope.tempLstDatosTitular.lstDistrito);
        }else{
          var url=BACK_END_URL+"distrito/"+tempTitular.titular.direcciones[0].departamentoID+"/"+tempTitular.titular.direcciones[0].provinciaID;        
          $http({
            method: 'GET',
            url: url
          }).then(function(response) {          
            $rootScope.tempLstDatosTitular.lstDistrito = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstDistrito);
          });                             
        }                
        return delay.promise;          
        
      }],      
      lstProvinciaLaboral:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){
        if(!tempTitular.titular.direcciones){
          return [];
        }

        if(!tempTitular.titular.direcciones[1]){
          return [];
        }

        if(!tempTitular.titular.direcciones[1].departamentoID){
          return [];
        }

        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstProvinciaLaboral){
          delay.resolve($rootScope.tempLstDatosTitular.lstProvinciaLaboral);
        }else{
          var url=BACK_END_URL+"provincia/"+tempTitular.titular.direcciones[1].departamentoID;			
          $http({
            method: 'GET',
            url: url
          }).then(function(response) {          
            $rootScope.tempLstDatosTitular.lstProvinciaLaboral = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstProvinciaLaboral);
          });
        }                           
        return delay.promise;                  
      }],
      lstDistritoLaboral:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){
        if(!tempTitular.titular.direcciones){
          return [];
        }

        if(!tempTitular.titular.direcciones[1]){
          return [];
        }
          
        if(!tempTitular.titular.direcciones[1].departamentoID||!tempTitular.titular.direcciones[1].provinciaID){
          return [];
        }

        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstDistritoLaboral){
          delay.resolve($rootScope.tempLstDatosTitular.lstDistritoLaboral);
        }else{
          var url=BACK_END_URL+"distrito/"+tempTitular.titular.direcciones[1].departamentoID+"/"+tempTitular.titular.direcciones[1].provinciaID;
          $http({
            method: 'GET',
            url: url
          }).then(function(response) {          
            $rootScope.tempLstDatosTitular.lstDistritoLaboral = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstDistritoLaboral);
          });
        }                           
        return delay.promise;                  
      }],      
      lstProvinciaNacimiento:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){        

        if(!tempTitular.titular.personaNacimientoUbigeo&&!tempTitular.titular.departamentoNacimientoID){
          return [];
        }
                
        if(tempTitular.titular.departamentoNacimientoID){
          var url=BACK_END_URL+"provincia/"+tempTitular.titular.departamentoNacimientoID; 
        }else{
          var ubigeoNacimiento = tempTitular.titular.personaNacimientoUbigeo;
          var ubigeoDepartamento = ubigeoNacimiento.substring(0, 2);
          if(!ubigeoDepartamento){
            return [];
          }
          var url=BACK_END_URL+"provincia/"+ubigeoDepartamento; 
        }
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();
        if($rootScope.tempLstDatosTitular.lstProvinciaNacimiento){
          delay.resolve($rootScope.tempLstDatosTitular.lstProvinciaNacimiento);             
        }else{
          $http({
            method: 'GET',
            url: url
          }).then(function (response) {				
            $rootScope.tempLstDatosTitular.lstProvinciaNacimiento = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstProvinciaNacimiento);             
          });                  
        }                
        return delay.promise;        
      }],
      lstDistritoNacimiento:['tempTitular','BACK_END_URL','$q','$http','$rootScope',function(tempTitular,BACK_END_URL,$q,$http,$rootScope){
        if(!tempTitular.titular.personaNacimientoUbigeo&&!tempTitular.titular.provinciaNacimientoID&&!tempTitular.titular.departamentoNacimientoID){
          return [];
        }
        
        if(tempTitular.titular.provinciaNacimientoID&&tempTitular.titular.departamentoNacimientoID){
          var url=BACK_END_URL+"distrito/"+tempTitular.titular.departamentoNacimientoID+"/"+tempTitular.titular.provinciaNacimientoID;        
        }else{
          var ubigeoNacimiento = tempTitular.titular.personaNacimientoUbigeo;
          var ubigeoDepartamento = ubigeoNacimiento.substring(0, 2);
          var ubigeoProvincia = ubigeoNacimiento.substring(2, 4);
          if(!ubigeoDepartamento||!ubigeoProvincia){
            return [];
          }
          var url=BACK_END_URL+"distrito/"+ubigeoDepartamento+"/"+ubigeoProvincia;        
        }
        if(!$rootScope.tempLstDatosTitular){
          $rootScope.tempLstDatosTitular = {};
        }
        var delay = $q.defer();        
        if($rootScope.tempLstDatosTitular.lstDistritoNacimiento){
          delay.resolve($rootScope.tempLstDatosTitular.lstDistritoNacimiento);
        }else{
          $http({
            method: 'GET',
            url: url
          }).then(function(response) {          
            $rootScope.tempLstDatosTitular.lstDistritoNacimiento = response.data.data;
            delay.resolve($rootScope.tempLstDatosTitular.lstDistritoNacimiento);
          });
        }                                   
        return delay.promise;
        
      }],
      /**
      * @feature V4-HU006
      */
      configuracionArchivos: ['contratoService','$q','$stateParams',function(contratoService,$q,$stateParams){
        var delay = $q.defer();
        contratoService.obtenerConfiguracionArchivoContrato()
            .then(function (data){
                delay.resolve(data);
            });
        return delay.promise;
      }]
    }
  })
  /**
   * V4-HU020
   *  - add resolve lstPais
   */
  .state('app.resumenContrato', {
    url: "/resumenContrato",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/resumenContrato.html",
        controller: 'resumenContratoCtrl'
      }
    },
    /*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
    resolve: {
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){          
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }]
      /** inicio @feature v4-HU020 **/
        ,lstPais: ['ubigeoService','$q','shareData','$rootScope',function(ubigeoService, $q,shareData,$rootScope){
            if(!$rootScope.tempLstDatosTitular){
                $rootScope.tempLstDatosTitular = {};
            }
            var delay = $q.defer();
            if($rootScope.tempLstDatosTitular.lstPais){
                shareData.lstPais = $rootScope.tempLstDatosTitular.lstPais;
                delay.resolve(shareData.lstPais);
            }else{
                ubigeoService.getPais()
                    .then(function (response) {
                        $rootScope.tempLstDatosTitular.lstPais = response;
                        shareData.lstPais = $rootScope.tempLstDatosTitular.lstPais;
                        delay.resolve(shareData.lstPais);
                    });
            }
            return delay.promise;
        }]
        /** fin @feature v4-HU020 **/
    }
    /*FIN - JOHN VELASQUEZ - CAMINOS APP*/    
  })

  // inicio @feature/MasterCard 
  .state('app.mensajeConfirmacionContrato', {
      url: "/mensajeConfirmacionContrato",
      cache: false,
      views: {
          'menuContent': {
              templateUrl: "views/app/mensajeConfirmacionContrato.html",
              controller: 'mensajeConfirmacionCtrl'
          }
      }
  })  

  .state('app.mensajeConfirmacionObligacion', {
      url: "/mensajeConfirmacionObligacion/:origenID",
      cache:false,
      views: {
          'menuContent': {
              templateUrl: "views/app/mensajeConfirmacionObligacion.html",
              controller: 'mensajeConfirmacionObligacionCtrl'
          }
      }
  })  
 // fin @feature/MasterCard 
  .state('app.profile', {
    url: "/home",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/profile.html",
        controller: "ProfileCtrl"
      }
    },
    resolve: {
      miEmbudo: ['$rootScope','proformaService','$q',function($rootScope,proformaService,$q){
        var delay = $q.defer();
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);
        proformaService.getMiEmbudo(firstDay,lastDay).then(function (data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      /*INICIO - JOHN VELASQUEZ - CAMINOS APP*/
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){          
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }]
      /*FIN - JOHN VELASQUEZ - CAMINOS APP*/
    }
  })
  /*INICIO JOHN VELASQUEZ SEGUIMIENTO EDV*/
  .state('app.seguimientoEdv',{
    url: "/seguimientoEdv",
    cache: false,
    views: {
        'menuContent': {
            templateUrl: "views/app/seguimientoEdv.html",
            controller: 'seguimientoEdvCtrl'
        }
    },
    resolve: {
    }
  })
 /*FIN JOHN VELASQUEZ SEGUIMIENTO EDV*/
  /**
   * @feature V4-HU016
   */
   .state('app.estadisticasRemates',{
    url: "/estadisticas/remates",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/estadisticas/remates.html",
        controller: 'estadisticasRematesCtrl'
      }
    },
    resolve: {
      programaProducto: ["$q","ProgramaProductoService", ($q, ProgramaProductoService) => {
        var delay = $q.defer();
        ProgramaProductoService.getProgramaProducto().then(function (data){
          delay.resolve(data);
        });
        return delay.promise;
      }]
    }
  })
  /*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
  .state('app.editProfile', {
    url: "/editProfile",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/editProfile.html",
        controller: "EditProfileCtrl"
      }
    },
    resolve: {
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){          
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
      telefonos: ['$rootScope','profileService','$q',function($rootScope,profileService,$q){
        var delay = $q.defer();
        profileService.getPhones().then(function (data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      config: ['$rootScope','profileService','$q',function($rootScope,profileService,$q){
        if($rootScope.profileConfig!==null && $rootScope.profileConfig!==undefined){          
          return $rootScope.profileConfig;
        }
        var delay = $q.defer();
        profileService.getContig().then(function (data){
          delay.resolve(data);
          $rootScope.profileConfig = data;
        });
        return delay.promise;
      }],
    }
  })
  /*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
  .state('app.bookmarks', {
    url: "/bookmarks",
    views: {
      'menuContent': {
        templateUrl: "views/app/bookmarks.html",
        controller: 'BookMarksCtrl'
      }
    }
  })
// setup an abstract state for the tabs directive
  /**
   * @feature V4-HU010
   */
  .state('app.notificacionPagoCia',{
    url: "/notificacion/pago/cia",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/notificacion/pago/cia.html",
        controller: 'NotificacionPagoCiaCtrl'
      }
    },
    resolve: {
      contratosCiaPendientes: ["$q","contratoService", "TIPO_BUSQUEDA_CONTRATO", ($q, contratoService, TIPO_BUSQUEDA_CONTRATO) => {
        var delay = $q.defer();
        contratoService.buscarContratosCiaPendientes(TIPO_BUSQUEDA_CONTRATO.TODOS)
            .then(function (data) {
              delay.resolve(data);
            });
        return delay.promise;
      }],
      configuracionRecordatorioPago: ["$q","contratoService", ($q, contratoService) => {
        var delay = $q.defer();
        contratoService.obtenerConfiguracionRecordatorioPago()
            .then(function (data) {
              delay.resolve(data);
            });
        return delay.promise;
      }]
    }
  })
 .state('app.registroProspecto', {
 url: "/registroProspecto",
 abstract : true,
    views: {
      'menuContent': {
        templateUrl: "views/app/registroProspecto.html",
        controller: "registroProspectoCtrl"
      }
    } 
 })

 .state('app.registroProspecto.proforma', {
  url: "/proforma/:init",
  cache:false,
  views: {
    'proforma-tab': {
      templateUrl: "views/app/tab-proforma.html", 
      controller: "cotizacionCtrl"
    }
  },
  resolve: {
    lstTipoBien:['$stateParams','tempProforma','shareData','ProgramaProductoService','$rootScope','$q', function ($stateParams,tempProforma,shareData,ProgramaProductoService,$rootScope,$q) {                
      if($rootScope.tempLstTipoBien){        
        return $rootScope.tempLstTipoBien;
      }else{
        var delay = $q.defer();
        ProgramaProductoService.getTiposDeBienPorUnidadNegocio().then(function(data){
              var resultLstTipoBien = [];
              angular.forEach(data, function (tipoBienId) {  
                resultLstTipoBien.push(shareData.mapSmvBienesServicios[tipoBienId]);
              });
              $rootScope.tempLstTipoBien = resultLstTipoBien;
              delay.resolve($rootScope.tempLstTipoBien);
        });           
        return delay.promise;
      }
    }],
    dataMarcas:['$rootScope','proformaService','$q','lstTipoBien', function ($rootScope,proformaService,$q,lstTipoBien) {
        /*
        if(lstTipoBien!==null && lstTipoBien!== undefined && lstTipoBien.length>1){
          return null;
        }
        */
        
        if($rootScope.dataMarcas){        
          return $rootScope.dataMarcas;
        }        

        var delay = $q.defer();
        proformaService.getMarcaModeloProforma().then(function(data){
          delay.resolve(data);    
          $rootScope.dataMarcas = data;  
        });        
        return delay.promise;
      
    }]    
  }
}) 


.state('app.registroProspecto.cliente', {
  url: "/cliente/:init",
  cache:false,
  views: {
    'cliente-tab': {
      templateUrl: "views/app/tab-cliente.html",
      controller: "clienteCtrl"
    }
  }
  /*INICIO INTEGRACION EC*/
  ,resolve: {
    cuota: function(usuarioService, $q) {
      var delay = $q.defer();
      usuarioService.obtenerCuota()
      .then(function(data){
          delay.resolve(data);
      });                 
      return delay.promise;
    }
  }
  /*FIN INTEGRACION EC*/
}) 
 // Each tab has its own nav history stack: 
  
 .state('tab.account', {
 url: '/account',
 views: {
 'tab-account': {
 templateUrl: 'templates/tab-account.html'
 }
 }
 })

  .state('app.obligacionesPorPagar', {
    url: "/obligacionesPorPagar",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/obligacionesPorPagar.html",
        controller: 'consultaObligacionesPorPagarCtrl',
        controllerAs: 'vm'
      }
    },
    resolve: {
      lstObligacionResult: function(obligacionesPorPagarService, $q) {
        var delay = $q.defer();
        obligacionesPorPagarService.getObligacionesPorNombre(' ')
        .then(function(data){
            delay.resolve(data);
        });           
        
        return delay.promise;
      }
    }
  })
  .state('app.detalleObligacion', {
    url: "/detalleObligacion/:obligacionPagoID",
    views: {
      'menuContent': {
        templateUrl: "views/app/detalleObligacionPorPagar.html",
        controller: 'detalleObligacionCtrl',
        controllerAs: 'vm',
        cache:false
      }
    },
    resolve: {
      obligacionResult: function(obligacionesPorPagarService, $q, $stateParams) {
        var delay = $q.defer();
        obligacionesPorPagarService.getDetalleObligacion($stateParams.obligacionPagoID)
        .then(function(data){
            delay.resolve(data);
        });           
        
        return delay.promise;
      }
    }
  })
  /**
   * @issue JIRAV3-005
   *    - add resolve 'me'
   */
  .state('app.consultaCliente', {
    url: "/consultaCliente",
    cache:false,
    views: {
      'menuContent': {
        templateUrl: "views/app/consultaCliente.html",
        controller: 'consultaClienteCtrl',
        controllerAs: 'vm'
      }
    },
    resolve: {
      listaProspecto: function(proformaService, $q, $stateParams,blockUI,$rootScope) {

        var delay = $q.defer();
        
        proformaService.getProspectoPorUsuario().then(function(data){
          //$rootScope.tempLstProformasProspecto = data;
          delay.resolve(data);
        });           
      
        return delay.promise;
      },
      /* inicio  @issue JIRAV3-005 */
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
          if($rootScope.me!==null && $rootScope.me!==undefined){
              return $rootScope.me;
          }
          var delay = $q.defer();
          loginService.me().then(function (data){
              delay.resolve(data);
              $rootScope.me = data;
          });
          return delay.promise;
      }]
      /* fin  @issue JIRAV3-005 */
    }    
  })
  .state('app.detalleCliente', {
    url: "/detalleCliente/:prospectoID",
    cache:false,
    views: {
      'menuContent': {
        templateUrl: "views/app/detalleCliente.html",
        controller: 'detalleClienteCtrl',
        controllerAs: 'vm',
        cache:false
      } 
    },
    resolve: {
      prospectoResult: function(prospectoService, $q, $stateParams,blockUI,$rootScope) {
        blockUI.start();
        var delay = $q.defer();
        
        prospectoService.obtenerDetalleProspecto($stateParams.prospectoID)
        .then(function(data){
          data.tipoDocumento = data.tipoDocumento + '';
          $rootScope.tempLstProformasProspecto = data;
          blockUI.stop();
          delay.resolve(data);
        });           
      
        return delay.promise;


      }
    }
  })
  /**
   * @feature V4-HU012
   */
  .state('app.reportecomisiones', {
    url: "/reporte/comisiones",
    cache:false,
    views: {
      'menuContent': {
        templateUrl: "views/app/reporteComisiones.html",
        controller: 'reporteComisionesCtrl'
      }
    },
    resolve: {

    }
  })
  .state('app.seguimientoCliente', {
    url: "/seguimientoCliente/:prospectoID",
    views: {
      'menuContent': {
        templateUrl: "views/app/seguimientoCliente.html",
        controller: 'seguimientoClienteCtrl',
        controllerAs: 'vm',
        cache:false
      } 
    },
    resolve: {
      prospectoResult: function(seguimientoService, $q, $stateParams,blockUI,$rootScope) {
        blockUI.start();
        var delay = $q.defer();
        
        seguimientoService.obtenerDetalleSeguimiento($stateParams.prospectoID)
        .then(function(data){
          $rootScope.tempLstSeguimientoProspecto = data;
          blockUI.stop();
          delay.resolve(data);
        });           
      
        return delay.promise;
        
        
      },proformasResult: function(prospectoService, $q, $stateParams,blockUI,$rootScope) {
        blockUI.start();
        var delay = $q.defer();
        
        prospectoService.obtenerDetalleProspecto($stateParams.prospectoID)
        .then(function(data){
          data.tipoDocumento = data.tipoDocumento + '';
          $rootScope.tempLstProformasProspecto = data;
          blockUI.stop();
          delay.resolve(data);
        });           
      
        return delay.promise;
        
        
      },plantillas: function(seguimientoService, $q,$stateParams,blockUI) {
        blockUI.start();
        var delay = $q.defer();
        
        seguimientoService.obtenerPlantillas($stateParams.prospectoID)
        .then(function(data){
          blockUI.stop();
          delay.resolve(data);
        });           
      
        return delay.promise;
        
        
      },configArchivosPlantilla: function(seguimientoService, $q,$stateParams,blockUI) {
        blockUI.start();
        var delay = $q.defer();
        
        seguimientoService.obtenerConfigArchivosPlantilla()
        .then(function(data){
          blockUI.stop();
          delay.resolve(data);
        });           
      
        return delay.promise;
      }
    }
  })
  .state('app.editarProspecto', {
    url: "/editarProspecto/:nroDocumento/:tipoDocumento/:prospectoID",
    cache:false,
    views: {
      'menuContent': {
        templateUrl: "views/app/editarProspecto.html",
        controller: 'editarProspectoCtrl',
        cache:false
      } 
    },
    resolve: {
      prospectoResult: function(proformaService, $q, $stateParams,blockUI,$rootScope) {
        blockUI.start();
        var delay = $q.defer();
        proformaService.getProspectoPorDocumento($stateParams.tipoDocumento,$stateParams.nroDocumento)
        .then(function(data){
          data.tipoDocumento = data.tipoDocumento + '';
          $rootScope.tempLstProformasProspecto = data;
          delay.resolve(data);
        });           
      
        return delay.promise;
        
        
      },
      pagosResult: function(prospectoService, $q, $stateParams,blockUI,$rootScope) {
        blockUI.start();
        var delay = $q.defer();
        
        prospectoService.obtenerDetalleProspecto($stateParams.prospectoID)
        .then(function(data){
          data.tipoDocumento = data.tipoDocumento + '';
          $rootScope.tempLstProformasProspecto = data;
          delay.resolve(data);
        });           
      
        return delay.promise;
        
        
      }
    }
  })

  /*****************************************************************/

  .state('app.clientProspects', {
    url: "/clientPropects",
    views: {
      'menuContent': {
        templateUrl: "views/app/prospect-list.html",
        controller: 'propectListCtrl',
        controllerAs: 'vm'
      }
    }
  })
  .state('app.consultaClienteTable', {
      url: "/consultaClienteTable",
      views: {
          'menuContent': {
              templateUrl: "views/app/search-client.html",
              controller: 'consultaClienteCtrl',
              controllerAs: 'vm'
          }
      }
  })
  .state('app.misVentas',{
    url: "/misVentas",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/misVentas.html",
        controller: 'misVentasCtrl',
        controllerAs: 'vm'
      }
    },
    resolve: {
      misVentas: ['proformaService','$q',function(proformaService,$q){        
        var delay = $q.defer();
        proformaService.getMisVentas().then(function (data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
    }
  })
  .state('app.misSeparaciones',{
    url: "/misSeparaciones",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/misSeparaciones.html",
        controller: 'misSeparacionesCtrl',
        controllerAs: 'vm'
      }
    },
    resolve: {
      misSeparaciones: ['proformaService','$q',function(proformaService,$q){        
        var delay = $q.defer();
        proformaService.getMisSeparaciones().then(function (data){
          delay.resolve(data);
        });
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
    }
  })
  .state('app.pagoCia',{
    url: "/pagoCIA/:new",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/pagoCIa.html",
        controller: 'pagoCiaCtrl',
        controllerAs: 'vm'
      }
    }
  })

  .state('app.consultaPagoCia',{
      url: "/consultaPagoCia/:numeroContrato/:contratoID",
      cache: false,
      views: {
          'menuContent': {
              templateUrl: "views/app/consultaPagoCia.html",
              controller: 'consultaPagoCiaCtrl',
              controllerAs: 'vm'
          }
      },
      resolve: {
          obligaciones: ['contratoService','$q','$stateParams',function(contratoService,$q,$stateParams){
              var delay = $q.defer();
              contratoService.obtenerObligacionesCiaDeContrato($stateParams.contratoID).then(function (data){
                  delay.resolve(data);
              });
              return delay.promise;
          }],
          contrato: ['contratoService','$q','$stateParams','TIPO_BUSQUEDA_CONTRATO',function(contratoService,$q,$stateParams,TIPO_BUSQUEDA_CONTRATO){
              var delay = $q.defer();
              if($stateParams.numeroContrato){
                  contratoService.buscarContratosCiaPendientes(TIPO_BUSQUEDA_CONTRATO.NRO_CONTRATO,$stateParams.numeroContrato.replace(/-/g,''))
                      .then(function (data){
                          delay.resolve(data[0]);
                      });
              }
              return delay.promise;
          }]
      }
  })
  .state('app.nuevaObligacionPagoCia',{
      url: "/nuevaObligacionPagoCia/:numeroContrato",
      cache: false,
      views: {
          'menuContent': {
              templateUrl: "views/app/nuevaObligacionPagoCia.html",
              controller: 'nuevaObligacionPagoCiaCtrl',
              controllerAs: 'vm'
          }
      },
      resolve: {
          contrato: ['contratoService','$q','$stateParams','TIPO_BUSQUEDA_CONTRATO',function(contratoService,$q,$stateParams,TIPO_BUSQUEDA_CONTRATO){
              var delay = $q.defer();
              if($stateParams.numeroContrato){
                  contratoService.buscarContratosCiaPendientes(TIPO_BUSQUEDA_CONTRATO.NRO_CONTRATO,$stateParams.numeroContrato.replace(/-/g,''))
                  .then(function (data){
                      delay.resolve(data[0]);
                  });
              }
              return delay.promise;
          }]
      }
  })


 //INICIO VTADIG2-18 John Velasquez 03-10-2017
  .state('app.documentoContrato',{
    url: "/documentoContrato/:contratoId",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/documentoContrato.html",
        controller: 'documentoContratoCtrl'
      }
    },
    resolve: {
      documentoContrato: ['contratoService','$q','$stateParams',function(contratoService,$q,$stateParams){
          var delay = $q.defer();
          if($stateParams.contratoId){
              contratoService.obtenerDocumentosDeContrato($stateParams.contratoId)
              .then(function (data){
                  delay.resolve(data);
              });
          }
          return delay.promise;
      }]
    }
  })
  .state('app.documentoContratoPersona',{
    url: "/documentoContratoPersona/:contratoId/:personaId",
    cache: false,
    views: {
      'menuContent': {
        templateUrl: "views/app/documentoContratoPersona.html",
        controller: 'documentoContratoPersonaCtrl'
      }
    },
    resolve: {
      documentoContratoPersona: ['contratoService','$q','$stateParams',function(contratoService,$q,$stateParams){
          var delay = $q.defer();
          if($stateParams.contratoId){
              contratoService.obtenerDocumentosDePersona($stateParams.contratoId,$stateParams.personaId)
              .then(function (data){
                  delay.resolve(data);
              });
          }
          return delay.promise;
      }],
      configuracionArchivos: ['contratoService','$q','$stateParams',function(contratoService,$q,$stateParams){
        var delay = $q.defer();
        if($stateParams.contratoId){
            contratoService.obtenerConfiguracionArchivoContrato()
            .then(function (data){
                delay.resolve(data);
            });
        }
        return delay.promise;
    }]
    }
  })
  //FIN VTADIG2-18 John Velasquez 03-10-2017

  .state('app.nuevaIncidencia',{
    url: "/nuevaIncidencia",
    cache: false,
    views: {
        'menuContent': {
            templateUrl: "views/app/nuevaIncidencia.html",
            controller: 'nuevaIncidenciaCtrl'
        }
    },
    resolve: {
      lstPuntoDeExhibicion:['shareData','$q',function(shareData,$q){
        var delay = $q.defer();        
        delay.resolve(shareData.lstPuntoDeExhibicion);
        return delay.promise;
      }],
      lstIncidenciaTipoCategoria:['$rootScope','shareData','$q',function($rootScope,shareData,$q){
        var delay = $q.defer();        
        delay.resolve($rootScope.listaGeneral.TIPO_CATEGORIA_DESC);
        console.log($rootScope.listaGeneral.TIPO_CATEGORIA_DESC);
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
      lstFuerzaDeVenta:['$rootScope','PersonaService','$q',function($rootScope,PersonaService,$q){
        var delay = $q.defer();
        if($rootScope.me!==null && $rootScope.me!==undefined){
          if($rootScope.me.vendedoresIDs){
            PersonaService.obtenerFuerzaVenta($rootScope.me.vendedoresIDs).then(function (data){
              delay.resolve(data);
            });
            return delay.promise;
          }
        }
        return null;
      }],
    }
  })
  .state('app.misIncidencias',{
    url: "/misIncidencias",
    cache: false,
    views: {
        'menuContent': {
            templateUrl: "views/app/misIncidencias.html",
            controller: 'misIncidenciasCtrl'
        }
    },
    resolve: {
      incidencias:['$q','$stateParams','incidenciaService','INCIDENCIA',function($q,$stateParams,incidenciaService,INCIDENCIA){
        var delay = $q.defer();        
        incidenciaService.bucarIncidencias(INCIDENCIA.ESTADO.PENDIENES).then(function(data){
            delay.resolve(data);
        });
        return delay.promise;
      }],
    }
  })
  .state('app.incidenciaDetalle',{
    url: "/incidenciaDetalle/:incidenciaID",
    cache: false,
    views: {
        'menuContent': {
            templateUrl: "views/app/incidenciaDetalle.html",
            controller: 'incidenciaDetalleCtrl'
        }
    },
    resolve: {
      incidencia:['$q','$stateParams',"incidenciaService",function($q,$stateParams,incidenciaService){
        var delay = $q.defer();        
        incidenciaService.obtenerIncidencia($stateParams.incidenciaID).then(function(data){
            delay.resolve(data);
        });
        return delay.promise;
      }],
      me: ['$rootScope','loginService','$q',function($rootScope,loginService,$q){
        if($rootScope.me!==null && $rootScope.me!==undefined){
          return $rootScope.me;
        }
        var delay = $q.defer();
        loginService.me().then(function (data){
          delay.resolve(data);
          $rootScope.me = data;
        });
        return delay.promise;
      }],
    }
  })
  .state('app.incidenciaChat',{
    url: "/incidenciaChat/:incidenciaChatId",
    cache: false,
    views: {
        'menuContent': {
            templateUrl: "views/app/incidenciaChat.html",
            controller: 'incidenciaChatCtrl'
        }
    },
    resolve: {
      mensajes:['$q','$stateParams',"incidenciaService",function($q,$stateParams,incidenciaService){
        var delay = $q.defer();        
        incidenciaService.obtenerChat($stateParams.incidenciaChatId).then(function(data){
            delay.resolve(data);
        });
        return delay.promise;
      }]
    }
  })
  /*INIT - GBENITES - VD2-RQ30 - 23/07/2018*/
  /**
   * @issue IRAV3-002
   *  - set cache: false for state
   */
      .state('app.detalleRestriccion', {
    url: '/contrato/etapa/:contratoID',
    /* inicio @issue JIRAV3-002 */
    cache: false,
    /* fin @issue JIRAV3-002 */
    views: {
      'menuContent': {
        templateUrl: "views/app/detalleRestriccion.html",
        controller: 'detalleRestriccionCtrl',
        controllerAs: 'vm'
        /* inicio @issue IRAV3-002 */
        //cache: false
        /* fin @issue IRAV3-002 */
      }
    },
    resolve: {
      detalleEventoContrato:['contratoService', '$q', '$stateParams', function (contratoService, $q, $stateParams) {
        var delay = $q.defer();
        contratoService.getEtapaContrato($stateParams.contratoID)
          .then(function (data) {
            delay.resolve(data);
          });

        return delay.promise;
      }],
      documentosSinAprobacion : ['contratoService', '$q', '$stateParams', function (contratoService, $q, $stateParams) {
        var delay = $q.defer();
        contratoService.getDocumentosSinAprobacion($stateParams.contratoID)
          .then(function (data) {
            delay.resolve(data);
          });

        return delay.promise;
      }], 
      requisitosPendientes :['contratoService', '$q', '$stateParams', function (contratoService, $q, $stateParams) {
        var delay = $q.defer();
        contratoService.getRequisitosEvalPendientes($stateParams.contratoID)
          .then(function (data) {
            delay.resolve(data);
          });

        return delay.promise;
      }], 
      observaciones : ['contratoService', '$q', '$stateParams', function (contratoService, $q, $stateParams) {
        var delay = $q.defer();
        contratoService.getObservaciones($stateParams.contratoID)
          .then(function (data) {
            delay.resolve(data);
          });

        return delay.promise;
      }]
    }
  })
  /*FIN - GBENITES - VD2-RQ30 - 23/07/2018*/  
  /* INICIO - NOTEROC - 01/04/2020 
  /**
   * @feature/Mastercard
   */

  .state('app.registrarPagoMastercardObligacion',{
        url:'/registrarPagoMastercardObligacion/:obligacionID/:origenObligacionID/:vistaID',
        cache:false,
        views:{
          'menuContent':{
            templateUrl:"views/app/registrarPagoMastercardObligacion.html",
            controller:"registrarPagoMastercardObligacionCtrl"
          }
        }
  })
  /**FIN - NOTEROC - 01/04/2020 */

;

  // if none of the above states are matched, use this as the fallback
  /* inicio issue Jira-LFII-1 lamat */
  //$urlRouterProvider.otherwise('/auth/walkthrough');
  /* fin issue Jira-LFII-1 lamat */
  /* inicio issue Jira-LFII-1 lamat */
  $urlRouterProvider.otherwise('/auth/login');
  /* fin issue Jira-LFII-1 lamat */
})

;

 /*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
var blobToBase64 = function(blob, cb) {
  var reader = new FileReader();
  reader.onload = function() {
    var dataUrl = reader.result;
    var base64 = dataUrl.split(',')[1];
    cb(base64);
  };
  reader.readAsDataURL(blob);
};
 /*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
