/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |V4-HU006					 |1.1.0    |John Velasquez   |16-07-2019 | add attr PERSONA to CONFIGURACION_APP |
 * |--------------------------------------------------------------------------------------------------------|
 */
angular.module('your_app_name.config', [])
.constant('WORDPRESS_API_URL', 'https://wordpress.startapplabs.com/blog/api/')
.constant('GCM_SENDER_ID', '574597432927')
.constant('BACK_END_URL','http://192.168.1.173:9090/')
/* INICIO - NOTEROC - 01/04/2020*/
//.constant('CFG_ENVIRONMENT', 'Version-4-PASE 3.11.1')
.constant('CFG_ENVIRONMENT', 'Version-MasterCard')
/* FIN - NOTEROC - 01/04/2020*/
/* inicio issue Jira-LFII-1 lamat */
//.constant('CFG_FONDO','img/fondo.jpg')
/* fin issue Jira-LFII-1 lamat */
/* inicio issue Jira-LFII-1 lamat */
.constant('CFG_FONDO','img/nuevo-fondo-background.jpg')
/* fin issue Jira-LFII-1 lamat */
.constant('CONFIGURACION_CAMARA',{
    FOTO:{
        quality : 100,
        targetWidth: 500,
        targetHeight: 500,
        sourceType: 1,//[0]: from device library, [1]: tomar foto, [2]: camera library
        allowEdit: false,
        correctOrientation: true,
        mediaType: 0,//[0]: foto, [1]: video, [2]: todo
        destinationType: 0,//[0]Base 64
        encodingType: 0
    },
    GALERIA:{
        sourceType: 0,//[0]: from device library, [1]: tomar foto, [2]: camera library
        allowEdit: false,
        mediaType: 0,//[0]: foto, [1]: video, [2]: todo
        destinationType: 0,//[0]Base 64
    }
})
.constant('CONFIGURACION_APP',{
    INCIDENCIAS: {
        MAXIMO_IMAGENES_ADJUNTAS: 5
    }
    /** inicio @feature V4-HU006 **/
    ,PERSONA: {
        DOCUMENTO_IDENTIDAD:{
            ARCHIVOS_MINIMOS: 2
        }
    }
    /** fin @feature V4-HU006 **/
})
//INICIO EVALCRED-21
.constant('APP',{
    ID:'com.pe.pandero.movil.IngVtaApp',
    MODULO:{
        REGISTRO_PROSPECTO:{
            ID: "registroProspecto"
        },
        PAGO_A_CUENTA:{
            ID: "pagoCuenta"
        },
        INGRESO_DE_VENTA: {
            ID: "ingresoDeVenta"
        }
    }
})
//FIN EVALCRED-21
;

