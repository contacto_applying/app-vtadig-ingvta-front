/*
 * FECHA		ISSUE		RESPONSABLE
 * 20.09.2017   VTAOBS1-3	JOHN VELASQUEZ
 * */
/*
 * FECHA		ISSUE		RESPONSABLE
 * 21.09.2017   VTAOBS1-3	JOHN VELASQUEZ
 * */
var gulp = require('gulp');

/*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/

var replace = require('gulp-replace-task');  
var args    = require('yargs').argv;  
var fs      = require('fs');
  /*INICIO - modificado por John Velasquez - VTAOBS1-3 - 21.09.2017*/
  var gulp = require('gulp');
  var gutil = require('gulp-util');
  var bower = require('bower');
  var sass = require('gulp-sass');
  var sh = require('shelljs');
  var gulps = require("gulp-series");  

  var sourcemaps = require('gulp-sourcemaps');
  var templateCache = require('gulp-angular-templatecache');

  var paths = {
    sass: ['./scss/**/*.scss'],
    templatecache: ['./www/views/**/*.html']
  };
  /*FIN - modificado por John Velasquez - VTAOBS1-3 - 21.09.2017*/
/*FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/

/*INICIO - modificado por John Velasquez - VTAOBS1-3 - 21.09.2017*/

  var requireDir = require('require-dir');
  requireDir('./gulp-tasks');

  //gulp.task('default', ['sass', 'templatecache','profile']);

  /*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
  // gulp.task('profile', function () {  
  //     // Get the environment from the command line
  //     var env = args.env || 'desarrollo';
    
  //     // Read the settings from the right file
  //     var filename = env + '.json';
  //     var settings = JSON.parse(fs.readFileSync('./profiles/' + filename, 'utf8'));
      
  //   // Replace each placeholder with the correct value for the variable.  
  //   gulp.src('www/environment/config.js')  
  //     .pipe(replace({
  //       patterns: [
  //         {
  //             match: 'aws_address',
  //             replacement: settings.services.aws_address
  //         },
  //         {
  //             match: 'app_environment',
  //             replacement: settings.app.environment
  //         },
  //         {
  //             match: 'fondo_login',
  //             replacement: settings.imagenes.fondo_login
  //         }
  //       ]
  //     }))
  //     .pipe(gulp.dest('./www/js/'));

  //     gulp.src('www/environment/ionic.app.css')  
  //     .pipe(replace({
  //       patterns: [
  //         {
  //           match: 'assertiveColor',
  //           replacement: settings.colors.assertiveColor
  //         }
  //       ]
  //     }))
  //     .pipe(gulp.dest('./www/css/'));
  //   });
  /*FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/

  gulps.registerTasks({
    "sass": (function(done) {
      gulp.src('./scss/ionic.app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
          errLogToConsole: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/css/'))
        .on('end', done);
    }),
    "templatecache": (function (done) {
      gulp.src(paths.templatecache)
        .pipe(templateCache({standalone:true, filename:'views.js', module:'your_app_name.views', root:'views/'}))
        .pipe(gulp.dest('./www/js/'))
        .on('end', done);
    }),
    "watch": (function() {
      gulp.watch(paths.sass, ['sass']);
      gulp.watch(paths.templatecache, ['templatecache']);
    }),
    "profile": (function () {  
                // Get the environment from the command line
                var env = args.env || 'desarrollo';
              
                // Read the settings from the right file
                var filename = env + '.json';
                var settings = JSON.parse(fs.readFileSync('./profiles/' + filename, 'utf8'));
              // Replace each placeholder with the correct value for the variable.  
              gulp.src('www/environment/config.js')  
                .pipe(replace({
                  patterns: [
                    {
                        match: 'aws_address',
                        replacement: settings.services.aws_address
                    },
                    {
                        match: 'app_environment',
                        replacement: settings.app.environment
                    },
                    {
                        match: 'fondo_login',
                        replacement: settings.imagenes.fondo_login
                    }
                  ]
                }))
                .pipe(gulp.dest('./www/js/'));
  
                gulp.src('www/environment/ionic.app.css')  
                .pipe(replace({
                  patterns: [
                    {
                      match: 'assertiveColor',
                      replacement: settings.colors.assertiveColor
                    }
                  ]
                }))
                .pipe(gulp.dest('./www/css/'));
              })
  });

  gulps.registerSeries("default", ["sass","templatecache", "watch"]);

/*FIN - modificado por John Velasquez - VTAOBS1-3 - 21.09.2017*/
