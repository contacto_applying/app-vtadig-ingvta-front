package pe.beyond.visanet.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import pe.beyond.visanet.events.recurrence.RecurrenceActivity;
import pe.beyond.visanet.manager.Constants;
import pe.beyond.visanet.manager.MPOSError;
import pe.beyond.visanet.manager.MPOSHelper;
import pe.beyond.visanet.manager.MPOSManagerSession;
import pe.beyond.visanet.manager.MPOSMerchant;
import pe.beyond.visanet.manager.MPOSResponseBean;
import pe.beyond.visanet.manager.MPOSSessionObject;
import pe.beyond.visanet.manager.listener.MPOSAuthorizationListener;
import pe.beyond.visanet.manager.listener.MPOSCancellationListener;
import pe.beyond.visanet.manager.listener.MPOSHistoryListener;
import pe.beyond.visanet.manager.listener.MPOSLoginListener;
import pe.beyond.visanet.manager.listener.MPOSRecurrenceListener;
import android.util.Log;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import java.math.BigDecimal;
import com.google.gson.Gson;

public class VisanetPlugin extends CordovaPlugin{

	private MPOSManagerSession manager;
	private CallbackContext callbackContext;
	private String visanetUrl;
	private String visanetApiKey;
	private boolean isVoucherRequired;

	@Override
	public void initialize (CordovaInterface initCordova, CordovaWebView webView) {
		log("initialize");
		cordova = initCordova;
		super.initialize (cordova, webView);
	}

	@Override
	public void onDestroy() {
		if (manager != null) {
			manager.disconnect();
		}
	}

	protected void pluginInitialize() {
		try{
			log("initializing");
			MPOSSessionObject.IS_LOG_ENABLED = true;
			this.visanetUrl = preferences.getString("VISANET_URL", "");
			this.visanetApiKey = preferences.getString("VISANET_API_KEY", "");
			manager = new MPOSManagerSession(cordova.getActivity(),
					this.visanetUrl, this.visanetApiKey);
			manager.setIsVoucherRequired(true);
		}catch(Exception e){
			Log.d("test",e.getStackTrace().toString());
			e.printStackTrace();
		}

	}

	@Override
	public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException{
		if(this.manager == null){
			this.manager = new MPOSManagerSession(cordova.getActivity(), this.visanetUrl, this.visanetApiKey);
			this.manager.setIsVoucherRequired(this.isVoucherRequired);
		}
		this.callbackContext = callbackContext;

		try{
			if("setVoucherRequired".equals(action)){
				boolean isVoucherRequired = args.getBoolean(0);
				this.setVoucherRequired(isVoucherRequired);
			}else{
				cordova.setActivityResultCallback (this);
				if ("authorize".equals(action)) {
					float amount = BigDecimal.valueOf(args.getDouble(0)).floatValue();
					String reference = args.getString(1);
					String email = args.getString(2);
					this.authorize(amount, reference, email);
				} else if("cancel".equals(action)){
					String reference = args.getString(0);
					this.cancel(reference);
				}else if("report".equals(action)){
					this.reports();
				}else{
					return false;
				}
			}
			return true;
		}catch(Exception e){
			log(e.getMessage());
			e.printStackTrace();
			callbackContext.error("Error in library "+ e.getMessage());
			return false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		log("onActivityResult ");
		log("resultCode "+resultCode);
		manager.parseResult(requestCode, resultCode, intent);
	}

	private void authorize(float amount, String reference, String email) throws Exception{
		manager.authorize(cordova.getActivity(), amount, reference, email, new MPOSAuthorizationListener()
		{
			public void mPOSAuthorization(MPOSResponseBean paramAnonymousMPOSResponseBean)
			{
				log("mPOSAuthorizationmPOSAuthorizationmPOSAuthorization");
				log(paramAnonymousMPOSResponseBean.toString());
				callbackContext.success(toJson(paramAnonymousMPOSResponseBean));
			}

			public void mPOSAuthorizationError(MPOSError paramAnonymousMPOSError)
			{
				log("mPOSAuthorizationErrormPOSAuthorizationErrormPOSAuthorizationError");
				log(paramAnonymousMPOSError.getMessage());
				callbackContext.error(toJson(paramAnonymousMPOSError));
			}
		});
	}

	private void cancel(String reference) throws Exception{
			manager.cancellation(cordova.getActivity(), reference, new MPOSCancellationListener()
			{
				public void mPOSCancellation(MPOSResponseBean paramAnonymousMPOSResponseBean)
				{
					log("mPOSCancellationmPOSCancellation");
					callbackContext.success(toJson(paramAnonymousMPOSResponseBean));
				}

				public void mPOSCancellationError(MPOSError paramAnonymousMPOSError)
				{
					log("mPOSCancellationErrormPOSCancellationError");
					callbackContext.error(toJson(paramAnonymousMPOSError));
				}
			});
	}

	private void reports() throws Exception{
		manager.history(cordova.getActivity(), new MPOSHistoryListener()
		{
			public void mPOSCancellationComplete(MPOSResponseBean paramAnonymousMPOSResponseBean) {
				log("mPOSCancellationComplete");
				callbackContext.success(toJson(paramAnonymousMPOSResponseBean));
			}

			public void mPOSGetVoucher(MPOSResponseBean paramAnonymousMPOSResponseBean)
			{
				log("mPOSGetVouchermPOSGetVoucher");
				callbackContext.success(toJson(paramAnonymousMPOSResponseBean));
			}

			public void mPOSHistoryError(MPOSError paramAnonymousMPOSError)
			{
				log("history mposErrormposErrormposError");
				callbackContext.error(toJson(paramAnonymousMPOSError));
			}
		});
	}

	private void setVoucherRequired(boolean isVoucherRequired) throws Exception{
		if(this.manager != null){
			this.manager.setIsVoucherRequired(isVoucherRequired);
			this.isVoucherRequired = isVoucherRequired;
			String status = (isVoucherRequired) ? "activado":"desactivado";
			callbackContext.success("Voucher requerido "+status);
		}else{
			callbackContext.error("manager was not initialized");
		}
	}

	private void log(String message){
		final String LIB_TAG = "VISANET";
		Log.d(LIB_TAG, message);
	}

	private String toJson(Object object){
		Gson gson = new Gson();
		return gson.toJson(object);
	}
}