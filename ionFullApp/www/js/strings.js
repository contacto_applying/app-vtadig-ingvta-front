angular.module('your_app_name.strings', [])
.constant('STRINGS',{
    INCIDENCIA:{
        ESTADO:{
            EN_PROGRESO: 'En progreso',
            PENDIENTE: 'Pendiente',
            RESUELTO: 'Resuelta'
        }
    }
});