/*
 * FECHA		ISSUE		RESPONSABLE
 * 20.09.2017   VTAOBS1-3	JOHN VELASQUEZ
 * */
/*
 * FECHA		ISSUE		RESPONSABLE
 * 21.09.2017   VTAOBS1-3	JOHN VELASQUEZ
 * */
/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |V4-HU002                     |1.0.1    |John Velasquez   |04.07.2019 | Mejor manejo de las variables y se agrega inforamción del dispositivo en los post request|
 * |--------------------------------------------------------------------------------------------------------|
 */

angular.module('your_app_name.factories', [])

.factory('FeedLoader', function ($resource){
  return $resource('https://ajax.googleapis.com/ajax/services/feed/load', {}, {
    fetch: { method: 'JSONP', params: {v: '1.0', callback: 'JSON_CALLBACK'} }
  });
})


// Factory for node-pushserver (running locally in this case), if you are using other push notifications server you need to change this
.factory('NodePushServer', function ($http){
  // Configure push notifications server address
  // 		- If you are running a local push notifications server you can test this by setting the local IP (on mac run: ipconfig getifaddr en1)
  var push_server_address = "http://192.168.1.102:8000";

  return {
    // Stores the device token in a db using node-pushserver
    // type:  Platform type (ios, android etc)
    storeDeviceToken: function(type, regId) {
      // Create a random userid to store with it
      var user = {
        user: 'user' + Math.floor((Math.random() * 10000000) + 1),
        type: type,
        token: regId
      };
      console.log("Post token for registered device with data " + JSON.stringify(user));

      $http.post(push_server_address+'/subscribe', JSON.stringify(user))
      .success(function (data, status) {
        console.log("Token stored, device is successfully subscribed to receive push notifications.");
      })
      .error(function (data, status) {
        console.log("Error storing device token." + data + " " + status);
      });
    },
    // CURRENTLY NOT USED!
    // Removes the device token from the db via node-pushserver API unsubscribe (running locally in this case).
    // If you registered the same device with different userids, *ALL* will be removed. (It's recommended to register each
    // time the app opens which this currently does. However in many cases you will always receive the same device token as
    // previously so multiple userids will be created with the same token unless you add code to check).
    removeDeviceToken: function(token) {
      var tkn = {"token": token};
      $http.post(push_server_address+'/unsubscribe', JSON.stringify(tkn))
      .success(function (data, status) {
        console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
      })
      .error(function (data, status) {
        console.log("Error removing device token." + data + " " + status);
      });
    }
  };
})


.factory('AdMob', function ($window){
  var admob = $window.AdMob;

  if(admob)
  {
    // Register AdMob events
    // new events, with variable to differentiate: adNetwork, adType, adEvent
    document.addEventListener('onAdFailLoad', function(data){
      console.log('error: ' + data.error +
      ', reason: ' + data.reason +
      ', adNetwork:' + data.adNetwork +
      ', adType:' + data.adType +
      ', adEvent:' + data.adEvent); // adType: 'banner' or 'interstitial'
    });
    document.addEventListener('onAdLoaded', function(data){
      console.log('onAdLoaded: ' + data);
    });
    document.addEventListener('onAdPresent', function(data){
      console.log('onAdPresent: ' + data);
    });
    document.addEventListener('onAdLeaveApp', function(data){
      console.log('onAdLeaveApp: ' + data);
    });
    document.addEventListener('onAdDismiss', function(data){
      console.log('onAdDismiss: ' + data);
    });

    var defaultOptions = {
      // bannerId: admobid.banner,
      // interstitialId: admobid.interstitial,
      // adSize: 'SMART_BANNER',
      // width: integer, // valid when set adSize 'CUSTOM'
      // height: integer, // valid when set adSize 'CUSTOM'
      position: admob.AD_POSITION.BOTTOM_CENTER,
      // offsetTopBar: false, // avoid overlapped by status bar, for iOS7+
      bgColor: 'black', // color name, or '#RRGGBB'
      // x: integer,		// valid when set position to 0 / POS_XY
      // y: integer,		// valid when set position to 0 / POS_XY
      isTesting: true, // set to true, to receiving test ad for testing purpose
      // autoShow: true // auto show interstitial ad when loaded, set to false if prepare/show
    };
    var admobid = {};

    if(ionic.Platform.isAndroid())
    {
      admobid = { // for Android
        banner: 'ca-app-pub-6869992474017983/9375997553',
        interstitial: 'ca-app-pub-6869992474017983/1657046752'
      };
    }

    if(ionic.Platform.isIOS())
    {
      admobid = { // for iOS
        banner: 'ca-app-pub-6869992474017983/4806197152',
        interstitial: 'ca-app-pub-6869992474017983/7563979554'
      };
    }

    admob.setOptions(defaultOptions);

    // Prepare the ad before showing it
    // 		- (for example at the beginning of a game level)
    admob.prepareInterstitial({
      adId: admobid.interstitial,
      autoShow: false,
      success: function(){
        console.log('interstitial prepared');
      },
      error: function(){
        console.log('failed to prepare interstitial');
      }
    });
  }
  else
  {
    console.log("No AdMob?");
  }

  return {
    showBanner: function() {
      if(admob)
      {
        admob.createBanner({
          adId:admobid.banner,
          position:admob.AD_POSITION.BOTTOM_CENTER,
          autoShow:true,
          success: function(){
            console.log('banner created');
          },
          error: function(){
            console.log('failed to create banner');
          }
        });
      }
    },
    showInterstitial: function() {
      if(admob)
      {
        // If you didn't prepare it before, you can show it like this
        // admob.prepareInterstitial({adId:admobid.interstitial, autoShow:autoshow});

        // If you did prepare it before, then show it like this
        // 		- (for example: check and show it at end of a game level)
        admob.showInterstitial();
      }
    },
    removeAds: function() {
      if(admob)
      {
        admob.removeBanner();
      }
    }
  };
})

.factory('iAd', function ($window){
  var iAd = $window.iAd;

  // preppare and load ad resource in background, e.g. at begining of game level
  if(iAd) {
    iAd.prepareInterstitial( { autoShow:false } );
  }
  else
  {
    console.log("No iAd?");
  }

  return {
    showBanner: function() {
      if(iAd)shareData
      {
        // show a default banner at bottom
        iAd.createBanner({
          position:iAd.AD_POSITION.BOTTOM_CENTER,
          autoShow:true
        });
      }
    },
    showInterstitial: function() {
      // ** Notice: iAd interstitial Ad only supports iPad.
      if(iAd)
      {
        // If you did prepare it before, then show it like this
        // 		- (for example: check and show it at end of a game level)
        iAd.showInterstitial();
      }
    },
    removeAds: function() {
      if(iAd)
      {
        iAd.removeBanner();
      }
    }
  };
})

.factory('shareData', function (){
  return {
    lstTipoDocumento: [], 
    mapTipoDocumento: {},
		TIPO_DOCUMENTO : {
			RUC: '8',
			DNI: '4',
			PAS: '3',
			CE: '2'
		},    
    lstSexo: [{'codigo':'1','descripcion':'MASCULINO'},
               {'codigo':'2','descripcion':'FEMENINO'}],
    //INICIO VTADIG2-185- José Martinez 12/02/2018               
    lstTipoMoneda: [{'codigo':1,'descripcionLarga':'SOLES'},
               {'codigo':2,'descripcionLarga':'DOLARES'}],               
    //FIN VTADIG2-185- José Martinez 12/02/2018               
    lstTipoBien: [{'codigo':1,'descripcion':'Inmueble'},
               {'codigo':2,'descripcion':'Vehiculo'}],
    lstDepartamento: [],
    lstProvincia: [],
    lstDistrito: [],
    lstTipoZona: [],
    lstTipoVia: [],
    lstSmvBienesServicios: [],
    mapSmvBienesServicios:{},
    lstTiposTrabajador:[{'codigo':'d','descripcion':'Dependiente'},{'codigo':'i','descripcion':'Independiente'},{'codigo':'n','descripcion':'No Remunerado'}],
    lstEstadoCivil: [],
    lstModalidadDevolucion: [],
    lstMedioComunicacionPreferente: [],
    lstBancos: [],
    lstActividadLaboral: [],
    
    lstPuntoDeExhibicion :[],
    lstTelefonos:[],
    lstPersonaOcupacion:[],
    lstTiposCorreo:[{'codigo':1,'descripcion':'Personal'},{'codigo':2,'descripcion':'Laboral'}],
    lstUsosVehiculo:[{'codigo':1,'descripcion':'Transporte Público'},{'codigo':2,'descripcion':'Servicio de Taxi'},{'codigo':3,'descripcion':'Particular'}],
    /**BORRAR DESDE ACA */
    lstIncidenciaTipoCategoria: [{'codigo':1, 'descripcion': 'Automóvil'},{'codigo':2, 'descripcion': 'Diseño Gráfico'},{'codigo':3, 'descripcion': 'Genérica'}],
    lstIncidenciaCategoria: [{'codigo':1, 'descripcion': 'INF-Falta su sombrilla'},{'codigo':2, 'descripcion': 'INF-Publicidad Desactualizada'},{'codigo':3, 'descripcion': 'INF-Estructura en mal estado'},{'codigo':4, 'descripcion': 'INF-Llaves de módulos'}],
    vendedores: [
      {"personaID":212,"tipoDocumentoID":"4","personaCodigoDocumento":"40754196","personaNombre":"ADA CATHERINE","personaApellidoPaterno":"HILARES","personaApellidoMaterno":"CAMARENA","personaNombreCompleto":"HILARES CAMARENA ADA CATHERINE","juridico":false,"personaCorreoTrabajo":"soporte.app.venta@pandero.com.pe","esTitularPrincipal":false,"excedePorcentajeMaximoVinculados":false,"excedeVmcaAsociados":false},
      {"personaID":415,"tipoDocumentoID":"4","personaCodigoDocumento":"08253640","personaNombre":"ADOLFO ANGEL","personaApellidoPaterno":"ARTICA","personaApellidoMaterno":"OLIVOS","personaNombreCompleto":"ARTICA OLIVOS ADOLFO ANGEL","juridico":false,"personaCorreoTrabajo":"soporte.app.venta@pandero.com.pe","esTitularPrincipal":false,"excedePorcentajeMaximoVinculados":false,"excedeVmcaAsociados":false},
      {"personaID":422,"tipoDocumentoID":"4","personaCodigoDocumento":"22194528","personaNombre":"ADOLFO BELIZARIO","personaApellidoPaterno":"CANALES","personaApellidoMaterno":"HUAMANI","personaNombreCompleto":"CANALES HUAMANI ADOLFO BELIZARIO","juridico":false,"personaCorreoTrabajo":"soporte.app.venta@pandero.com.pe","esTitularPrincipal":false,"excedePorcentajeMaximoVinculados":false,"excedeVmcaAsociados":false}
    ]
    /**HASTA ACA */
  };
})

.factory('tempProspecto',function(){
  return {
    prospecto: {},
    
  };
})

.factory('tempProforma',function(){
  return {
    proforma:{
      vtaProformas: [],
      perPuntoExhibicion:{}
    }
  };
})
.factory('tempProformaDetalle',function(){
  return {
    proforma:{
      vtaProformas: []
    }
  };
})
.factory('tempContrato',function(){
  return {
    contrato:{},
    titulares:[],
  };
})
.factory('tempTitular',function(){
  return {
    titular:{},
  };
})

.factory('numContrato',function(){
  return {
    numContrato:"",
  };
})

.factory('indiceTitular',function(){
  return 0;
})

.factory('listaGeneral',function(){
  return {
    listaGeneral:[],
  };
})
/**
 * 
 * Mejor manejo de las variables y se agrega inforamción del dispositivo en los post request y Content-Type application/x-www-form-urlencoded;charset=utf-8 => /oauth/token
 * 
 * @feature V4-HU002
 */
.factory('generalInterceptor', function ($q, $sessionStorage, $location, $injector, $timeout, BACK_END_URL,$sessionStorage) {
   //var requestInitiated;
   var requestCount = 0;
  return {
    request: function (config) {
      if(!config.url.endsWith('.html')&&
         !config.url.endsWith('.js')&&
         !config.url.endsWith('.css')&&
         !config.url.endsWith('.ttf')&&
         !config.url.endsWith('.woff')&&
         !config.url.endsWith('.woff2')&&
         !config.url.endsWith('.exp')){
        //requestInitiated = true;
        requestCount = requestCount + 1;
        $injector.get("$ionicLoading").show();
        /** inicio @feature V4-HU002 **/
        
        config.data = config.data || {};
        config.params = config.params || {};
        
        if($sessionStorage.token) config.params['access_token'] = $sessionStorage.token;
        if($sessionStorage.appVersion) config.params['appVersion'] = $sessionStorage.appVersion;
        //config.params['appVersion'] = '3.8.1';

        if (config.method.toLowerCase() === 'post' && config.headers["Content-Type"] === "application/x-www-form-urlencoded;charset=utf-8") {
          if (!config.data.indexOf("?")<0) config.data += "?";
          if ($sessionStorage.simInfo && $sessionStorage.simInfo.phoneNumber) config.data += "&celular=" + $sessionStorage.simInfo.phoneNumber;
          if ($sessionStorage.simInfo && $sessionStorage.simInfo.deviceId) config.data += "&imei=" + $sessionStorage.simInfo.deviceId;
          if ($sessionStorage.deviceInfo && $sessionStorage.deviceInfo.manufacturer) config.data += "&marca=" + $sessionStorage.deviceInfo.manufacturer;
          if ($sessionStorage.deviceInfo && $sessionStorage.deviceInfo.model) config.data += "&modelo=" + $sessionStorage.deviceInfo.model;
        }
      }
      return config || $q.when(config);
      /** fin @feature V4-HU002 **/
    },
    response: function (response) {
      
      if(!response.config.url.endsWith('.html')&&
         !response.config.url.endsWith('.js')&&
         !response.config.url.endsWith('.css')&&
         !response.config.url.endsWith('.ttf')&&
         !response.config.url.endsWith('.woff')&&
         !response.config.url.endsWith('.woff2')&&
         !response.config.url.endsWith('.exp')){
        //requestInitiated = false;
        requestCount = requestCount - 1;
        $timeout(function() {
          if(requestCount<=0) {            
            $injector.get("$ionicLoading").hide();
          }    
        },300);            
      }
      if (response.status == 401) {
        location.href = '';
      }
      return response;
    },
    responseError: function (rejection) {
      if(!rejection.config.url.endsWith('.html')&&
         !rejection.config.url.endsWith('.js')&&
         !rejection.config.url.endsWith('.css')&&
         !rejection.config.url.endsWith('.ttf')&&
         !rejection.config.url.endsWith('.woff')&&
         !rejection.config.url.endsWith('.woff2')&&
         !rejection.config.url.endsWith('.exp')){
        //requestInitiated = false;
        requestCount = 0;
        
        $sessionStorage.ultimoError = rejection.data
        $injector.get("$ionicLoading").hide();
      
        var $state = $injector.get('$state');
        if(rejection.status == -1){
          $state.go('noConnection');        
        }
        /*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
        else if(rejection.status == 505){          
          $state.go('versionNotSupported');
        }
        /*FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
        else if (rejection.status >= 500 || rejection.status == 417) {
          var type = 'warning';
          var message = 'Error inesperado';
          if (rejection.data.message != null && rejection.data.message !== undefined) {
            type = rejection.data.type;
            message = rejection.data.message;
          }

          var popup = $injector.get('$ionicPopup');
          var alertPopup = popup.alert({ title: 'Alerta', template: message , okType: 'button-assertive'});
          $q.reject(rejection);
        } else if (rejection.status == 403) {
          location.href ='#/app/accessDenied';
        } else if (rejection.status == 401) {
          location.href = '';
        } /*else if (rejection.status >= 400) { 
          var type = 'warning';
          var message = 'Error inesperado';
          if (rejection.data.message != null && rejection.data.message !== undefined) {
            type = rejection.data.type;
            message = rejection.data.message;
          }

          var popup = $injector.get('$ionicPopup');
          var alertPopup = popup.alert({ title: 'Alerta', template: message , okType: 'button-assertive'});
        }*/
      }

      return rejection;
    }

  };
})
;
