angular.module('your_app_name').controller('SettingsController', function ($ionicPlatform, $scope, $ionicModal, StorageService) {

  var onSuccess = function (success) {
    alert(success);
    StorageService.saveVoucherRequired($scope.config.isVoucherRequired);
    //...
  };

  var onError = function (error) {
    alert(error);
    //...
  };

  $scope.config = {
    isVoucherRequired : StorageService.getVoucherRequired()
  };

  $ionicModal.fromTemplateUrl('modal.html', {
    scope : $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });

  $scope.openSettingsModal = function () {
    $scope.config.isVoucherRequired = StorageService.getVoucherRequired();
    $scope.modal.show();
  };

  $scope.hideSettingsModal = function () {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $ionicPlatform.ready(function () {
    $scope.setVoucherRequired = function () {
      window.visanet.setVoucherRequired($scope.config.isVoucherRequired,
        onSuccess, onError);
    };
  });

});


