/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |VTAOBS1-3                    |1.0      |John Velasquez   |20.09.2017 |                                  |
 * |VTADIG2-504					 |1.0      |John Velasquez   |01-08-2018 | integración débito automático    |
 * |V4-HU002                     |1.0.2    |John Velasquez   |04.07.2019 | authService -> credenciales se envian en el cuerpo de la solicitud|
 * |V4-HU003                     |1.1.0    |John Velasquez   |12.07.2019 | add proformaService->obtenerMediosDeValidacion, proformaService->enviarValidacionProforma, proformaService->validarProforma
 * |V4-HU006					 |1.1.0    |John Velasquez   |16-07-2019 | agregar funciones para la gestión de los documentos de las personas, add service base64Utils |
 * |V4-HU010					 |1.1.0    |John Velasquez   |30.07.2019 | aggregar funciones recuperación de contratos pendientes de cia y envio de recordatorio de pago de cía |
 * |V4-HU011                     |1.0.0    |John Velasquez |16/08/2019 | add service simuladorPremiosComisionesService|
 * |V4-HU012					 |1.0      |John Velasquez   |13-08-2019 | add 'reporteComisionService'    |
 * |V4-HU016                     |1.1.0    |John velasquez   |25/07/2019 | agregar service {@link #estadisticasAsmbleaRematesService}  |
 * |V4-HU020                     |1.0.1   |John Velasquez   |12-07-2019 | cliente no domiciliado |
 * |PL-20						 |1.0		|Nataly Otero	 |03/11/2020	| integración pago link	|
 * |--------------------------------------------------------------------------------------------------------|
 */
angular.module('your_app_name.services', [])

.service('FeedList', function ($rootScope, FeedLoader, $q){
	this.get = function(feedSourceUrl) {
		var response = $q.defer();
		//num is the number of results to pull form the source
		FeedLoader.fetch({q: feedSourceUrl, num: 20}, {}, function (data){
			response.resolve(data.responseData);
		});
		return response.promise;
	};
})


// PUSH NOTIFICATIONS
/*.service('PushNotificationsService', function ($rootScope, $cordovaPush, NodePushServer, GCM_SENDER_ID){
	this.register = function() {
		var config = {};

		// ANDROID PUSH NOTIFICATIONS
		if(ionic.Platform.isAndroid())
		{
			config = {
				"senderID": GCM_SENDER_ID
			};

			$cordovaPush.register(config).then(function(result) {
				// Success
				console.log("$cordovaPush.register Success");
				console.log(result);
			}, function(err) {
				// Error
				console.log("$cordovaPush.register Error");
				console.log(err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(JSON.stringify([notification]));
				switch(notification.event)
				{
					case 'registered':
						if (notification.regid.length > 0 ) {
							console.log('registration ID = ' + notification.regid);
							NodePushServer.storeDeviceToken("android", notification.regid);
						}
						break;

					case 'message':
						if(notification.foreground == "1")
						{
							console.log("Notification received when app was opened (foreground = true)");
						}
						else
						{
							if(notification.coldstart == "1")
							{
								console.log("Notification received when app was closed (not even in background, foreground = false, coldstart = true)");
							}
							else
							{
								console.log("Notification received when app was in background (started but not focused, foreground = false, coldstart = false)");
							}
						}

						// this is the actual push notification. its format depends on the data model from the push server
						console.log('message = ' + notification.message);
						break;

					case 'error':
						console.log('GCM error = ' + notification.msg);
						break;

					default:
						console.log('An unknown GCM event has occurred');
						break;
				}
			});

			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
			//   // Success!
			// }, function(err) {
			//   // Error
			// });
		}

		if(ionic.Platform.isIOS())
		{
			config = {
				"badge": true,
				"sound": true,
				"alert": true
			};

			$cordovaPush.register(config).then(function(result) {
				// Success -- send deviceToken to server, and store for future use
				console.log("result: " + result);
				NodePushServer.storeDeviceToken("ios", result);
			}, function(err) {
				console.log("Registration error: " + err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(notification.alert, "Push Notification Received");
			});
		}
	};
})
*/

// BOOKMARKS FUNCTIONS
.service('BookMarkService', function (_, $rootScope){

	this.bookmarkFeedPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_feed_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_feed_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.link == bookmark_post.link; });

		if(!existing_post){
			user_bookmarks.push({
				link: bookmark_post.link,
				title : bookmark_post.title,
				date: bookmark_post.publishedDate,
				excerpt: bookmark_post.contentSnippet
			});
		}

		window.localStorage.ionFullApp_feed_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.bookmarkWordpressPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_wordpress_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.id == bookmark_post.id; });

		if(!existing_post){
			user_bookmarks.push({
				id: bookmark_post.id,
				title : bookmark_post.title,
				date: bookmark_post.date,
				excerpt: bookmark_post.excerpt
			});
		}

		window.localStorage.ionFullApp_wordpress_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.getBookmarks = function(){
		return {
			feeds : JSON.parse(window.localStorage.ionFullApp_feed_bookmarks || '[]'),
			wordpress: JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks || '[]')
		};
	};
})


// WP POSTS RELATED FUNCTIONS
.service('PostService', function ($rootScope, $http, $q, WORDPRESS_API_URL){

	this.getRecentPosts = function(page) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_recent_posts/' +
		'?page='+ page +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};


	this.getPost = function(postId) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_post/' +
		'?post_id='+ postId +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.shortenPosts = function(posts) {
		//we will shorten the post
		//define the max length (characters) of your post content
		var maxLength = 500;
		return _.map(posts, function(post){
			if(post.content.length > maxLength){
				//trim the string to the maximum length
				var trimmedString = post.content.substr(0, maxLength);
				//re-trim if we are in the middle of a word
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf("</p>")));
				post.content = trimmedString;
			}
			return post;
		});
	};

	this.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};

})
/*INICIO INTEGRACION EC*/
//.service('ProformaDetailService', function ($rootScope, $http, $q, BACK_END_URL){
.service('ProformaDetailService', function ($rootScope, $http, $q, BACK_END_URL, APP){
/*FIN INTEGRACION EC*/
	return {
		/*INICIO INTEGRACION EC*/
		//obtenerDetalleProformaContrato : function(id){
		obtenerDetalleProformaContrato : function(id, modulo){
		/*FIN INTEGRACION EC*/
			var delay = $q.defer();
			/*INICIO INTEGRACION EC*/
			//$http.get(BACK_END_URL+'detalleProformaContrato/'+id)
			//.then(function(response){
			var uri = URI(BACK_END_URL+'detalleProformaContrato/'+id)
						.addSearch("modulo", modulo)
						.addSearch("aplicacion", APP.ID)
						.toString();
			$http.get(uri).then(function(response){
			/*FIN INTEGRACION EC*/
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
				/** INICIO ticket-44914 Anderson Estela Coronel 29-oct-2020 */
				if(response.status === 422){
					delay.resolve(response.data);
				}
				/** FIN ticket-44914 Anderson Estela Coronel 29-oct-2020 */
			});		
		    return delay.promise;
	  },
	   obtenerDetalleProforma : function(id){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'detalleProforma/'+id)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{       
						var proforma = response.data.data;
						proforma.articuloID= proforma.articuloID+'';
						proforma.grupoID= proforma.grupoID+'';
						proforma.marcaID= proforma.marcaID+'';
						proforma.productoID= proforma.productoID+'';
						proforma.tipoBienID= proforma.tipoBienID+'';
						delay.resolve(proforma);
					}
				}
			});		
		    return delay.promise;
	  }	  
	};
})

.service('ProgramaProductoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		/** INICIO APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */
		//getProgramaProducto : function(){
		getProgramaProducto : function(tipoBien){
		/** FIN APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */
			var delay = $q.defer();
			/** INICIO APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */
			//$http.get(BACK_END_URL+'programaProducto) 
			$http.get(BACK_END_URL+'programaProducto?tipoBien='+tipoBien)
			/** FIN APPVENTA-71 Anderson Estela Coronel - 02/dic/2020 */               		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
	  },
	  getTiposDeBienPorProducto: function(productoId){
			var delay = $q.defer();			
			$http.get(BACK_END_URL+'tipoBien?productoId='+productoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
	  },
	  getTiposDeBienPorUnidadNegocio: function(){		  	
			var delay = $q.defer();			
			$http.get(BACK_END_URL+'tipoBienProforma')
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
	  }		  
	};
})
.service('GrupoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
	  getByProductoId : function(programaProductoID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'grupo?productoID='+programaProductoID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getCertificadosByGroupId : function(groupID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'certificado?grupoID='+groupID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	};
})

.service('MarcaService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		getMarca : function(productoID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'marca?productoId='+productoID)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
		getModelos : function(marcaID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'articulo?marcaId='+marcaID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }	  
	};
})

.service('ListaGeneralService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		getListaGeneral : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'listaGeneral')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getPuntosDeVenta : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'puntoDeVenta')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }  
	};
})
.service('VinculadoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		obtenerEmpleados : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'empleado')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  } 
	  /* INICIO ticket 44101 Anderson Estela Coronel 08/oct/2020 */
	   ,obtenerEmpleadosVinculado : function(){
		var delay = $q.defer();
		$http.get(BACK_END_URL+'vinculado')                		
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
		} 
	 /* FIN ticket 44101 Anderson Estela Coronel 08/oct/2020 */
	};
})

/*INICIO INTEGRACION EC*/
/**
 * @feature V4-HU006
 * - add functions obtenerFilesDocumentoIdentidad, guardarFileDocumentoIdentidad, eliminarFileDocumentoIdentidad
 */
.service('PersonaService', function (tempContrato,$rootScope, $http, $q, BACK_END_URL,$ionicPopup, APP){
/*FIN INTEGRACION EC*/
	return {
		getDataPDF417 : function (data){
	        var colLength = 16;
	        var rowLimit = 7;
	        var dataToParse =  data.substring(0,colLength * rowLimit);
	        var DNI = dataToParse.substring(2,10);
	        var apellidoPaterno = "", apellidoMaterno = "", nombres = "";
			var colLength = 16, rowLimit = 7, initNamePosition = 10;
			for(var i=initNamePosition;i<(colLength*rowLimit);i++){
				if(i<=(colLength*3) + 1){
					apellidoPaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*3) + 1 && i<=(colLength*5) + 9){
					apellidoMaterno+=dataToParse.charAt(i);
				}
				
				if(i>(colLength*5) + 9 ){
					nombres+=dataToParse.charAt(i);
				}
			}
			var dataPDF147 = {};
			dataPDF147.nombres = nombres.trim();
			dataPDF147.apellidoPaterno = apellidoPaterno.trim();
			dataPDF147.apellidoMaterno = apellidoMaterno.trim();
			return dataPDF147;
		}
		/*INICIO INTEGRACION EC*/
		//,obtenerPersona : function(tipo,numero){
		,obtenerPersona : function(tipo,numero, modulo){
		/*FIN INTEGRACION EC*/
			var delay = $q.defer();
			/*INICIO INTEGRACION EC*/
			//$http.get(BACK_END_URL+'persona?tipo='+tipo+'&nro='+numero)
			var uri = URI(BACK_END_URL+'persona')
						.addSearch("tipo", tipo)
						.addSearch("nro", numero)
						.addSearch("modulo", modulo)
						.addSearch("aplicacion", APP.ID)
						.toString();
			$http.get(uri)
			/*FIN INTEGRACION EC*/
			.then(function(response){
				if(response.status === 200 || response.status === 417){
					if(response.data.code!==100 && response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  validaPersona : function(tipo,numero,grupoID, certificadoPosicionID){
			var delay = $q.defer();
			var url = BACK_END_URL+'validapersona?tipo='+tipo+'&nro='+numero;
			if(grupoID!==undefined&&certificadoPosicionID!==undefined){
				url = url+'&grupoID='+grupoID+"&certificadoPosicionID="+certificadoPosicionID;
			}
			$http.get(url)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  existeComoTitular : function(tipo,numero){
		var existe = false;
		//buscar en titulares
		angular.forEach(tempContrato.titulares,function(item){
			if( tipo == item.tipoDocumentoID && numero.toLowerCase()==item.personaCodigoDocumento.toLowerCase()){
				var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Persona con documento "+ numero+" ya está agregado como titular." , okType: 'button-assertive'});
				existe = true;
			}
			//si no lo encontramos en titulares
			if(!existe){
				//lo buscamos en los relacionados del titular
				angular.forEach(item.personasRelacionadas,function(relacionada){
					if( tipo == relacionada.tipoDocumentoID && numero.toLowerCase()==relacionada.personaCodigoDocumento.toLowerCase()){
						var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Persona con documento "+ numero +" ya está agregado como Relacionado de " +item.personaNombreCompleto , okType: 'button-assertive'});
						existe = true;
					}
				});
			}			
		});
		return existe;
	  },
	  obtenerFuerzaVenta: function(listaEDV){
		let parameters = {
			listaEDV: listaEDV
		};
		let config = {
			params: parameters
		};
		var delay = $q.defer();
		$http.get(BACK_END_URL+'listaFuerzaVenta',config)                		
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
					//swal({title: '',type: response.data.type,text: response.data.message});
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
	}

		/**
		 * 
		 * obtiene lista de los archivos del documento de la persona
		 * identificada por un tipo de documento y un codigo de documento
		 * 
		 * @feature V4-HU006
		 * @param tipoDocumento: tipo de documento
		 * @param documento: codigo de documento
		 * @param conData: indica si se incluye la data en base64
		 * @returns {*}
		 */
		, obtenerFilesDocumentoIdentidad: function(tipoDocumento, documento, conData){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'persona/file/documentoIdentidad')
				.addSearch("tipoDocumento", tipoDocumento)
				.addSearch("documento", documento)
				.addSearch("withData", conData || false)
				.toString();
			$http.get(uri)
				.then(function(response){
					if(response.status === 200 || response.status === 417){
						if(response.data.code!==100 && response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		},
		/**
		 * 
		 * incluye un archivo al repositorio de documento de identidad de la persona
		 * 
		 * @feature V4-HU006
		 * @param tipoDocumento
		 * @param documento
		 * @param base64File
		 * @returns {*}
		 */
		guardarFileDocumentoIdentidad: function(tipoDocumento, documento, base64File){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'persona/file/documentoIdentidad')
				.addSearch("tipoDocumento", tipoDocumento)
				.addSearch("documento", documento)
				.toString();
			$http.post(uri, base64File)
				.then(function(response){
					if(response.status === 200 || response.status === 417){
						if(response.data.code!==100 && response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		},
		/**
		 * 
		 * incluye un archivo al repositorio de documento de identidad de la persona
		 * 
		 * @feature V4-HU006
		 * @param tipoDocumento
		 * @param documento
		 * @param nombreArchivo
		 * @returns {*}
		 */
		eliminarFileDocumentoIdentidad: function(tipoDocumento, documento, nombreArchivo){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'persona/file/documentoIdentidad')
				.addSearch("tipoDocumento", tipoDocumento)
				.addSearch("documento", documento)
				.addSearch("archivo", nombreArchivo)
				.toString();
			$http.delete(uri)
				.then(function(response){
					if(response.status === 200 || response.status === 417){
						if(response.data.code!==100 && response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		}
	};
})
.service('departamento', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		getDepartamento : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'departamento')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }  
	};
})
.service('ubigeoService', function($rootScope, $http, $q, BACK_END_URL){
	return {
		getPais : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'pais')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		  },
		  //INICIO VTADIG2-59 JOHN VELASQUEZ
		  getDepartamento: function(departamentoId){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'departamento/'+departamentoId)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		  },
		  getProvincia: function(departamentoId,provinciaId){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'departamento/'+departamentoId+"/provincia/"+provinciaId)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		  },
		  getDistrito: function(departamentoId,provinciaId,distritoId){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'departamento/'+departamentoId+"/provincia/"+provinciaId+"/distrito/"+distritoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		  }
		  //FIN VTADIG2-59 JOHN VELASQUEZ
	};
})
.service('obligacionPagoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		generarObligacion : function(obligacionPagoBean){

			var delay = $q.defer();
			$http.post(BACK_END_URL+'obligacion',obligacionPagoBean)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  	},
	  	pagarObligacion : function(pagoBean){
			var delay = $q.defer();
			$http.post(BACK_END_URL+'pagoAsincrono',pagoBean)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  obtenerConfiguracionObligacionPago: function(){
		  	var delay = $q.defer();
			$http.get(BACK_END_URL+'configuracionObligacionPago')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }
	};
})
/*INICIO INTEGRACION EC*/
//.service('prospectoService', function ($rootScope, $http, $q, BACK_END_URL){
.service('prospectoService', function ($rootScope, $http, $q, BACK_END_URL, APP){
/*FIN INTEGRACION EC*/
	return {
		/*INICIO INTEGRACION EC*/
		obtenerEvaluacionCrediticia: function(tipoDocumento, nroDocumento, modulo){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'persona/evaluacionCrediticia')
						.addSearch("tipoDocumento", tipoDocumento)
						.addSearch("documento", nroDocumento)
						.addSearch("modulo", modulo)
						.addSearch("aplicacion", APP.ID)
						.toString();
			$http.get(uri)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
		},
		actualizarEvaluacionCrediticia: function(evaluacionCrediticia, modulo){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'persona/evaluacionCrediticia')
				.addSearch("tipoDocumento", evaluacionCrediticia.tipoDocumento)
				.addSearch("documento", evaluacionCrediticia.documento)
				.addSearch("modulo", modulo)
				.addSearch("aplicacion", APP.ID)
				.toString();
			$http.put(uri, evaluacionCrediticia)
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							delay.resolve(response.data);
						}
					}
				});
			return delay.promise;
		},
		/*FIN INTEGRACION EC*/
		obtenerDetalleProspecto : function(prospectoID){
			var delay = $q.defer();
			/*INICIO INTEGRACION EC*/
			//$http.get(BACK_END_URL+'detalleProspecto/' + prospectoID)
			var uri = URI(BACK_END_URL+'detalleProspecto/' + prospectoID)
						.addSearch("aplicacion", APP.ID)
						.toString();
			$http.get(uri)
			/*FIN INTEGRACION EC*/
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  // INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
	  envioCorreoTelefonoNoPerteneceEDV : function(nombre, tipoDocumento, codigoDocumento,numeroTelefono){
	  // FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			var delay = $q.defer();
			// INICIO - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			$http.get(BACK_END_URL+'enviarCorreoTelefonoNoPerteneceEDV/' + nombre + "/" + tipoDocumento + "/" + codigoDocumento+ "/" + numeroTelefono)                		
			// FIN - modificado por Jose Martinez - VTAOBS1-90 - 21.09.2017
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  envioCorreoOFAC : function(nombre, tipoDocumento, codigoDocumento){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'enviarCorreoOFAC/' + nombre + "/" + tipoDocumento + "/" + codigoDocumento)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }
	};
})
.service('seguimientoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		obtenerDetalleSeguimiento : function(prospectoID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'seguimiento/' + prospectoID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type == 'warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  	},
		  registrarSeguimiento : function(seguimiento){
				var delay = $q.defer();
				$http.post(BACK_END_URL+'registrarSeguimiento',seguimiento)                		
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
							alert(response.data.message);
						}else{                				                			
							delay.resolve(response.data);
						}
					}
				});		
			    return delay.promise;
		  },
		  obtenerPlantillas : function(prospectoId){
				var delay = $q.defer();
				$http.get(BACK_END_URL+'obtenerPlantillasSeguimiento?prospectoId='+prospectoId)                		
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type == 'warning'){
							delay.reject(response);
							//swal({title: '',type: response.data.type,text: response.data.message});
						}else{                				                			
							delay.resolve(response.data.data);
						}
					}
				});		
			    return delay.promise;
		  },
		  obtenerConfigArchivosPlantilla : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'obtenerConfigArchivosPlantilla')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type == 'warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
	  	  },
		  obtenerVistaPrevia : function(plantillaId, prospectoId, data){
				var delay = $q.defer();
				
				var url = BACK_END_URL+'obtenerVistaPrevia?plantillaId=' + plantillaId + "&prospectoId="+prospectoId;
				
				$http.post(url,data)                		
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type == 'warning'){
							delay.reject(response);
							//swal({title: '',type: response.data.type,text: response.data.message});
						}else{                				                			
							delay.resolve(response.data.data);
						}
					}
				});		
			    return delay.promise;
		  }	  
	};
})
.service('obligacionesPorPagarService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		getObligacionesPorDocumento : function(valorBusqueda){
			var delay = $q.defer();
			$http.post(BACK_END_URL+'obligacionPorPagar/documento/' + valorBusqueda,$rootScope.me)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type ==='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getObligacionesPorNombre : function(valorBusqueda){
			var delay = $q.defer();
			$http.post(BACK_END_URL+'obligacionPorPagar/nombre/' + valorBusqueda,$rootScope.me)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type === 'warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getDetalleObligacion: function(obligacionId){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'obligacionPorPagar/obligacionId/' + obligacionId)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type==='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
	  }
	};
})
/*INICIO INTEGRACION EC*/
//.service('proformaService', function ($rootScope, $http, $q, BACK_END_URL,OBLIGACION_PAGO,shareData){
.service('proformaService', function ($rootScope, $http, $q, BACK_END_URL,OBLIGACION_PAGO,shareData, APP){
/*INICIO INTEGRACION EC*/
	return {
		getProspectoPorUsuario : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'prospectos')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
		/*INICIO INTEGRACION EC*/
		//getProspectoPorDocumento : function(tipoDocumento, nroDocumento){
		getProspectoPorDocumento : function(tipoDocumento, nroDocumento, modulo){
			var delay = $q.defer();

			//$http.get(BACK_END_URL+'prospecto?tipoDocumento=' + tipoDocumento + '&numeroDocumento=' + nroDocumento)
var uri = URI(BACK_END_URL+'prospecto')
						.addSearch("tipoDocumento", tipoDocumento)
						.addSearch("numeroDocumento", nroDocumento)
						.addSearch("modulo", modulo)
						.addSearch("aplicacion", APP.ID)
						.toString();
			$http.get(uri)
		/*FIN INTEGRACION EC*/
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}else if(response.status === 417){			               				                			
					delay.resolve(response.data.data);
					
				}else{
					delay.reject(response);
					alert("Ha ocurrido un error inesperado.");
				}
			}, function errorCallback(response) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			    alert("Ha ocurrido un error inesperado.");
			});		
		    return delay.promise;
	  },
	  registrarProforma : function(proforma){
	  		if(proforma.vtaProspecto !== undefined){
		  		if(proforma.vtaProspecto.nombre !== undefined){
					proforma.vtaProspecto.nombre = proforma.vtaProspecto.nombre.toUpperCase();
				}
				if(proforma.vtaProspecto.apellidoMaterno !== undefined){
					proforma.vtaProspecto.apellidoMaterno = proforma.vtaProspecto.apellidoMaterno.toUpperCase();
				}
				if(proforma.vtaProspecto.apellidoPaterno !== undefined){
					proforma.vtaProspecto.apellidoPaterno = proforma.vtaProspecto.apellidoPaterno.toUpperCase();
				}
				if(proforma.vtaProspecto.codigoDocumento !== undefined){
					proforma.vtaProspecto.codigoDocumento = proforma.vtaProspecto.codigoDocumento.toUpperCase();
				}
				if(proforma.vtaProspecto.correo !== undefined){
					proforma.vtaProspecto.correo = proforma.vtaProspecto.correo.toUpperCase();
				}
				if(proforma.vtaProspecto.contacto !== undefined){
					proforma.vtaProspecto.contacto = proforma.vtaProspecto.contacto.toUpperCase();
				}
				if(proforma.vtaProspecto.razonSocial !== undefined){
					proforma.vtaProspecto.razonSocial = proforma.vtaProspecto.razonSocial.toUpperCase();
				}
			}
			var delay = $q.defer();
			$http.post(BACK_END_URL+'registrarProforma',proforma)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						alert(response.data.message);
					}else{                				                			
						delay.resolve(response.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  registrarProspecto : function(prospecto){
			var delay = $q.defer();
			if(prospecto.nombre !== undefined){
				prospecto.nombre = prospecto.nombre.toUpperCase();
			}
			if(prospecto.apellidoMaterno !== undefined){
				prospecto.apellidoMaterno = prospecto.apellidoMaterno.toUpperCase();
			}
			if(prospecto.apellidoPaterno !== undefined){
				prospecto.apellidoPaterno = prospecto.apellidoPaterno.toUpperCase();
			}
			if(prospecto.codigoDocumento !== undefined){
				prospecto.codigoDocumento = prospecto.codigoDocumento.toUpperCase();
			}
			if(prospecto.correo !== undefined){
				prospecto.correo = prospecto.correo.toUpperCase();
			}
			if(prospecto.contacto !== undefined){
				prospecto.contacto = prospecto.contacto.toUpperCase();
			}
			if(prospecto.razonSocial !== undefined){
				prospecto.razonSocial = prospecto.razonSocial.toUpperCase();
			}
			$http.post(BACK_END_URL+'registrarProspecto',prospecto)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						alert(response.data.message);
					}else{                				                			
						delay.resolve(response.data);
					}
				}
			});		
		    return delay.promise;
	  },	  
	  getProformasPorNombre : function(valorBusqueda){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'prospecto/' + valorBusqueda)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getMarcaModeloProforma : function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'infoProforma')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  getMisVentas: function() {
		var delay = $q.defer();
		$http.get(BACK_END_URL+'ventas')                		
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
					//swal({title: '',type: response.data.type,text: response.data.message});
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
	  },
	  getMiEmbudo: function(firstDay,lastDay) {
		var delay = $q.defer();
		$http.get(BACK_END_URL+'embudo',{params: {"firstDay": firstDay, "lastDay" : lastDay}})
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
					//swal({title: '',type: response.data.type,text: response.data.message});
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
	  },                		
	  getMisSeparaciones: function() {
		var delay = $q.defer();
		$http.get(BACK_END_URL+'separaciones')  
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
					//swal({title: '',type: response.data.type,text: response.data.message});
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
	  },
	  ajustarProformas: function(lstProforma){
		for(var item in lstProforma){
			var cuotaMensual = lstProforma[item].certificadoBean.cuotaMensual;
			//console.log("cuotaMensual: " + cuotaMensual);
			var pagos = 0;
			var pendiente = 0;
			for(var pago in  lstProforma[item].lstObligacionPagoBean){
				if(lstProforma[item].lstObligacionPagoBean[pago].lngEstadoObligacion === OBLIGACION_PAGO.ESTADO.PAGADO ){
					pagos += lstProforma[item].lstObligacionPagoBean[pago].bdImporte;
				}

				if(lstProforma[item].lstObligacionPagoBean[pago].lngEstadoObligacion === OBLIGACION_PAGO.ESTADO.REGISTRADO){
					pendiente = lstProforma[item].lstObligacionPagoBean[pago].bdImporte;
				}			
			}
			//console.log("pagos: " + pagos);
			var porcentaje = Math.trunc((pagos/cuotaMensual) * 10000)/100  ;
			var porcentajePendiente = Math.trunc((pendiente/cuotaMensual) * 10000)/100  ;
			
			lstProforma[item].nmSeparationPct = porcentaje?porcentaje:0;
			lstProforma[item].nmSeparation = pagos;
			if(lstProforma[item].contratoId !==0){
				lstProforma[item].mMensjae = lstProforma[item].contratoNumero;
			}else{
				porcentaje = porcentaje>100?100:porcentaje;		
				if(porcentaje>0){
					lstProforma[item].mMensjae = "Separación : " + porcentaje + "%";
				}		

				porcentajePendiente = porcentajePendiente>100?100:porcentajePendiente;	
				if(porcentajePendiente>0){
					lstProforma[item].mMensjaeSep = "Separación : " + porcentajePendiente + "%";
				}
			}
			if(lstProforma[item].prospectoBean){
				if(lstProforma[item].prospectoBean.tipoDocumento!==undefined){
					lstProforma[item].prospectoBean.tipoDocumentoObj = shareData.mapTipoDocumento[lstProforma[item].prospectoBean.tipoDocumento];
				}				
			}
			lstProforma[item].tieneDebito = lstProforma[item].contratoDebito && lstProforma[item].contratoDebito.length > 0
		}
		return lstProforma;
	  },
	  enviarCorreoProforma : function(proformaID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'enviaCorreoProforma/' + proformaID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  },
	  enviarCorreoContratosResueltos: function(tipoDocumento, numeroDocumento, nombre) {
			console.log("enviandoContratosResueltos => " + tipoDocumento + " " + numeroDocumento + ", " + nombre);
			var delay = $q.defer();
			$http.post(BACK_END_URL + 'emailContratosResueltos?tipoDocumento=' + tipoDocumento + "&numeroDocumento=" + numeroDocumento + "&nombre=" + nombre).then(function(response) {
				if (response.status === 200) {
					if (response.data.message !== '' && response.data.type == 'warning') {
						delay.reject(response);
                    }else {
                        delay.resolve(response.data.data);
					}
				}
			});
			return delay.promise;
	  },
	  enviarCorreoContratosLegal: function(tipoDocumento, numeroDocumento, nombre) {
            console.log("enviandoContratosLegal => " + tipoDocumento + " " + numeroDocumento + ", " + nombre);
            var delay = $q.defer();
            $http.post(BACK_END_URL + 'emailContratosLegal?tipoDocumento=' + tipoDocumento + "&numeroDocumento=" + numeroDocumento + "&nombre=" + nombre).then(function(response) {
                if (response.status === 200) {
                    if (response.data.message !== '' && response.data.type == 'warning') {
                        delay.reject(response);
                        //swal({title: '',type: response.data.type,text: response.data.message});
                    } else {
                        delay.resolve(response.data.data);
                    }
                }
            });
           	return delay.promise;
	  },
	  enviarCorreoOFAC: function(tipoDocumento, numeroDocumento, nombre){
		console.log("enviandoCorreoOfac => "+tipoDocumento+" "+numeroDocumento+", "+nombre);
		var delay = $q.defer();
		$http.post(BACK_END_URL+'emailListaOfac?tipoDocumento=' + tipoDocumento+"&numeroDocumento="+numeroDocumento+"&nombre="+nombre)	
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
					//swal({title: '',type: response.data.type,text: response.data.message});
				}else{                				                			
					delay.resolve(response.data.data);
				}
			}
		});		
		return delay.promise;
	  },

	  ajustarSeparaciones: function(lstProforma){
		for(var item in lstProforma){

			var cuotaMensual = lstProforma[item].certificadoBean.cuotaMensual;
			//console.log("cuotaMensual: " + cuotaMensual);
			var pagos = 0;
			var pendiente = 0;
			for(var pago in  lstProforma[item].lstObligacionPagoBean){
				if(lstProforma[item].lstObligacionPagoBean[pago].lngEstadoObligacion === OBLIGACION_PAGO.ESTADO.PAGADO ){
					pagos += lstProforma[item].lstObligacionPagoBean[pago].bdImporte;
				}

				if(lstProforma[item].lstObligacionPagoBean[pago].lngEstadoObligacion === OBLIGACION_PAGO.ESTADO.REGISTRADO){
					pendiente = lstProforma[item].lstObligacionPagoBean[pago].bdImporte;
				}			
			}
			//console.log("pagos: " + pagos);
			var porcentaje = Math.trunc((pagos/cuotaMensual) * 10000)/100  ;
			var porcentajePendiente = Math.trunc((pendiente/cuotaMensual) * 10000)/100  ;
			
			lstProforma[item].nmSeparationPct = porcentaje?porcentaje:0;
			lstProforma[item].nmSeparation = pagos;
			
			if(lstProforma[item].prospectoBean){
				lstProforma[item].mMensjae = lstProforma[item].proformaID;
				if(lstProforma[item].prospectoBean.tipoDocumento!==undefined){
					lstProforma[item].prospectoBean.tipoDocumentoObj = shareData.mapTipoDocumento[lstProforma[item].prospectoBean.tipoDocumento];
				}				
			}
		}
		return lstProforma;
	  },
	  getCuotaCertificado: function(grupoId, certificadoPosicionId, nroCuotas){
		var delay = $q.defer();
		if(grupoId===undefined||grupoId===null){
			delay.reject({message: "grupoID es obligatorio"});
		}else{
			if(certificadoPosicionId===undefined||certificadoPosicionId===null){
			certificadoPosicionId = 0;
			}
			if(nroCuotas===undefined||nroCuotas===null){
				nroCuotas = 0;
			}
			$http.get(BACK_END_URL+'cuotaCertificado/'+grupoId+'/'+certificadoPosicionId+'/'+nroCuotas)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});
		}				
		return delay.promise;
	  },

	  inversionesInmueble: function(){
		var delay = $q.defer();
		$http.get(BACK_END_URL+'inversionInmueble')	
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	  }
	  ,obtenerMediosDeValidacion: function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL + 'proforma/validacion/medio')
				.then(function (response) {
					if (response.status === 200) {
						if (response.data.message !== '' && response.data.type == 'warning') {
							delay.reject(response);
							//swal({title: '',type: response.data.type,text: response.data.message});
						} else {
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		}
		,enviarValidacionProforma: function(medioId, proformaId){
            var delay = $q.defer();
            $http.post(BACK_END_URL + `proforma/${proformaId}/validacion`,{medioId: medioId})
                .then(function (response) {
                    if (response.status === 200) {
                        if (response.data.message !== '' && response.data.type == 'warning') {
                            delay.reject(response);
                            //swal({title: '',type: response.data.type,text: response.data.message});
                        } else {
                            delay.resolve(response.data.data);
                        }
                    }
                });
            return delay.promise;
        }
        ,validarProforma: function(proforma, validacion){
            var delay = $q.defer();
            $http.get(`${validacion.link}${validacion.codigo}`)
                .then(function (response) {
                    if (response.status === 200) {
                        if (response.data.message !== '' && response.data.type == 'warning') {
                            delay.reject(response);
                            //swal({title: '',type: response.data.type,text: response.data.message});
                        } else {
                            delay.resolve(response.data.data);
                        }
                    }
                });
            return delay.promise;
        }
	};
})
.service('blockUI',function($ionicLoading){
	return{
		start: function(){
			$ionicLoading.show({
				template: 'Cargando...'
			});
		},
		stop:function(){
			$ionicLoading.hide();
		}
	};
})
/**
 *
 * 
 * @feature V4-HU002
 *  - se injectan $httpParamSerializer y $http
 *  - authenticate -> las credenciales se envian en el cuerpo de la solicitud
 */
.service('authService',function($resource,$rootScope, $sessionStorage , BACK_END_URL,$httpParamSerializer, $http){
	return {
		/**
		 * 
		 * @param credentials
		 * @returns {Promise<any>}
		 * @feature V4-HU002
		 *  - las credenciales se envian en el cuerpo de la solicitud
		 */
		authenticate: function(credentials){
			var data = {
				grant_type: "password",
				username: credentials.username,
				password: credentials.password
			};
			var req = {
				method: 'POST',
				url: BACK_END_URL+'oauth/token',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
					'Authorization':'Basic ' + window.btoa('web:secret')
				},
				data: $httpParamSerializer(data)
			};
			return new Promise((resolve, reject) => {
				$http(req).then(
					response => {
						if(response.status !== 200){
							reject(response.message ? response.message: response.data);
						}
						else{
							resolve(response.data);
						}
					},
					error => {
						reject(error)
					}
				)
			});
		},
		clearCredentials: function(){
	        $rootScope.globals = {};
	        //$cookieStore.remove('globals');
	        //$sessionStorage.remove('token');
	        //$sessionStorage.empty();
	        /*
	        angular.forEach($sessionStorage, function (item,key) {
	          $sessionStorage.removeItem(key);
	        });
	        */

	        console.log($sessionStorage);
	        $sessionStorage.$reset();
	        console.log($sessionStorage);
		}		

	};
})
/**
 *
 *
 * @feature V4-HU002
 *  - login -> se integra a los cambios hechos en authService.authenticate
 */
.service('loginService',function(authService,$sessionStorage,$http, $q, BACK_END_URL,$rootScope){
	return {
		/**
		 *
		 *
		 * @feature V4-HU002
		 *  - login -> se integra a los cambios hechos en authService.authenticate
		 */
		login: function(credenciales,callback){
			authService.authenticate(credenciales)
            /** inicio @feature V4-HU002 **/
			//.token()
            //.$promise
            /** fin @feature V4-HU002 **/
            .then(
				function (res) {
					console.log("res");
					console.log(res);
					if(typeof res.error !== 'undefined'){
						callback({success:false, doc:res.error_description});
						return;
					}
					else if(res.responseCode==-1){
						callback({success:false, doc:res.responseMessage});
						return;
					}
					$sessionStorage.token = res.access_token;
					$sessionStorage.timeout = res.expires_in;
					callback({success:true, doc:res});
            	},
				function(error){
					/** inicnio @feature V4-HU002 **/
					callback({success:false, doc: error.error_description ? error.error_description : "Error al iniciar sesion"});
					/** fin @feature V4-HU002 **/
				}
			).catch(function (err) {
              callback({success:false});
            });
		},
		me: function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'me')
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		verificaUsuarioActivo: function(){
			var usuarioID = $rootScope.me.usuarioID;
			var delay = $q.defer();
			$http.get(BACK_END_URL+'verificaUsuarioActivo/'+usuarioID)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		}
	};
})
// INICIO VTADIG2-185- José Martinez 12/02/2018
/**
 * @feature V4-HU020
 *  - add injects [DIRECCIONES]
 */
.service("validacionService",function(ESTADO_CIVIL,PERSONA_TIPO,TIPO_MONEDA,DIRECCION,tempContrato,$ionicPopup){
// FIN VTADIG2-185- José Martinez 12/02/2018
	return {
		// INICIO VTADIG2-185- José Martinez 12/02/2018
		obtenerIngresoMensualDolares : function (moneda, ingresoMensual,listaTipoCambios){
			if(parseInt(moneda)===TIPO_MONEDA.DOLARES) return ingresoMensual;
			
			if(parseInt(moneda)===TIPO_MONEDA.SOLES){
				var tipoCambioCompra = listaTipoCambios.find(d=>d.tipo ==='VENTA' && d.monedaOrigen===TIPO_MONEDA.DOLARES);
				console.log('tipoCambio:'+tipoCambioCompra.tipoCambio);
				var ingresoMensualDolares = ingresoMensual/tipoCambioCompra.tipoCambio;
				return this.aDosDecimales(ingresoMensualDolares);
			}
            return 0;
        },
		// FIN VTADIG2-185- José Martinez 12/02/2018
		aDosDecimalesTrunc : function (num, fixed){
            if(fixed== undefined ) fixed = 2;
            console.log('num'+num);
            if(num === undefined || isNaN(num)){
                return 0;
            }
            var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
            var result = num.toString().match(re)[0];
            return parseFloat(result);
        },
		aDosDecimales: function (value){
			// INICIO VTADIG2-185- José Martinez 12/02/2018
            if(value === undefined || isNaN(value)){
                return 0;
            }	
			// FIN VTADIG2-185- José Martinez 12/02/2018		
			if(!value.toFixed){
				return value;
			}
			return parseFloat(value.toFixed(2));
		},
		esCasado : function (estadoCivil){
			if(estadoCivil){
				if(estadoCivil==="C"||estadoCivil==="N"||estadoCivil==="V"){
					return true;
				}
			}else{				
				return false;
			}				
		},
		/**
		 * 
		 * @param persona
		 * @param shareData
		 * @param asociadoTipoID
		 * @returns {{direcciones}}
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		ajustarPersona: function (persona,shareData,asociadoTipoID){
			var str;
			var res;
			var month;

			if(persona.estadoCivilID){
				persona.estadoCivilID = persona.estadoCivilID.trim();
			}
			if(persona.personaTipoID){
				persona.personaTipoID = persona.personaTipoID.trim();
			}		
			persona.personaIngresoNetoPrincipal = persona.personaIngresoMensual;
			if(!persona.tipoDocumento){			
				if(persona.tipoDocumentoID===undefined){
					//$scope.persona.tipoDocumento = $scope.lstTipoDocumento[3];
					persona.tipoDocumento = shareData.mapTipoDocumento[shareData.TIPO_DOCUMENTO.DNI];
					persona.tipoDocumentoID = persona.tipoDocumento.codigo;
				}else{
					angular.forEach(shareData.lstTipoDocumento,function(tipoDocumento){
						if(persona.tipoDocumentoID===tipoDocumento.codigo){
							persona.tipoDocumento = tipoDocumento;
							persona.tipoDocumentoID = persona.tipoDocumento.codigo;				
						}
					});
				}
			}			

			persona.personaFechaModificacion = undefined;
			persona.personaFechaCreacion = undefined;
			if(persona.personaFechaIngresoTrabajo){
				if(persona.personaFechaIngresoTrabajo.getDate===undefined){
					str = persona.personaFechaIngresoTrabajo;
					res = str.split(" ")[0].split("/");
					month = parseInt(res[1])-1;
					persona.personaFechaIngresoTrabajo = new Date(res[2], month, res[0]);
				}
			}
			if(persona.personaFechaNacimiento){
				if(persona.personaFechaNacimiento.getDate===undefined){
					str = persona.personaFechaNacimiento;
					res = str.split(" ")[0].split("/");
					month = parseInt(res[1])-1;
					persona.personaFechaNacimiento = new Date(res[2], month, res[0]);
				}
			}
			if(persona.personaFechaConstitucion){
				if(persona.personaFechaConstitucion.getDate===undefined){
					str = persona.personaFechaConstitucion;
					res = str.split(" ")[0].split("/");
					month = parseInt(res[1])-1;
					persona.personaFechaConstitucion = new Date(res[2], month, res[0]);
				}
			}
			if(persona.personaNacimientoUbigeo){				
				var ubigeoNacimiento = persona.personaNacimientoUbigeo.trim();
				var ubigeoDepartamento = ubigeoNacimiento.substring(0, 2).trim();
				var ubigeoProvincia = ubigeoNacimiento.substring(2, 4).trim();
				var ubigeoDistrito = ubigeoNacimiento.substring(4, 6).trim();
				persona.departamentoNacimientoID = ubigeoDepartamento?ubigeoDepartamento:undefined; 
				persona.provinciaNacimientoID = ubigeoProvincia?ubigeoProvincia:undefined;
				persona.distritoNacimientoID = ubigeoDistrito?ubigeoDistrito:undefined;
			}
			if(persona.actividadLaboralID){
				persona.actividadLaboralID = persona.actividadLaboralID.trim();
				angular.forEach(shareData.lstActividadLaboral,function(item){
					if(parseInt(persona.actividadLaboralID)==parseInt(item.codigo)){
						persona.personaGiroNegocioObj = item;
					}
				});
			}	
			/** inicio @feature V4-HU020 **/
			if(persona.personaTipoID === 'N') {
				persona.esResidente = persona.esResidente !== false;
			} else {
				persona.esResidente = true;
			}
			persona.esResidenteOriginal = persona.esResidente;
			/** fin @feature V4-HU020 **/
			if(persona.direcciones!==undefined){
				if(persona.direcciones.length>=1){
					var direccionPersonal = null;
					var direccionLaboral = null;
					angular.forEach(persona.direcciones,function(direccionTmp){
						if(direccionTmp !== undefined&&direccionTmp !== null){
							if(direccionTmp.tipoZonaID!==undefined&&direccionTmp.tipoZonaID!==null){
								direccionTmp.tipoZonaID = direccionTmp.tipoZonaID.trim();
							}
							if(direccionTmp.tipoViaID!==undefined&&direccionTmp.tipoViaID!==null){
								direccionTmp.tipoViaID = direccionTmp.tipoViaID.trim();
							}
							/** inicio @feature V4-HU020 **/
							if([DIRECCION.TIPO.PERSONAL, DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO].includes(direccionTmp.direccionTipoID)) {
							/** inicio @feature V4-HU020 **/
								direccionPersonal = direccionTmp;
							/** inicio @feature V4-HU020 **/
							}else if([DIRECCION.TIPO.LABORAL, DIRECCION.TIPO.LABORAL_NO_DOMICILIADO].includes(direccionTmp.direccionTipoID)){
							/** fin @feature V4-HU020 **/
								direccionLaboral = direccionTmp;
							}
							if(direccionTmp.departamentoID){
								direccionTmp.departamentoID = direccionTmp.departamentoID.trim();
								direccionTmp.departamentoID = direccionTmp.departamentoID?direccionTmp.departamentoID:undefined;
							}
							if(direccionTmp.provinciaID){
								direccionTmp.provinciaID = direccionTmp.provinciaID.trim();
								direccionTmp.provinciaID = direccionTmp.provinciaID?direccionTmp.provinciaID:undefined;
							}
							if(direccionTmp.distritoID){
								direccionTmp.distritoID = direccionTmp.distritoID.trim();
								direccionTmp.distritoID = direccionTmp.distritoID?direccionTmp.distritoID:undefined;
							}
						}
					});

					if(direccionPersonal === null){
						/** inicio @feature V4-HU020 **/
						direccionPersonal = {direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL: DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO};
						/** fin @feature V4-HU020 **/
					}
					if(direccionLaboral === null){
						/** inicio @feature V4-HU020 **/
						direccionLaboral = {direccionTipoID: persona.esResidente ? DIRECCION.TIPO.LABORAL: DIRECCION.TIPO.LABORAL_NO_DOMICILIADO};
						/** fin @feature V4-HU020 **/
					}

					if(persona.personaTipoID === 'J'){
						persona.direcciones=[direccionPersonal];
					}else{
						persona.direcciones=[direccionPersonal,direccionLaboral];
					}
				}
			}else{
				if(persona.personaTipoID === 'J'){
					/** inicio @feature V4-HU020 **/
					persona.direcciones=[{direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL: DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO}];
					/** fin @feature V4-HU020 **/
				}else{
					/** inicio @feature V4-HU020 **/
					persona.direcciones=[{direccionTipoID: persona.esResidente ? DIRECCION.TIPO.PERSONAL: DIRECCION.TIPO.PERSONAL_NO_DOMICILIADO},
										 {direccionTipoID: persona.esResidente ? DIRECCION.TIPO.LABORAL: DIRECCION.TIPO.LABORAL_NO_DOMICILIADO}];
					/** fin @feature V4-HU020 **/
				}
			}
			
			
			
			if(persona.personaEsEmpleado){
				var newVinculada;
				if(!persona.personasVinculadas){
					persona.personasVinculadas = [];
					newVinculada = JSON.parse(JSON.stringify(persona));
					newVinculada.personasVinculadas = undefined;
					newVinculada.personasRelacionadas = undefined;
					newVinculada.ingresoTotalMensual = undefined;
					if(!newVinculada.personaTipoRelacionID){
						newVinculada.personaTipoRelacionID = "8";
					}
					persona.personasVinculadas.push(newVinculada);
				}else{
					var yaVinculado = false;
					angular.forEach(persona.personasVinculadas,function(vinculada){
						if(vinculada.tipoDocumentoID===persona.tipoDocumentoID&&vinculada.personaCodigoDocumento===persona.personaCodigoDocumento){
							yaVinculado=true;
						}
					});
					newVinculada = JSON.parse(JSON.stringify(persona));
					newVinculada.personasVinculadas = undefined;
					newVinculada.personasRelacionadas = undefined;
					newVinculada.ingresoTotalMensual = undefined;
					if(!newVinculada.personaTipoRelacionID){
						newVinculada.personaTipoRelacionID = "8";
					}
					if(!yaVinculado){
						persona.personasVinculadas.push(newVinculada);	
					}					
				}
			}
			if(persona.personasRelacionadas){
				if(persona.estadoCivilID!==ESTADO_CIVIL.CASADO&&persona.estadoCivilID!==ESTADO_CIVIL.CONVIVIENTE&&persona.personaTipoID!==PERSONA_TIPO.JURIDICA){
					persona.personasRelacionadas = [];
				}
				/*SI ES PERSONA NATURAL*/
				/**NO PUEDE TENER COMO RELACIONADAS A PERSONAS JURIDICAS*/
				var tempRelacionadas = [];
				if(persona.personaTipoID === PERSONA_TIPO.NATURAL){										
					angular.forEach(persona.personasRelacionadas,function(relacionada,$index){
						if(relacionada.personaTipoID !== PERSONA_TIPO.JURIDICA){
							tempRelacionadas.push(relacionada);
						}
					});
					persona.personasRelacionadas = tempRelacionadas;
				}
				angular.forEach(persona.personasRelacionadas,function(relacionada,$index){					
					/*ASIGNARLE OBJETOS TIPO DE DOCUMENTo SEGUN EL ATRIBUTO tipoDocumentoID*/
					angular.forEach(shareData.lstTipoDocumento,function(tipoDocumento){
						if(relacionada.tipoDocumentoID===tipoDocumento.codigo){
							relacionada.personaTipoDocumentoNombre = tipoDocumento.descripcion;	
								relacionada.tipoDocumento=tipoDocumento;
						}
					});
					/*ASIGNARLE OBJETO OCUPACION SEGUN EL ATRIBUTO lstPersonaOcupacion*/
					angular.forEach(shareData.lstPersonaOcupacion, function (item) {
							if(item.codigo == relacionada.personaOcupacion){
								relacionada.personaOcupacionObj=item;
							}
					});
					/*CAMBIAR 'REPRESENTANTE LEGAL' POR 'REP. LEGAL'*/
					angular.forEach(shareData.lstTipoRelacionada,function(tipoRelacion){
						if(parseInt(relacionada.personaTipoRelacionID)===parseInt(tipoRelacion.codigo)){
							relacionada.tipoRelacionNombre = tipoRelacion.descripcion;
							if(relacionada.personaTipoRelacionID.trim){
								if(parseInt(relacionada.personaTipoRelacionID.trim())==17){
									relacionada.tipoRelacionNombre = "REP. LEGAL";
								}
							}else if(parseInt(relacionada.personaTipoRelacionID)==17){
								relacionada.tipoRelacionNombre = "REP. LEGAL";
							}													
						}
					});
					/**/
					if(parseInt(relacionada.personaTipoRelacionID)===17){
							relacionada.sexoID=relacionada.sexoID;
					}

					if(relacionada.paisID){
						relacionada.paisID=relacionada.paisID+'';
					}else{
						relacionada.paisID = "189";
					}
					/** inicio @feature V4-HU020 **/
					if(relacionada.paisResidenciaId){
						relacionada.paisResidenciaId = relacionada.paisResidenciaId+'';
					}
					/** fin @feature V4-HU020 **/
					/* QUITAR DATA INNECESARIA */
					relacionada.personaFechaModificacion = undefined;
					relacionada.personaFechaCreacion = undefined;
					relacionada.medioComunicacionPreferenteID = undefined;
					/*ATRIBUTO PARA EL CAMPO 'TELÉFONO FIJO O CELULAR'*/
					if(relacionada.personaTelefono===undefined || relacionada.personaTelefono===""){
							relacionada.personaTelefono = relacionada.personaTelefonoMovil;
					}
					if(relacionada.personaTelefono===undefined || relacionada.personaTelefono===""){
						if(relacionada.direcciones){
							if(relacionada.direcciones.length>0){
									relacionada.personaTelefono = relacionada.direcciones[0].direccionTelefono1;
							}
						}
					}                                       

					/*AJUSTAR FORMATO DE FECHAS */					
					if(relacionada.personaFechaIngresoTrabajo){
						if(relacionada.personaFechaIngresoTrabajo.getDate===undefined){
							//ver si es una fecha pero en formato string: "2003-10-31T05:00:00.000Z"
							var tempDate = new Date(relacionada.personaFechaIngresoTrabajo);
							if(!isNaN(tempDate.getTime())){//getTime devuelve NaN cuando la fecha es invalida
								//si es una fecha valida tempDate es la fecha correcta
								relacionada.personaFechaIngresoTrabajo = tempDate;
							}else{
								str = relacionada.personaFechaIngresoTrabajo;
								res = str.split(" ")[0].split("/");
								month = parseInt(res[1])-1;
								relacionada.personaFechaIngresoTrabajo = new Date(res[2], month, res[0]);
							}							
						}
					}
					if(!relacionada.personaFechaNacimiento){
						if(relacionada.personaFechaNacimiento.getDate===undefined){
							//ver si es una fecha pero en formato string: "2003-10-31T05:00:00.000Z"
							var tempDate = new Date(relacionada.personaFechaNacimiento);
							if(!isNaN(tempDate.getTime())){//getTime devuelve NaN cuando la fecha es invalida
								//si es una fecha valida tempDate es la fecha correcta
								relacionada.personaFechaNacimiento = tempDate;
							}else{
								str = relacionada.personaFechaNacimiento;
								res = str.split(" ")[0].split("/");
								month = parseInt(res[1])-1;
								relacionada.personaFechaNacimiento = new Date(res[2], month, res[0]);
							}							
						}
					}
					if(relacionada.direcciones){
						if(relacionada.direcciones[0]&&!relacionada.direccionDomiciliada){
							relacionada.direccionDomiciliada = relacionada.direcciones[0];
						}
						if(relacionada.direcciones[1]&&!relacionada.direccionLaboral){
							relacionada.direccionLaboral = relacionada.direcciones[1];
						}
					}
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					if(relacionada.personaID && relacionada.monedaIDIngresoMensual){
						relacionada.monedaIDIngresoMensual = relacionada.monedaIDIngresoMensual.toString();	
					}						
					// FIN VTADIG2-185- José Martinez 12/02/2018
				});
			}else{
				persona.personasRelacionadas = [];
			}
			if(persona.personasVinculadas){
				angular.forEach(persona.personasVinculadas,function(vinculada){
					angular.forEach(shareData.lstTipoDocumento,function(tipoDocumento){
						if(vinculada.tipoDocumentoID===tipoDocumento.codigo){
							vinculada.personaTipoDocumentoNombre = tipoDocumento.descripcion;								
							vinculada.tipoDocumento=tipoDocumento;
						}
					});
					angular.forEach(shareData.lstTipoRelacionada,function(tipoRelacion){
						if(parseInt(vinculada.personaTipoRelacionID)===parseInt(tipoRelacion.codigo)){
							if(parseInt(vinculada.personaTipoRelacionID) == 16){
								vinculada.tipoRelacionNombre = "Cónyuge";
							}else{
								vinculada.tipoRelacionNombre = tipoRelacion.descripcion;
								if(vinculada.personaTipoRelacionID.trim){
									if(parseInt(vinculada.personaTipoRelacionID.trim())==17){
										vinculada.tipoRelacionNombre = "REP. LEGAL";
									}	
								}else if(parseInt(vinculada.personaTipoRelacionID)==17){
									vinculada.tipoRelacionNombre = "REP. LEGAL";
								}						
							}
						}
					});
					vinculada.personaFechaModificacion = undefined;
					vinculada.personaFechaCreacion = undefined;
					vinculada.personaFechaIngresoTrabajo = undefined;
					vinculada.personaFechaNacimiento = undefined;
					vinculada.personaFechaNacimiento = undefined;
				});
			}else{
				persona.personasVinculadas = [];
			}
			if(!persona.personaOcupacionObj){
				angular.forEach(shareData.lstPersonaOcupacion,function(ocupacion){
					if(persona.personaOcupacion==ocupacion.codigo){
						persona.personaOcupacionObj = ocupacion;
					}
				});
			}
			if(persona.personaEsEmpleado){
				persona.personaTieneVinculado = true;
			}
			if(!persona.tipoDocumento){
				//persona.tipoDocumento = $scope.lstTipoDocumento[3];			
				persona.tipoDocumento = shareData.mapTipoDocumento[shareData.TIPO_DOCUMENTO.DNI];//$scope.lstTipoDocumento[3];
			}
			if(!persona.paisID){
				persona.paisID = "189";
			}else{
				persona.paisID = persona.paisID+'';
			}
			/** inicio @feature V4-HU020 **/
			if(persona.paisResidenciaId){
				persona.paisResidenciaId = persona.paisResidenciaId+'';
			}
			/** fin @feature V4-HU020 **/
			if(!persona.direcciones){
				persona.direcciones = [];
			}
			if(!persona.asociadoTipoID){
				persona.asociadoTipoID = asociadoTipoID;
			}
			if(persona.estadoCivilID){
				persona.estadoCivilID = persona.estadoCivilID.trim();
			}

			if(persona.estadoCivilID){
				if(persona.estadoCivilID==="C"||persona.estadoCivilID==="N"||persona.estadoCivilID==="V"){
					persona.esCasado = true;
				}
			}else{				
				persona.esCasado = false;
			}


			if(persona.personaRUC===undefined){
				persona.personaRUC="";
			}
			
			if(persona.personaTipoID === 'J'){
				if( persona.personaCorreoTrabajo  ==  undefined ){
					persona.personaCorreoTrabajo = persona.personaCorreoPersonal;
					persona.personaDetalleContacto =  persona.contacto;
				}
				persona.personaCorreoPersonal=undefined;
			}
			// INICIO VTADIG2-185- José Martinez 12/02/2018
			if(persona.personaID && !persona.monedaIDIngresoMensual){
				//alert(persona.personaNombre+ ' no tiene registrado el tipo de moneda');
				persona.monedaIDIngresoMensual='2';
			}else if(persona.personaID && persona.monedaIDIngresoMensual){
				persona.monedaIDIngresoMensual = persona.monedaIDIngresoMensual.toString();	
			}				

			persona.personaIngresoMensualDolares = this.obtenerIngresoMensualDolares(persona.monedaIDIngresoMensual,persona.personaIngresoMensual,tempContrato.contrato.listaTipoCambios);			
			// FIN VTADIG2-185- José Martinez 12/02/2018
			persona.modoEdicion=false;

			return persona;
		},
		validarTelefono: function(string){
			/*if(isNaN(parseInt(string))){
				return false;
			}*/
			return (string.length == 9 || string.length == 0);			
		},
		validarCorreElectronico: function (mail) {  
			/* Inicio VTAPROD-191 aestela 16-09-2019*/
			return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(mail); 
			/* Fin VTAPROD-191 aestela 16-09-2019*/
			/* Inicio VTAPROD-191 aestela 16-09-2019*/
  			// return /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail); 
			/* Fin VTAPROD-191 aestela 16-09-2019*/
		},
		formatearTelefono: function(string){
			if(string===undefined||string===null)return '';
			string = string.replace(/[^\d]/gi, '');
			return isNaN(parseInt(string))?'':string.substr(0,12);
		},
		formatearCelular: function(string){
			var number = parseInt(string);
			if(number){
				number = Math.abs(number);
				var l = (''+number).length;				
				if(l > 9){
					number = number/10;
				}
				number = Math.trunc(number);
			}else{
				number='';
			}
			return ''+number;
		},
        /**
		 * @issue -
         * @param string
         * @param length
         * @returns {*}
		 * @issue VTADIG2-504, add length as second param
         */
		formatearSoloNumeros: function(string, length){
			while(!(/^[0-9]*$/.test(string))){
				string = string.substr(0,string.length-1);
			}			
            /*inicio @issue VTADIG2-504 */
			if(length){
				string = string.substr(0, length);
			}
            /*fin @issue VTADIG2-504 */
			return string;
		},
		formatearCodigoContrato: function(string){
			while(!(/^[0-9]*$/.test(string))){
				string = string.substr(0,string.length-1);
			}
			string = string.substr(0,9);
			return string;
		},
		formatearDosDecimales: function(string){
			if(!string){
				return string;
			}
			if(/^[0-9]$/.test(string)){
				return string;
			}
			while(!(/^[0-9]+([.][0-9]{0,2})?$/.test(string))){
				string = string.substr(0,string.length-1);
				if(!string){
					return string;
				}
			}
			return string;
		},
		formatearDNI: function(string){
			while(!(/^[0-9]*$/.test(string))){
				string = string.substr(0,string.length-1);
			}
			string = string.substr(0,8);
			return string;
		},
		validarDNI: function(string){
			var esValido = /^[0-9]{8,8}$/.test(string);
			if (!esValido) var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El DNI debe tener 8 dígitos" , okType: 'button-assertive'});
			return esValido;
		},
		validarRUC: function(numeroDocumento,tipoPersona,showAlert){
			if(showAlert=== undefined) showAlert = false;
			var esValido = /^[0-9]{11,11}$/.test(numeroDocumento);
			if (!esValido){
				if(showAlert) {
					$ionicPopup.alert({ title: 'Alerta', template: "El RUC debe tener 11 dígitos" , okType: 'button-assertive'});	
				}else{
					return "El RUC debe tener 11 dígitos";
				}
				return esValido;
			} 
			if(tipoPersona==='N'){
			    /* inicio @isssue VTADIG2-540 */
				//var esValido = numeroDocumento.indexOf('10')===0? true:false;
                var esValido = (numeroDocumento.indexOf('10')===0 || numeroDocumento.indexOf('15')===0)? true:false;
                /* fin @isssue VTADIG2-540 */
				if (!esValido){
					if(showAlert){
						/* inicio @isssue VTADIG2-540 */
						$ionicPopup.alert({ title: 'Alerta', template: "El RUC debe empezar con el 10 o 15" , okType: 'button-assertive'});
						/* fin @isssue VTADIG2-540 */	
					}  else{
						/* inicio @isssue VTADIG2-540 */
						return "El RUC debe empezar con el 10 o 15";
						/* fin @isssue VTADIG2-540 */	
					}
					return esValido;
				} 				
			} else if(tipoPersona==='J'){
				var esValido = ( numeroDocumento.indexOf('17')===0 || numeroDocumento.indexOf('20')===0)? true:false;
				if (!esValido){
					if(showAlert)  $ionicPopup.alert({ title: 'Alerta', template: "El RUC debe empezar con el 17 o 20" , okType: 'button-assertive'});	
					return esValido;
				} 				
			}
			return esValido;				
		},
		validarCarnetExtranjeria: function(string){
			var esValido = /^[0-9a-zA-Z]{0,12}$/.test(string);
			if (!esValido) var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El C.E. debe tener 12 dígitos como máximo" , okType: 'button-assertive'});
			return esValido;							
		},
		validarPasaporte: function(string){
			var esValido = /^[0-9a-zA-Z]{0,12}$/.test(string);
			if (!esValido) var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "El P.S. debe tener 12 dígitos como máximo" , okType: 'button-assertive'});
			return esValido;							
		},
		formatearRUC: function(string,tipoPersona){
			while(!(/^[0-9]*$/.test(string))){
				string = string.substr(0,string.length-1);
			}
			string = string.substr(0,11);
			return string;
		},
		formatearCarnetExtranjeria: function(string){
			if(string!==undefined){
				string = string.split(" ").join("");
				if(string.length>12){
					string = string.substr(0,12);
				}
				return string;
			}else{
				return "";
			}
		},
		formatearPasaporte: function(string){
			if(string!==undefined){
				string = string.split(" ").join("");
				if(string.length>12){
					string = string.substr(0,12);
				}
				return string;
			}else{
				return "";
			}
		},
		formatearDocumento: function(tipoDocumento,codigoDocumento,tipoPersona){
			if(tipoPersona=== undefined){
				tipoPersona = 'N';
				if(parseInt(tipoDocumento) === 8){
					tipoPersona = 'J';
				}
			}
			var formatearDNI = this.formatearDNI;
			var formatearRUC = this.formatearRUC;
			var formatearCarnetExtranjeria = this.formatearCarnetExtranjeria;
			var formatearPasaporte = this.formatearPasaporte;
			if(parseInt(tipoDocumento.codigo)===4){//DNI
				return formatearDNI(codigoDocumento);
			}else if(parseInt(tipoDocumento.codigo)===8){//RUC
				return formatearRUC(codigoDocumento,tipoPersona);
			}else if(parseInt(tipoDocumento.codigo)===2){//CE
				return formatearCarnetExtranjeria(codigoDocumento);
			}else if(parseInt(tipoDocumento.codigo)===3){//PS
				return formatearPasaporte(codigoDocumento);
			}else{
				return codigoDocumento;
			}			
		},
		validarDocumento: function(tipoDocumento,codigoDocumento,showAlert,tipoPersona){
			if(tipoPersona === undefined){
				tipoPersona = 'N';
				if(parseInt(tipoDocumento.codigo) === 8){
					tipoPersona = 'J';
				}	
			}
			var validarDNI = this.validarDNI;
			var validarRUC = this.validarRUC;
			var validarCarnetExtranjeria = this.validarCarnetExtranjeria;
			var validarPasaporte = this.validarPasaporte;
			if(parseInt(tipoDocumento.codigo)===4){//DNI
				return validarDNI(codigoDocumento);
			}else if(parseInt(tipoDocumento.codigo)===8){//RUC
				return validarRUC(codigoDocumento,tipoPersona,showAlert);
			}else if(parseInt(tipoDocumento.codigo)===2){//CE
				return validarCarnetExtranjeria(codigoDocumento);
			}else if(parseInt(tipoDocumento.codigo)===3){//PS
				return validarCarnetExtranjeria(codigoDocumento);
			}else{
				return false;
			}			
		}
	};
})
/*INICIO INTEGRACION EC*/
/**
 * @feature V4-HU005
 *  - no se hace la verificación de la validación de documento de identidad
 * @feature V4-HU006
 *  - add dependecy injetions [CONFIGURACION_APP]
 *  - agregar validación, si se requiere, de los archivos del documento de identidad de la persona
 */
.service("validacionDeTitular",function(_,validacionService,$rootScope,$filter,TIPO_ASOCIADO,TIPO_DOCUMENTO,TABAJADOR,contratoService,DATOS_RUC,TIPO_RELACION_ID, $q, APP, prospectoService, CONFIGURACION_APP){
/*FIN INTEGRACION EC*/
	var validarDatosPersonaRelacionada;
	var dateFilter = $filter('date');
	var TIPO_TRABAJADOR = TABAJADOR.TIPO;
	return {
		validarPersonaJuridaActiva: function(personaEstado){
			if(personaEstado===DATOS_RUC.ESTADO.ACTIVO || personaEstado ===undefined){
				return true;
			}
			return false;
		},
		validarPersonaJuridaHabida: function(pesonaFlag_22){
			if(pesonaFlag_22===DATOS_RUC.FLAGG_22.HABIDO || pesonaFlag_22 === undefined){
				return true;
			}
			return false;			
		},
		/**
		 * 
		 * @param persona
		 * @param tempContrato
		 * @param form
		 * @returns {boolean}
		 * @feature V4-HU005
		 *  - no se hace la verificación de la validación de documento de identidad
         * @feature V4-HU006
         *  - validar, si se requiere, los archivos del documento de identidad
         * @feature V4-HU020
         *  - cliente no domiciliado
		 */
		validarDatosPersonaNatural: function(persona,tempContrato,form){

			if(!$rootScope.respuestaError) $rootScope.respuestaError = {};

			if($rootScope.me.documentoDigital){
				/** inicio @feature V4-HU005 **//*
				if(!persona.personaDocumentoValidado){
					$rootScope.respuestaError.mensaje="Es necesario escanear el DNI";
					return false;	
				}
				*//** fin @feature V4-HU005 **/
			}

			if(!persona.tipoDocumentoID){		
				$rootScope.respuestaError.mensaje="El tipo de documento es obligatorio";
				return false;
			}else if(!persona.personaCodigoDocumento){
				$rootScope.respuestaError.mensaje="El número de documento es obligatorio";
				if(form != undefined){
					form.inputDocumento.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.personaNombre){
				$rootScope.respuestaError.mensaje="El nombre es obligatorio";
				if(form != undefined){
					form.inputNombre.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.personaApellidoPaterno){
				$rootScope.respuestaError.mensaje="El Apellido Paterno es obligatorio";
				if(form != undefined){
					form.inputApPaterno.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.personaApellidoMaterno && parseInt(persona.tipoDocumentoID) === parseInt(TIPO_DOCUMENTO.DNI)){
				$rootScope.respuestaError.mensaje="El Apellido Materno es obligatorio";
				if(form != undefined){
					form.inputApMaterno.$setValidity("validar", false);
				}					
				return false;
			}else if(!validacionService.validarCorreElectronico(persona.personaCorreoPersonal)){
				$rootScope.respuestaError.mensaje="El correo personal es obligatorio, y debe tener el formato user@domain.com";
				if(form != undefined){
					form.inputEmailN.$setValidity("validar", false);
				}					
				return false;
			}else if((!persona.personaTelefono || persona.personaTelefono==='') &&
					(!persona.direcciones[0].direccionTelefono1 || persona.direcciones[0].direccionTelefono1 ==='')){
				$rootScope.respuestaError.mensaje="Es necesario que ingrese el número de teléfono o celular personal";
				if(form != undefined){
					form.inputTelf.$setValidity("validar", false);
				}					
				return false;
			}else if(persona.direcciones[0].direccionTelefono1 && persona.direcciones[0].direccionTelefono1 !=='' && persona.direcciones[0].direccionTelefono1.length<6){					
				$rootScope.respuestaError.mensaje="El teléfono personal debe tener como mínimo 6 dígitos";
				if(form != undefined){
					form.inputTelf.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.direcciones[1] && persona.direcciones[1].direccionTelefono1 && persona.direcciones[1].direccionTelefono1 !=='' && persona.direcciones[1].direccionTelefono1.length<6){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El teléfono laboral ingresado debe tener como mínimo 6 dígitos";
				if(form != undefined){
                    /*inicio @issue EVALCRED-FINAL*/
					form.inputTelfLab.$setValidity("validar", false);
                    /*fin @issue EVALCRED-FINAL*/
				}					
				return false;
			}else if(persona.personaTelefono && persona.personaTelefono!==''&&persona.personaTelefono.length!=9) {
                $rootScope.respuestaError.mensaje = "El celular debe tener 9 dígitos";
                if (form != undefined) {
                    form.inputCel.$setValidity("validar", false);
                }
                return false;
            /*inicio @issue VTAPROD-180*/
            }
                /*}else if(persona.personaRUC){
                    var tipoDocumentoTmpRUC = {'codigo':8};
                    $rootScope.respuestaError.mensaje =  validacionService.validarDocumento(tipoDocumentoTmpRUC,persona.personaRUC,false,'N');
                    if($rootScope.respuestaError.mensaje===true){
                        $rootScope.respuestaError.mensaje=null;
                        return true;
                    }else{
                        if(form != undefined){
                            form.inputRucOpc.$setValidity("validar", false);
                        }
                    }
                    return false;
                }else if(!persona.sexoID){*/
            if(persona.personaRUC) {
                var tipoDocumentoTmpRUC = {'codigo': 8};
                $rootScope.respuestaError.mensaje = validacionService.validarDocumento(tipoDocumentoTmpRUC, persona.personaRUC, false, 'N');
                if ($rootScope.respuestaError.mensaje !== true) {
                    if (form != undefined) {
                        form.inputRucOpc.$setValidity("validar", false);
                    }
                    return false;
                }
            }
            if(!persona.sexoID){
            /*fin @issue VTAPROD-180*/
				$rootScope.respuestaError.mensaje="El sexo es obligatorio";
				if(form != undefined){
					form.selectSexo.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.estadoCivilID){
				$rootScope.respuestaError.mensaje="El Estado civil es obligatorio";
				if(form != undefined){
					form.selectEstCivil.$setValidity("validar", false);
				}					
				return false;
			}else if((persona.estadoCivilID==='C'|| persona.estadoCivilID==='N') && persona.personasRelacionadas.length===0){
				$rootScope.respuestaError.mensaje="La información del cónyuge es obligatoria";
				return false;
			}else if(persona.personaTieneVinculado&&persona.personasVinculadas.length===0){
				$rootScope.respuestaError.mensaje="Si usted tiene parientes que trabajan en Pandero debe ingresarlos como personas vinculadas";
				return false;
			}else if(!persona.personaFechaNacimiento){
				$rootScope.respuestaError.mensaje="La fecha de nacimiento es obligatorio, ingrese una fecha válida";
				if(form != undefined){
					form.inputFecNacimiento.$setValidity("validar", false);
				}					
				return false;
			}else {
				var hoy=new Date();
				var diferencia=hoy-persona.personaFechaNacimiento;
				console.log(diferencia);
				console.log(typeof(diferencia));	

				var fechaHoy= dateFilter( new Date(), 'dd/MM/yyyy');
				var fechasHoy=fechaHoy.split("/");
				var momentHoy = moment([parseInt(fechasHoy[2]), parseInt(fechasHoy[1])-1, parseInt(fechasHoy[0])]);
				
				var personaFechaNacimiento= dateFilter(persona.personaFechaNacimiento, 'dd/MM/yyyy');	  			
				var fechas=personaFechaNacimiento.split("/");
				var momentNacimiento = moment([parseInt(fechas[2]), parseInt(fechas[1])-1, parseInt(fechas[0])]);

				diferencia = momentHoy.diff(momentNacimiento, 'years');
				
				if(diferencia<18 || diferencia>70){
					if(persona.personaTipoRelacionID != TIPO_RELACION_ID.REPRESENTANTE_LEGAL){
						$rootScope.respuestaError.mensaje="La persona no puede ingresar a Pandero ya que no cumple los rangos de edad definidos (18 a 70 años)";			
						if(form != undefined){
							form.inputFecNacimiento.$setValidity("validar", false);
						}						
						return false;
					}
				}
				if(!persona.paisID){
					$rootScope.respuestaError.mensaje="El país de nacionalidad es obligatorio";
					if(form != undefined){
						form.selectPaisNac.$setValidity("validar", false);
					}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(!persona.esResidente && !persona.paisResidenciaId){
					$rootScope.respuestaError.mensaje="El país de recidencia es obligatorio";
					if(form != undefined){
						form.selectPaisRec.$setValidity("validar", false);
					}
					return false;
				}
				/** fin @feature V4-HU020 **/
				/*Solo para el caso del titular, no para el respaldo*/
				if(persona.asociadoTipoID===TIPO_ASOCIADO.TITULAR){
					if(!persona.departamentoNacimientoID && persona.paisID==="189"){
						$rootScope.respuestaError.mensaje="El departamento de nacimiento es obligatorio";
						if(form != undefined){
							form.selectDepartamentoNac.$setValidity("validar", false);
						}
						return false;
					}
					if(!persona.provinciaNacimientoID && persona.paisID==="189"){
						$rootScope.respuestaError.mensaje="La provincia de nacimiento es obligatorio";
						if(form != undefined){
							form.selectProvinciaNac.$setValidity("validar", false);
						}
						return false;
					}
					if(!persona.distritoNacimientoID && persona.paisID==="189"){
						$rootScope.respuestaError.mensaje="El distrito de nacimiento es obligatorio";
						if(form != undefined){
							form.selectDistritoNac.$setValidity("validar", false);
						}
						return false;
					}
					/** inicio  @feature V4-HU006 **/
					if(persona.validarDocumentos && (!persona.archivosDocumentoIdentidad || persona.archivosDocumentoIdentidad.length < CONFIGURACION_APP.PERSONA.DOCUMENTO_IDENTIDAD.ARCHIVOS_MINIMOS)) {
						$rootScope.respuestaError.mensaje="Debes cargar al menos "+CONFIGURACION_APP.PERSONA.DOCUMENTO_IDENTIDAD.ARCHIVOS_MINIMOS+" archivos del documento de identidad DNI/CE";
						return false;
					}
					/** fin  @feature V4-HU006 **/
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].tipoViaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El tipo de vía de la dirección personal es obligatorio";
					if(form != undefined){
						form.selectTipoVia.$setValidity("validar", false);
					}						
					return false;
				}
				if(!persona.direcciones[0].direccionDetalle){
					$rootScope.respuestaError.mensaje="La dirección personal es obligatorio";
					if(form != undefined){
						form.inputDireccion.$setValidity("validar", false);
					}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].direccionNombreZona && persona.direcciones[0].tipoZonaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El nombre de la zona de la dirección personal es obligatorio";
					if(form != undefined){
						form.inputZona.$setValidity("validar", false);
					}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].tipoZonaID && persona.direcciones[0].direccionNombreZona){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El tipo de zona de la dirección personal es obligatorio";
					if(form != undefined){
						form.inputTipoZona.$setValidity("validar", false);
					}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].departamentoID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El departamento de la dirección personal es obligatorio";
					if(form != undefined){
						form.inputDepartamentoTitular.$setValidity("validar", false);
				}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].provinciaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="La provincia de la dirección personal es obligatorio";
					if(form != undefined){
						form.inputProvinciaTitular.$setValidity("validar", false);
				}						
					return false;
				}
				/** inicio @feature V4-HU020 **/
				if(persona.esResidente && !persona.direcciones[0].distritoID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El distrito de la dirección personal es obligatorio";
					if(form != undefined){
						form.inputDistritoTitular.$setValidity("validar", false);
				}						
					return false;
				}
				if(!persona.trabajadorTipoID){
					$rootScope.respuestaError.mensaje="El tipo de trabajador es obligatorio";
					if(form != undefined){
						form.inputTipoTrabajo.$setValidity("validar", false);
				}						
					return false;
				}
				if(persona.trabajadorTipoID!==TIPO_TRABAJADOR.NO_REMUNERADO){
					if(!persona.personaEmpresaNombre){
						$rootScope.respuestaError.mensaje="El nombre de la empresa es obligatorio";
						if(form != undefined){
							form.inputCentroTrabajo.$setValidity("validar", false);
						}							
						return false;
					}
					if(!persona.personaFechaIngresoTrabajo){
						$rootScope.respuestaError.mensaje="La fecha de ingreso al trabajo es obligatorio, ingrese una fecha válida";
						if(form != undefined){
							form.inputFechaIngresoTrabajo.$setValidity("validar", false);
						}							
						return false;
					}
					if(persona.personaFechaIngresoTrabajo){
						var hoy = new Date();
						if(persona.personaFechaIngresoTrabajo>hoy){
							$rootScope.respuestaError.mensaje="La fecha de ingreso al trabajo es obligatorio, ingrese una fecha válida";
							if(form != undefined){
								form.inputFechaIngresoTrabajo.$setValidity("validar", false);
							}							
							return false;
						}						
					}
				}else{
					if(!persona.personaIngresoMensual){
						persona.personaIngresoMensual = 0;
					}
				}
				/*inicio @issue VTADIG2-447*/
                if(persona.personaActividadMinera && persona.trabajadorTipoID == 'n'){
                    $rootScope.respuestaError.mensaje="Una persona No remunerada, no puede realizar Actividad Minera.";
                    return false;
                }
                /*fin @issue VTADIG2-447*/
				hoy=new Date();

				fechaHoy= dateFilter( new Date(), 'dd/MM/yyyy');
				fechasHoy=fechaHoy.split("/");
				momentHoy = moment([parseInt(fechasHoy[2]), parseInt(fechasHoy[1])-1, parseInt(fechasHoy[0])]);
				var noPersonaFechaIngresoTrabajo = false;
				if(!persona.personaFechaIngresoTrabajo){
					persona.personaFechaIngresoTrabajo = new Date();
					noPersonaFechaIngresoTrabajo = true;
				}
				var personaIngresoTrabajo= dateFilter(persona.personaFechaIngresoTrabajo, 'dd/MM/yyyy');	  			
				fechas=personaIngresoTrabajo.split("/");
				var momentIngresoTrabajo = moment([parseInt(fechas[2]), parseInt(fechas[1])-1, parseInt(fechas[0])]);

				diferencia=momentHoy.diff(momentIngresoTrabajo, 'years');
				console.log("AÑOS ->",diferencia);
				persona.antiguedadInsuficiente=0;
				if(diferencia < 3){
					persona.antiguedadInsuficiente=diferencia;
				}
				if(noPersonaFechaIngresoTrabajo){
					persona.personaFechaIngresoTrabajo = undefined;
				}				
				if(!persona.personaOcupacion){
					$rootScope.respuestaError.mensaje="La ocupación de la persona es obligatorio";
					return false;
				}
				// INICIO VTADIG2-185- José Martinez 12/02/2018
				if(!persona.monedaIDIngresoMensual){
					$rootScope.respuestaError.mensaje="El tipo de moneda es obligatorio";
					if(form != undefined){
						form.selectTipoMoneda.$setValidity("validar", false);
					}											
					return false;
				}	
				// FIN  VTADIG2-185- José Martinez 12/02/2018			
				if(persona.personaIngresoMensual===undefined || persona.personaIngresoMensual===""){
					$rootScope.respuestaError.mensaje="El ingreso mensual es obligatorio";
					if(form != undefined){
						form.inputIngresoNeto.$setValidity("validar", false);
					}						
					return false;
				}
				/*Solo para el caso del titular, no para el respaldo*/
				if(persona.asociadoTipoID===TIPO_ASOCIADO.TITULAR){
					if(persona.trabajadorTipoID!==TIPO_TRABAJADOR.NO_REMUNERADO){
						if((!persona.direcciones[1].direccionTelefono1 || persona.direcciones[1].direccionTelefono1==='')&&persona.personaTipoID!=="N"){
							$rootScope.respuestaError.mensaje="El teléfono laboral es obligatorio";
							if(form != undefined){
								form.inputTelfLab.$setValidity("validar", false);
				}								
							return false;
						}
						if(!validacionService.validarCorreElectronico(persona.personaCorreoTrabajo)&&persona.personaTipoID!=="N"){
							$rootScope.respuestaError.mensaje="El correo laboral es obligatorio, y debe tener el formato user@domain.com";
							if(form != undefined){
								form.inputEmailLab.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && !persona.direcciones[1].tipoViaID){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="El tipo de vía de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputTipoViaLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						if(!persona.direcciones[1].direccionDetalle){
							$rootScope.respuestaError.mensaje="La dirección laboral es obligatorio";
							if(form != undefined){
								form.inputDireccionLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && persona.direcciones[1].tipoZonaID && !persona.direcciones[1].direccionNombreZona){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="El nombre de la zona de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputZonaLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && persona.direcciones[1].direccionNombreZona && !persona.direcciones[1].tipoZonaID){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="El tipo de zona de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputTipoZonaLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && !persona.direcciones[1].departamentoID){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="El departamento de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputDepartamentoLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && !persona.direcciones[1].provinciaID){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="La provincia de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputProvinciaLaboral.$setValidity("validar", false);
				}								
							return false;
						}
						/** inicio @feature V4-HU020 **/
						if(persona.esResidente && !persona.direcciones[1].distritoID){
						/** fin @feature V4-HU020 **/
							$rootScope.respuestaError.mensaje="El distrito de la dirección laboral es obligatorio";
							if(form != undefined){
								form.inputDistritoLaboral.$setValidity("validar", false);
				}								
							return false;
						}
					}					
					var array = $rootScope.me.usuarioTelefonos.split(',');
					if(persona.personaTipoID == 'N'){
						console.log(array);
						for (var i=0 ; i< array.length ; i++){
							if(array[i]){
								if( array[i] == persona.direcciones[0].direccionTelefono1 ){
									$rootScope.respuestaError.mensaje="El número de telefono no pertenece al titular";
									contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.direcciones[0].direccionTelefono1);
									return false;
								}
								if( array[i] == persona.personaTelefono	){
									$rootScope.respuestaError.mensaje="El número de celular no pertenece al titular";
									contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.personaTelefono);
									return false;
								}
								if( array[i] == persona.direcciones[1].direccionTelefono1 ){
									$rootScope.respuestaError.mensaje="El número de telefono laboral no pertenece al titular";
									contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.direcciones[1].direccionTelefono1);
									return false;
								}	
							}							
						}	
					}
				}																							
				if(persona.estadoCivilID.trim()!=="C"&&persona.estadoCivilID.trim()!=="N"){
					persona.personasRelacionadas = [];
				}
				if(persona.personasRelacionadas!==undefined){
					var validRelacionadas = true;
					validarDatosPersonaRelacionada = this.validarDatosPersonaRelacionada;
					var relacionadaIncompleta = undefined;
					angular.forEach(persona.personasRelacionadas,function(relacionada){
						validRelacionadas = validRelacionadas&&validarDatosPersonaRelacionada(relacionada,false,1,undefined,tempContrato);
						if(!validRelacionadas&&!relacionadaIncompleta){
							relacionadaIncompleta = relacionada;
						}
					});
					if(!validRelacionadas){
						$rootScope.respuestaError.mensaje="Persona relacionada "+(relacionadaIncompleta.personaNombreCompleto?relacionadaIncompleta.personaNombreCompleto:relacionadaIncompleta.personaNombre)+": "+$rootScope.respuestaError.mensaje;
						return false;
					}
				}
				if(persona.personasVinculadas!==undefined){
					var validVinculadas = true;
					validarDatosPersonaRelacionada = this.validarDatosPersonaRelacionada;
					var vinculadaIncompleta = undefined;
					angular.forEach(persona.personasVinculadas,function(relacionada){
						validVinculadas = validVinculadas&&validarDatosPersonaRelacionada(relacionada,false,0, undefined, tempContrato);
						if(!validVinculadas&&!vinculadaIncompleta){
							vinculadaIncompleta = relacionada;
						}
					});
					if(!validVinculadas){
						$rootScope.respuestaError.mensaje="Persona vinculada "+(vinculadaIncompleta.personaNombreCompleto?vinculadaIncompleta.personaNombreCompleto:vinculadaIncompleta.personaNombre)+": "+$rootScope.respuestaError.mensaje;
						return false;
					}
				}
				return true;								
			}
		},
		/**
		 * 
		 * @param persona
		 * @param showAlerts
		 * @param tipo
		 * @param form
		 * @param tempContrato
		 * @param modulo
		 * @returns {*}
		 * @feature V4-HU020
		 *  - cliente no domiciliado
		 */
		validarDatosPersonaRelacionada: function(persona,showAlerts,tipo,form, tempContrato, modulo){
			var delay = $q.defer();
			var data = {};
    		angular.forEach(form, function (value, key) {
        		if (typeof value === 'object' && value.hasOwnProperty('$modelValue')){
        			var nameElement = value.$name;
        			form[nameElement].$setValidity("validar", true);
        		}
    		});
    		/** inicio @feature V4-HU020 **/
    		persona.esResidente = persona.esResidente !== false;
			/** fin @feature V4-HU020 **/

			if(!$rootScope.respuestaError) $rootScope.respuestaError = {};
			var TIPOS = {
				VINCULADA: 0,
				RELACIONADA: 1
			};
			var valid = true;
			if(!persona.tipoDocumentoID){
				$rootScope.respuestaError.mensaje="Tipo de documento es obligatorio";
				valid = false;
			}else if(!persona.personaCodigoDocumento){
				$rootScope.respuestaError.mensaje="Documento es obligatorio";
				if(form != undefined){
					form.inputDocumento.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.personaNombre){
				$rootScope.respuestaError.mensaje="Nombre es obligatorio";
				if(form != undefined){
					form.inputNombre.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.personaApellidoPaterno){
				$rootScope.respuestaError.mensaje="Apellido paterno es obligatorio";
				if(form != undefined){
					form.inputApPaterno.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.personaApellidoMaterno && parseInt(persona.tipoDocumentoID) === parseInt(TIPO_DOCUMENTO.DNI)){
				$rootScope.respuestaError.mensaje="Apellido Materno es obligatorio";
				if(form != undefined){
					form.inputApMaterno.$setValidity("validar", false);
				}					
				valid = false;
			}else if((!persona.personaTelefono ||  persona.personaTelefono==='') &&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				$rootScope.respuestaError.mensaje="Es necesario que ingrese el número de teléfono o celular";
				if(form != undefined){
					form.inputCel.$setValidity("validar", false);
				}					
				valid = false;
			}else if((persona.personaTelefono && persona.personaTelefono!==''&&persona.personaTelefono.length<6)&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){					
				$rootScope.respuestaError.mensaje="El teléfono debe tener como mínimo 6 dígitos";
				if(form != undefined){
					form.inputCel.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.personaTipoRelacionID){
				$rootScope.respuestaError.mensaje="Tipo de "+((parseInt(tipo) === parseInt(TIPOS.RELACIONADA))?"Relación":"Vínculo") +" es obligatorio";
				if(form != undefined){
					form.inputTipoRelacion.$setValidity("validar", false);
				}					
				valid = false;
			}else if((!validacionService.validarCorreElectronico(persona.personaCorreoPersonal) )&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				$rootScope.respuestaError.mensaje="El correo es obligatorio, y debe tener el formato user@domain.com";
				if(form != undefined){
					form.inputEmailN.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.personaFechaNacimiento &&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				$rootScope.respuestaError.mensaje="Fecha de nacimiento es obligatorio";
				if(form != undefined){
					form.inputFecNacimiento.$setValidity("validar", false);
				}					
				valid = false;
			}else if(!persona.paisID&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				$rootScope.respuestaError.mensaje="Pais es obligatorio";
				if(form != undefined){
					form.selectPaisNac.$setValidity("validar", false);
				}					
				valid = false;
			}

			var array = $rootScope.me.usuarioTelefonos.split(',');
			
			console.log(array);
			for (var i=0 ; i< array.length ; i++){
				if(array[i]){
					if( array[i] == persona.personaTelefono ){
						$rootScope.respuestaError.mensaje="El número de telefono no pertenece al titular";
						contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.personaTelefono);
						valid = false;
					}	
				}							
			}
			

			if(parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				var hoy=new Date();
				var diferencia=hoy-persona.personaFechaNacimiento;
				console.log(diferencia);
				console.log(typeof(diferencia));	

				var fechaHoy= dateFilter( new Date(), 'dd/MM/yyyy');
				var fechasHoy=fechaHoy.split("/");
				var momentHoy = moment([parseInt(fechasHoy[2]), parseInt(fechasHoy[1])-1, parseInt(fechasHoy[0])]);
				
				var personaFechaNacimiento= dateFilter(persona.personaFechaNacimiento, 'dd/MM/yyyy');	  			
				var fechas=personaFechaNacimiento.split("/");
				var momentNacimiento = moment([parseInt(fechas[2]), parseInt(fechas[1])-1, parseInt(fechas[0])]);

				diferencia = momentHoy.diff(momentNacimiento, 'years');
				
				if(diferencia<18 || diferencia>70){
					if(	parseInt(persona.personaTipoRelacionID) !== TIPO_RELACION_ID.CONYUGUE&&
						parseInt(persona.personaTipoRelacionID) !== TIPO_RELACION_ID.CONVIVIENTE){
						$rootScope.respuestaError.mensaje="La persona no puede ingresar a Pandero ya que no cumple los rangos de edad definidos (18 a 70 años)";
						if(form != undefined){
							form.inputFecNacimiento.$setValidity("validar", false);
						}						
					valid = false;
					}
				}	
			}
			
			if(valid&&persona.personaIngresoMensualCalif&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				//todos los casos
				if(!persona.personaOcupacionObj){
					$rootScope.respuestaError.mensaje="La ocupación es obligatorio";
					valid = false;
				}
				//si no es un trabajor no remunerado
				if(persona.trabajadorTipoID !== TIPO_TRABAJADOR.NO_REMUNERADO){
					if(!persona.personaEmpresaNombre){
						$rootScope.respuestaError.mensaje="El centro de trabajo es obligatorio";
						if(form != undefined){
							form.inputEmpresaNombre.$setValidity("validar", false);
						}						
						valid = false;
					// INICIO VTADIG2-185- José Martinez 12/02/2018
					}else if(!persona.monedaIDIngresoMensual){
						$rootScope.respuestaError.mensaje="El tipo de moneda es obligatorio";
						if(form != undefined){
							form.selectTipoMoneda.$setValidity("validar", false);
						}						
						valid = false;
					// FIN VTADIG2-185- José Martinez 12/02/2018
					}else if(!persona.personaIngresoMensual || parseFloat(persona.personaIngresoMensual)<=0){
						$rootScope.respuestaError.mensaje="El ingreso mensual neto es obligatorio";
						if(form != undefined){
							form.inputIngresoMensual.$setValidity("validar", false);
						}						
						valid = false;
					}else if(!persona.direccionLaboral){
						$rootScope.respuestaError.mensaje="Ingrese todos los datos laborales";
						valid = false;
					}else if(!persona.direccionLaboral.direccionDetalle){
						$rootScope.respuestaError.mensaje="La dirección laboral es obligatorio";
						if(form != undefined){
							form.inputDireccionLaboral.$setValidity("validar", false);
						}						
						valid = false;
					/** inicio @feature V4-HU020 **/
					}else if(persona.esResidente && !persona.direccionLaboral.departamentoID){
					/** fin @feature V4-HU020 **/
						$rootScope.respuestaError.mensaje="El departamento laboral es obligatorio";
						if(form != undefined){
							form.inputDepartamentoLaboral.$setValidity("validar", false);
						}						
						valid = false;
					/** inicio @feature V4-HU020 **/
					}else if(persona.esResidente && !persona.direccionLaboral.provinciaID){
					/** fin @feature V4-HU020 **/
						$rootScope.respuestaError.mensaje="La provincia laboral es obligatorio";
						if(form != undefined){
							form.inputProvinciaLaboral.$setValidity("validar", false);
						}						
						valid = false;
					/** inicio @feature V4-HU020 **/
					}else if(persona.esResidente && !persona.direccionLaboral.distritoID){
					/** fin @feature V4-HU020 **/
						$rootScope.respuestaError.mensaje="El distrito laboral es obligatorio";
						if(form != undefined){
							form.inputDistritoLaboral.$setValidity("validar", false);
						}						
						valid = false;
					}
					/** inicio @feature V4-HU020 **/
					else if(!persona.esResidente && !persona.paisResidenciaId) {
						$rootScope.respuestaError.mensaje="El país de residencia es obligatorio";
						if(form != undefined){
							form.selectPaisRec.$setValidity("validar", false);
						}
						valid = false;
					}
					/** fin @feature V4-HU020 **/
				/*INICIO INTEGRACION EC*/
				}				
				if(persona.evaluacionCrediticia&&persona.evaluacionCrediticia.aprobado==false){
					$rootScope.respuestaError.mensaje=persona.evaluacionCrediticia.mensaje;
					valid = false;
				/*FIN INTEGRACION EC*/
				}
			}
			if(valid&&persona.personaTipoRelacionID === '17'&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				if(!persona.sexoID){
					$rootScope.respuestaError.mensaje="El sexo es obligatorio";
					if(form != undefined){
						form.selectSexo.$setValidity("validar", false);
				}						
					valid = false;
				}else if(!persona.direccionDomiciliada){
					$rootScope.respuestaError.mensaje="Ingrese todos los datos domiciliares";
					valid = false;
				/** inicio @feature V4-HU020 **/
				}else if(persona.esResidente && !persona.direccionDomiciliada.tipoViaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El tipo de vía es obligatoria";
					if(form != undefined){
						form.selectTipoVia.$setValidity("validar", false);
				}						
					valid = false;
				}else if (!persona.direccionDomiciliada.direccionDetalle){
					$rootScope.respuestaError.mensaje="La dirección es obligatoria";
					if(form != undefined){
						form.inputDireccion.$setValidity("validar", false);
				}						
					valid = false;
				/** inicio @feature V4-HU020 **/
				}else if (persona.esResidente && persona.direccionDomiciliada.tipoZonaID && !persona.direccionDomiciliada.direccionNombreZona){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El nombre de la zona es obligatoria";
					if(form != undefined){
						form.inputZona.$setValidity("validar", false);
				}						
					valid = false;
				/** inicio @feature V4-HU020 **/
				}else if (persona.esResidente && persona.direccionDomiciliada.direccionNombreZona && !persona.direccionDomiciliada.tipoZonaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El tipo de zona es obligatoria";
					if(form != undefined){
						form.inputTipoZona.$setValidity("validar", false);
				}						
					valid = false;
				/** inicio @feature V4-HU020 **/
				}else if (persona.esResidente && !persona.direccionDomiciliada.departamentoID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El departamento es obligatorio";
					if(form != undefined){
						form.inputDepartamento.$setValidity("validar", false);
				}						
					valid = false;
				/** inicio @feature V4-HU020 **/
				}else if (persona.esResidente && !persona.direccionDomiciliada.provinciaID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="La provincia es obligatoria";
					if(form != undefined){
						form.inputProvincia.$setValidity("validar", false);
				}						
					valid = false;
				/** inicio @feature V4-HU020 **/
				//}else if (!persona.direccionDomiciliada.distritoID){
				}else if (persona.esResidente && !persona.direccionDomiciliada.distritoID){
				/** fin @feature V4-HU020 **/
					$rootScope.respuestaError.mensaje="El distrito es obligatorio";
					if(form != undefined){
						form.inputDistrito.$setValidity("validar", false);
				}						
					valid = false;
				}
				/** inifio @feature V4-HU020 **/
				else if(!persona.esResidente && !persona.paisResidenciaId) {
					$rootScope.respuestaError.mensaje="El país de residencia es obligatorio";
					if(form != undefined){
						form.selectPaisRec.$setValidity("validar", false);
					}
					valid = false;
				}
				/** fin @feature V4-HU020 **/
			/*INICIO INTEGRACION EC*/
			} 
			//return valid;
			if(valid&&persona.personaIngresoMensualCalif&&parseInt(tipo)===parseInt(TIPOS.RELACIONADA)){
				prospectoService.obtenerEvaluacionCrediticia(persona.tipoDocumentoID,persona.personaCodigoDocumento, modulo)
				.then(function(ev){
					if(ev&&ev.aprobado==false){
						valid = false;
						$rootScope.respuestaError.mensaje = ev.mensaje;
					}
					delay.resolve(valid);
				})
			}else{
				delay.resolve(valid);
			}
			return delay.promise;
			/*FIN INTEGRACION EC*/
		},
		/**
		 * 
		 * @param persona
		 * @param tempContrato
		 * @param form
		 * @returns {boolean}
		 * @feature V4-HU005
		 *  - no se hace la verificación de la validación de documento de identidad
         * @feature V4-HU020
         *  - cliente no domiciliado
		 */
		validarDatosPersonaJuridica: function(persona,tempContrato,form){
			if(!$rootScope.respuestaError) $rootScope.respuestaError = {};

			var validarDatosPersonaRelacionada;

			if($rootScope.me.documentoDigital){
				/** inicio @feature V4-HU005 **//*
				if(persona.personasRelacionadas){
					for (const relacionado of persona.personasRelacionadas) {
						if(relacionado.tipoDocumentoID==TIPO_DOCUMENTO.DNI&&!relacionado.personaDocumentoValidado){
							$rootScope.respuestaError.mensaje="Para continuar debe escanear el dni de los representantes legales.";
							return false;
						}
					}					
				}
				*//** fin @feature V4-HU005 **/

			}

			if(!persona.tipoDocumentoID){
				$rootScope.respuestaError.mensaje="El tipo de documento es obligatorio";
				return false;
			}else if(!persona.personaCodigoDocumento){
				$rootScope.respuestaError.mensaje="El número de documento es obligatorio";
				if(form != undefined){
					form.inputDocumento.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.personaRazonSocial){
				$rootScope.respuestaError.mensaje="La razón social es obligatorio";
				if(form != undefined){
					form.inputRazonSocial.$setValidity("validar", false);
				}					
				return false;
			}else if(!validacionService.validarCorreElectronico(persona.personaCorreoTrabajo)){
				$rootScope.respuestaError.mensaje="El correo personal es obligatorio, y debe tener el formato user@domain.com";
				if(form != undefined){
					form.inputEmailJ.$setValidity("validar", false);
				}					
				return false;
			}else if(!validacionService.validarCorreElectronico(persona.personaCorreoFacturacionElectronica)){
				$rootScope.respuestaError.mensaje="El correo de facturación electrónica es obligatorio, y debe tener el formato user@domain.com";
				if(form != undefined){
					form.inputEmailFact.$setValidity("validar", false);
				}					
				return false;
			}else if((!persona.personaTelefono || persona.personaTelefono==='') &&
			(!persona.direcciones[0].direccionTelefono1 || persona.direcciones[0].direccionTelefono1 ==='')){
				$rootScope.respuestaError.mensaje="Es necesario que ingrese el número de teléfono o celular";
				if(form != undefined){
					form.inputTelf.$setValidity("validar", false);
				}					
				return false;
			}
			
			var array = $rootScope.me.usuarioTelefonos.split(',');

			if(persona.direcciones[0].direccionTelefono1 && persona.direcciones[0].direccionTelefono1 !=='' ){					
				if(persona.direcciones[0].direccionTelefono1.length<6){
					$rootScope.respuestaError.mensaje="El teléfono fijo debe tener como mínimo 6 dígitos";
					if(form != undefined){
						form.inputTelf.$setValidity("validar", false);
					}					
					return false;
				}

				for (var i=0 ; i< array.length ; i++){
					if(array[i]){
						if( array[i] == persona.direcciones[0].direccionTelefono1 ){
							$rootScope.respuestaError.mensaje="El número de teléfono no pertenece al titular";
							contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.direcciones[0].direccionTelefono1);
							return false;
						}
					}
				}
			}
			

						
			
			if(persona.personaTelefono && persona.personaTelefono!==''){
				if(persona.personaTelefono.length!=9){
					$rootScope.respuestaError.mensaje="El celular debe tener 9 dígitos";
					if(form != undefined){
						form.inputCel.$setValidity("validar", false);
					}					
					return false;
				}
				for (var i=0 ; i< array.length ; i++){
					if(array[i]){
						if( array[i] == persona.personaTelefono ){
							$rootScope.respuestaError.mensaje="El número de celular no pertenece al titular";
							contratoService.enviarEmailPorDuplicacionTelf(parseInt(tempContrato.contrato.proformaID),persona.personaTelefono);
							return false;
						}
					}
				}				
				
			} 
			var representanteLegal =_.find(persona.personasRelacionadas, function(persona){ return persona.personaTipoRelacionID == 17; });

			if(persona.personasRelacionadas.length===0){
				// INICIO VTADIG2-185- José Martinez 12/02/2018
				$rootScope.respuestaError.mensaje="Debe ingresar almenos una persona relacionada";
				// FIN VTADIG2-185- José Martinez 12/02/2018
				return false;
			}else if(persona.personaTieneVinculado&&persona.personasVinculadas.length===0){
				$rootScope.respuestaError.mensaje="Debe ingresar personas vinculadas";
				return false;
			}else if(!persona.personaFechaConstitucion){
				$rootScope.respuestaError.mensaje="La fecha de constitución es obligatorio, ingrese una fecha válida";
				if(form != undefined){
					form.inputFechaConsitucion.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.paisID){
				$rootScope.respuestaError.mensaje="El país es obligatorio";
				if(form != undefined){
					form.selectPaisNac.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/	
			}
			else if(!persona.esResidente && !persona.paisResidenciaId){
				$rootScope.respuestaError.mensaje="El país de residencia es obligatorio";
				if(form != undefined){
					form.selectPaisRec.$setValidity("validar", false);
				}
				return false;
			/** fin @feature V4-HU020 **/	
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].tipoViaID){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El tipo de vía de la dirección es obligatorio";
				if(form != undefined){
					form.selectTipoVia.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.direcciones[0].direccionDetalle){
				$rootScope.respuestaError.mensaje="La dirección es obligatorio";
				if(form != undefined){
					form.inputDireccion.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].direccionNombreZona && persona.direcciones[0].tipoZonaID){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El nombre de la zona de la dirección es obligatorio";
				if(form != undefined){
					form.inputZona.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].tipoZonaID && persona.direcciones[0].direccionNombreZona){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El tipo de zona de la dirección es obligatorio";
				if(form != undefined){
					form.inputTipoZona.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].departamentoID){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El departamento de la dirección es obligatorio";
				if(form != undefined){
					form.inputDepartamentoTitular.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].provinciaID){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="La provincia de la dirección es obligatorio";
				if(form != undefined){
					form.inputProvinciaTitular.$setValidity("validar", false);
				}					
				return false;
			/** inicio @feature V4-HU020 **/
			}else if(persona.esResidente && !persona.direcciones[0].distritoID){
			/** fin @feature V4-HU020 **/
				$rootScope.respuestaError.mensaje="El distrito de la dirección es obligatorio";
				if(form != undefined){
					form.inputDistritoTitular.$setValidity("validar", false);
				}					
				return false;
			}else if(!persona.personaGiroNegocioObj){
				$rootScope.respuestaError.mensaje="El giro de negocio es obligatorio";
				return false;
			// INICIO VTADIG2-185- José Martinez 12/02/2018
			}else if(!persona.monedaIDIngresoMensual){
				$rootScope.respuestaError.mensaje="El tipo de moneda es obligatorio";
				if(form != undefined){
					form.selectTipoMonedaJuridico.$setValidity("validar", false);
				}					
				return false;
			// FIN VTADIG2-185- José Martinez 12/02/2018
			}else if(!persona.personaIngresoMensual){
				$rootScope.respuestaError.mensaje="La facturación mensual es obligatorio";
				if(form != undefined){
					form.inputFacMensual.$setValidity("validar", false);
				}					
				return false;
			}else if(persona.personaGrupoEconomico&&!persona.personaEmpresaNombre){
				$rootScope.respuestaError.mensaje="Si pertenece a un grupo económico debe ingresar el nombre de la empresa relacionada";
				if(form != undefined){
					form.inputEmpresaRelacionada.$setValidity("validar", false);
				}					
				return false;
			}else if (!representanteLegal){
				$rootScope.respuestaError.mensaje="Debe registrarse por lo menos un representante legal.";
				return false;
			}else{
				if(persona.personasRelacionadas!==undefined){
					var validRelacionadas = true;					
					validarDatosPersonaRelacionada = this.validarDatosPersonaRelacionada;
					var relacionadaIncompleta = undefined;
					angular.forEach(persona.personasRelacionadas,function(relacionada){
						validRelacionadas = validRelacionadas&&validarDatosPersonaRelacionada(relacionada,false,1,undefined,tempContrato);
						if(!validRelacionadas&&!relacionadaIncompleta){
							relacionadaIncompleta = relacionada;
						}
					});
					if(!validRelacionadas){
						$rootScope.respuestaError.mensaje="Persona relacionada "+(relacionadaIncompleta.personaNombreCompleto?relacionadaIncompleta.personaNombreCompleto:relacionadaIncompleta.personaNombre)+": "+$rootScope.respuestaError.mensaje;
						return false;
					}
				}
			 	if(persona.personasVinculadas!==undefined){
					var validVinculadas = true;
					validarDatosPersonaRelacionada = this.validarDatosPersonaRelacionada;
					var vinculadaIncompleta = undefined;
					angular.forEach(persona.personasVinculadas,function(relacionada){
						validVinculadas = validVinculadas&&validarDatosPersonaRelacionada(relacionada,false,0, undefined, tempContrato);
						if(!validVinculadas&&!vinculadaIncompleta){
							vinculadaIncompleta = relacionada;
						}
					});
					if(!validVinculadas){
						$rootScope.respuestaError.mensaje="Persona vinculada "+(vinculadaIncompleta.personaNombreCompleto?vinculadaIncompleta.personaNombreCompleto:vinculadaIncompleta.personaNombre)+": "+$rootScope.respuestaError.mensaje;
						return false;
					}
				}
				return true;	
			}
		},
		validardatosPersona: function(persona, showAlerts, tempContrato,form){
			
			/*SETEA TODOS LOS ELEMENTOS DEL FORMULARIO A VALIDOS*/
			var data = {};
    		angular.forEach(form, function (value, key) {
        		if (typeof value === 'object' && value.hasOwnProperty('$modelValue')){
        			var nameElement = value.$name;
        			form[nameElement].$setValidity("validar", true);
        		}
    		});

			if(!$rootScope.respuestaError) $rootScope.respuestaError = {};

			if(persona.personaTipoID==='N'){
				return this.validarDatosPersonaNatural(persona, tempContrato,form);
			}else if(persona.personaTipoID==='J'){				
				return this.validarDatosPersonaJuridica(persona,tempContrato,form);
			}else{
				if(showAlerts){$rootScope.respuestaError.mensaje="Tipo persona no conocido";}
				return false;
			}
		}
	};
})
.service('pagoService', function ($rootScope, $http, $q, BACK_END_URL){
	return {
		pagarObligacionMPOS : function(mposResponseBeanObject,obligacionPagoID){
			// Obtener los datos de respuesta del MPOS
			var transactionID = mposResponseBeanObject.transactionId;
			var transactionLocalDate = mposResponseBeanObject.transactionLocalDate;
			var transactionLocalTime = mposResponseBeanObject.transactionLocalTime;
			var currencyCode = mposResponseBeanObject.currencyCode;
			var voucherType = mposResponseBeanObject.voucherType;
			var transactionAmount = mposResponseBeanObject.transactionAmount;
			var bankDesc = mposResponseBeanObject.bank; 

			// Obtener el objeto de pago SAF
			var objCajaRequestBean={};			
			var objPagoBean={};
			objPagoBean.solicitudID=0;
			objPagoBean.transactionId=transactionID;
			objPagoBean.transactionLocalDate=transactionLocalDate;
			objPagoBean.transactionLocalTime=transactionLocalTime;
			objPagoBean.currencyCode=currencyCode;
			objPagoBean.voucherType=voucherType;
			objPagoBean.transactionAmount=transactionAmount;
			objPagoBean.bank=bankDesc;
			objPagoBean.modalidadPago="1"; // 1=MPOS, 2=Asbanc, 3=Pasarela
			objCajaRequestBean.pagoBean=objPagoBean;
			objCajaRequestBean.usuarioSesionBean=angular.copy($rootScope.me);
			objCajaRequestBean.usuarioSesionBean.imagenUsuario = null;
			objCajaRequestBean.usuarioSesionBean.usuarioClave = null;

			// Obtener objeto de pago asincrono
			var objPagoAsincronoBean = {};
			objPagoAsincronoBean.obligacionPagoID=obligacionPagoID;
			objPagoAsincronoBean.estadoTransaccion="1";
			objPagoAsincronoBean.transaccionID=transactionID;
			objPagoAsincronoBean.fechaTransaccion=transactionLocalDate+' '+transactionLocalTime;
			objPagoAsincronoBean.monedaTransaccion=currencyCode;
			objPagoAsincronoBean.montoTransaccion=transactionAmount;
			objPagoAsincronoBean.modalidadPago="1"; // 1=MPOS, 2=Asbanc, 3=Pasarela
			objPagoAsincronoBean.responseTransaccion=mposResponseBeanObject;
			objPagoAsincronoBean.requestPagoSAF=objCajaRequestBean;

			// Realizar pago asincrono en SAF
			var delay = $q.defer();
			$http.post(BACK_END_URL+'pagoAsincrono',objPagoAsincronoBean)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type==='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
	    },
		pagarDirectoObligacionMPOS : function(mposResponseBeanObject,obligacionPagoID){
			// Obtener los datos de respuesta del MPOS
			var transactionID = mposResponseBeanObject.transactionId;
			var transactionLocalDate = mposResponseBeanObject.transactionLocalDate;
			var transactionLocalTime = mposResponseBeanObject.transactionLocalTime;
			var currencyCode = mposResponseBeanObject.currencyCode;
			var voucherType = mposResponseBeanObject.voucherType;
			var transactionAmount = mposResponseBeanObject.transactionAmount;
			var bankDesc = mposResponseBeanObject.bank; 

			// Obtener el objeto de pago SAF
			var objCajaRequestBean={};			
			var objPagoBean={};
			objPagoBean.solicitudID=0;
			objPagoBean.transactionId=transactionID;
			objPagoBean.transactionLocalDate=transactionLocalDate;
			objPagoBean.transactionLocalTime=transactionLocalTime;
			objPagoBean.currencyCode=currencyCode;
			objPagoBean.voucherType=voucherType;
			objPagoBean.transactionAmount=transactionAmount;
			objPagoBean.bank=bankDesc;
			objPagoBean.modalidadPago="1"; // 1=MPOS, 2=Asbanc, 3=Pasarela
			objCajaRequestBean.pagoBean=objPagoBean;
			objCajaRequestBean.usuarioSesionBean=$rootScope.me;

			// Obtener objeto de pago asincrono
			var objPagoAsincronoBean = {};
			objPagoAsincronoBean.obligacionPagoID=obligacionPagoID;
			objPagoAsincronoBean.estadoTransaccion="1";
			objPagoAsincronoBean.transaccionID=transactionID;
			objPagoAsincronoBean.fechaTransaccion=transactionLocalDate+' '+transactionLocalTime;
			objPagoAsincronoBean.monedaTransaccion=currencyCode;
			objPagoAsincronoBean.montoTransaccion=transactionAmount;
			objPagoAsincronoBean.modalidadPago="1"; // 1=MPOS, 2=Asbanc, 3=Pasarela
			objPagoAsincronoBean.responseTransaccion=mposResponseBeanObject;
			objPagoAsincronoBean.requestPagoSAF=objCajaRequestBean;

			// Realizar pago directo en SAF
			var delay = $q.defer();
			$http.post(BACK_END_URL+'pagoDirecto',objPagoAsincronoBean)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type==='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}else{
					delay.reject(response);
				}
			});		
		    return delay.promise;
	    },
	    getObligacionPago: function(obligacionId){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'/obtenerObligacion/obligacionId/' + obligacionId)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type==='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}else{
					delay.reject(response);
				}
			});
		    return delay.promise;
	    }    
	};
})
/**
 * @feature V4-HU012
 */
.service('reporteComisionService', function ($q, $injector, $cordovaFileTransfer, $timeout, $sessionStorage, $ionicPopup, MIME_TYPE,BACK_END_URL){
	return {
		descargarPdf: (ano, mes) => {
			let delay = $q.defer();
			let url = `${BACK_END_URL}reporte/comision/pdf?ano=${ano}&mes=${mes}&access_token=${$sessionStorage.token}&appVersion=${$sessionStorage.appVersion}`;
			
			let filename = `reporteComision_${ano}_${mes}.pdf`;
			let targetPath = filename;
			if (cordova.file.documentsDirectory) {
				targetPath = cordova.file.documentsDirectory + filename;
			} else {
				targetPath = cordova.file.externalRootDirectory + filename;
			}
			let trustHosts = true;
			let options = {};

			$injector.get("$ionicLoading").show();
			$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
				.then(function(result) {
					$injector.get("$ionicLoading").hide();
					let ext = 'pdf';
					cordova.plugins.fileOpener2.open(
						targetPath,
						MIME_TYPE[ext.toLowerCase()],
						{
							error : function(){
								$ionicPopup.alert({ title: 'Archivo', template: "No se pudo abrir el archivo." , okType: 'button-assertive'});
							},
							success : function(){

							}
						}
					);
				}, function(err) {
					$injector.get("$ionicLoading").hide();
					let message = err.body ? JSON.parse(err.body).message : "Error al descargar el archivo";
					$ionicPopup.alert({title: 'Alerta', template: message, okType: 'button-assertive'});
				}, function (progress) {
					$timeout(function () {
					});
				});
			return delay.promise;
		}
	}
})
/**
 * 
 * @feature V4-HU010
 *  - add methods {@link #obtenerConfiguracionRecordatorioPago()} and {@link #enviarRecordatorioPagoCia(contratos)} 
 */
.service('contratoService',function ($rootScope, $http, $q, BACK_END_URL,validacionService,shareData,$timeout, $cordovaFileTransfer,$injector,MIME_TYPE,$ionicPopup){
	return {

			// INIT GBENITES -VD2-RQ30 - 25/07/2018 Etapa del contrato
			getEtapaContrato: function (contratoId) {
				var delay = $q.defer();
				$http.get(BACK_END_URL + 'contrato/etapa/' + contratoId)
					.then(function (response) {
						if (response.status === 200) {
							if (response.data.message !== '' && response.data.type === 'warning') {
								delay.reject(response.data.message);
							} else {
								delay.resolve(response.data.data);
							}
						}
					});
				return delay.promise;
			},
			getDocumentosSinAprobacion: function (contratoID) {
				var delay = $q.defer();
				$http.get(BACK_END_URL + 'contrato/documentos/' + contratoID)
					.then(function (response) {
						if (response.status === 200) {
							if (response.data.message !== '' && response.data.type === 'warning') {
								delay.reject(response.data.message);
							} else {
								delay.resolve(response.data.data);
							}
						}
					});
				return delay.promise;
			},
			getRequisitosEvalPendientes: function (contratoID) {
				var delay = $q.defer();
				$http.get(BACK_END_URL + 'contrato/requisitos/' + contratoID)
					.then(function (response) {
						if (response.status === 200) {
							if (response.data.message !== '' && response.data.type === 'warning') {
								delay.reject(response.data.message);
							} else {
								delay.resolve(response.data.data);
							}
						}
					});
				return delay.promise;
			},
			getObservaciones: function (contratoId) {
				var delay = $q.defer();
				$http.get(BACK_END_URL + 'contrato/observaciones/' + contratoId)
					.then(function (response) {
						if (response.status === 200) {
							if (response.data.message !== '' && response.data.type === 'warning') {
								delay.reject(response.data.message);
							} else {
								delay.resolve(response.data.data);
							}
						}
					});
				return delay.promise;
			},
			//FIN GBENITES - VD2-RQ30 - 25/07/2018
		contarFormatoContrato: function(formatoContrato, productoId,tipoBienID){
			var delay = $q.defer();
			// INICIO - modificado por Jose Martinez - VTAOBS1-89 - 21.09.2017
			$http.post(BACK_END_URL+'contarFormato/'+formatoContrato+"/"+productoId+"/"+tipoBienID)
			// FIN - modificado por Jose Martinez - VTAOBS1-89 - 21.09.2017
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
		},	
		getImpuestoValor: function(){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'impuestoValor')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		getComisionValor: function(productoID,grupoid){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'comision?productoID='+productoID+"&grupoid="+grupoid)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		calcularCia : function(certificadoValor,porcentajeCIA,factorIGV){
			var cia = validacionService.aDosDecimales(certificadoValor*porcentajeCIA*(1+factorIGV));	
			return cia;
		},
		calcularProrrateo : function(tempContrato,nroCuotasProrrateo, porcentajeCuotaAdmin, factorIGV){

			var programaDuracionMeses = tempContrato.contrato.grupoSeleccionado.duracionPrograma;
			
			var certificadoValor = tempContrato.contrato.certificadoSeleccionado.certificadoValor;

			var capitalCuota = validacionService.aDosDecimales(certificadoValor/programaDuracionMeses);
			var admCuota = validacionService.aDosDecimalesTrunc(capitalCuota*porcentajeCuotaAdmin*(1+factorIGV));

			if(nroCuotasProrrateo>0){			
				var programaDuracionMesesProrrateo = programaDuracionMeses-nroCuotasProrrateo;
				capitalCuota = validacionService.aDosDecimales(certificadoValor/programaDuracionMesesProrrateo);
				admCuota = validacionService.aDosDecimalesTrunc(capitalCuota*porcentajeCuotaAdmin*(1+factorIGV));				
			}
			return validacionService.aDosDecimales(capitalCuota + admCuota);
		},
		enviarEmailPorDuplicacionTelf: function(proformaID , telfDuplicado){
			var delay = $q.defer();
			$http.post(BACK_END_URL+'emailTelfDuplicado?proformaID='+proformaID+'&telfDuplicado='+telfDuplicado.toString())                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		comprobarHorasAntesAsamblea: function(grupoID,nroAsamblea){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'comprobarHorasAntesAsamblea/'+grupoID+'/'+nroAsamblea)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data);
					}
				}
			});		
		    return delay.promise;
		},
		obtenerContratoPorNumero: function(numeroContrato){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'obtenerContratoPorNumero/'+numeroContrato)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		buscarContratosCiaPendientes: function(tipo,valor1,valor2){
			var delay = $q.defer();			
			var url = BACK_END_URL+'obtenerContratosCIAPendientes?tipo='+tipo+"&valor1="+valor1;
			if(valor2!=undefined&&valor2!=null){
				url = url + "&valor2="+valor2
			}
			console.info(url);
			$http.get(url)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
		    return delay.promise;
		},
		/**
		 * @feature V4-HU010
		 * @returns {*}
		 */
		obtenerConfiguracionRecordatorioPago: () => {
			var delay = $q.defer();
			$http.get(BACK_END_URL+'pago/cia/recordatorio/config')
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		},
		/**
		 * @feature V4-HU010
		 * @param contratos
		 * @returns {*}
		 */
		enviarRecordatorioPagoCia: (contratos) => {
			var delay = $q.defer();
			$http.post(BACK_END_URL+'pago/cia/recordatorio', contratos)
				.then(function(response){
					if(response.status === 200){
						if(response.data.message !== '' && response.data.type=='warning'){
							delay.reject(response);
						}else{
							response.data.data.message = response.data.message;
							delay.resolve(response.data.data);
						}
					}
				});
			return delay.promise;
		},
		obtenerObligacionesCiaDeContrato: function(contratoId){
            var delay = $q.defer();
            $http.get(BACK_END_URL+'obtenerCIAPendientesPorContrato?contratoId='+contratoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},

		enviarCorreopagoPendiente: function(contratoId, contratoNumero){
			var delay = $q.defer();
            $http.post(BACK_END_URL+'enviarCorreoPagoPendiente?contratoId='+contratoId+"&contratoNumero="+contratoNumero)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		enviarCorreopagoRealizado: function(contratoId){
			var delay = $q.defer();
            $http.post(BACK_END_URL+'enviarCorreoPagoRealizado?contratoId='+contratoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		enviarCorreoObligacionPago: function(proformaId){
			var delay = $q.defer();
            $http.post(BACK_END_URL+'enviarCorreoObligacionPago?proformaId='+proformaId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		enviarCorreoConfirmacionPago: function(proformaId){
			var delay = $q.defer();
            $http.post(BACK_END_URL+'enviarCorreoConfirmacionPago?proformaId='+proformaId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		//INICIO VTADIG2-18 John Velasquez 03-10-2017
		obtenerDocumentosDeContrato: function(contratoId){
			var delay = $q.defer();
            $http.get(BACK_END_URL+'contratoDetalleListaAsociados/'+contratoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});			
            return delay.promise;
		},
		obtenerDocumentosDePersona: function(contratoId, personaId){
			var delay = $q.defer();
            $http.get(BACK_END_URL+'obtenerPersonaListaDocumentos/'+personaId+'?contratoId='+contratoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		agregarArchivo: function(contratoId,personaId,tipoDocumentoId,documentoContratoId, documentoId, file){
			var delay = $q.defer();
			
			var myUrl = BACK_END_URL+"documentoContrato/archivo";
			var fd = new FormData;
			
			fd.append('contratoId',contratoId);
			fd.append('personaId',personaId);
			fd.append('tipoDocumentoId',tipoDocumentoId);
			fd.append('documentoContratoId',documentoContratoId);
			fd.append('documentoId',documentoId)
			fd.append('file',file,file.name);
			fd.append('tipoArchivoId',1)

			$http.post(myUrl,fd ,{
                transformRequest: angular.indentity,
                headers: {'Content-Type': undefined}
            })
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data);
					}
				}
			});
			return delay.promise;			
		},
		eliminarArchivo: function(contratoId,personaId,archivoId){
			var delay = $q.defer();
            $http.delete(BACK_END_URL+'documentoContrato/archivo/'+archivoId+'?contratoId='+contratoId+'&personaId='+personaId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data);
					}
				}
			});
            return delay.promise;
		},
		descargarArchivo: function(contratoId,personaId,archivo){			
			var delay = $q.defer();
			var url = BACK_END_URL+'documentoContrato/archivo/'+archivo.archivoId;
			//var ref = cordova.InAppBrowser.open(url, '_system', 'location=yes');
			//delay.resolve(browserRef);
			
			//var url = "http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
			var targetPath = archivo.nombre;
			if (cordova.file.documentsDirectory) {
				targetPath = cordova.file.documentsDirectory + archivo.nombre;
            } else {				
				targetPath = cordova.file.externalRootDirectory + archivo.nombre;
            }
			var trustHosts = true;
			var options = {};

			//$cordovaFileTransfer.download("http://www.itvalledelguadiana.edu.mx/librosdigitales/Isaac%20Asimov%20-%20Yo%20Robot.pdf", targetPath, options, trustHosts)
			//$scope = $injector.get("$scope");
			//if($scope){
			//	$scope.progress = 0;
			//}
			$injector.get("$ionicLoading").show();			
			$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
				.then(function(result) {
				// Success!
					console.log(result)
					$injector.get("$ionicLoading").hide();
					//var ref = cordova.InAppBrowser.open(result.nativeURL, '_system', 'location=no');
					var filename = result.nativeURL.substring(result.nativeURL.lastIndexOf('/')+1, result.nativeURL.length);
					var ext = filename.substring(filename.lastIndexOf('.')+1, filename.length);					
					cordova.plugins.fileOpener2.open(
						targetPath,
						MIME_TYPE[ext.toLowerCase()],
						{
							error : function(){
								$ionicPopup.alert({ title: 'Archivo', template: "No se pudo abrir el archivo." , okType: 'button-assertive'});
							}, 
							success : function(){

							} 
						}
					);
				}, function(err) {
				// Error
					$injector.get("$ionicLoading").hide();
					console.log(err)
				}, function (progress) {
					$timeout(function () {
						//console.log(progress.loaded +" of "+progress.total);
						//console.log((progress.loaded / progress.total) * 100);
						//if($scope){
						//	$scope.progress = (progress.loaded / progress.total) * 100;
						//}
					});
				});		
            return delay.promise;
		},
		enviarDocumento: function(documentoContratoId){
			var delay = $q.defer();
            $http.put(BACK_END_URL+'documentoContrato/'+documentoContratoId)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data);
					}
				}
			});
            return delay.promise;
		},
		obtenerConfiguracionArchivoContrato: function(){
			var delay = $q.defer();
            $http.get(BACK_END_URL+'parametrosArchivo')
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		//FIN VTADIG2-18 John Velasquez 03-10-2017
		obtenerConveniosInstitucionales: function(){
			var delay = $q.defer();
            $http.get(BACK_END_URL+'convenioInstitucional')
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		},
		obtenerConfiguracionGrupo: function(configuracionGrupo){
			var delay = $q.defer();
            $http.get(BACK_END_URL+'configuracionByGrupo?configuracionGrupo='+configuracionGrupo)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
            return delay.promise;
		}
	};
})
.service("fechaService",function(){
	return {
		calcularDiferenciaEnMeses: function(fecha1, fecha2){
			if(!fecha1.getDate||!fecha2.getDate){
				console.info("Ambos parámetros son necesarios");
				return;
			}
			if(!fecha1.getDate||!fecha2.getDate){
				console.info("Ambos parámetros deben ser de tipo Date");
				return;
			}
			var fechaMenor;
			var fechaMayor;
			if(fecha1>fecha2){
				fechaMayor = fecha1;
				fechaMenor = fecha2;
			}else{
				fechaMayor = fecha2;
				fechaMenor = fecha1;
			}
			var diferenciaMeses = ((fechaMayor.getFullYear()-fechaMenor.getFullYear())*12) + (fechaMayor.getMonth() - fechaMenor.getMonth()) - ((fechaMayor.getDate()-fechaMenor.getDate()<0)?1:0);
			return diferenciaMeses;
		}
	}
}).service("escanerService",function(PDF417PLUGIN, $ionicPopup, $ionicPlatform){
	return {
		pdf417ScannerScan: function(process){
			var hex2a = this.hex2a;
			$ionicPlatform.ready(function() {
			cordova.plugins.pdf417Scanner.scan(
				// Register the callback handler
				function callback(scanningResult) {					
					// handle cancelled scanning
					if (scanningResult.cancelled == true) {
						//alert("Cancelled!");
						return;
					}					
					// Obtain list of recognizer results
					var resultList = scanningResult.resultList;					
					// Iterate through all results
					for (var i = 0; i < resultList.length; i++) {
						// Get individual result
						var recognizerResult = resultList[i];	
						if (recognizerResult.resultType == "Barcode result") {	
										
							if (recognizerResult.type === "CODE39") {				
								process("CODE39", recognizerResult.data);			
							}else if (recognizerResult.type === "PDF417") {								
								var raw = hex2a(recognizerResult.raw);	
								process("PDF417", raw);	
							}
						}	
					}
				},				
				// Register the error callback
				function errorHandler(err) {
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
					console.log("Scanning failed: " + err);
				},
				PDF417PLUGIN.TYPES, PDF417PLUGIN.OPTIONS, PDF417PLUGIN.LICENCEIOS, PDF417PLUGIN.LICENCEANDROID
			);
			});	
			
		},
		barcodeScanner: function(process){
			cordova.plugins.barcodeScanner.scan(
				function (result) {

					if(result.format === "CODE_39"){
						process("CODE39", result.text);
					}else if(result.format === "PDF_417"){
						process("PDF417", result.text);
					}
				},
				function (error) {
					var alertPopup = $ionicPopup.alert({ title: 'Alerta', template: "Se produjo un error con el escaner." , okType: 'button-assertive'});
					console.log("Scanning failed: " + error);
				},
				{
					preferFrontCamera : false, // iOS and Android
					showFlipCameraButton : true, // iOS and Android
					showTorchButton : true, // iOS and Android
					torchOn: false, // Android, launch with the torch switched on (if available)
					prompt : "Por favor ubique el codigo de barras dentro del area del escaner", // Android
					resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
					formats : "CODE_39,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
					orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
					disableAnimations : true // iOS
				}
			);
		},
		hex2a : function (hexx) {
		    var hex = hexx.toString();//force conversion
		    var str = '';
		    for (var i = 0; i < hex.length; i += 2)
		        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		    return str;
		}
	}
})
/*INICIO - JOHN VELASQUEZ - EDITAR PERFIL*/
.service('profileService',function($q,$http,BACK_END_URL){
	this.changeAvatarFromBase64 = function(name,imageBase64){
		var delay = $q.defer();
		$http.put(BACK_END_URL+"/profile/avatar",{
			nombre: name,
			data: imageBase64
		})
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve({message:response.data.message, data: response.data.data.data});
				}
			}
		})
		return delay.promise;
	}
	this.getPhones = function(){
		var delay = $q.defer();
		$http.get(BACK_END_URL+'/profile/telefonos')
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;
	}
	this.updatePhones = function(phones){
		var delay = $q.defer();
		$http.put(BACK_END_URL+'/profile/telefonos',phones)
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;
	}
	this.getContig = function(){
		var delay = $q.defer();
		$http.get(BACK_END_URL+'/profile/configuracion')
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;	
	}
})
/*FIN - JOHN VELASQUEZ - EDITAR PERFIL*/
/*INICIO - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
.service('versionService',function($q,$cordovaAppVersion,WEB_VERSION_ID) {

	return {
		getCurrentVersionNumber: function(){
			var delay = $q.defer();
			try{
					if($cordovaAppVersion){
						if($cordovaAppVersion.getVersionNumber){
							$cordovaAppVersion.getVersionNumber().then(function (version) {
								delay.resolve(version);
							});
						}else{
							delay.resolve(WEB_VERSION_ID);						
						}
					}else{
						delay.resolve(WEB_VERSION_ID);					
					}				
			}catch(e){				
				delay.resolve(WEB_VERSION_ID);				
			}	
			return delay.promise;
		},
		getCurrentVersionCode: function(){
			var delay = $q.defer();
			try{
					if($cordovaAppVersion){
						if($cordovaAppVersion.getVersionCode){
							$cordovaAppVersion.getVersionCode().then(function (version) {
								delay.resolve(version);
							});
						}else{
							delay.resolve(WEB_VERSION_ID);						
						}
					}else{
						delay.resolve(WEB_VERSION_ID);					
					}
			}catch(e){
				delay.resolve(WEB_VERSION_ID);				
			}			
			return delay.promise;
		}
	}
})
/*FIN - modificado por John Velasquez - VTAOBS1-3 - 20.09.2017*/
/*INICIO VTADIG2-59 JOHN VELASQUEZ 28-12-2017*/
.service("speechService",function($q,$http,BACK_END_URL,$cordovaFileTransfer){
	this.getSpeechContrato = function(contrato,titular,representanteLegal,respaldo){
		var delay = $q.defer();
		$http.post(BACK_END_URL+'speech/contrato',{
			contrato: contrato, 
			titular:titular, 
			representanteLegal: representanteLegal,
			respaldo: respaldo
		})
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;
	}
	this.getSpeak = function(speech){
		var delay = $q.defer();
		$http.post(BACK_END_URL+'speech',speech)
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data);
				}
			}
		});
		return delay.promise;
	}
	this.getSpeakBase64 = function(speech){
		var delay = $q.defer();
		$http.post(BACK_END_URL+'speech/base64',speech)		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;
	}
})
/*FIN VTADIG2-59 JOHN VELASQUEZ 28-12-2017*/
/**
 * 
 * 
 * @feature V4-HU006
 *  - se quito el prefijo "data:image/jpeg;base64," de la respuesta, solo se entrega la data en base64
 */
	.service('camaraService',function($q, CONFIGURACION_CAMARA){
	return {
		/**
		 * 
		 * @returns {*}
		 * @feature V4-HU006
		 *  - retirar prefijo "data:image/jpeg;base64,"
		 */
		hacerCaptura: function() {
			var q = $q.defer();
			if (navigator) {
				if(navigator.camera){
					navigator.camera.getPicture(function (result) {
						/** inicio @feature V4-HU006 **/
						//q.resolve('data:image/jpeg;base64,' + result);
						q.resolve(result);
						/** fin @feature V4-HU006 **/
					}, function (err) {
						q.reject(err);
					}, CONFIGURACION_CAMARA.FOTO);
				}else{
					q.reject({ message: 'Navigator->Camera is undefined' });
				}				
			} else {
				q.reject({ message: 'Navigator is undefined' });
			}
		   return q.promise;
		},
		/**
		 * 
		 * @returns {*}
		 * @feature V4-HU006
		 *  - retirar prefijo "data:image/jpeg;base64,"
		 */
		seleccionarImagen: function () {
			var q = $q.defer();
			if (navigator) {
				if (navigator.camera) {
					navigator.camera.getPicture(function (result) {
						/** inicio @feature V4-HU006 **/
						//q.resolve('data:image/png;base64,' + result);
						q.resolve(result);
						/** fin @feature V4-HU006 **/
					}, function (err) {
						q.reject(err);
					}, CONFIGURACION_CAMARA.GALERIA);
				} else {
					q.reject({ message: 'Navigator->Camera is undefined' });
				}
			} else {
				q.reject({ message: 'Navigator is undefined' });
			}
			return q.promise;
		}
	 }
})
.service('incidenciaService',function (shareData,$q,$http,BACK_END_URL,$ionicPopup) {
	return {
		getCategoria: function(tipoCategoriaId){
			/*var q = $q.defer();
			q.resolve(shareData.lstIncidenciaCategoria);
			return q.promise;*/
			var delay = $q.defer();
			$http.get(BACK_END_URL+'categoria/'+tipoCategoriaId)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
		},
		enviarIncidencia: function(incidencia){
			var delay = $q.defer();
   
			$http.post(BACK_END_URL+'incidencia',incidencia)              		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						alert(response.data.message);
					}else{                				                			
						delay.resolve(response.data);
					}
				} 

				/*else {
					var alertPopup = $ionicPopup.alert({ title: 'Error', template: response.data.message , okType: 'button-assertive'});
					return false;
				}*/
			});		
			return delay.promise;
		},
		bucarIncidencias: function(estadoID){
			let parameters = {
                estadoID: estadoID
            };
            let config = {
                params: parameters
            };
			var delay = $q.defer();
			$http.get(BACK_END_URL+'incidencia',config)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
		},
		obtenerIncidencia: function(incidenciaID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'incidencia/'+incidenciaID)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
		},
		obtenerChat: function(incidenciaID){
			var delay = $q.defer();
			$http.get(BACK_END_URL+'incidencia/'+incidenciaID+'/mensaje')                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
						delay.resolve(response.data.data);
					}
				}
			});		
			return delay.promise;
		},
		validarIncidencia: function(incidencia,form){
			var validacion = {
				valido: true,
				mensaje: undefined
			}
			/*if(!incidencia.fuerzaDeVentaID){
				validacion.valido = false;
				validacion.mensaje = 'La fuerza de venta es obligatorio';
			}else*/
			if(!incidencia.puntoVenta||!incidencia.puntoVentaID){
				validacion.valido = false;
				validacion.mensaje = 'El punto de venta es obligatorio';
			}else
			if(!incidencia.tipoCategoria||!incidencia.tipoCategoriaID){
				validacion.valido = false;
				validacion.mensaje = 'El tipo de categoría es obligatorio';
			}else
			if(!incidencia.categoria||!incidencia.categoriaID){
				validacion.valido = false;
				validacion.mensaje = 'La categoría es obligatorio';
			}else
			if(!incidencia.asunto){
				validacion.valido = false;
				validacion.mensaje = 'El asunto es obligatorio';
			}else
			if(!incidencia.mensaje){
				validacion.valido = false;
				validacion.mensaje = 'El mensaje es obligatorio';
			}
			return validacion;
		},
		actualizarIncidencia: function(incidenciaID,estadoID){
			let parameters = {
                estadoID: estadoID
            };
            let config = {
                params: parameters
            };
			var delay = $q.defer();
			$http.put(BACK_END_URL+'incidencia/'+incidenciaID,null,config)              		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						alert(response.data.message);
					}else{                				                			
						delay.resolve(response.data);
					}
				}
			});		
			return delay.promise;
		},
	}
})
.service('chatService',function($q,shareData,$rootScope,$http,$filter,BACK_END_URL){
	return {
		enviarMensaje: function(chatId, mensaje){
			/*var q = $q.defer();		
			q.resolve({
				personaId: $rootScope.me.vendedor.personaID,
				personaNombre: $rootScope.me.vendedor.personNombreCompleto,
				texto: mensaje,
				fechaCreacion: moment().format('DD/MM/YYYY'),
				horaCreacion: moment().format('h:mm a')
			});
			return q.promise;*/
			let mensajeObj = {};
			mensajeObj.incidenciaID = chatId;
			mensajeObj.texto = mensaje;
			var delay = $q.defer();
			$http.post(BACK_END_URL+'incidencia/'+chatId+'/mensaje',mensajeObj)                		
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{                				                			
                        /*inicio @issue VTAPROD-160*/
                        let fechaRegistro = new Date(response.data.data);
                        /*fin @issue VTAPROD-160*/
						delay.resolve({
							personaID: $rootScope.me.usuarioID,
							personaNombre: $rootScope.me.personNombreCompleto,
							texto: mensaje,
                            /*inicio @issue VTAPROD-160*/
                            //fechaCreacion: moment().format('DD/MM/YYYY'),
                            //horaCreacion: moment().format('h:mm a')
                            fechaCreacion: moment(fechaRegistro).format('DD/MM/YYYY'),
                            horaCreacion: moment(fechaRegistro).format('H:mm:ss')
                            /*fin @issue VTAPROD-160*/
						});
					}
				}
			});		
		    return delay.promise;
		}
	}
})
.service('generadorDeMapas',function(){
	return {
		generarMapaDesdeArreglo: function(arrayObj,attrToKey){
			var map = {}
			angular.forEach(arrayObj, function(obj){
				map[obj[attrToKey]] = obj;
			})
			return map;
		}
	}
})
/*INICIO INTEGRACION EC*/
//.service('usuarioService',function($rootScope,USUARIO,ROL){
.service('usuarioService',function($rootScope,USUARIO, BACK_END_URL,ROL, $http,  $q){
/*FIN INTEGRACION EC*/
	var me = $rootScope.me;
	var tipoUsuario = me.tipoUsuarioVentas;
	var tipoRol = me.rolID;
	return {
		esVendedor: function(){
			return (tipoUsuario === USUARIO.TIPO.VENDEDOR||tipoUsuario === USUARIO.TIPO.VENDEDOR_INTERNO||tipoUsuario === USUARIO.TIPO.VENDEDOR_EXTERNO);
		},
		esSupervisor: function(){
			return tipoUsuario === USUARIO.TIPO.SUPERVISOR
		},
		esDeOficina: function(){
			return tipoUsuario === USUARIO.TIPO.OFICINA
		},
		esDeMarketing: function(){
			return tipoRol === ROL.TIPO.JEFE_MARKETING || tipoRol === ROL.TIPO.ASISTENTE_MARKETING
		}
		/*INICIO INTEGRACION EC*/
		,obtenerCuota: function(){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'usuario/evaluacionCrediticia/cuota')
						.toString();
			$http.get(uri)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
						//swal({title: '',type: response.data.type,text: response.data.message});
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
		    return delay.promise;
		}
		/*FIN INTEGRACION EC*/
	}
})
/**
 * @feature V4-HU011
 */
.service('simuladorPremiosComisionesService',function($q,$http,BACK_END_URL){
	this.obtenerReporte = function(cuota, valorCertificados){
		var delay = $q.defer();
		$http.get(BACK_END_URL+`simulacion/premioscomisiones?cuota=${cuota}&valorCertificados=${valorCertificados}`)
			.then(function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}
			});
		return delay.promise;
	}
})
/*INICIO JOHN VELASQUEZ SEGUIMIENTO EDV*/
.service('seguimientoEdvService',function($q,$http,$filter,BACK_END_URL){
	var dateFilter = $filter('date');
	this.enviarSeguimientoPorCorreo = function(filtro){
		var delay = $q.defer();
		$http.get(BACK_END_URL+'reporte?fechaInicio='+dateFilter(filtro.fechaInicio, 'dd-MM-yyyy')+'&fechaFin='+dateFilter(filtro.fechaFin, 'dd-MM-yyyy'))
		.then(function(response){
			if(response.status === 200){
				if(response.data.message !== '' && response.data.type=='warning'){
					delay.reject(response);
				}else{
					delay.resolve(response.data.data);
				}
			}
		});
		return delay.promise;
	}
})
/*FIN JOHN VELASQUEZ SEGUIMIENTO EDV*/
/*INICIO JOHN VELASQUEZ GRABACION 2.0*/
.service('recorderService', function($q,$cordovaFile){
	this.newRecord = function(directory, filename, limit){
		
		recorder = new Object();

		recorder.STATUS = {
			PRE_RECORDING: 0,
			RECORDING: 1,
			RECORD_COMPLETE: 2,
			STOP: 3
		}

		recorder.status = recorder.STATUS.PRE_RECORDING; 
		
		var moveFile = function(savedFilePath){
			var delay = $q.defer();
			var originalFileName = savedFilePath.split('/')[savedFilePath.split('/').length - 1];                    
			$cordovaFile.copyFile(
				cordova.file.dataDirectory, originalFileName,
				directory, filename
			)
			.then(function (success) {
				delay.resolve(directory+filename);
			}, function (error) {
				delay.reject(JSON.stringify(error));
			});
			return delay.promise;
		}

		recorder.stop = function() {
			var delay = $q.defer();
			if(recorder.status == recorder.STATUS.RECORDING) {
				window.plugins.audioRecorderAPI.stop(
				function(savedFilePath) {
					moveFile(savedFilePath)
					.then(function(path){
						recorder.status = recorder.STATUS.RECORD_COMPLETE;
						delay.resolve(path);
					}, function(error){
						console.error(error);
						delay.reject("Error al transferir el archivo de la grabación");
					});
				}, 
				function(msg) {
					console.error(msg);
					delay.reject("Error al para la grabación del audio");
				});
			}else{
				recorder.status = recorder.STOP;
			}			
			return delay.promise;
		}
		
		recorder.start = function() {
			var delay = $q.defer();
			if(recorder.status != recorder.STATUS.RECORDING){
				window.plugins.audioRecorderAPI.record(function(savedFilePath) {
					moveFile(savedFilePath)
					.then(function(path){						
						delay.resolve(path);
					}, function(error){
						console.error(error);
						delay.reject("Error al transferir el archivo de la grabación");
					});
				}, function(msg) {
					delay.reject("Error al para la grabación del audio");
				}, limit);
				recorder.status = recorder.STATUS.RECORDING;
			}
			return delay.promise;
		}
		
		/*recorder.playback = function() {
			window.plugins.audioRecorderAPI.playback(function(msg) {
				// complete
				//alert('ok: ' + msg);
			}, function(msg) {
				// failed
				//alert('ko: ' + msg);
			});
		}*/

		return recorder;
	}
})
/*FIN 	JOHN VELASQUEZ GRABACION 2.0*/
/**
 * @issue VTADIG2-504
 * @author John Velasquez
 * @name debitoAutomaticoService
 */
.service('debitoAutomaticoService', function($q, $http, BACK_END_URL){
    /**
     * Obtiene los tipos de cuanda de un banco que sean validos a usar para débito automático
     * @param bancoId
     * @returns {*}
     */
    this.getTipocuenta = function(bancoId){
        var delay = $q.defer();
        $http.get(BACK_END_URL+'debitoAutomatico/tipoCuenta?banco='+bancoId)
            .then(function(response){
                if(response.status === 200){
					delay.resolve(response.data);
                }
            });
        return delay.promise;
    };
    /**
     * Valida la es información para el registro del débito automático
     * @param debitos
     * @returns {Validacion}
     */
    this.validarDebitoAutomatico = function(debitos){

        if (debitos === undefined || debitos === null) throw new Exception("Parametro debitos en requerido.");

        let validacion = new Validacion(true,"éxito.");
        let debitos_activos = debitos.filter(d => d.activo);

        if (validacion.valido&&debitos_activos.find(d => d.banco === null || d.banco === undefined || d.banco.codigo === null || d.banco.codigo === undefined || d.banco.codigo === "")) {
            validacion.valido = false;
            validacion.mensaje = "Es necesario que seleccione un banco.";
        }
        if (validacion.valido&&debitos_activos.find(d => d.tipoCuenta === null || d.tipoCuenta === undefined || d.tipoCuenta.id === null || d.tipoCuenta.id === undefined || d.tipoCuenta.id === "")) {
            validacion.valido = false;
            validacion.mensaje = "Es necesario que seleccione un tipo de cuenta.";
        }
        if (validacion.valido&&(debito = debitos_activos.find(d => d.numeroCuenta === null || d.numeroCuenta === undefined || d.numeroCuenta.length != d.tipoCuenta.longitud))) {
            validacion.valido = false;
            validacion.mensaje = "Es necesario para el tipo de cuenta "+debito.tipoCuenta.descripcion+"que ingrese un número de cuenta de " + debito.tipoCuenta.longitud + " digitos.";
        }
        return validacion;
    };
	
    this.enviarFormularioVisaNet = function(contratoId){
		var delay = $q.defer();
		$http.post(BACK_END_URL+'/debitoAutomatico/visanet/form/mail?contratoId='+contratoId)
			.then(function(response){
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	}
    
})
/**
 * @feature V4-HU006
 */
.service('base64Utils', function(){
	this.lengthInBytes = (stringBase64)=>{
		if (stringBase64 === null || stringBase64 === undefined || stringBase64.length === 0){
			return 0;
		}
		let characterCount = stringBase64.length;
		let paddingCount =  (stringBase64.substr(characterCount-2, 2).match(/=/g)||[]).length;
		return (3 * (characterCount / 4)) - paddingCount;
	}
	this.lengthInKB = (stringBase64)=>{
		return this.lengthInBytes(stringBase64)/1024;
	}
})
/**
 * @feature V4-HU016
 */
.service('estadisticasAsmbleaRematesService', function($q, $http, BACK_END_URL){
	//this.promedioUltimos = (ultimos) => {
	this.promedioUltimos = () => {
		var delay = $q.defer();
		//$http.get(BACK_END_URL+'estadistica/asamblea/remates/promedio?ultimos='+ultimos)
		$http.get(BACK_END_URL+'estadistica/asamblea/remates/promedio')
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	}
})
/**
 * @feature V4-HU030
 */
.service('ViajeNuevoVendedorService', function($q, $http, $rootScope, BACK_END_URL, USUARIO){
	this.obtenerVendedores = () => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/supervisor/${$rootScope.me.personaID}/vendedor`);
		
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.obtenerVendedor = (personaId) => {
		var delay = $q.defer();
		var uri = null;
		if($rootScope.me.tipoUsuarioVentas === USUARIO.TIPO.SUPERVISOR){
			uri = URI(`${BACK_END_URL}viajeNuevoVendedor/supervisor/${$rootScope.me.personaID}/vendedor/${personaId}`);	
		} else if ($rootScope.me.tipoUsuarioVentas === USUARIO.TIPO.VENDEDOR) {
			uri = URI(`${BACK_END_URL}viajeNuevoVendedor/vendedor/${$rootScope.me.personaID}`);
		}
		
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.guardarObservacion = (personaId, observacion, options) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/supervisor/${$rootScope.me.personaID}/vendedor/${personaId}/observacion`);
		if(options){
			Object.keys(options).forEach(option => {
				uri.addSearch(option, options[option]);
			});
		};
		$http.post(uri.toString(), observacion)
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.obtenerReporteSemanal = (personaId, semana, options) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/supervisor/${$rootScope.me.personaID}/vendedor/${personaId}/semana/${semana}/reporte`);
		if(options){
			Object.keys(options).forEach(option => {
				uri.addSearch(option, options[option]);
			});
		};
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.obtenerTiposFeedback = (options) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/general/feedback/tipo`);
		if(options){
			Object.keys(options).forEach(option => {
				uri.addSearch(option, options[option]);
			});
		};
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.enviarFeedback = (personaId, semana, feedback, options) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/supervisor/${$rootScope.me.personaID}/vendedor/${personaId}/semana/${semana}/feedback`);
		if(options){
			Object.keys(options).forEach(option => {
				uri.addSearch(option, options[option]);
			});
		};
		$http.post(uri.toString(), feedback)
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.confirmarFeedBack = (personaId, semana) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/vendedor/${personaId}/semana/${semana}`);
		
		$http.put(uri.toString(), {
			feedbackIndConfirmacion: true
		})
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.obtenerPasaporte = (personaId) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/vendedor/${personaId}/pasaporte`);
		
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	};
	this.enviarPasaporte = (personaId, pasaporte) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/vendedor/${personaId}/pasaporte`);

		$http.put(uri.toString(), pasaporte)
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	}
})
/**
 * @feature V4-HU030
 */
.service('FormularioService', function($q, $http, $rootScope, BACK_END_URL){
	this.obtenerFormulario = (codigo, options) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}formulario/${codigo}`);
		if(options){
			Object.keys(options).forEach(option => {
				uri.addSearch(option, options[option]);
			});
		};
		$http.get(uri.toString())
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	}
	this.obtenerFormularioPasaporte = (personaId, semana) => {
		var delay = $q.defer();
		var uri = URI(`${BACK_END_URL}viajeNuevoVendedor/vendedor/${personaId}/semana/${semana}/pasaporte`);

		$http.put(uri.toString(), {
			feedbackIndConfirmacion: true
		})
			.then(function(response) {
				if(response.status === 200){
					delay.resolve(response.data);
				}
			});
		return delay.promise;
	}
})
/**
 * @feature/-Mastercard
 */
.service('pagoMastercardService',function($q,$http,$rootScope,BACK_END_URL){
	this.validaDiasAsamblea=(obligacionId)=>{
		var delay=$q.defer();
		$http.get(BACK_END_URL+'validarpagoPOS/obligacionId/'+obligacionId)
			.then(function(response){
				if(response.status===200){
					delay.resolve(response.data);
				}else{
					delay.reject(response.data);
				}
			});
			return delay.promise;
	},
	this.guardarPagoMastercard =(data)=>{
		var delay = $q.defer();
		$http.post(BACK_END_URL+'transaccionPOS',data)    
		         		
		.then(function(response){
			if(response.status ===200){
				delay.resolve(response.data);
			}else{
				delay.reject(response.data);	
			}
		});		
		return delay.promise;
	  }
	
})
/**
 * @feature/pagoLink
 */
.service('pagoLinkService',function($q,$http,$rootScope,BACK_END_URL){
	this.generarPagoLink=(obligacionId)=>{
		var delay=$q.defer();
		$http.get(BACK_END_URL+'pagolink/'+obligacionId)
			.then(function(response){
				if(response.status===200){
					delay.resolve(response.data);
				}else {
					delay.reject(response.data);
				}
			});
				return delay.promise;
	}	
})
.service('referidoService',function($q,$http,BACK_END_URL){
	return {
		getReferidoList: function(vendedorId){
			var delay = $q.defer();
			var uri = URI(BACK_END_URL+'listarReferidosPendVendedor/'+vendedorId).toString();
			console.log("URI: " + uri);
			$http.get(uri)
			.then(
			function(response){
				if(response.status === 200){
					if(response.data.message !== '' && response.data.type=='warning'){
						delay.reject(response);
					}else{
						delay.resolve(response.data.data);
					}
				}else{
					delay.reject(response);
				}
			}, 
			function errorCallback(response) {
				alert("Ha ocurrido un error inesperado.");
			});
			return delay.promise;
		}
	}
})
;
