angular.module('your_app_name.config', [])
.constant('WORDPRESS_API_URL', 'https://wordpress.startapplabs.com/blog/api/')
.constant('GCM_SENDER_ID', '574597432927')
.constant('BACK_END_URL','@@aws_address')
.constant('CFG_ENVIRONMENT', '@@app_environment')
.constant('CFG_FONDO','@@fondo_login')

.constant('CONFIGURACION_CAMARA',{
    FOTO:{
        quality : 100,
        targetWidth: 500,
        targetHeight: 500,
        sourceType: 1,//[0]: from device library, [1]: tomar foto, [2]: camera library
        allowEdit: false,
        correctOrientation: true,
        mediaType: 0,//[0]: foto, [1]: video, [2]: todo
        destinationType: 0,//[0]Base 64
        encodingType: 0
    },
    GALERIA:{
        sourceType: 0,//[0]: from device library, [1]: tomar foto, [2]: camera library
        allowEdit: false,
        mediaType: 0,//[0]: foto, [1]: video, [2]: todo
        destinationType: 0,//[0]Base 64
    }
})
.constant('CONFIGURACION_APP',{
    INCIDENCIAS: {
        MAXIMO_IMAGENES_ADJUNTAS: 5
    }
})
/*INICIO INTEGRACION EC*/
.constant('APP',{
    ID:'com.pe.pandero.movil.IngVtaApp',
    MODULO:{
        REGISTRO_PROSPECTO:{
            ID: "registroProspecto"
        },
        PAGO_A_CUENTA:{
            ID: "pagoCuenta"
        },
        INGRESO_DE_VENTA: {
            ID: "ingresoDeVenta"
        }
    }
})
/*FIN INTEGRACION EC*/;

