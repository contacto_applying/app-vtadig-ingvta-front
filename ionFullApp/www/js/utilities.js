/**
 * |--------------------------------------------------------------------------------------------------------|
 * |@issue                       |@version |@author          |@date      | @desc                            |
 * |--------------------------------------------------------------------------------------------------------|
 * |VTADIG2-504					 |1.0      |John Velasquez   |01-08-2018 | integración débito automático    |
 * |--------------------------------------------------------------------------------------------------------|
 */
function Base64Encode(str, encoding = 'utf-8') {
    var bytes = new (TextEncoder || TextEncoderLite)(encoding).encode(str);        
    return base64js.fromByteArray(bytes);
  }
  
  function Base64Decode(str, encoding = 'utf-8') {
    var bytes = base64js.toByteArray(str)
    return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes)
  }
  
  function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }
  
  function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
  }
  
  function _base64ToArrayBuffer(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }
  
  function _arrayBufferToBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  function dataURLtoFile(dataurl, filename) {
    let arr = dataurl.split(',');
    let mime = arr[0].match(/:(.*?);/)[1];
    let bstr = window.atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);
    let ext = mime.split('/')[1]; 
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], `${filename}.${ext}`, {type:mime});
 }

 function urltoFile(url, filename, mimeType){
    return (fetch(url)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename, {type:mimeType});})
    );
}

  function dateFormat(date){	
  
    let dd = date.getDate();
    let mm = date.getMonth()+1; //Enero es 0!
    let yyyy = date.getFullYear();
    
    if(dd<10){dd='0'+dd;} 
    if(mm<10){mm='0'+mm;} 

    let strDate = dd+'/'+mm+'/'+yyyy;
    return strDate;
}

function getDurationOfMedia(media){
  return new Promise((resolve, reject)=>{
    if(media&&media.media){
      media.setVolume(0);
      media.media.play();
      setTimeout(()=>{
        var duration = media.media.getDuration();
        media.media.stop();
        media.setVolume(1);
        resolve(duration);
      },100)
    }else{
      reject(-1);
    }
  })  
}

/**
 * @issue VTADIG2-504
 * @author John Velasquez
 * @date 02-08-2018
 * @desc clase de uso general para manejar validacones
 * @version 1.0.0
 */
class Validacion {


    constructor(validado, mensaje){
      this._validado = validado;
        this._mensaje = mensaje;
    }


    get valido(){
        return this._validado;
    }

    set valido(value){
        if(typeof(value) !== "boolean"){
            throw new Exception("tipo de dato incorrecto, se requiere un boolean");
        }
        this._validado = value;
    }

    get mensaje(){
        return this._mensaje;
    }

    set mensaje(value){
        if(typeof(value) !== "string"){
            throw new Exception("tipo de dato incorrecto, se requiere un string");
        }
        this._mensaje = value;
    }
}