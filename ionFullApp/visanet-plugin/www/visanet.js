var visanet = function(){
    const PLUGIN_NAME = "VisanetPlugin";
    const AUTHORIZE_ACTION = "authorize";
    const CANCEL_ACTION = "cancel";
    const REPORTS_ACTION = "report";
    const VOUCHER_REQUIRED_ACTION = "setVoucherRequired";

    const success = function (success) {
        console.log("success : " +success);
    };
    const error = function (error) {
        console.log("error : " + error);
    };

    return {
        authorize: function (amount, reference, email, onSuccess, onError) {
            onSuccess = (typeof onSuccess !== 'function') ? success : onSuccess;
            onError = (typeof onError !== 'function') ? error : onError;
            cordova.exec(onSuccess, onError, PLUGIN_NAME, AUTHORIZE_ACTION, [amount, (reference || ""), (email || "")]);
        },
        cancel: function (referenceCode, onSuccess, onError) {
            onSuccess = (typeof onSuccess !== 'function') ? success : onSuccess;
            onError = (typeof onError !== 'function') ? error : onError;
            cordova.exec(onSuccess, onError, PLUGIN_NAME, CANCEL_ACTION, [(referenceCode || "")]);
        },
        reports: function (onSuccess, onError) {
            onSuccess = (typeof onSuccess !== 'function') ? success : onSuccess;
            onError = (typeof onError !== 'function') ? error : onError;
            cordova.exec(onSuccess, onError, PLUGIN_NAME, REPORTS_ACTION,[]);
        },
        setVoucherRequired: function (isVoucherRequired, onSuccess, onError) {
            onSuccess = (typeof onSuccess !== 'function') ? success : onSuccess;
            onError = (typeof onError !== 'function') ? error : onError;
            cordova.exec(onSuccess, onError, PLUGIN_NAME, VOUCHER_REQUIRED_ACTION, [isVoucherRequired]);
        }
    }
};
module.exports = visanet();